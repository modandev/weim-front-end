const fs = require('fs');
const opn = require('opn');

fs.readdir('./src/assets/icon', (err, files) => {
  if (err) {
    return;
  }

  opn(`http://localhost/icons?i=${files.filter(file => file.includes('.svg')).map(file => file.replace('.svg', ''))}`);
});
