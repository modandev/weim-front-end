export default {
  'user.weim.signUp': 'Sign up to WEIM',
  'user.weim.signIn': 'Sign in to WEIM',
  'user.signUp.required': 'Only user with IB account can successfully sign up. We will open up the oppotunities in the near future.',
  'user.signUp.subscribe': 'You may click on the sign up option to inform us your contack information, we will connect you once the platform is opened up for non-manager. You may also follow our wechat official account for the most upfated news.',
  'user.follow.wechat.official.account': 'Wechat official account',
  'user.consult.wechat.custom.service': 'Customer service',
  'user.settings.locale.select': 'Select Language',
  'user.settings.locale.chinese': '中文',
  'user.settings.locale.english': 'English',
  'user.accountManagement': 'Account Management',
  'user.accountManagement.myAnalysis': 'My Analysis',
  'user.accountManagement.myAccounts': 'My Accounts',
  'user.accountManagement.addAccount': 'Add Account',
  'user.userManagement': 'User Management',
  'user.userManagement.following': 'Following',
  'user.me.accountStatus': 'Status',
  'user.me.accountStatus.FAILED': '关联失败',
  'user.me.accountStatus.ACTIVE': '已激活',
  'user.me.accountStatus.PENDING_VERIFICATION': '正在审核中',
  'user.me.account.name.placeholder': 'Account name',
  'user.me.aboutAccount.placeholder': 'About this account',
  'user.me.account.name.tips': '*Account name can only be edited once',
  'user.me.account.name.modify.tip': 'The Account Name can’t be changed after this edit.\nAre you sure you want to make this edit?',
  'user.settings.user': 'Setting',
  'user.settings.favorite': 'Preference',
  'user.settings.locale': 'Language',
  'user.settings.riseAndFall': 'Color',
  'user.settings.riseAndFall.GURD': '绿涨红跌',
  'user.settings.riseAndFall.GDRU': '绿跌红涨',
  'user.settings.nickname': 'Nickname',
  'user.settings.connect': 'Connect',
  'user.settings.connected': 'Connected',
  'user.connectIB.questionOne': 'Benefits of being an investment managers?',
  'user.connectIB.answerOne': `Free subscription to all accounts;
One-to-one communication with other investment managers;
View Analysis and Posts of other investment managers;
Obtain detailed transaction records of other investment managers;
Obtain potential fund management opportunities;
Obtain potential employment opportunities.`,
  'user.connectIB.questionTwo': 'Why do I need to submit an IB account and set up a sub-account?',
  'user.connectIB.answerTwo': `The core foundation of WEIM is based on real trading data.
Through system integration with IB, we thus realized observable and quantifiable result evaluation.
The risk control constraints of the investment group in which it operates only affect the sub-account, and will not have any impact on the investment manager’s IB main account and other sub-accounts.
Each sub-account corresponds to only one investment group. You need to open multiple sub-accounts if intended to join different groups, (For questions about opening a sub-account, please contact IB customer service for consultation)`,
  'user.connectIB.questionThree': 'Who is responsible for sub-account funds?',
  'user.connectIB.answerThree': `The funds in the sub-account belong to individuals and are managed by IB.
The gains and losses realized by the trading activities in the account also belong to the investment manager himself.
WEIM Investment Group only conducts risk control constraints on sub-accounts.
There is no capital exchange, management fees, commission and etc between WEIM and the investment manage.`,
  'user.connectIB.questionFour': 'Why set up a funding requirement of $100,000?',
  'user.connectIB.answerFour': `WEIM hopes to bring together a group of excellent and diligent professional secondary market investors to share investment resources and learn together.
100,000 US dollars can be regarded as the entry barrier for professional investors. The threshold is not absolute. For young candidates who have not yet met the funding requirements but have extraordinary strength and strong will, the funding requirements can be lowered appropriately. Please contact us.`,
  'user.connectIB.howToBeManager': 'How to become an Investment Manager of WEIM?',
  'user.connectIB.toBeManagerSteps': `Register for an IB Interactive Brokers account(www.ibkr.com), and open a sub-account.（contact IB for how-to）
Please provide your IB sub-account UID, first and last name, IB registered email to WEIM.
WEIM will submit your account information to IB for review and will send an email to your IB registered email afterwards for confirmation.
Click the link in the email to set up your account.`,
  'user.connectIB.subaccount.uid': 'IB sub-account UID',
  'user.connectIB.subaccount.lastname': 'Last Name',
  'user.connectIB.subaccount.firstname': 'First Name',
  'user.connectIB.subaccount.email': 'Registered email of IB',
  'user.connectIB.subaccount.connect.done': 'Request received',
  'user.connectIB.subaccount.connect.connected': 'UID has been linked to you current user account',
  'user.connectIB.subaccount.connect.notify': 'WEIM will submit your information to IB for review and association. IB will send a confirmation email to your email within 2 days.',
  'user.connectIB.subaccount.connect.help': 'For any questions, you may contact us by email or WeChat Customer Service. We\'ll solve the problem with you in the first place.',
  'user.connectIB.moreQuestion.help': 'For more question, please email to:',
  'user.account': 'Account',
  'user.panel.account.profile': 'Account Profile',
  'user.panel.investment.profile': 'Manager Profile',
  'user.panel.position.title': 'Portfolio Assets',
  'user.panel.my.position.title': 'Position',
  'user.panel.my.account.title': 'Accounts',
  'user.panel.my.picked.title': 'My List',
  'user.panel.NAV': 'Net Asset Value',
  'user.panel.position.trade.buy': 'Buy',
  'user.panel.position.trade.sell': 'Sell',
  'user.panel.position.trade.analysis': 'Analysis',
  'user.panel.position.addAnalysis2Asset': 'Add analysis to this asset',
  'user.watchlist.title': 'Watchlist',
  'user.watchlist.filter.all': 'All',
  'user.panel.position.add.analysis': 'Add analysis',
  'user.watchlist.earningsCalendar': 'Earnings Calendar',
  'user.watchlist.ticker': 'Ticker',
  'user.watchlist.name': 'Name',
  'user.watchlist.symbol': 'Symbol',
  'user.watchlist.exchange': 'Exchange',
  'user.watchlist.currentPrice': 'Current Price',
  'user.watchlist.accountsHolding': 'Accounts Holding',
  'user.analysis.investmentResearch': 'Analysis',
  'user.analysis.investmentResearch.title.placeholder': 'Untitled',
  'user.analysis.investmentResearch.draft': 'Draft',
  'user.analysis.investmentResearch.saving': 'Saving...',
  'user.analysis.investmentResearch.publish': 'Publish',
  'user.analysis.investmentResearch.summary.placeholder': 'Summary',
  'user.analysis.investmentResearch.body': 'Body',
  'user.analysis.investmentResearch.subject.placeholder': 'Tell your story',
  'user.analysis.investmentResearch.forecast.description': `Price will reach
%price
in
%days
days, therefore`,
  'user.analysis.investmentResearch.PriceForecasting': 'Predicted Price',
  'user.analysis.investmentResearch.Day': 'day',
  'user.analysis.investmentResearch.PredictedArrivalTime': 'Predicted Timeframe',
  'user.analysis.investmentResearch.': '',
  'user.analysis.investmentResearch.forecast.LONG': 'Bullish',
  'user.analysis.investmentResearch.forecast.SHORT': 'Bearish',
  'user.analysis.investmentResearch.publish.tips': 'You won\'t be able to edit your analysis after publishing it.\nDo you want to continue?',
  'user.analysis.investmentResearch.discard': 'Discard',
  'user.analysis.investmentResearch.discard.tips': 'Your draft will be permanently deleted after discarding it.\nDo you want to continue?',
  'user.analysis.investmentResearch.previewPage.title': 'Preview page, unpublished',
  'user.analysis.published': 'Analysis has been published',
  'user.analysis.shareWithWorld': 'Share it with the world!',
  'user.account.connectingTip': 'Connecting your account. Please pay attention to your email in the next 1-2 days.',
  'user.followersList': 'Followers List',
  'user.DataFromYahoo': 'Data from yahoo finance  ·  Calculated in USD',
  'user.priceInUSD': 'Price in {country}',
  'user.Liquidated': 'Liquidated',
  'user.Time-weighted Return': 'Time-weighted Return',
  'user.Money-weighted Return': 'Money-weighted Return',
  'use.connectMessageTitle': 'Join WEIM and become a Manager',
  'use.connect.status.success.title': 'We are glad to have you!',
  'use.connect.status.error.title': 'It seems that something went wrong when connecting！',

  'use.connect.status.success.message': 'It might take 1-2 days to complete the connection.\n Please pay close attention to your email.\n \n Once the connection is complete,you will be able to access WEIM using this account',

  'use.connect.status.error.message.submitted':'The UID you submitted has been connected to another user.\nPlease contact our customer service team for any questions.',

  'use.connect.status.error.message.unfortunately':'Unfortunately, Your account funding has not reached our prerequisite.\n WEIM is looking for partners with funding reach $100,000.\n Please contact our customer service team for any questions.',

  'use.connect.status.error.message.other':'Please contact our customer service team for any questions.',

  'user.connect.title': 'Connecting your Interactive Brokers(IB) account to WEIM',

  'user.connect.sub_title': 'Through system integration with Interactive Brokers, we thus realized observable and quantifiable result evaluation.',

  'user.connect.uid_message': 'Register for an IB Interactive Brokers account(www.ibkr.com), and open a sub-account.（contact IB for how-to）',

  'user.connect.email_action':' WEIM will submit your account information to IB for review and will send an email to your IB registered email afterwards for confirmation.',

  'user.connect.Creating your account profile': 'Creating your account profile',

  'user.connect.userNicknameAction' :'Account name can only be edited once',

  'user.connect.submitButton' :'Finish',

  'user.connect.nextTime': 'Create next time when sign in',

  'user.connect.BINDING_IN_PROGRESS': 'Connecting your account. Please pay attention to your email in the next 1-2 days.',

  'user.connect.BINDING_IN_PROGRESS_Link': ' ',

  'user.connect.ACCT_CONNECT_FAIL': 'UID Connect Failed,click here to',
  'user.connect.ACCT_CONNECT_FAIL_Link': 'View result',

  'user.connect.ACCT_PENDING_INFO': 'Account connect successfully,click here to',

  'user.connect.ACCT_PENDING_INFO_Link': 'activate account',






};
