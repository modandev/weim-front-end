export default {
  "message.required": "Field cannot be empty",
  "message.format.error": "Incorrect format",
  "message.noData": "No data",
  "message.globalSearchBar.keyword.format.error": "Keywords can only be composed of Chinese, English and numbers, no more than 20 digits",
  "message.account.nickname.format.error": "Account name can only be composed of Chinese, English and numbers, 3-20 digits",
  "message.account.nickname.exist": "The account name already exists",
  "message.username.format.error": "Nickname can only be composed of Chinese, English and numbers, no more than 16 digits",
  "message.chart.empty": "No data detected"
};
