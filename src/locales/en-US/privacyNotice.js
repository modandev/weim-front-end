export default {
  "privacyNotice.title": "Privacy Notice",
  "privacyNotice.introduction.title": "Introduction",
  "privacyNotice.introduction.content.paragraph": `This privacy notice serves to articulate publicly how WEIM will handle the personally identifiable information it gathers and uses in the normal course of business. This will demonstrate how WEIM will adhere to the legal requirements and agency policy determinations that enable gather and sharing of information to occur in a manner that protects personal privacy interests.
Information Covered by This Privacy Notice.
This privacy notice applies to personal information, including any information collected by WEIM, as described in detail below. This privacy notice applies to all websites operated by WEIM and WEIM’s products and services (collectively, the “Services”). When you use WEIM’s Services, your information will be collected, used, and shared as described within the provisions of this notice.`,
  "privacyNotice.informationWeCollect.title": "Information We Collect",
  "privacyNotice.informationWeCollect.content.paragraph": "WEIM may collect personal information when 1) it is provided to us by the data subject; 2) when a data subject contacts us or uses our products and services; 3) a third party shares your information after receiving your consent to do so; and 4) when required by law.",
  "privacyNotice.informationWeCollect.content.list.informationProvidedToWEIM.title": "How We Collect Information - Information Provided to WEIM",
  "privacyNotice.informationWeCollect.content.list.informationProvidedToWEIM.items": `WEIM may collect information provided to WEIM prior to or during any registration process, including information given in connection with a co-branded offer;
Information given when you call or email WEIM (for support or otherwise);
Payment information when purchasing WEIM products.`,
  "privacyNotice.informationWeCollect.content.list.informationCollectedThroughUse.title": "How We Collect Information – Information Collected Through Use",
  "privacyNotice.informationWeCollect.content.list.informationCollectedThroughUse.items": `We may use technologies that collect information when your visit our site or use or products and services;
We use cookies and other techniques to better understand our online customers. For more details and to opt out, please see the ‘Cookies’ section of this policy.`,
  "privacyNotice.informationWeCollect.content.list.thirdParties.title": "How We Collect Information – Third Parties",
  "privacyNotice.informationWeCollect.content.list.thirdParties.items": "WEIM may collect information we receive from trusted third parties who have gained your consent to share the information.",
  "privacyNotice.informationWeCollect.content.list.whenRequiredByLaw.title": "How We Collect Information – When Required by Law",
  "privacyNotice.informationWeCollect.content.list.whenRequiredByLaw.items": "WEIM may collect personal information when required by law, for example during the Know Your Customer/Anti-Money Laundering process.",
  "privacyNotice.informationWeCollect.content.list.socialMedia.title": "Social Media",
  "privacyNotice.informationWeCollect.content.list.socialMedia.items": "Our platform uses information gathered from social media widgets and applications from third parties. WEIM does not accept or claim any responsibility in relation to those websites’ privacy policies or customer agreements made on those platforms. You can control the level of privacy settings at the applicable social networking sites.",
  "privacyNotice.howWeUseUrInformation.title": "How we Use Your Information",
  "privacyNotice.howWeUseUrInformation.content.paragraph": "WEIM processes personal data only for the purposes explicitly described. WEIM uses this information for the following reasons:",
  "privacyNotice.howWeUseUrInformation.content.list.provideProductsAndServices.title": "Providing you with your products and services",
  "privacyNotice.howWeUseUrInformation.content.list.provideProductsAndServices.items": `Processing payments and customer care;
Provide you with useful content.`,
  "privacyNotice.howWeUseUrInformation.content.list.improveServices.title": "To improve our services",
  "privacyNotice.howWeUseUrInformation.content.list.marketingAndCustomizingServices.title": "Marketing and customizing our services for you",
  "privacyNotice.howWeUseUrInformation.content.list.marketingAndCustomizingServices.items": `This includes sharing information with third party vendors;
Our use of your information to advertise is done with your consent where required by applicable law.`,
  "privacyNotice.howWeUseUrInformation.content.list.researchAndAnalytics.title": "Research and analytics",
  "privacyNotice.howWeUseUrInformation.content.list.researchAndAnalytics.items": "This may include designing more effective marketing campaigns and conducting analyses on our customer base.",
  "privacyNotice.howWeUseUrInformation.content.list.asRequiredByLaw.title": "As required by law",
  "privacyNotice.howWeUseUrInformation.content.list.asRequiredByLaw.items": "This may include credit checks, fraud prevention measures, and KYC/AML policies.",
  "privacyNotice.sharingInformation.title": "Sharing Information",
  "privacyNotice.sharingInformation.content.paragraph": "WEIM will treat your information as confidential and will only share your information in limited circumstances including:",
  "privacyNotice.sharingInformation.content.list.items": `Partners or agents involved with delivering the services you have ordered;
Companies who are engaged to perform services on behalf of WEIM;
Law enforcement agencies, regulatory organizations, or other public authorities if required by law;
Third parties that we use for marketing services or third-party advertisers.`,
  "privacyNotice.howWeSafeguardYourInformation.title": "How we Safeguard Your Information",
  "privacyNotice.howWeSafeguardYourInformation.content.list.items": `WEIM uses up-to-date security standards on all hardware and software to protect your information from unauthorized access, accidental loss, unauthorized disclosure, or destruction;
WEIM will never ask for your personal account information by an unsolicited means of communication. The data subject is responsible for keeping separate personal and account information secure.`,
  "privacyNotice.cookiePolicy.title": "Cookie Policy",
  "privacyNotice.cookiePolicy.content.list.items": `Cookies are small data files that allow WEIM to record information when you interact with our website and services. WEIM uses cookies to improve our website in order to better serve our customers’ interests and needs. Your browser will retain these cookies as a text file and are not linked to your personal information;
Persistent cookies will remain on your computer until they are manually deleted. WEIM uses these cookies to recognize your preferences when you repeatedly visit our website or use our services;
Session cookies collect information on a particular period of browsing and expire at the end of your browsing session;
Google Analytics is a third-party web analytics tool that allows us to improve our website based on trends and general information. Individual visitors to our site are not disclosed. Google Analytics uses cookies to collect this information;
Cookies will only be used with your consent. Your internet browser may also allow you to change your personal cookies settings in the “preferences” menu.`,
  "privacyNotice.yourRightsAndChoices.title": "Your Rights and Choices",
  "privacyNotice.yourRightsAndChoices.content.list.marketing.title": "Marketing",
  "privacyNotice.yourRightsAndChoices.content.list.marketing.items": "Your email and contact preferences may be changed at any time through the ‘name of settings page’. You may also opt-out of receiving messages and promotional materials by sending a message to cs@yloan.io. You may still receive service and support messages as well as disclosure statements required by law.",
  "privacyNotice.yourRightsAndChoices.content.list.correctionAndErasure.title": "Correction and Erasure",
  "privacyNotice.yourRightsAndChoices.content.list.correctionAndErasure.items": `You have the right to correct any personal information WEIM collects that is inaccurate or incomplete unless a legal obligation to retain this information exists. In the request to correct data, the data subject must clearly detail why the information is inaccurate or incomplete so that we may best determine if a correction is required;
WEIM strives to retain and process your information for only as long as necessary. In some circumstances, you have the right to request that WEIM erase the personal information that we collect. Please provide a detailed explanation for the reason of the request so that we can determine if there is a valid reason for erasure.`,
  "privacyNotice.yourRightsAndChoices.content.list.accessAndPortability.title": "Access and Portability",
  "privacyNotice.yourRightsAndChoices.content.list.accessAndPortability.items": "You have the right to know what personal information of yours WEIM collects and to access said information. This information should be provided to you in a commonly used format.",
  "privacyNotice.yourRightsAndChoices.content.list.restrictionOfProcessingToStorageOnly.title": "Restriction of Processing to Storage Only",
  "privacyNotice.yourRightsAndChoices.content.list.restrictionOfProcessingToStorageOnly.items": "In certain circumstances you may have the right to restrict the processing of your personal information for reasons other than general storage. This may include when you believe the information, we collect is inaccurate; in which case WEIM will restrict processing until the data is verified. Please provide a detailed explanation for your request so that we can best determine if processing your data should be restricted.",
  "privacyNotice.yourRightsAndChoices.content.list.withdrawalOfConsentAndObjectionOfProcessing.title": "Withdrawal of Consent and Objection of Processing",
  "privacyNotice.yourRightsAndChoices.content.list.withdrawalOfConsentAndObjectionOfProcessing.items": `You have the right, in certain circumstances, to object to the processing of your personal information. This includes automated decisions that have impacted you. Please provide a detailed explanation for your request so that we can best determine if WEIM should stop processing your data for that purpose;
You may withdraw your consent at any time in circumstances where WEIM relies on your consent for the processing of your personal information. You may request to object to the processing of your personal information when that processing is based on WEIM’s legitimate interest.`,
  "privacyNotice.EUDataProcessingRetentionAndTransfers.title": "EU Data Processing, Retention, and Transfers",
  "privacyNotice.EUDataProcessingRetentionAndTransfers.content.list.items": "If WEIM sends personal information from the European Economic Area (EEA) to a country not in the EAA, WEIM will ensure that the data is properly protected. These transfers will be pursuant to the Commission’s standard international data transfer contractual clauses (specifically decision 2004/915/EC for Controller-Controller transfers and decision 2010/87/EU for Controller-Processor transfers).",
};
