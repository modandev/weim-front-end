export default {
  "All": '全部',
  'home.discover.investmentResearch': '投资研究',
  'home.discover.transactionNewsletters': '交易简讯',
  'home.Account': '账号',
  'home.Loading': '加载中...',

  'home.No Information': '暂无资讯',
  'home.NoData': '暂无数据',
  'home.globalAccount': '所有账号',
  'home.globalAccount.follow': '关注账号',

  'home.picked.universal': '所有标的',
  'home.picked.mine': '我的自选',

  'home.stockMarket.HongKong': '港股',
  'home.stockMarket.US': ' 美股',
  'home.stockMarket.Shanghai&Shenzhen': '沪深',

  'home.hot.purchase': '热门买入资产',
  'home.hot.sold': '热门卖出资产',
  'home.hot.tradingAccounts': '{amount} 个账户正在交易',
  'home.hot.deals': '{amount}笔交易',

  'home.main.newsletters.placeholder': '写一篇简讯',
  'home.main.editingDepthAnalysis': '编辑深度分Follows析',
  'home.main.readMore': '阅读全文',

  'home.chat.messageBox.newMessage': '新消息',
  'home.chat.messageBox.searchName.placeholder': '输入一个或多个名字',
  'home.chat.messageBox.typeIn.placeholder': '输入信息',

  'home.newsletter.comment.placeholder': '输入内容',
  'home.newsletter.comment.showMore': '显示更多',
  'home.newsletter.comment.Push': '收起',

  'home.Publish': '发布',
  'home.shares': '股',
  'home.user.follow': '关注',
  'home.user.cancelFollow': '已关注',
  'home.user.Follows': '粉丝: {count}人',
  'home.rank.Follows': '粉丝: {count}',
  'home.user.Following': '关注账号: {count}',
  'home.stock.WatchList': '自选',
  'home.stock.CancelWatch': '已加自选',
  'home.stock.Code': '代码',
  'home.stock.AccountsTrading': '个账户正在交易',
  'home.stock.Transaction': '笔交易',
  'home.stock.MostBoughtAsset': '热门买入资产',
  'home.stock.MostSaleAsset': '热门卖出资产',

  'home.tweet.Week': '{count}周',
  'home.tweet.Month': '{count}月',

  'home.tweet.ViewFull': '阅读全文',
  'home.copy': '点击复制',
  'home.NoMore': '没有更多数据',
  'home.Cancel': '取消',
  'home.Chart.Sell': '卖出',
  'home.Chart.Buy': '买入',
  'home.copied': '已复制',
  'home.Weekly gain': '本周收益',
  'home.rank.title': '收益榜',
  'home.comment.time': 'MM-DD HH:mm',
  'home.tweet.selectOrder': '选择交易',
  'home.tweet.There are noOrders': '未找到36小时内交易信息',
  'home.time.second': '{count} 秒',
  'home.time.min': '{count} 分钟',
  'home.time.hour': '{count} 小时',
  'home.time.day': '{count} 天',
  'home.time.week': '{count} 周',
  'home.time.month': '{count} 月',
  'home.time.year': '{count} 年',
  'home.Chart.Now': '现价',
  'home.TargetPrice': '目标价格',
  'home.goEdit': '编辑投资研究',
  'home.user.Analysis': '投资研究',
  'home.user.Upvated': '获赞',
  'home.user.Comments': '评论量',
  'home.user.Posts': '交易简讯',
  'home.analysis.Newest': '最新的',
  'home.analysis.MostReply': '最多赞的',
  'home.analysis.MostComments': '最多评论的',
  'home.Chart.Prediction': '目标价格',
  'home.analysis.LONG': '看涨',
  'home.analysis.SHORT': '看跌',
  'home.title.Followers': '粉丝列表',
  'home.title.TopViewAnalysis': '热搜投研',
  'home.user.Ror': '整体收益',
  'home.user.PositionsNo': '现有仓位',


  'stock.NetAssetValue': '资产净值',
  'stock.Open': '开盘',
  'stock.MktCap': '市值',
  'stock.PrevClose': '昨收',
  'stock.High': '最高',
  'stock.PERatio': '市盈率',
  'stock.52-WkHigh': '52周高',
  'stock.Low': '最低',
  'stock.DivYield': '股息率',
  'stock.52-WkLow': '52周低',

  'assets.account.mktVI': '净值比例',
  'assets.account.Unrlzd': '持仓收益',
  'assets.account.Analyses': '投资研究',
  'assets.account.Posts': '交易简讯',

  'connect.page.title': '帮助我们做的更好,请填写以下表格',
  'connect.page.Email_address': '邮箱地址',
  'connect.page.Email_address_description': '若您已是WEIM会员,请使用登录邮箱',
  'connect.page.Description': '详细描述',
  'connect.page.Description_description': '我们会以邮件的形式尽快回复您',
  'connect.page.add_file': '添加文件',
  'connect.page.Submit': '提交',
  'connect.page.Wechat_official_account': 'Wechat official account',
  'connect.page.Customer_service': 'Customer service',
  'connect.page.Attachments': '附件',
  'home.NoFollowing': '暂无关注的人',
  'home.NoWatching': '暂无自选资产',
  'home.noaccount': '暂无账号',
  'home.noanalysis': '暂无投资研究',
  'home.nopost': '暂无交易简讯',

};
