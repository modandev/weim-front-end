export default {
  "aboutUs.our.name": "我们的名字",
  "aboutUs.our.name.meaning": "中文音译：万木，取义于“一枝难独秀，万木易长青”。我们在投资行业笃行精英自治的集体智慧和众包协同。",
  "aboutUs.principle.and.concept.title": "万 木 的 原 则 和 理 念",
  "aboutUs.principles": `1. 组织一个精英投资群体；
2. 遵守一致的风控规则；
3. 使用一致的思维框架；
4. 寻求一致的收益目标；
5. 构建一个集体智慧组合；`,
  "aboutUs.concept": "不同背景和技能的团队，对于上面的五点原则会有不同的具体规则。尽管不同投资流派会有各自的核心投资思想，但是只要在上述五个维度长期严格执行，当精英投资群体的成员构成足够多元，分散和独立，坚信这个集体组合会实现长期稳定优异回报。万木理念的基础是基于精英自治的集体智慧和众包协同。",
  "aboutUs.vision.title": "万 木 的 远 见",
  "aboutUs.vision.weThink": "万木认为，现在普遍的基金产品对于投资人而言，信息披露无法做到彻底的全面，及时和透明，投资人需要更多的知情权和自主权。我们预见投资行业将出现基于组合实时复制的新的产品形式。",
  "aboutUs.vision.personal": "个人",
  "aboutUs.vision.personal.desc": "个人投资人自主跟踪交易不同流派的集体源组合或子组合（因为实操限制，也有一些流派的集体组合不适合跟踪交易）",
  "aboutUs.vision.organization": "机构",
  "aboutUs.vision.organization.desc": "机构投资人发行跟踪组合产品或定制、优化组合产品以及衍生产品。投资人投资此类产品，可进行穿透性实时查询。",
  "aboutUs.vision.weBelieve": "展望未来，你预计将产生多少虽然流派不同，但都是长期优秀的基于集体精英智慧的组合？有多少个人投资者自动跟踪交易这些组合？又将会有多少投资机构发行跟踪组合的资产管理产品？我们坚信，这个是投资管理行业的一个远大创新产品！参与其中的人都是在创造未来，拭目以待！",
  "aboutUs.investmentGroup.title": "万 木 投 资 群 组",
  "aboutUs.whatIs.investmentGroup": "投资界有多种方法论和流派，他们都可以在万木设立各自的独立群体（称之为“投资群组”）。",
  "aboutUs.investmentGroup.weThink": "我们认为万木的理念框架具有一定的普适性，当然也有完全不适用的领域。",
  "aboutUs.investmentGroup.different": "在WEIM.com, 每个投资群组的背后都代表成员相同和接近的投资理念。群组之间有差异也可能会存在类同之处，不同群组的交易可能会有趋同也可能存在互斥。",
  "aboutUs.investmentGroup.base": "时间是检验真理的利器，团队协作和持续优化演进是在投资市场生存和成功的基础。",
  "aboutUs.investmentGroup.example": "群组实例：",
  "aboutUs.investmentGroup.example.oneSideDoubling": "单边翻倍群",
  "aboutUs.investmentGroup.example.flyCutter": "飞刀群",
  "aboutUs.investmentGroup.example.smallMarketValueGrowth": "小市值成长群",
  "aboutUs.example.title": "群 组 展 示 ：单 边 翻 倍",
  "aboutUs.example.meaning": "我们群组的江湖名号“单边翻倍” （核心理念：遵循一致的风控规则，使用二元博弈分析，抓住三种单边行情，遵循四个工作流程，实现每次五十%以上预期回报目标）。下面是我们对于万木理念的数量化阐述和具体含义解释。",
  "aboutUs.example.elite": "组织一个精英投资群体",
  "aboutUs.example.elite.desc": `人员数量：理想状况为每一个细分领域十名以上，长期保持独立性，流动性和适度淘汰。
人员资质：典型背景为买方的投资岗位中高层人士，或者有投资经验的行业资深从业人士。`,
  "aboutUs.example.rm": "遵守一致的风控规则",
  "aboutUs.example.rm.desc": `风控规则： 只交易正股，没有杠杆，每个标的累计开仓总成本不超过20%。
交易规则：可多可空（以多为主），不鼓励频繁交易，买入后避免大幅反向调整。`,
  "aboutUs.example.thinkingFrame": "使用一致的思维框架",
  "aboutUs.example.thinkingFrame.desc": "运用二元博弈方法分析影响重大的少数核心问题；深刻理解产生单边行情背后的三种根本原因和模型；严格遵循四个工作流程。",
  "aboutUs.example.revenueTarget": "寻求统一的收益目标",
  "aboutUs.example.revenueTarget.desc": `预期时间：通常重点持有半年至一年，少数标的长期持有。
目标收益：每次交易预期目标收益50%-100%，聚焦伟大交易。`,
  "aboutUs.example.combination": "构建一个集体智慧组合",
  "aboutUs.example.combination.desc": "在个人组合的基础上通过集体自主加权产生集体组合，如果集体投资所涵盖的行业和领域足够广泛，还可以从这个集体组合中分离出来可定制和优化的细分子组合。",
  "aboutUs.example.why": "作为“单边翻倍”群组，为何我们坚信万木的理念框架？因为大道至简：上述集体组合的产生方法符合支持我们的投资理念产生长期优秀回报的重要原则：基于去中心化精英自制的集体智慧，减少交易频次和冲动，聚焦伟大交易，通过跨周期，跨行业，跨市场的分散投资，从长期来看避免了个体投资人普遍的不稳定弱点。",
  "aboutUs.faq.howToJoin": "如何加入万木？",
  "aboutUs.faq.howToJoin.answer": `成为投资经理:
需要注册IB盈透证券账户（www.ibkr.com），并设立子账户，然后请联系万木客服，提供IB盈透账户姓名，账号，邮箱，即可申请成为候选投资经理。投资经理可以在万木连接IB的多个子账户。
成为会员:
通过注册后即可成为万木会员。`,
  "aboutUs.faq.equityOfInvestmentManager": "万木投资经理权益",
  "aboutUs.faq.equityOfInvestmentManager.answer": `投资经理是万木的重要角色，根据业绩表现的层级可享受如下权益：
1. 免费订阅其他投资经理账户
2. 和其他投资经理一对一交流
3. 查看投资经理的投资报告
4. 获取投资经理交易记录
5. 获得所在投资群组的深度讨论参与机会
6. 向投资人通过展现研究和投资能力，获得潜在资金管理的机会。
7. 向专业资产管理机构展现研究和投资能力，获得潜在就职机会。`,
  "aboutUs.faq.membershipInterests": "万木会员权益",
  "aboutUs.faq.membershipInterests.answer": `会员可以免费获得投资经理的交易行为信息。
会员可以通过订阅模式查看投资经理的交易详情和投资报告。`,
  "aboutUs.faq.howToCreateInvestmentGroup": "如何创建一个投资群组？",
  "aboutUs.faq.howToCreateInvestmentGroup.answer": `万木笃行精英自治的集体智慧和众包协同的投资理念。任何人都可以在万木平台上创建风格独树一帜的投资群组，创建人即为组长。组长是群组的管理者，也是群组规则的制定者，需要遵循以下原则并细化（可参考“单边翻倍”群组的案例示范）。资料准备完整并提交后万木会视情况予以批准。
1. 组织一个精英投资群体；
2. 遵守一致的风控规则；
3. 使用一致的思维框架；
4. 寻求一致的收益目标；
5. 构建一个集体智慧组合。`,
  "aboutUs.faq.howToJoinInvestmentGroup": "如何加入一个投资群组？",
  "aboutUs.faq.howToJoinInvestmentGroup.answer": "加入投资群组前，需申请成为万木投资经理。然后，群组成员候选人需要提交简历和研究报告，同时需阐述投资理念和交易风格，以及个人二级市场投资业绩作为参考材料。资料递交给投资群组后，由组长和其成员决定是否接受。投资经理可以申请加入多个群组，但是加入每个投资群组需要设置独立的对应于该群组的IB子账户。",
  "aboutUs.faq.howToBuildingACollectivePortfolio": "如何构建集体组合？",
  "aboutUs.faq.howToBuildingACollectivePortfolio.answer": `一个投资群组只产生一个集体组合，展示本群组的投资能力，便于未来投资产品的设计研发。产生优秀集体组合的投资群组，可以获得更多资金管理的潜在机会。产生集体组合的三个核心步骤如下，每个步骤的决策模型由本群组内部决定。
1. 决定集体组合采纳样本，不是所有成员都被纳入。
2. 每个被采纳的样本会员，通过汇总所有样本会员的组合产生集体组合数据源。
3. 通过对集体组合数据源的加权，产生代表该群组的集体组合。`,
  "aboutUs.contactUs.title": "联 系 我 们",
  "aboutUs.contactUs.wechat.officialAccount": "万木微信公众号",
  "aboutUs.contactUs.wechat.customerService": "万木客服微信",
};
