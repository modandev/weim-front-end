export default {
  "message.required": "字段不能为空",
  "message.format.error": "格式有误",
  "message.noData": "暂无数据",
  "message.globalSearchBar.keyword.format.error": "搜索词只能由中文、英文及数字组成，不超过 20 位",
  "message.account.nickname.format.error": "账号名称只能由中文、英文及数字组成，3 - 20 位",
  "message.account.nickname.exist": "该账号名已经存在",
  "message.username.format.error": "昵称只能由中文、英文及数字组成，不超过 16 位",
  "message.chart.empty": "暂未同步到数据"
};
