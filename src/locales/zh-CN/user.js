export default {
  'user.weim.signUp': '注册 WEIM 账号',
  'user.weim.signIn': '登录到 WEIM',
  'user.signUp.required': '目前只有拥有 IB 账号的用户才能注册，我们将在后期开放给更多的用户',
  'user.signUp.subscribe': '您可点击注册留下您的联系方式，平台开放后我们会通过您留下的联系方式通知您，您也可以关注我们的微信公众号获取及时信息',
  'user.follow.wechat.official.account': '扫码关注 WEIM 微信公众号，获得更多新鲜资讯',
  'user.consult.wechat.custom.service': '任何问题，联系微信客服',
  'user.settings.locale.select': '请选择语言',
  'user.settings.locale.chinese': '中文',
  'user.settings.locale.english': 'English',
  'user.accountManagement': '账号管理',
  'user.accountManagement.myAnalysis': '我的分析',
  'user.accountManagement.myAccounts': '我的账号',
  'user.accountManagement.addAccount': '加账号',
  'user.userManagement': '用户管理',
  'user.userManagement.following': '正在关注',
  'user.me.accountStatus': '账号状态',
  'user.me.accountStatus.FAILED': '关联失败',
  'user.me.accountStatus.ACTIVE': '已激活',
  'user.me.accountStatus.PENDING_VERIFICATION': '正在审核中',
  'user.me.account.name.placeholder': '账号名称',
  'user.me.aboutAccount.placeholder': '关于该账号',
  'user.me.account.name.tips': '*账号名称只能编辑一次',
  'user.me.account.name.modify.tip': '账号名称编辑后无法再更改，\n是否保存本次编辑？',
  'user.settings.user': '用户设置',
  'user.settings.favorite': '偏好设置',
  'user.settings.locale': '语言',
  'user.settings.riseAndFall': '涨跌',
  'user.settings.riseAndFall.GURD': '绿涨红跌',
  'user.settings.riseAndFall.GDRU': '绿跌红涨',
  'user.settings.nickname': '昵称',
  'user.settings.connect': '关联',
  'user.settings.connected': '已关联',
  'user.connectIB.questionOne': '投资管理人有哪些权益?',
  'user.connectIB.answerOne': `免费订阅其他投资管理人账户;
和其他投资人一对一交流;
查看其他投资管理人的交易简讯和优质研究报告;
获取其他投资管理人详细交易记录;
向投资人展现研究和投资能力,获得潜在至今管理机会;
向专业资产管理机构展现研究和投资能力,获得潜在就职机会.`,
  'user.connectIB.questionTwo': '为何需要提交 IB 账号并设立子账户?',
  'user.connectIB.answerTwo': `万木平台以及投资群组的核心基础是基于实盘交易,我们通过与IB的深度合作和系统整合,使得投资群组的风控原则保持一致,进而实现长期可观测、可量化的结果评价.
IB的个人账户可以设置子账户,提交子账户UID并验证后,所在投资群组的风控约束只作用于该子账户,而不会对投资管理人的IB主账户和其他子账户产生任何影响.
每个子账户只对应一个投资群组,如果有意加入不同群组,需要开通多个子账户.(开立子账户相关问题,请自行联系IB客服咨询)`,
  'user.connectIB.questionThree': '子账户资金由谁托管?',
  'user.connectIB.answerThree': `子账户的资金属于个人,由IB托管,账户内交易行为所实现的收益和亏损也完全归属于投资管理本人,万木投资群组仅对子账户做风控约束,万木和投资管理人之间不涉及资金往来,不涉及管理费、提成等.`,
  'user.connectIB.questionFour': '为何设立10万美元的资金要求?',
  'user.connectIB.answerFour': `万木希望汇集一群优秀而勤勉的专业二级市场投资人,在此共享投资收益、共同学习.10万美元可以视作专业投资人的进入门槛.门槛不是绝对的,对于尚未达到资金要求,但实力非凡、有强烈意愿的年轻候选人,资金要求可以适当降低,请和我们联系.`,
  'user.connectIB.howToBeManager': '如何成为万木投资管理人?',
  'user.connectIB.toBeManagerSteps': `有意加入的专业投资者,首先需要注册IB盈透证券账户(www.ibkr.com),并开立子账户.
其次,请提供您的IB子账号UID,IB账号的姓和名,IB注册邮箱.
WEIM会提交您的账号信息到IB审核,并会在成功后发一封邮件到您的IB注册邮箱里.
点击邮件里的链接即可成功登录.`,
  'user.connectIB.subaccount.uid': 'IB子账户UID',
  'user.connectIB.subaccount.lastname': 'IB账户姓',
  'user.connectIB.subaccount.firstname': 'IB账户名',
  'user.connectIB.subaccount.email': 'IB注册邮箱',
  'user.connectIB.subaccount.connect.done': '已成功收到您的请求',
  'user.connectIB.subaccount.connect.connected': '该 UID 已被关联到您所登录的账号',
  'user.connectIB.subaccount.connect.notify': 'WEIM会将您的信息提交到IB进行审核和关联。IB将在2天内给您的邮箱发确认邮件。',
  'user.connectIB.subaccount.connect.help': '若有任何疑问，您可通过邮件或者微信客服联系我们。我们会在第一时间为您解决问题。',
  'user.connectIB.moreQuestion.help': '更多问题, 请发送申请邮件到:',
  'user.account': '账号',
  'user.panel.account.profile': '账户简介',
  'user.panel.investment.profile': '投资经理简介',
  'user.panel.position.title': '现有仓位',
  'user.panel.my.account.title': '账号',
  'user.panel.my.picked.title': '自选列表',
  'user.panel.NAV': '资产净值',
  'user.panel.position.trade.buy': '买入',
  'user.panel.position.trade.sell': '卖出',
  'user.panel.position.trade.analysis': '深度分析',
  'user.panel.position.addAnalysis2Asset': '深度分析到该资产',
  'user.watchlist.title': '自选列表',
  'user.watchlist.filter.all': '全部自选',
  'user.panel.position.add.analysis': '增加投资研究',
  'user.watchlist.ticker': 'Ticker',
  'user.watchlist.name': '公司名称',
  'user.watchlist.symbol': '代码',
  'user.watchlist.exchange': '交易所',
  'user.watchlist.currentPrice': '现价',
  'user.watchlist.accountsHolding': '持仓账户',
  'user.watchlist.earningsCalendar': '财报日历',

  'user.analysis.investmentResearch': '投资研究',
  'user.analysis.investmentResearch.title.placeholder': '无标题',
  'user.analysis.investmentResearch.draft': '草稿',
  'user.analysis.investmentResearch.saving': '保存中...',
  'user.analysis.investmentResearch.publish': '发布',
  'user.analysis.investmentResearch.summary.placeholder': '概要',
  'user.analysis.investmentResearch.body': '正文',
  'user.analysis.investmentResearch.subject.placeholder': '编写正文',
  'user.analysis.investmentResearch.forecast.description': `价格会在
%days
天内到达
%price
， 所以要`,

  'user.analysis.investmentResearch.PriceForecasting': '预测价格',
  'user.analysis.investmentResearch.Day': '天',
  'user.analysis.investmentResearch.PredictedArrivalTime': '预测到达时间',

  'user.analysis.investmentResearch.forecast.LONG': '看涨',
  'user.analysis.investmentResearch.forecast.SHORT': '看跌',
  'user.analysis.investmentResearch.publish.tips': '发布后投资研究无法被更改。\n是否直接发布？',
  'user.analysis.investmentResearch.discard': '删除',
  'user.analysis.investmentResearch.discard.tips': '您的投资研究将被彻底删除，无法复原。\n确定继续删除？',
  'user.analysis.investmentResearch.previewPage.title': '预览页，还未发布',
  'user.analysis.published': '文章已成功发布啦',
  'user.analysis.shareWithWorld': '跟世界分享一下！',
  'user.account.connectingTip': '账号正在关联中，需要大概1-2个工作日，请耐心留意您的邮箱。',
  'user.followersList': '粉丝列表',
  'user.DataFromYahoo': '数据来自雅虎财经·以美元计算',
  'user.priceInUSD': '以{country}计算',
  'user.Liquidated': '已平仓',
  'user.Time-weighted Return': '时间权重收益',
  'user.Money-weighted Return': '规模权重收益',
  'use.connectMessageTitle': '加入WEIM并成为投资经理',
  'use.connect.status.success.title': '我们很高兴您的加入！',
  'use.connect.status.error.title': '连接UID账号时似乎出了点问题！',

  'use.connect.status.success.message': '完成UID账号连接可能需要1-2天。\n请密切注意您的电子邮件。\n\n只有在UID的连接已完成后您才可以登陆WEIM',

  'use.connect.status.error.message.submitted': '您提交的UID账号已连接到另一个用户。\n 如有任何疑问，请联系我们的客户服务团队。',

  'use.connect.status.error.message.unfortunately': '很抱歉，您的账号交易额没有达到WEIM的期望；\n WEIM 正在寻找交易额至少$100,000的合作伙伴；\n 如有任何疑问，请联系我们的客户服务团队。',

  'use.connect.status.error.message.other': '如有任何疑问，请联系我们的客户服务团队。',

  'user.connect.title': '关联你的IB盈透证劵UID账号到WEIM',

  'user.connect.sub_title': '通过与IB的系统集成，我们实现了可观察和可量化的结果评估',

  'user.connect.uid_message': '注册IB盈透证券账号(www.ibkr.com)，并开设一个子帐户。（有关方法，请与IB联系）',

  'user.connect.email_action': 'WEIM会将您的帐户信息提交给IB进行关联，随后向您的IB注册邮箱发送电子邮件进行确认。',

  'user.connect.Creating your account profile': '建立你的账号信息',

  'user.connect.userNicknameAction' :'帐户名称只能编辑一次',

  'user.connect.submitButton' :'完成',

  'user.connect.nextTime': '下次登录时创建',

  'user.connect.BINDING_IN_PROGRESS': '账号正在关联中，需要大概1-2个工作日，请耐心留意您的邮箱。',
  'user.connect.BINDING_IN_PROGRESS_Link': ' ',

  'user.connect.ACCT_CONNECT_FAIL': '账号关联失败，点击此处',
  'user.connect.ACCT_CONNECT_FAIL_Link': '查看原因',

  'user.connect.ACCT_PENDING_INFO': '账号已关联成功，在此',
  'user.connect.ACCT_PENDING_INFO_Link': '激活账号',



};
