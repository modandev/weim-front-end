import component from './en-US/component';
import menu from './en-US/menu';
import user from './en-US/user';
import home from './en-US/home';
import aboutUs from './en-US/aboutUs';
import message from './en-US/message';
import privacyNotice from './en-US/privacyNotice';
import userTerms from './en-US/userTerms';

export default {
  "common.confirm": "Confirm",
  "common.search": "Search",
  "common.signIn": "Sign in",
  "common.signUp": "Sign up",
  "common.settings": "Setting",
  "common.logout": "Logout",
  "common.send": "Send",
  "common.optional": "自选",
  "common.follow": "MyFans",
  "common.unfollow": "Unfollow",
  "common.follower": "Follower",
  "common.share": "Share",
  "common.symbol": "Symbol",
  "common.back": "Back",
  "common.publish": "Publish",
  "common.notification": "Notification",
  "common.edit": "Edit",
  "common.cancel": "Cancel",
  "common.save": "Save",
  "common.seeMore": "View More",
  "common.notFound": "Page not found",
  "common.copied": "Copied",
  "common.privacy": "Privacy",
  "common.userTerms": "User Terms",
  ...component,
  ...menu,
  ...user,
  ...home,
  ...aboutUs,
  ...message,
  ...userTerms,
  ...privacyNotice,
};
