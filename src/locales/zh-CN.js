import component from './zh-CN/component';
import menu from './zh-CN/menu';
import user from './zh-CN/user';
import home from './zh-CN/home';
import aboutUs from './zh-CN/aboutUs';
import message from './zh-CN/message';
import privacyNotice from './zh-CN/privacyNotice';
import userTerms from './zh-CN/userTerms';

export default {
  "common.confirm": "确认",
  "common.search": "搜索",
  "common.signIn": "登录",
  "common.signUp": "注册",
  "common.settings": "设置",
  "common.logout": "退出登录",
  "common.send": "发送",
  "common.optional": "自选",
  "common.follow": "关注",
  "common.unfollow": "取消关注",
  "common.follower": "粉丝",
  "common.share": "分享",
  "common.symbol": "代码",
  "common.back": "返回",
  "common.publish": "发布",
  "common.notification": "通知中心",
  "common.edit": "编辑",
  "common.cancel": "取消",
  "common.save": "保存",
  "common.seeMore": "查看更多",
  "common.notFound": "页面飘走了",
  "common.copied": "已复制",
  "common.privacy": "万木隐私保护指引",
  "common.userTerms": "万木社区协议",
  ...component,
  ...menu,
  ...user,
  ...home,
  ...aboutUs,
  ...message,
  ...userTerms,
  ...privacyNotice,
};
