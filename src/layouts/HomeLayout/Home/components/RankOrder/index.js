import React, { useState, useCallback, useEffect } from 'react';
import Button from '@/components/Buttons';
import styles from './styles.less';
import SelectButton from '@/components/SelectButton';
import { AccountCard, CopyText, Icon, InlineDialog } from '@/components';
import { formatMessage, FormattedMessage } from 'umi-plugin-react/locale';
import { router } from 'umi';
import Spinner from '@atlaskit/spinner/spinner';
import { connect, useDispatch } from 'dva';
import FollowButton from '@/components/FollowButton';
import NoData from '@/layouts/HomeLayout/Home/components/NoData';

const options = [
  {
    value: '1W',
    label: <div className={styles.option}> {formatMessage({ id: 'home.tweet.Week' }, { count: 1 })}</div>,
  },
  {
    value: '1M',
    label: <div className={styles.option}>{formatMessage({ id: 'home.tweet.Month' }, { count: 1 })} </div>,
  },
  {
    value: '3M',
    label: <div className={styles.option}> {formatMessage({ id: 'home.tweet.Month' }, { count: 3 })} </div>,
  },
];

export const RankOrder = connect(({ home }) => ({
  accountsRank: home.accountsRank,
}))(
  ({ dispatch, accountsRank }) => {
    const defaultOption = options[0];
    const [isMore, setIsMore] = useState(false);
    const [loading, setLoading] = useState(true);
    const getAccountsRank = useCallback((scope) => {
      dispatch({
        type: 'home/rankList',
        variables: {
          type: 'SELL',
          pageSize: 5,
          scope,
        },
      });
    });
    const showMore = useCallback(() => {
      setIsMore(!isMore);
    });
    const onSelect = useCallback(async (scope) => {
      setIsMore(false);
      setLoading(true);
      getAccountsRank(scope);
    });

    useEffect(() => {
      getAccountsRank(defaultOption.value);
    }, [defaultOption.value]);

    useEffect(() => {
      setIsMore(accountsRank.length <= 3);
    }, [accountsRank.length]);

    useEffect(() => {
      accountsRank.length && setLoading(false);
    }, [accountsRank]);
    return (
      <section className={styles.hot}>
        <header>
          <div className={styles.title}>
            {formatMessage({ id: 'home.rank.title' })}
          </div>
          <SelectButton
            style={{ height: '35px' }}
            defaultOption={defaultOption}
            onChange={onSelect}
            options={options} />
        </header>
        <ul>
          {accountsRank.map((item, index) => {
            if (index >= 3 && !isMore) return null;
            if (index >= 5 && isMore) return null;
            return <Item key={index}
                         dispatch={dispatch}
                         item={item} />;
          })}

          {loading ? <div className={styles.loading}>
            <Spinner />
          </div> : !accountsRank.length ? <NoData /> : null
          }
        </ul>

        {accountsRank.length > 3 ?
          <footer>
          <Button
            appearance="subtle"
            onClick={showMore}
            css={{ height: '24px' }}
            iconAfter={<Icon type="right-black" />}>
        {formatMessage({
          id: !isMore ? 'home.newsletter.comment.showMore' : 'home.newsletter.comment.Push',
        })}
        </Button>
        </footer> : null}
      </section>);
  });

const Item = ({ item, dispatch }) => {
  const dvaDispatch = useDispatch();
  const { rate_of_return, account, accountTrading } = item;
  const [loading, setLoading] = useState(false);
  const changeAccountStatus = useCallback(async (is_follows) => {
    setLoading(true);
    await dvaDispatch({ type: 'home/fetchFollow', is_follows, account_id: account.account_id });
  });
  const goDashboard = useCallback(() => {
    window.open(`/dashboard/${account.user_id}/accounts/${account.account_id}`);
  });
  const goFans = useCallback(() => {
    router.push(`/followers/${account.user_id}/accounts/${account.account_id}`);
  });

  useEffect(() => {
    setLoading(false);
  }, [item, item.account]);

  return <li className={styles.item}
             key={account.account_id}>
    <div className={styles.title}>{account.name}
    </div>
    <div className={'row_box'}>

       <InlineDialog
         isOpen={false}
         placement="auto-start"
         delay={500}
         content={<AccountCard style={{ width: '300px' }}
                               account={{ ...account, ...accountTrading }} />}>
      <div className={styles.avatar}
           onClick={goDashboard}>
        {account.header_url ? <img src={account.header_url}
                                   alt="" /> :
          <div className={styles.default_avatar}>
            <span>
              {(account.nickname).slice(0, 1)}
            </span>
          </div>
        }
      </div>
       </InlineDialog>

      <div className={styles.message_box}>
         <InlineDialog
           isOpen={false}
           placement="auto-start"
           content={<AccountCard style={{ width: '300px' }}
                                 account={{ ...account, ...accountTrading }} />}
           delay={500}>
            <div className={styles.user_name}
                 onClick={goDashboard}>
              {account.nickname}
            </div>
         </InlineDialog>
        <div
          onClick={goFans}
          className={styles.message_line}>
          {formatMessage({ id: 'home.rank.Follows' }, {
            s: account.no_of_followers > 1 ? 's' : '',
            count: account.no_of_followers || 0,
          })}
        </div>

      </div>
        <div>
          <FollowButton
            userId={account.user_id}
            is_followed={account.is_followed}
            account_id={account.account_id} />
        </div>
      </div>
      <div className={styles.account}>
        <Icon type="shouyiguize" />&nbsp;&nbsp;{formatMessage({ id: 'home.Weekly gain' })}: {parseFloat((rate_of_return || 0)).toFixed(2)}%
      </div>
  </li>;
};

