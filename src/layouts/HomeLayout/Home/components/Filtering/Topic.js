import classnames from 'classnames';
import { formatMessage } from 'umi-plugin-react/locale';
import styles from './index.less';
import { useCallback } from 'react';

const Topic = ({ setStreamQuery, postType }) => {
  const onChange = useCallback((e) => {
    if (postType !== e.target.value) {
      setStreamQuery({ postType: e.target.checked ? '' : e.target.value === 'ANALYSIS' ? 'TWEET' : 'ANALYSIS' });
    }
  });

  return (
    <section className={styles.topic}>
    <ul>
      <li className="clearfix">
        <label>
          <input type="checkbox"
                 value="ANALYSIS"
                 checked={postType === '' || postType === 'ANALYSIS'}
                 className={`checkbox-circle`}
                 onChange={onChange}
                 readOnly
          />&nbsp;&nbsp;
          <span className={classnames(styles.label, styles.aggravate)}>
            {formatMessage({ id: 'home.discover.investmentResearch' })}
          </span>
        </label>
      </li>
      <li className="clearfix">
        <label>
          <input type="checkbox"
                 value="TWEET"
                 checked={postType === '' || postType === 'TWEET'}
                 onChange={onChange}
                 className={`checkbox-circle`}
                 readOnly
          />&nbsp;&nbsp;
          <span className={classnames(styles.label, styles.aggravate)}>
            {formatMessage({ id: 'home.discover.transactionNewsletters' })}
          </span>
        </label>
      </li>
    </ul>
  </section>
  );
};

export default Topic;
