import { useCallback, useState } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';
import { Icon, Collapse } from '@/components';
import styles from './index.less';
import { WATCHLIST, DELETE_WATCH_LIST } from '@/shared/graphql/gql/follow';
import { useMutation, useApolloClient, useQuery } from '@apollo/react-hooks';
import classnames from 'classnames';

const Panel = Collapse.Panel;

const Picked = ({ setStreamQuery, watchedOnly }) => {
  const { data: { watchlist = [] } = {} } = useQuery(WATCHLIST);
  const [deleteWatchlist] = useMutation(DELETE_WATCH_LIST, {});

  const onChange = (e) => {
    setStreamQuery({ watchedOnly: e.target.value === '1' ? true : false });
  };

  return (
    <section className={styles.picked}>
       <ul>
        <li className={classnames(!watchedOnly ? styles.active : '')}>
          <label>
            <input type="radio"
                   name="picked"
                   color="green"
                   value="0"
                   checked={!watchedOnly}
                   onChange={onChange}
                   className={`radio-circle`}
                   readOnly
            />&nbsp;&nbsp;
            <span className={classnames(styles.label, styles.aggravate)}>
              {formatMessage({ id: 'home.picked.universal' })}
            </span>
          </label>
        </li>
         <li className={classnames(!!watchedOnly ? styles.active : '')}>
           <label>
            <input type="radio"
                   name="picked"
                   color="green"
                   value="1"
                   checked={!!watchedOnly}
                   onChange={onChange}
                   className={`radio-circle`}
                   readOnly
            />&nbsp;&nbsp;
            <span className={classnames(styles.label, styles.aggravate)}>
              {formatMessage({ id: 'home.picked.mine' })}
            </span>
          </label>
         </li>
       </ul>


    </section>
  );
};

export default Picked;
