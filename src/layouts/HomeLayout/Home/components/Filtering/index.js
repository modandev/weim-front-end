import { memo, useState, useCallback } from 'react';
import Topic from './Topic';
import GlobalAccount from './GlobalAccount';
import Picked from './Picked';
import StockMarket from './StockMarket';
import { connect } from 'dva';

import styles from './index.less';

export default memo(connect(({ home }) => (
  { streamQuery: home.streamQuery }))(({ streamQuery, dispatch }) => {
  const setStreamQuery = useCallback((data = {}) => dispatch({ type: 'home/setStreamQuery', streamQuery: data }));
  return (
    <div className={styles.filtering}>
      <Topic setStreamQuery={setStreamQuery}
             postType={streamQuery.postType} />
      <GlobalAccount setStreamQuery={setStreamQuery}
                     followedOnly={streamQuery.followedOnly} />
      <Picked setStreamQuery={setStreamQuery}
              watchedOnly={streamQuery.watchedOnly} />
    </div>
  );
}));
