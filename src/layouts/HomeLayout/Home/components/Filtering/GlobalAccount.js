import { useCallback, useState } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';
import { Collapse } from '@/components';
import { useQuery } from '@apollo/react-hooks';
import { FOLLOWS } from '@/shared/graphql/gql/follow';
import styles from './index.less';
import classnames from 'classnames';

const Panel = Collapse.Panel;

const GlobalAccount = ({ setStreamQuery, followedOnly }) => {
  const { data: { follows = [] } = {} } = useQuery(FOLLOWS);
  const onChange = (e) => {
    setStreamQuery({ followedOnly: e.target.value === '1' ? true : false });
  };

  return (
    <section className={styles.global_account}>
      <ul className={styles.list}>
        <li className={classnames(!followedOnly ? styles.active : '')}>
          <label>
            <input type="radio"
                   name="global"
                   value="0"
                   checked={!followedOnly}
                   onChange={onChange}
                   className={`radio-circle`}
                   readOnly
            />&nbsp;&nbsp;
            <span className={classnames(styles.label, styles.aggravate)}>
              {formatMessage({ id: 'home.globalAccount' })}
            </span>
          </label>
          </li>

          <li className={!!followedOnly ? styles.active : ''}>
            <label>
              <input type="radio"
                     name="global"
                     value="1"
                     checked={!!followedOnly}
                     onChange={onChange}
                     className={`radio-circle`}
                     readOnly
              />&nbsp;&nbsp;
              <span className={classnames(styles.label, styles.aggravate)}>
                {formatMessage({ id: 'home.globalAccount.follow' })}
              </span>
            </label>
            <Collapse>
                <ul>
                 {follows.map(item => <li>{item.nickname}</li>)}
                </ul>
            </Collapse>
          </li>

        </ul>
    </section>
  );
};

export default GlobalAccount;
