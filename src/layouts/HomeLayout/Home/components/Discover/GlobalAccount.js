import { formatMessage } from 'umi-plugin-react/locale';
import { Collapse } from 'components';
import { useQuery } from '@apollo/react-hooks';
import { FOLLOWS } from '@/shared/graphql/gql/follow';
import styles from './index.less';

const Panel = Collapse.Panel;

const GlobalAccount = () => {
  const { data: { follows = [] } = {} } = useQuery(FOLLOWS);

  return (
    <section className={styles.global_account}>
      <h3>{formatMessage({ id: 'home.globalAccount' })}</h3>
      <Collapse>
        <Panel header={formatMessage({ id: 'home.globalAccount.follow' })}>
          <ul>
           {follows.map(item => <li>{item.nickname}</li>)}
          </ul>
        </Panel>
      </Collapse>
    </section>
  );
};

export default GlobalAccount;
