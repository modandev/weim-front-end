import { formatMessage } from 'umi-plugin-react/locale';

import styles from './index.less';

const StockMarket = () => (
  <section className={styles.stock_market}>
    <h3>{formatMessage({ id: 'home.stockMarket.HongKong' })}</h3>
    <h3>{formatMessage({ id: 'home.stockMarket.US' })}</h3>
    <h3>{formatMessage({ id: 'home.stockMarket.Shanghai&Shenzhen' })}</h3>
  </section>
);

export default StockMarket;
