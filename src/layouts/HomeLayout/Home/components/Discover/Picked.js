import { useCallback, useState } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';
import { Icon, Collapse } from '@/components';
import styles from './index.less';
import { WATCHLIST, DELETE_WATCH_LIST } from '@/shared/graphql/gql/follow';
import { useMutation, useApolloClient, useQuery,  } from '@apollo/react-hooks';

const Panel = Collapse.Panel;

const Picked = () => {
  const { data: { watchlist = [] } = {} } = useQuery(WATCHLIST);
  const [deleteWatchlist] = useMutation(DELETE_WATCH_LIST, {
  });

  return (
    <section className={styles.picked}>
      <h3>{formatMessage({ id: 'home.picked.universal' })}</h3>
      <Collapse activeKey={'0'}>
        <Panel key={'0'} header={formatMessage({ id: 'home.picked.mine' })}>
          <ul className="clearfix">
            {watchlist.map(({ticker}) => (
              <li key={ticker}>
                <span>{ticker}</span>
                <Icon type="close-circle"
                      onClick={() => {
                        deleteWatchlist({ variables: { ticker: ticker } })}} />
              </li>
            ))}
          </ul>
        </Panel>
      </Collapse>
    </section>
  );
};

export default Picked;
