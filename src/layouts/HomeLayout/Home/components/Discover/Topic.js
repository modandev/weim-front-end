import classnames from 'classnames';
import { formatMessage } from 'umi-plugin-react/locale';
import { Icon } from '@/components';

import styles from './index.less';

const Topic = () => (
  <section className={styles.topic}>
    <ul>
      <li className="clearfix">
        <span className={classnames(styles.label, styles.aggravate)}>
          {formatMessage({ id: 'home.discover.investmentResearch' })}
        </span>
        <Icon type="check-circle" />
      </li>
      <li className="clearfix">
          <span className={classnames(styles.label, styles.aggravate)}>
            {formatMessage({ id: 'home.discover.transactionNewsletters' })}
          </span>
          <Icon type="check-circle" />
      </li>
    </ul>
  </section>
);

export default Topic;
