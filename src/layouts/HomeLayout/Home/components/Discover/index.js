import { memo } from 'react';
import Topic from './Topic';
import GlobalAccount from './GlobalAccount';
import Picked from './Picked';
import StockMarket from './StockMarket';

import styles from './index.less';

export default memo(() => {
  return (
    <aside className={styles.discover}>
      <Topic />
      <GlobalAccount />
      <Picked />
      {/*<StockMarket />*/}
    </aside>
  );
});
