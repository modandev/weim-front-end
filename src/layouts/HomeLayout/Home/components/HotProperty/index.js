import React, { useState, useCallback, useContext, useEffect } from 'react';
import Button from '@/components/Buttons';
import styles from './styles.less';
import SelectButton from '@/components/SelectButton';
import { Icon } from '@/components';
import { HOT_ASSET } from '@/shared/graphql/gql/tweet';
import { useQuery, useApolloClient, useMutation } from '@apollo/react-hooks';
import { formatMessage } from 'umi-plugin-react/locale';
import { COMMIT_WATCH_LIST, DELETE_WATCH_LIST } from '@/shared/graphql/gql/follow';
import Spinner from '@atlaskit/spinner';
import { connect, useDispatch } from 'dva';
import EditorDoneIcon from '@atlaskit/icon/glyph/editor/done';
import { router } from 'umi';
import NoData from '../NoData';

export const BuyHotProperty = connect(({ home }) => ({
  hotListOfSell: home.hotListOfSell,
}))(
  ({ hotListOfSell, dispatch }) => {
    useEffect(() => {
      setHotList('1W');
    }, [setHotList]);
    const options = [
      {
        value: '1W',
        label: <div className={styles.option}> {formatMessage({ id: 'home.tweet.Week' }, { count: 1 })}</div>,
      },
      {
        value: '1M',
        label: <div className={styles.option}>{formatMessage({ id: 'home.tweet.Month' }, { count: 1 })} </div>,
      },
      {
        value: '3M',
        label: <div className={styles.option}> {formatMessage({ id: 'home.tweet.Month' }, { count: 3 })} </div>,
      },
    ];
    const defaultOption = options[0];

    const setHotList = useCallback(async (scope) => {
      return await dispatch({
        type: 'home/hotList',
        key: 'hotListOfSell',
        variables: {
          side: 'BUY',
          scope,
        },
      });
    });


    useEffect(() => {
    }, [hotListOfSell]);

    return <HotProperty title={formatMessage({ id: 'home.stock.MostBoughtAsset' })}
                        hotList={hotListOfSell}
                        setHotList={setHotList}
                        defaultOption={defaultOption}
                        options={options}
    />;
  });

export const SellHotProperty = connect(
  ({ home }) => ({
    hotListOfBuy: home.hotListOfBuy,
  }))(
  ({ hotListOfBuy, dispatch }) => {
    useEffect(() => {
      setHotList('1W');
    }, [setHotList]);
    const setHotList = useCallback(async (scope) => {
      return await dispatch({
        type: 'home/hotList',
        key: 'hotListOfBuy',
        variables: {
          side: 'SELL',
          scope,
        },
      });
    });
    useEffect(() => {
    }, [hotListOfBuy]);
    const options = [
      {
        value: '1W',
        label: <div className={styles.option}>{formatMessage({ id: 'home.tweet.Week' }, { count: 1 })}</div>,
      },
      {
        value: '1M',
        label: <div className={styles.option}> {formatMessage({ id: 'home.tweet.Month' }, { count: 1 })}</div>,
      },
      {
        value: '3M',
        label: <div className={styles.option}>{formatMessage({ id: 'home.tweet.Month' }, { count: 3 })} </div>,
      },
    ];
    const defaultOption = options[0];
    return <HotProperty title={formatMessage({ id: 'home.stock.MostSaleAsset' })}
                        hotList={hotListOfBuy}
                        setHotList={setHotList}
                        side={'SELL'}
                        defaultOption={defaultOption}
                        options={options}
    />;
  });


export const HotProperty = ({ options, defaultOption, title, setHotList, hotList }) => {
  const [isMore, setIsMore] = useState(false);
  const [loading, setLoading] = useState(true);

  const showMore = useCallback(() => {
    setIsMore(!isMore);
  });

  const onSelect = useCallback(async (scope) => {
    setLoading(true);
    await setHotList(scope);
    setLoading(false);
  });

  useEffect(() => {
    hotList.length && setLoading(false);
    setIsMore(hotList.length <= 3);
  }, [hotList.length]);

  return <section className={styles.hot}>
    <header>
      <div className={styles.title}>
        {title}
      </div>
      <SelectButton
        style={{ height: '35px' }}
        defaultOption={defaultOption}
        onChange={onSelect}
        options={options} />
    </header>
    <ul>
      {hotList.map((item, index) => {
        if (index >= 3 && !isMore) return null;
        if (index >= 5 && isMore) return null;
        return <Item key={index}
                     item={item} />;
      })}

    </ul>
    {
      loading ? <div className={styles.loading}>
        <Spinner />
      </div>
        : !hotList.length
        ? <NoData />
        : null
    }
    {hotList.length > 3 ?
      <footer>
        <Button appearance="subtle"
                onClick={showMore}
                css={{ height: '24px' }}
                iconAfter={<Icon type="right-black" />}>
          {formatMessage({ id: !isMore ? 'home.newsletter.comment.showMore' : 'home.newsletter.comment.Push' })}
        </Button>
           </footer> : null}
  </section>;
};

const Item = ({ item }) => {
  const dispatch = useDispatch();
  const { ticker, tradeNo, accountNo } = item;
  const [fetchCommitWatchlist, { loading: commitLoading }] = useMutation(COMMIT_WATCH_LIST, {
    variables: {
      ticker: ticker.key,
    },
    onCompleted() {
      dispatch({ type: 'home/changeWatchStatus', ticker: ticker.key });
    },
  });

  const [fetchDeleteWatchlist, { loading: deleteLoading }] = useMutation(DELETE_WATCH_LIST, {
    variables: {
      ticker: ticker.key,
    },
    onCompleted() {
      dispatch({ type: 'home/changeWatchStatus', ticker: ticker.key });
    },
  });

  const goDetail = useCallback(() => {
    router.push(`/asset/${ticker.key}/account`);
  });

  return <li className={styles.item}>
    <div onClick={goDetail}
         className={styles.title}>{ticker.name}</div>
    <div className={'row_box'}>
      <div className={styles.message_box}>
        <div className={styles.message_line}>
          <span className={styles.code}
                onClick={goDetail}>
            {formatMessage({ id: 'home.stock.Code' })}:{' '}{ticker.symbol}
          </span>
        </div>
        <div className={styles.message_line}>
          {!ticker.is_watch ?
            <Button
              iconBefore={<Icon type="add" />}
              onClick={fetchCommitWatchlist}
              isLoading={commitLoading}
              appearance="ghost"
              css={{ height: '26px', position: 'static' }}>
              {formatMessage({ id: 'home.stock.WatchList' })}
          </Button> :
            <Button
              onClick={fetchDeleteWatchlist}
              appearance="ghost"
              isLoading={deleteLoading}
              css={{ height: '26px' }}>
              <div className="flex-center">
                <div style={{ marginLeft: '-5px' }}
                     className="flex-center">
                  <EditorDoneIcon />
                </div>
                {formatMessage({ id: 'home.stock.CancelWatch' })}
              </div>

            </Button>}
        </div>
      </div>
      <div className={styles.space_line}></div>
      <div>
        <div className={`${styles.main_message} ${styles.message_line}`}>{accountNo}{formatMessage({ id: 'home.stock.AccountsTrading' }, { count: accountNo > 1 ? 's' : '' })}</div>
        <div className={`${styles.main_message} ${styles.message_line}`}>{tradeNo}{formatMessage({ id: 'home.stock.Transaction' }, { count: tradeNo > 1 ? 's' : '' })}</div>
      </div>
    </div>
  </li>;
};

