import React, { useCallback, useReducer } from 'react';
import { Link } from 'umi';
import { formatMessage } from 'umi-plugin-react/locale';
import styles from './index.less';
import { BuyHotProperty, SellHotProperty } from './components/HotProperty';
import Filtering from './components/Filtering';
import { HomeContext } from '@/store';
import { initialState, reducer } from '@/store/home';
import {RankOrder} from './components/RankOrder';
import { HotStock } from '@/layouts/DepthAnalysisLayout/HotStock';
import { Redirect } from 'umi';
import { connect } from 'react-redux';

const HomeLayouts = ({ children, currentUser }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  if (!currentUser || !currentUser.uid) {
    return <Redirect to={`/login?`} />};

  return <HomeContext.Provider value={{ dispatch, state }}>
    <section className={styles.homeWrap}>
    <aside>
      <Filtering />
    </aside>
      <div className={styles.content}
           id={'home_content'}>
        {children}
      </div>
    <aside className={styles.right}>
      <BuyHotProperty />
      <div style={{ width: '100%', height: '16px' }} />
      <SellHotProperty />
      <div style={{ width: '100%', height: '16px' }} />
      <RankOrder  dispatch={dispatch}/>
      <div style={{ width: '100%', height: '16px' }} />
      <HotStock />
      {/* <div className={styles.links}>
        <Link to="/user-terms">
          {formatMessage({ id: 'common.userTerms' })}
        </Link>
        <Link to="/privacy-notice">
          {formatMessage({ id: 'common.privacy' })}
        </Link>
      </div> */}
    </aside>
  </section>
  </HomeContext.Provider>;
}

export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(HomeLayouts);
