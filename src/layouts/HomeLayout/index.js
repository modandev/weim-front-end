import Home from './Home';
import React from 'react';
import { Redirect } from 'umi';
import { connect } from 'react-redux';

const HomeLayouts = ({ children, currentUser }) => {
  if (currentUser && currentUser.uid) {
    return <Home>
      {children}
    </Home>;
  }
  return <Redirect to={`/login?`} />;
};

export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(HomeLayouts);
