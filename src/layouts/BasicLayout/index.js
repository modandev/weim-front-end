/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from 'react';
import { connect } from 'dva';
import { GlobalHeader, GlobalNotice, Notification } from '@/components';
import { INQUIRED_LOCALE_SETTING_KEY } from '@/shared/constants';

const BasicLayout = ({ location, notices, dispatch, children, route }) => {
  const { pathname } = location;
  const onNoticeClose = payload => dispatch({ type: 'global/removeNotice', payload });

  useEffect(() => {
    const inquired = localStorage.getItem(INQUIRED_LOCALE_SETTING_KEY);

    !inquired && dispatch({
      type: 'global/addNotice',
      payload: { type: 'LOCALE_SETTING', unique: true , onPressClose: () => {localStorage.setItem(INQUIRED_LOCALE_SETTING_KEY, 1);
      }}
    });
  }, []);

  return (
    <div>
      {notices && notices.length ? notices.map((notice, index) => {
        return <GlobalNotice key={index} onRequestClose={onNoticeClose} {...notice} />
      }) : null}
      <GlobalHeader location={location}/>

      <main>
        <div className={pathname === '/about-us' || /^(\/connect_account)|(\/manager_intro)/.test(pathname) ? '' : 'type-area'}>
          {children}
        </div>
      </main>
      <Notification />
    </div>
  );
}

export default connect(({ global }) => ({
  notices: global.notices
}))(BasicLayout);
