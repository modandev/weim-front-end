import React, { useState, useCallback, useEffect } from 'react';
import Button from '@/components/Buttons';
import styles from './styles.less';
import SelectButton from '@/components/SelectButton';
import { CopyText, Icon } from '@/components';
import { formatMessage, FormattedMessage } from 'umi-plugin-react/locale';
import Spinner from '@atlaskit/spinner/spinner';
import { TRENDING_ANALYSIS } from '@/shared/graphql/gql/depth';
import { useQuery } from '@apollo/react-hooks';
import { stringify, parse } from 'querystring';
import NoData from '@/components/NoData';

const options = [
  {
    value: '1W',
    label: <div className={styles.option}> {formatMessage({ id: 'home.tweet.Week' }, { count: 1 })}</div>,
  },
  {
    value: '1M',
    label: <div className={styles.option}>{formatMessage({ id: 'home.tweet.Month' }, { count: 1 })} </div>,
  },
  {
    value: '3M',
    label: <div className={styles.option}> {formatMessage({ id: 'home.tweet.Month' }, { count: 3 })} </div>,
  },
];

export const HotStock = ({ dispatch }) => {
  const search = parse(window.location.search.replace(/^\?/, ''));
  const { loading, data: { trendingAnalysis = [] } = {} } = useQuery(TRENDING_ANALYSIS);

  const [isMore, setIsMore] = useState(false);
  const showMore = useCallback(() => {
    setIsMore(!isMore);
  });

  useEffect(() => {
    setIsMore(trendingAnalysis.length <= 3);
  }, [trendingAnalysis.length]);

  return (
    <section className={styles.hot}>
      <header>
        <div className={styles.title}>
          {formatMessage({ id: 'home.title.TopViewAnalysis' })}
        </div>
      </header>
      <ul>
        {loading ? <div className={styles.loading}>
            <Spinner />
            </div> : trendingAnalysis.length ?
          trendingAnalysis.map((item, index) => {
            if (index >= 3 && !isMore) return null;
            if (index >= 5 && isMore) return null;
            return <Item key={item.post_id}
                         dispatch={dispatch}
                         item={item.detail} />;
          }): <NoData />}
      </ul>

      {trendingAnalysis.length > 3 ?
        <footer>
          <Button
            appearance="subtle"
            onClick={showMore}
            css={{ height: '24px' }}
            iconAfter={<Icon type="right-black" />}>
        {formatMessage({
          id: !isMore ? 'home.newsletter.comment.showMore' : 'home.newsletter.comment.Push',
        })}
        </Button>
        </footer> : null}
  </section>);
};

const Item = ({ item }) => {
  const goDetail = useCallback(() => window.open(`/analyses/${item.post_id}?${stringify({
    ticker: item.ticker.key,
    accountId: item.account_id,
    userId: item.account.user_id
  })}`));

  return <li className={styles.item}
             onClick={goDetail}>
    <div className={styles.ticker_title}>
      <span onClick={goDetail} className={styles.code}>
        <FormattedMessage id="home.stock.Code" />: {item.ticker.symbol}
      </span>
      &nbsp;{item.title}
    </div>
    <div className={'row_box'}>

    </div>
    <div className={styles.account}>
      <div className={styles.fans}>
        <div className={styles.comment}>
          <Icon type="comment-gray"
                style={{ width: '17px', height: '17px' }} />
          <span className={styles.no}>{item.comments_no}</span>
        </div>

        <div className={styles.comment}>
          <Icon type="thumbs-up-gray"
                style={{ width: '16px', height: '16px' }} />
          <span className={styles.no}>{item.likes_no}</span>
        </div>
      </div>

      <div className={`${styles.direction} ${item.prediction_side ===
      'LONG' ? styles.long : styles.short}`}>
            {formatMessage({
              id: `home.analysis.${item.prediction_side || 'LONG'}`,
            })}
          </div>
    </div>
  </li>;
};

