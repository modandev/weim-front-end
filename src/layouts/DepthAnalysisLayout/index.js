import React, { useCallback, useReducer } from 'react';
import styles from './index.less';
import AvatarCart from '@/components/AvatarCart';
import { HotStock } from './HotStock';

export default ({ children }) => {
  return <section className={styles.depth_analysis_wrap}>
      <div className={styles.content}
           id={'home_content'}>
        {children}
      </div>
    <aside className={styles.right}>
      <AvatarCart />
      <div style={{ width: '100%', height: '10px' }} />
      <HotStock />
    </aside>
  </section>;
}
