import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Redirect } from 'umi';
import { stringify } from 'querystring';
import getRoutes from '../../routes/getRoutes';

class SecurityLayout extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }

  _routes = getRoutes();

  checkUserState = async() => {
    const { dispatch } = this.props;
    const sessionToken = localStorage.getItem('token');
    if (sessionToken) {
      await dispatch({ type: 'user/viewer' });
    } else {
      this.props.dispatch( {type:'user/updateCurrentState',payload: {
        currentUser: {}
      }})
    }
    this.setState({ isReady: true });
  }

   componentDidMount() {
    this.checkUserState()
     window.addEventListener('visibilitychange', (data) => {
       if(document.visibilityState === 'visible') {
         this.checkUserState()
       }
     })
  }

  auth = () => {
    const { props, _routes } = this;
    const { pathname } = props.location;
    const matchedKey = Object.keys(_routes).find(key => {
      if (key === pathname || (key !== '/' && pathname.indexOf(key) === 0)) {
        return true;
      }

      return false;
    });
    return matchedKey ? !_routes[matchedKey].authority : true;
  };

  render() {
    const { isReady } = this.state;
    const { children, currentUser } = this.props;
    const isLogin = currentUser && currentUser.uid;

    if (!isReady) {
      return null;
    }

    if (!isLogin) {
      const accessible = this.auth();
      if (!accessible && window.location.pathname=== '/login') {
        const queryString = stringify({
          redirect: window.location.href,
        });
        return <Redirect to={`/login?${queryString}`} />;
      }
    } else {
      const accessible = this.auth();
    }
    return <>
      {children}
    </>;
  }
};

export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(SecurityLayout);
