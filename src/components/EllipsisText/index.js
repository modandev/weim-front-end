import React, { useRef, useEffect, useState } from 'react';
import Tooltip from '../Tooltip';
import styles from './styles.less';
import { formatMessage, getLocale } from 'umi-plugin-react/locale';

export default (props) => {
  const outer = useRef(null);
  const inner = useRef(null);
  const [disable, setDisable] = useState(false);

  useEffect(() => {
    if (outer && inner) {
      setDisable(!(inner.current.getBoundingClientRect().width > outer.current.getBoundingClientRect().width));
    }
  }, [props.children, props.text]);


  return (
    <Tooltip delay={2000}
             disable={disable}
             hideTooltipOnClick={true}
             content={props.text || props.children}
             position="top">
    <div className={styles.copyText}
         ref={outer}
         style={props.style}>
      <span ref={inner}>
        {props.children || props.text}
      </span>
  </div>
</Tooltip>);
}
