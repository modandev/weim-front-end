import { PureComponent, cloneElement, Children, createRef } from 'react';
import classnames from 'classnames';
import BodyBabyFactory from 'shared/BodyBabyFactory';
import DocumentEvent from 'shared/DocumentEvent';
import Option from './Option';

import styles from './index.less';

const DROPDOWN = Symbol.for('select.dropdown');
const CLASS_SELECTOR = 'select-dropdown';

class Select extends PureComponent {
  constructor(props) {
    const { value, defaultValue } = props;

    super(props);
    this.state = {
      value: defaultValue || value,
    };
    this.select = createRef();
  }

  _entered = false;
  _selector = null;

  componentDidMount() {
    const { current } = this.select;

    current.addEventListener('mouseenter', this.onMouseEnter, false);
    current.addEventListener('mouseleave', this.onMouseLeave, false);
    DocumentEvent.on('click', this.onDocumentClick);
  }

  componentWillUnmount() {
    const { current } = this.select;

    current.removeEventListener('mouseenter', this.onMouseEnter, false);
    current.removeEventListener('mouseleave', this.onMouseLeave, false);
    DocumentEvent.off('click', this.onDocumentClick);
  }

  toggle = method => {
    this._selector.querySelector(`.${CLASS_SELECTOR}`).classList[method]('hidden');
  };

  onMouseEnter = e => {
    e.stopPropagation();
    this._entered = true;
  };

  onMouseLeave = e => {
    e.stopPropagation();
    this._entered = false;
  };

  onOptionClick = value => {
    const { onSelect } = this.props;

    value !== this.state.value && this.setState({ value });
    typeof onSelect === 'function' && onSelect(value);
  };

  onDocumentClick = () => {
    if (!this._entered && this._selector) {
      this.toggle('add');
    }
  };

  onSelectClick = () => {
    if (!this[DROPDOWN]) {
      const { current } = this.select;
      const { width, height } = getComputedStyle(current);
      const { top, left } = current.getBoundingClientRect();

      this._selector = new BodyBabyFactory({
        cssText: 'position: absolute; width: 100%; top: 0; left: 0',
      })
      .exFactory(
        <div
          className={CLASS_SELECTOR}
          style={{
            width: width,
            minWidth: width,
            top: top + Number.parseInt(height, 10) + 2,
            left,
          }}
        >
          <div className="select-dropdown-container">
            {Children.map(this.props.children, child =>
              cloneElement(child, {
                onClick: this.onOptionClick,
              })
            )}
          </div>
        </div>
      );
      this[DROPDOWN] = true;
    } else {
      this.toggle('remove');
    }
  };

  render() {
    let value = '';

    Children.forEach(this.props.children, ({ props }) => {
      if (props.value === this.state.value) {
        value = props.children;
      }
    });

    return (
      <div ref={this.select} className={classnames(styles.select, "weim-select")} onClick={this.onSelectClick}>
        <div className={classnames(styles.select__container, "weim-select-container")}>
          <span className={classnames(styles.select__label, "weim-select-label")}>
            {value}
          </span>
        </div>
      </div>
    );
  }
}

Object.defineProperty(Select, 'Option', {
  value: Option,
  enumerable: true,
});

export default Select;
