import classnames from 'classnames';

import styles from './option.less';

const Option = ({ value, children, onClick }) => {
  const onSelect = () => onClick(value);

  return (
    <div className={classnames(styles.option, "weim-select-option")} onClick={onSelect}>
      <span>{children}</span>
    </div>
  );
};

export default Option;
