import styles from './index.less';

export default ({ message }) => (
  <p className={styles.message}>{message}</p>
);
