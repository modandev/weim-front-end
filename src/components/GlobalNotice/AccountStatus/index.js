import styles from './index.less';
import { connect } from 'dva';
import { formatMessage } from 'umi-plugin-react/locale';

export default connect(({ user }) => ({
  userStatus: user.userStatus,

}))(
  ({ message, userStatus, onPress, onClose, linkMessage }) => (
    <p className={styles.message_status}
    >{formatMessage({ id: message })} <a onClick={() => {
      onPress();
    }}>{formatMessage({ id: linkMessage })}</a></p>
  ),
);
