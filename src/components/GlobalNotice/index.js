import { useState } from 'react';
import Icon from '../Icon';
import LocaleSetting from './LocaleSetting';
import Tips from './Tip';
import AccountStatus from './AccountStatus'
import styles from './index.less';

const TYPE_MAP = {
  LOCALE_SETTING: LocaleSetting,
  TIPS: Tips,
  ACCOUNT_STATUS: AccountStatus,
};

export default ({ type, id, onRequestClose, onPressClose, ...rest }) => {
  const [visible, setVisible] = useState(true);
  const onClose = () => {
    setVisible(false);
    onRequestClose(id || type);
    onPressClose && onPressClose();
  };

  const Component = TYPE_MAP[type];

  return visible ? (
    <div className={styles.notice}>
      <div className="type-area clearfix">
        <div className={styles.notice__container}>
          <div className={styles.notice__content}>
            <Component onClose={onClose} {...rest} />
          </div>
          <Icon as="button" type="close-gray" onClick={onClose} />
        </div>
      </div>
    </div>
  ) : null;
};
