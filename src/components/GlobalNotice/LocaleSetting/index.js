import { useState } from 'react';
import { getLocale, setLocale as updateLocale, formatMessage } from 'umi-plugin-react/locale';
import Button from '../../Button';
import Select from '../../Select';
import { INQUIRED_LOCALE_SETTING_KEY, SUPPORTED_LOCALES } from 'shared/constants';

import styles from './index.less';

const { Option } = Select;

export default ({ onClose }) => {
  const [locale, setLocale] = useState(getLocale());
  const onSelect = locale => setLocale(locale);
  const onConfirm = () => {
    localStorage.setItem(INQUIRED_LOCALE_SETTING_KEY, 1);
    updateLocale(locale, true);
    onClose();
  };

  return (
    <div className={styles.localeSetting}>
      <span className={styles.label}>
        {formatMessage({ id: 'user.settings.locale.select' })}
      </span>
      <Select defaultValue={locale} onSelect={onSelect}>
        {SUPPORTED_LOCALES.map(({ key, value }) => (
          <Option key={key} value={value}>
            {formatMessage({ id: `user.settings.locale.${key}` })}
          </Option>
        ))}
      </Select>
      <Button
        shape="square"
        onClick={onConfirm}
      >
        {formatMessage({ id: 'common.confirm' })}
      </Button>
    </div>
  );
};
