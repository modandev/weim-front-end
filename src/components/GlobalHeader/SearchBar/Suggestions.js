/* eslint-disable jsx-a11y/anchor-is-valid */
import classnames from 'classnames';
import { formatMessage } from 'umi-plugin-react/locale';
import Icon from '../../Icon';

import styles from '../index.less';

export default ({ hot, history, onSetKeyword, onClearHistory }) => {
  if ((!history || !history.length) && (!hot || !hot.length)) {
    return null;
  }

  return (
    <div className={styles.suggestions}>
      {history && history.length ? (
        <div className={styles.history_search}>
          <div className={classnames(styles.title, 'clearfix')}>
            <h4>{formatMessage({ id: 'component.globalHeader.search.history' })}</h4>
            <a onClick={onClearHistory}>
              <Icon type="trash" />
              {formatMessage({ id: 'component.globalHeader.search.clear' })}
            </a>
          </div>
          <ul>
            {history.map(({ word }) => (
              <li key={word} onClick={() => onSetKeyword(word)}>{word}</li>
            ))}
          </ul>
        </div>
      ) : null}
      {hot && hot.length ? (
        <div className={styles.hot_search}>
          <div className={classnames(styles.title, 'clearfix')}>
            <h4>{formatMessage({ id: 'component.globalHeader.search.hot' })}</h4>
          </div>
          <ul>
            {hot.map(({ word }) => (
              <li key={word} onClick={() => onSetKeyword(word)}><Icon type="hot" /> {word}</li>
            ))}
          </ul>
        </div>
      ) : null}
    </div>
  );
};
