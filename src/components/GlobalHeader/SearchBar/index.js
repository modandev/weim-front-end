import { createRef, Component } from 'react';
import { connect } from 'dva';
import { formatMessage } from 'umi-plugin-react/locale';
import debounce from 'lodash.debounce';
import { GLOBAL_HEADER_SEARCH_KEYWORD_REGEXP } from 'shared/regexps';
import Icon from '../../Icon';
import Suggestions from './Suggestions';
import Results from './Results';
import Tips from '../../Tips';

import styles from '../index.less';

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyword: '',
      tipsVisible: false,
      loading: true
    };
    this.input = createRef();
    this.doSearchDebounced = debounce(this.doSearch, 500);
  }

  _suggestionsFetched = false;
  _waitKeywordEntered = false;

  onSearchFocus = () => this.input.current && this.input.current.focus();

  fetchSuggestions = () => {
    const { currentUser, dispatch } = this.props;

    if (!this._suggestionsFetched && currentUser.uid) {
      this._suggestionsFetched = true;
      dispatch({ type: 'global/searchHistory' });
      dispatch({ type: 'global/searchHot' });
    }
  }

  onCompositionStart = () => (this._waitKeywordEntered = true);

  onCompositionEnd = e => {
    this._waitKeywordEntered = false;

    this.onChange(e);
  };

  onChange = e => this.onSetKeyword(e.target.value);

  onSetKeyword = keyword =>
    this.setState({ keyword, tipsVisible: false }, () => {
      if (this._waitKeywordEntered) return;

      if (!keyword){
        this.setState({
          loading: true
        })
      }

      if (keyword && keyword.trim()) {
        keyword = keyword.trim();

        if (GLOBAL_HEADER_SEARCH_KEYWORD_REGEXP.test(keyword)) {
          this.doSearchDebounced(keyword);
        } else {
          this.setState({ tipsVisible: true });
        }
      }
    });

  onSearch = () => this.doSearch(this.state.keyword);

  onResultClick = word =>
    this.props.dispatch({ type: 'global/commitSearchWord', payload: { word } });

  doSearch = async (q) => {
    this.setState({
      loading: true
    })
    const res = await this.props.dispatch({ type: 'global/search', payload: { q } });
    this.setState({
      loading: false
    })
  };

  isEmpty = value => !value || !value.trim();

  onClearHistory = () => this.props.dispatch({ type: 'global/deleteSearchHistory' });

  render() {
    const { keyword, tipsVisible } = this.state;
    const { searchHot, searchHistory, searchResult } = this.props;

    return (
      <div tabIndex="-1" onFocus={this.onSearchFocus} className={styles.navbar__search}>
        <Tips
          floated
          type="danger"
          visible={tipsVisible}
          header={formatMessage({ id: 'message.globalSearchBar.keyword.format.error' })}
        >
          <input
            maxLength={20}
            value={keyword}
            ref={this.input}
            onChange={this.onChange}
            onFocus={this.fetchSuggestions}
            onCompositionStart={this.onCompositionStart}
            onCompositionEnd={this.onCompositionEnd}
            className={styles.navbar__search_input}
            placeholder={formatMessage({ id: 'common.search' })}
          />
          <Icon as="button" type="search" onClick={this.onSearch} />
          {!tipsVisible && (
            <div className={styles.navbar__search_dropdown}>
              {this.isEmpty(keyword)
                ? (
                <Suggestions
                  hot={searchHot}
                  history={searchHistory}
                  onSetKeyword={this.onSetKeyword}
                  onClearHistory={this.onClearHistory}
                />
              ) : (
                <Results
                  keyword={keyword}
                  data={searchResult}
                  onSearch={this.onSearch}
                  onResultClick={this.onResultClick}
                  searchLoading={this.state.loading}
                />
              )}
            </div>
          )}
        </Tips>
      </div>
    );
  }
}

export default connect(({ user, global }) => ({
  currentUser: user.currentUser,
  searchResult: global.searchResult,
  searchHistory: global.searchHistory,
  searchHot: global.searchHot,
}))(SearchBar);
