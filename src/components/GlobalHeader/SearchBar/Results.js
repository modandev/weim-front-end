import { useState } from 'react';
import { Link } from 'umi';
import classnames from 'classnames';
import { useMutation } from '@apollo/react-hooks';
import { formatMessage } from 'umi-plugin-react/locale';
import { getUrlProperty } from 'shared/locationParser';
import { COMMIT_WATCH_LIST } from 'shared/graphql/gql/follow';
import Tabs from '../../Tabs';
import Icon from '../../Icon';
import Button from '../../Buttons';
import Spinner from '@atlaskit/spinner';
import styles from '../index.less';

const { TabPane } = Tabs;
const origin = getUrlProperty('origin');
const DATA_MAP = {
  '1': 'accounts',
  '2': 'tickers',
  '3': 'users'
};

export default ({ keyword, data, dispatch, onSearch, onResultClick, searchLoading }) => {
  let { accounts, tickers, users } = data;

  accounts = accounts || [];
  tickers = tickers || [];
  users = users || [];

  const dataSource = { accounts, tickers, users };
  const [currentTab, setCurrentTab] = useState('1');
  const [seenMore, setSeenMore] = useState({});
  const [inProgress, setInProgress] = useState([]);
  const [doWatch ,{loading: watchLoading}] = useMutation(COMMIT_WATCH_LIST);

  const onWatch = ticker => {
    setInProgress([...inProgress, ticker]);
    doWatch({ variables: { ticker } }).then( (res) => {
      const _inProgress = [...inProgress];

      _inProgress.splice(_inProgress.indexOf(ticker), 1);
      setInProgress(_inProgress);
      onSearch();
    });
  };


  const onTabClick = key => setCurrentTab(key);

  const onSeenMore = () => setSeenMore({ ...seenMore, [currentTab]: true });

  const onAccountItemClick = (nickname, uid, accountId) => {
    onResultClick(nickname);
    window.open(`${origin}/dashboard/${uid}/accounts/${accountId}`, '_blank');
  };

  const onTickerItemClick = (symbol, ticker) => {
    onResultClick(symbol);
    window.open(`${origin}/asset/${ticker}/account`, '_blank');
  };

  const onUserItemClick = (nickname, uid) => {
    onResultClick(nickname);
    window.open(`${origin}/dashboard/${uid}/accounts`, '_blank');
  };

  return (
    <div className={styles.result}>
      <Tabs onTabClick={onTabClick} defaultActiveKey="1">
        <TabPane header={formatMessage({ id: 'component.globalHeader.search.result.tab.account' })} key="1">
          <div className={styles.tab_pane}>
            {searchLoading? <Loading />: accounts.length ? (seenMore[currentTab] ? accounts.slice(0, 10) : accounts.slice(0, 5)).map(({ nickname, user_id, account_id }) => (
              <div key={account_id} className={styles.search_result_item} onClick={() => onAccountItemClick(nickname, user_id, account_id)}>
                <div className={styles.item_info}>
                  <div className={styles.name}>{nickname}</div>
                  {/* <div className={styles.attach}>
                    {formatMessage({ id: 'common.follower' })}: 100
                  </div> */}
                </div>
              </div>
            )) : <p className={styles.hint}>{formatMessage({ id: 'message.noData' })}</p>}
          </div>
        </TabPane>
        <TabPane header={formatMessage({ id: 'component.globalHeader.search.result.tab.assets' })} key="2">
          <div className={styles.tab_pane}>
            {tickers.length ? (seenMore[currentTab] ? tickers.slice(0, 10) : tickers.slice(0, 5)).map(({ ticker, tickerDetail: { name, exchange, country_code, symbol, is_watch } }) => {
              let unmatchBefore = '';
              let match = '';
              let unmatchAfter = symbol;

              if (keyword && symbol) {
                const index = symbol.indexOf(keyword.toUpperCase());

                if (index > -1) {
                  unmatchBefore = symbol.slice(0, index);
                  match = keyword.toUpperCase();
                  unmatchAfter = symbol.slice(index + keyword.length);
                }
              }

              return (
                <div key={ticker} className={styles.search_result_item}>
                  <div className={styles.item_info} onClick={() => onTickerItemClick(symbol, ticker)}>
                    <div className={classnames(styles.name, styles.normal)}>
                      {name || exchange}
                    </div>
                    <div className={styles.attach}>
                      <span className={styles.badge}>{country_code}</span>
                      <span className={styles.code}>
                        {unmatchBefore}
                        {match && <span className={styles.hilite}>{match}</span>}
                        {unmatchAfter}
                      </span>
                    </div>
                  </div>
                  <div className={styles.item_action}>
                    {is_watch ? (
                      <span>{formatMessage({ id: 'home.stock.CancelWatch' })}</span>
                    ) : (
                      <Button
                        appearance="primary"
                        isLoading={inProgress.includes(ticker) && watchLoading}
                        onClick={() => onWatch(ticker)}
                        iconBefore={<Icon type="add-white" />}
                        css={{ height: '24px', position: 'static' }}
                      >
                        {formatMessage({ id: 'home.stock.WatchList' })}
                      </Button>
                    )}
                  </div>
                </div>
              )
            }) : <p className={styles.hint}>{formatMessage({ id: 'message.noData' })}</p>}
          </div>
        </TabPane>
        <TabPane header={formatMessage({ id: 'component.globalHeader.search.result.tab.user' })} key="3">
          <div className={styles.tab_pane}>
            {users.length ? (seenMore[currentTab] ? users.slice(0, 10) : users.slice(0, 5)).map(({ nickname, uid }) => (
              <div key={uid} className={styles.search_result_item} onClick={() => onUserItemClick(nickname, uid)}>
                <div className={styles.item_info}>
                  <div className={styles.name}>{nickname}</div>
                  {/* <div className={styles.attach}>
                    {formatMessage({ id: 'component.globalHeader.search.result.tab.account' })}: 4
                  </div> */}
                </div>
              </div>
            )) : <p className={styles.hint}>{formatMessage({ id: 'message.noData' })}</p>}
          </div>
        </TabPane>
      </Tabs>
      {!seenMore[currentTab] && dataSource[DATA_MAP[currentTab]].length > 5 ? (
        <div className={styles.footer}>
          <div className={styles.more} onClick={onSeenMore}>
            {formatMessage({ id: 'common.seeMore' })}
            <Icon type="right-gray" />
          </div>
        </div>
      ) : null}
    </div>
  );
};

const Loading = () => {
  return <div className={styles.loading}>
    <Spinner/>
    <span>
      {formatMessage({id:'home.Loading'})}
    </span>
  </div>
}
