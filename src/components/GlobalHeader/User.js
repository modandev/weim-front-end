import { connect } from 'dva';
import { Link } from 'umi';
import { formatMessage } from 'umi-plugin-react/locale';
import Avatar from '../Avatar';
import Icon from '../Icon';

import styles from './index.less';

const User = ({ dispatch, currentUser, isMangerIntro = false }) => {
  const { uid, avatar, lastName } = currentUser;
  const isLogin = currentUser && uid;

  const onLogout = () =>
    dispatch({
      type: 'user/logOut',
      payload: {
        input: {
          clientMutationId: '',
        },
      },
    });

  // 如果页面为manger_into 返回特殊的userheader
  if (window.location.pathname === '/manager_intro') {
    return <Link to="/login">
      <div className={styles.navbar__user_sign}>
              <Icon type="user" />
              <span>Sign In</span>
    </div>
    </Link>;
  }
  return (
    <div className={styles.navbar__user}>
      <div className={styles.navbar__user_main}>

        {isLogin ? (
          <div className={styles.navbar__user_avatar}>
            <Link to="/personal/accounts">
              <Avatar src={avatar}
                      small
                      theme="white"
                      backup={lastName} />
            </Link>
          </div>
        ) : (
          <>
            <div className={styles.navbar__user_sign}>
              <Icon type="user" />
              <span> {formatMessage({ id: 'component.globalHeader.sign' })}</span>
            </div>
          </>
        )}
      </div>
      <div className={styles.navbar__user_dropdown}>
        {isLogin ? (
          <>
            {/* <Link to="/depth-analysis">
              <Icon type="analysis" />
              {formatMessage({ id: 'component.globalHeader.depthAnalysis' })}
            </Link>
            <Link to="/personal/messages">
              <Icon type="newsletter" />
              {formatMessage({ id: 'component.globalHeader.myMessage' })}
            </Link>
            <i className={styles.separator} /> */}
            <Link to="/analyses/edit">
              <Icon type="analyses" />
              {formatMessage({ id: 'component.globalHeader.Analysis' })}
            </Link>
            <Link to="/personal/accounts">
              <Icon type="my_account" />
              {formatMessage({ id: 'component.globalHeader.myAccounts' })}
            </Link>
            <Link to="/personal/settings">
              <Icon type="settings" />
              {formatMessage({ id: 'component.globalHeader.settings' })}
            </Link>
            <Link to="/contactus">
              <Icon style={{ width: '22px', height: '20px', marginLeft: '2px' }}
                    type="customer-service" />
              {formatMessage({ id: 'component.globalHeader.Contact_us' })}
            </Link>
            {/* <Link to="/notifications">
              <Icon type="bell" />
              {formatMessage({ id: 'component.globalHeader.notification' })}
            </Link> */}
            <i className={styles.separator} />
            {/* <Link to="/contact-us">
              <Icon type="customer-service" />
              {formatMessage({ id: 'component.globalHeader.contactWeim' })}
            </Link> */}
            <Link
              to="/"
              onClick={onLogout}
              className={styles.text}
            >
                {formatMessage({ id: 'component.globalHeader.logout' })}
            </Link>
          </>
        ) : (
          <>
            <Link to="/login">
              <Icon type="login" />
              {formatMessage({ id: 'component.globalHeader.signIn' })}
            </Link>
            <Link to="/join">
              <Icon type="register" />
              {formatMessage({ id: 'component.globalHeader.signUp' })}
            </Link>
            <Link to="/contactus">
              <Icon style={{ width: '22px', height: '20px', marginLeft: '2px' }}
                    type="customer-service" />
              {formatMessage({ id: 'component.globalHeader.Contact_us' })}
            </Link>
            {/* <i className={styles.separator} />
            <Link to="/contact-us">
              <Icon type="customer-service" />
              {formatMessage({ id: 'component.globalHeader.contactWeim' })}
            </Link> */}
          </>
        )}
      </div>
    </div>
  );
};

export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(User);
