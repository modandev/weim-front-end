import { Link } from 'umi';

import styles from './index.less';

function Logo (props) {
  return (
    <div className={styles.navbar__logo}>
      <h1>
        <Link to="/">Weim</Link>
      </h1>
    </div>
  );
}

export default Logo;
