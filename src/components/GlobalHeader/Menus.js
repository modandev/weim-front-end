import classnames from 'classnames';
import { connect } from 'dva';
import { Link } from 'umi';
import { formatMessage } from 'umi-plugin-react/locale';

import styles from './index.less';

function MenuItem ({ active, href, label }) {
  return (
    <Link
      to={href}
      className={
        classnames(
          styles.navbar__menus_item,
          { [styles.navbar__menus_item_active]: active }
        )
      }
    >
      {label}
    </Link>
  );
}

function Menus ({ currentUser, activeMenu: active }) {
  const menuList = [
    { href: '/home/stream', label: formatMessage({ id: 'menu.home' }) },
    { href: `/dashboard/${currentUser.uid || ''}/accounts`, label: formatMessage({ id: 'menu.dashboard' }) , key: `/dashboard/${currentUser.uid || ''}`},
    { href: '/about-us', label: formatMessage({ id: 'menu.aboutUs' }) },
  ];


  return (
    <nav className={styles.navbar__menus}>
      {menuList.map(({ href, key, ...rest }, index) => {
        return (
        <MenuItem
          {...rest}
          key={index}
          href={href}
          isLogin={!!currentUser.uid}
          active={active === '/' ? active === '/'
            : (active.includes('/dashboard/') ? window.location.pathname.includes(key)
              : active.includes('/home') ? href.includes('/home')
                :  active === href )
          }
        />
      )})}
    </nav>
  );
}

export default connect(({ user, global }) => ({
  currentUser: user.currentUser,
  activeMenu: global.activeMenu,
}))(Menus);
