import { memo } from 'react';
import classnames from 'classnames';
import Logo from './Logo';
import SearchBar from './SearchBar';
import Menus from './Menus';
import User from './User';
import { connect } from 'dva';
import styles from './index.less';
import { Link } from 'umi';

const NavBar = connect(
  ({ user }) => ({
    active: user.userStatus.active,
    bindingAccountId: user.userStatus.signup_redirect === 'BINDING_IN_PROGRESS' ? user.userStatus.account_bind_in_progress.account_id : '',
    isLogin: !!(user.currentUser && user.currentUser.uid),
  }),
)(({ active, isLogin, location, bindingAccountId }) => {
    return (
      <header className={styles.navbar}>
      <div className={classnames(styles.container, 'type-area')}>
        <Logo />
        {active && isLogin && location.pathname !== '/manager_intro'  ? <>
          <SearchBar />
          <Menus />
          <User />
            </> : <div style={{ flex: 1 }}></div>
        }
        {location.pathname === '/manager_intro' ?
          <>
            <Link style={{ color: '#fff', marginRight: '100px' }}
                  to={bindingAccountId ? '/connect_account/status/' + bindingAccountId : '/connect_account/add_IB'}>JOIN US</Link>
            <User isMangerIntro={true} />
          </> : null
        }
      </div>
    </header>
    );
  },
);
export default memo(NavBar);
