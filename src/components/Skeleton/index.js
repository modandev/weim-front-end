/* eslint-disable jsx-a11y/heading-has-content */
import classnames from 'classnames';

import styles from './index.less';

const AVATAR_SHAPE = ['circle', 'square'];
const AVATAR_SIZE = ['large', 'small', 'default'];

const Skeleton = ({ avatar, loading, paragraph, title, chart, children }) => {
  if (!loading) {
    return children;
  }

  let avatarClasses = {};

  if (avatar) {
    let { size, shape } = typeof avatar === 'object' ? avatar : {};

    if (!AVATAR_SIZE.includes(size)) {
      size = 'default';
    }

    if (!AVATAR_SHAPE.includes(shape)) {
      shape = 'circle';
    }

    avatarClasses = {
      [styles[size]]: true,
      [styles[shape]]: true
    };
  }

  return (
    <div className={classnames(styles.skeleton, 'weim-skeleton')}>
      {(avatar || title) ? (
        <div className={classnames(styles.header, 'weim-skeleton-header')}>
          {!!avatar && <span className={classnames(styles.avatar, styles.active, avatarClasses, 'weim-skeleton-avatar')} />}
          {!!title && (
            <div className={classnames(styles.title, 'weim-skeleton-title')}>
              {Array.from({ length: title.rows || 1 }, (_, index) => (
                <h3
                  key={index}
                  className={styles.active}
                  style={{ ...(title.width ? { width: title.width } : {}) }}
                />
              ))}
            </div>
          )}
        </div>
      ) : null}
      {paragraph && (avatar || title) ? (
        <div className={classnames(styles.blank, 'weim-skeleton-blank')} />
      ) : null}
      {paragraph ? (
        <div className={classnames(styles.content, 'weim-skeleton-content')}>
          {Array.from({ length: paragraph.rows || 3 }, (_, index) => (
            <p
              key={index}
              className={classnames(styles.paragraph, styles.active, 'weim-skeleton-paragraph')}
              style={{ ...(paragraph.width ? { width: paragraph.width } : {}) }}
            />
          ))}
        </div>
      ) : null}
      {chart ? (
        <div className={classnames(styles.chart, styles.active, 'weim-skeleton-chart')} />
      ) : null}
    </div>
  )
};

export default Skeleton;
