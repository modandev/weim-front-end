import React, { useCallback, useEffect, memo, useState } from 'react';
import {
  Axis,
  Chart,
  Annotation,
  LineAdvance,
  Tooltip,
  Legend,
  Area,
  View,
} from 'bizcharts';
import { semicolon } from '@/shared/helpers';
import { tickerMarketData } from '@/shared/graphql/gql/user';
import { useQuery } from '@apollo/react-hooks';
import styles from './styles.less';
import { formatMessage } from 'umi-plugin-react/locale';
import dayjs from 'dayjs';
import Spinner from '@atlaskit/spinner';
import { ORDERS } from '@/shared/graphql/gql/trade';

const { DataMarker, Region, DataRegion } = Annotation;

export default memo(({ ticker, analysis, pagetype, height, padding }) => {
  const [now, setNow] = useState({});
  const [tickerData, setTickerData] = useState([]);
  const { account_id, prediction_side, prediction_price, created_at, prediction_days } = analysis;


  const { data } = useQuery(tickerMarketData,
    {
      skip: !ticker,
      variables: {
        ticker: ticker,
        start: dayjs().subtract(2, 'month').format('YYYY-MM-DDT00:00:00+0000'),
        end: dayjs().format('YYYY-MM-DDT00:00:00+0000'),
      },
      onCompleted({ tickerMarketData }) {
        if (tickerMarketData.length) {
          setNow(tickerMarketData[tickerMarketData.length - 1]);
          setTickerData(getGuessData(tickerMarketData));
        }
      },
    });

  const getCreatedDate = (data) => {
    const createdDay = dayjs(created_at).format('dd');

    if (createdDay === 'Su') {
      return dayjs(created_at).subtract(2, 'day');
    }
    if (createdDay === 'Sa') {
      return dayjs(created_at).subtract(1, 'day');
    }
    return dayjs(created_at);
  };

  const getGuessData = (tickerMarketData) => {
    let endBackground = {};
    const createdDate = getCreatedDate(created_at).format('YYYY-MM-DD');
    const createdAtData = tickerMarketData.find(item => {
      return item.date === createdDate;
    }) || tickerMarketData[tickerMarketData.length - 1];

    const GuessList = new Array(parseInt(Math.min(Math.abs(prediction_days || 0), 5))).fill('').map((item, key) => {
      const lastDay = Math.floor(prediction_days / 5) * key;
      const date = dayjs(created_at).add(prediction_days - lastDay, 'day').format('YYYY-MM-DD');
      let close = parseFloat(prediction_price) - (parseFloat(prediction_price) - createdAtData.close) / prediction_days * parseInt(lastDay);
      close = key === 0 ? close : close * ((key % 2) * 0.2 + 0.9);
      if (key === 0) {
        endBackground = {
          date: date,
          close: close.toFixed(2),
          type: 'background',
        };
      }
      if (endBackground.close < close) {
        endBackground.close = close;
      }
      return {
        date,
        close: close.toFixed(2),
        type: 'guess',
      };
    });

    return [
      ...tickerMarketData,

      {
        ...createdAtData,
        isCreated: true,
        type: 'guess',
        y: 0,
      },
      endBackground,
      {
        ...createdAtData,
        isCreated: true,
        type: 'background',
      },
      ...GuessList];
  };

  return <div className={styles.chart_box}>
    {tickerData.length ?
      <Chart
        errorContent=""
        autoFit
        defaultInteractions={[]}
        padding={padding || [40, 60, 40, 60]}
        animate={false}
        height={height || 230}
        scale={{
          close: {
            type: 'linear',
            nice: true,
          },
          date: {
            type: 'time',
            nice: true,
            range: [0, 0.95],
            tickCount: 6,
          },
        }}
        placeholder={(
          <div className={styles.empty}>
            <h6>{formatMessage({ id: 'home.TargetPrice' })}</h6>
          </div>
        )}
        onGetG2Instance={g2Chart => {
          g2Chart.animate(false);

          // 缩放交互
          g2Chart.interaction('drag', {
            type: 'XY',
          }).interaction('zoom', {
            type: 'XY',
          });
        }}
        data={tickerData}>
        <Axis
          name="date"
          label={{
            formatter(text) {
              const [year, month, day] = text.split('-');

              return `${+year}-${+month}-${+day}`;
            },
          }}
        />
        <Axis
          name="close"
          label={{
            formatter(text) {
              return semicolon(text);
            },
          }}
        />
        <Legend
          name="type"
          visible={false}
          itemName={{
            formatter(text) {
              return text;
            },
          }}
        />
        <LineAdvance
          animate={false}
          area={{
            color:
              ['type', (typeVal) => {
                if (typeVal === 'guess') {
                  return 'white';
                }
                if (typeVal === 'background') {
                  return 'l (0) 0:#D4D4D4 1:#D4D4D4';
                }
                return '';
              }],
          }}
          position="date*close*type"
          shape={
            ['type', (typeVal) => {
              if (typeVal === 'guess') {
                return 'dash';
              }
              return 'line';
            }]
          }
          color={
            ['type', (typeVal) => {
              if (typeVal === 'guess') {
                return '#59c1ff';
              }
              if (typeVal === 'background') {
                return '#00000000';
              }
              return '';
            }]
          }
        />
        <DataMarker position={[dayjs(created_at).add(prediction_days, 'day').format('YYYY-MM-DD'), prediction_price]}
                    autoAdjust={false}
                    text={{
                      position: 'center',
                      content: (formatMessage({ id: 'home.Chart.Prediction' })) + '\n' +
                        semicolon(parseFloat(prediction_price).toFixed(2)),
                      style: {
                        textAlign: 'center',
                        fontSize: 12,
                        fontWeight: 'bold',
                        fill: '#2B96FF',
                        stroke: 'transparent',
                      },
                    }}
                    region={
                      { backgroundColor: 'green' }
                    }
                    line={{
                      length: 10,
                      style: {
                        fill: 'transparent',
                        stroke: 'transparent',
                      },
                    }}
                    point={{
                      style: {
                        stroke: '#F5D372',
                        fill: '#F5D372',
                      },
                    }}
                    direction={'upward'}
        />
        <DataMarker position={[now.date, now.close]}
                    autoAdjust={false}
                    text={{
                      position: 'center',
                      content: (formatMessage({ id: 'home.Chart.Now' })) + '\n' +
                        parseFloat(semicolon(now.close)).toFixed(2),
                      style: {
                        fontSize: 12,
                        fontWeight: 'bold',
                        fill: '#FF9700',
                        stroke: 'transparent',
                      },
                    }}
                    region={
                      { backgroundColor: 'green' }
                    }
                    line={{
                      length: 10,
                      style: {
                        fill: 'transparent',
                        stroke: 'transparent',
                      },
                    }}
                    point={{
                      style: {
                        fill: '#FF9700',
                        stroke: '#FF9700',
                      },
                    }}
                    direction={'upward'}
        />
        <DataMarker
          position={[(tickerData.find(item => item.isCreated) || {}).date,
            (tickerData.find(item => item.isCreated) || {}).close]}
          autoAdjust={false}
          point={{
            style: {
              stroke: 'transparent',
              fill: '#2B96FF',
            },
          }}
        />

        <Annotation.DataRegion
          region={{
            style: {
              fill: 'l (0) 0:#D4D4D440 1:#D4D4D440',
              opacity: 1,
            },
          }
          }
          start={[getCreatedDate(created_at).format('YYYY-MM-DD'), 0]}
          end={[dayjs(created_at).add(prediction_days, 'day').format('YYYY-MM-DD'), 100]}
          lineLength={0}
        />


        {/*<TradeMark*/}
        {/*  now={now}*/}
        {/*  pagetype={pagetype}*/}
        {/*  accountId={account_id}*/}
        {/*  ticker={ticker} />*/}
        <Tooltip
          title="date"
          showCrosshairs
          marker={{ background: 'transparent' }}
        >
        {(title, [item]) => (<Tol title={title}
                                  item={item}></Tol>
        )}
      </Tooltip>
	</Chart> : <Spinner />}
    </div>;
});

const Tol = memo(({ title, item }) => {
  return <div className={styles.tooltip}>
              <div className={styles.title}>{title.split('T')[0]}</div>
              <div className={styles.value}>{semicolon((+item.value).toFixed(2))}</div>
            </div>;
});

const TradeMark = ({ accountId, ticker, now, pagetype }) => {
  const { data: { orders = [] } = {} } = useQuery(ORDERS,
    {
      variables: {
        accountId,
        ticker,
      },
    });

  return (<>
      {pagetype === 'stream' ? null : orders.map((item, key) => {
        return <DataMarker
          key={key}
          position={[dayjs(item.time).format('YYYY-MM-DD'), item.avg_price]}
          autoAdjust={false}
          text={{
            position: 'center',
            content: (item.side === 'SELL' ? formatMessage({ id: 'home.Chart.Sell' }) : formatMessage({ id: 'home.Chart.Buy' })) + '\n' +
              parseFloat(semicolon(item.avg_price)).toFixed(2),
            style: {
              fontSize: 12,
              fontWeight: 'bold',
              textAlign: 'center',
              fill: '#2B96FF',
            },
          }}
          region={
            { backgroundColor: '#2B96FF' }
          }
          line={{
            length: 10,
            style: {
              fill: 'transparent',
              stroke: 'transparent',
            },
          }}
          point={{
            style: {
              stroke: '#2B96FF',
              fill: '#2B96FF',
            },
          }}
          direction="upward"
        />;
      })}
    </>
  );

};
