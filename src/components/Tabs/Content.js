import classnames from 'classnames';

import styles from './index.less';

export default ({ data, active }) => {
  return (
    <div className={styles.content}>
      {data.map(({ key, element }) => (
        <div
          key={key}
          className={
            classnames(
              styles.pane,
              { [styles.active]: active === key }
            )
          }
        >
          {element}
        </div>
      ))}
    </div>
  );
};
