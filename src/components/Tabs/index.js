import { useState, useCallback, Children } from 'react';
import TabPane from './TabPane';
import Nav from './Nav';
import Content from './Content';

import styles from './index.less';

const Tabs = ({ defaultActiveKey, onTabClick, children }) => {
  const nav = [];
  const content = [];

  const [activeKey, setActiveKey] = useState(defaultActiveKey === undefined ? '1' : defaultActiveKey);

  const onNavChange = useCallback(key => {
    typeof onTabClick === 'function' && onTabClick(key);
    setActiveKey(key);
  }, [onTabClick]);

  Children.forEach(children, ({ key, props }, index) => {
    key = key === undefined ? index + 1 : key;

    nav.push({ key, label: props.header });
    content.push({ key, element: props.children });
  });

  return (
    <div className={styles.tabs}>
      <Nav data={nav} onChange={onNavChange} active={activeKey}  />
      <Content data={content} active={activeKey} />
    </div>
  );
};

Tabs.TabPane = TabPane;

export default Tabs;
