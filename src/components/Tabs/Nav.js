import classnames from 'classnames';

import styles from './index.less';

export default ({ data, active, onChange }) => {
  return (
    <div className={styles.nav}>
      <ul>
        {data.map(({ key, label }) => (
          <li
            key={key}
            onClick={() => onChange(key)}
            className={classnames({ [styles.active]: key === active })}
          >
            {label}
          </li>
        ))}
      </ul>
    </div>
  )
};
