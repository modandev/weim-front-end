import React from 'react';
import TextArea from '@atlaskit/textarea';

export default () => {

  return <TextArea appearance={'subtle'}/>
}
