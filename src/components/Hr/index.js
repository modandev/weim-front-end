import React from 'react';

export default ({ color }) => <hr style={{ backgroundColor: color || '#EEEDED50', width: '100%', margin: 0, borderWidth: 0 ,height: '1px' }} />
