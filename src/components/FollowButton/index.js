import React, { useState, useCallback, useContext } from 'react';
import Button from '@/components/Buttons';
import { FormattedMessage, formatMessage } from 'umi-plugin-react/locale';
import { useDispatch } from 'dva';
import Icon from '@/components/Icon';
import EditorDoneIcon from '@atlaskit/icon/glyph/editor/done';
import PropTypes from 'prop-types';
import { connect } from 'dva';

export const useFollow = ({ account_id, onChange }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const changeAccountStatus = useCallback(async (is_followed) => {
    setLoading(true);
    await dispatch({ type: 'home/fetchFollow', is_followed, account_id: account_id });
    setLoading(false);
    onChange && onChange(is_followed);
  });
  return [loading, changeAccountStatus];
};


const FollowButton = ({ is_followed, userId, account_id, selfInfo, onChange, css, mode, ...rest }) => {
  const [loading, changeAccountStatus] = useFollow({ account_id, onChange });
  if (selfInfo.uid == userId) {
    return null;
  }
  return (!is_followed ?
      <Button
        isLoading={loading}
        onClick={changeAccountStatus.bind(null, false)}
        css={{ ...css, height: '30px' }}
        iconBefore={<Icon type="add-white" />}
        appearance="primary"
        {...rest}>
        <FormattedMessage id={'home.user.follow'} />
      </Button> :

      <Button onClick={changeAccountStatus.bind(null, true)}
              isLoading={loading}
              css={{ ...css, height: '30px' }}
              appearance="default"
              {...rest}
      >
        <div className="flex-center">
              <div style={{ marginLeft: '-5px' }}
                   className="flex-center">
              <EditorDoneIcon />
            </div>
          <FormattedMessage id={'home.user.cancelFollow'} />
        </div>
      </Button>
  );
};

FollowButton.defaultProps = {
  shape: 'round',
  css: {},
  mode: 'light',
};

FollowButton.propTypes = {
  is_followed: PropTypes.bool,
  ...Button.propTypes,
};

export default connect(({ user }) => ({
  selfInfo: user.currentUser,
}))(FollowButton);
