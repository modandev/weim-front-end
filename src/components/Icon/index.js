
/**
 * 关于目前 assets/icon 下的命名
 *
 * 如果颜色为主题色或者图标本身唯一，则直接命令，如 close、add
 * 如果为其他颜色，则添加颜色后缀，如 close-gray、add-black
 * 如果带背景，则添加 circle/square 后缀，如 close-circle、close-square-black
 */

import { memo, useState, useEffect } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import styles from './index.less';

const Icon = memo(({ as, type, onClick, className,style, ...rest }) => {
  const [src, setSrc] = useState(null);

  useEffect(() => {
    (async () => {
      const { default: src } = await import(`../../assets/icon/${type}.svg`);

      setSrc(src);
    })();
  }, [type]);

  if (!src) {
    return null;
  }

  return as === 'button'
    ? (
      <button onClick={onClick} className={classnames(styles.icon_button, "weim-icon-container")}>
        <img src={src} {...rest} style={style} className={classnames('weim-icon', className)} alt="" />
      </button>
    ) : (
      <img src={src} onClick={onClick} {...rest} style={style} className={classnames('weim-icon', className)} alt="" />
    );
});

Icon.propTypes = {
  as: PropTypes.oneOf(['button']),
  type: PropTypes.string.isRequired,
}

export default Icon;
