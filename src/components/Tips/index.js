/* eslint-disable react-hooks/exhaustive-deps */
import { useRef, useState, useEffect } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import styles from './index.less';

const Tips = ({ type, header, visible, description, floated, children }) => {
  const prevVisible = useRef(visible);
  const [visibility, setVisibility] = useState(false);
  const [shown, setShown] = useState(false);

  const show = () => {
    setVisibility(true);
    setTimeout(() => setShown(true), 100);
  };

  const hide = () => {
    setShown(false);
    setTimeout(() => setVisibility(false), 300);
  };

  useEffect(() => {
    visible && show();
  }, []);

  useEffect(() => {
    if (visible !== prevVisible.current) {
      prevVisible.current = visible;

      visible ? show() : hide();
    }
  }, [visible]);

  return (
    <div className={classnames(styles.wrapper, 'weim-tips-wrapper')}>
      {children}
      <div
        className={
          classnames(
            styles.tips,
            styles[type],
            {
              [styles.floated]: floated,
              [styles.visibility]: visibility,
              [styles.shown]: shown,
            },
            'weim-tips'
          )
        }
      >
        <div className={classnames(styles.header, 'weim-tips-header')}>
          {header}
        </div>
        {description && (
          <p className={classnames(styles.description, 'weim-tips-description')}>
            {description}
          </p>
        )}
      </div>
    </div>
  );
};

Tips.propTypes = {
  type: PropTypes.oneOf(['safe', 'danger']),
  visible: PropTypes.bool,
  header: PropTypes.any,
  description: PropTypes.string,
  floated: PropTypes.bool
};

export default Tips;
