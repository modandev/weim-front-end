import React, { useEffect, useState, useCallback } from 'react';
import styles from './styles.less';
import { connect } from 'dva';
import { formatMessage } from 'umi-plugin-react/locale';
import FollowButton from '@/components/FollowButton';
import Button from '@/components/Buttons';
import { router } from 'umi';
import qs from 'querystring';
import { semicolon } from '@/shared/helpers';
import { AccountCard, EllipsisText, InlineDialog } from '@/components';
import Big from 'big.js';

const css = {
  flex: 1,
  padding: '0',
  height: '50px',
  display: 'flex',
  justifyContent: 'center',
  alignItem: 'center',

};
export default connect(({ home }) => ({
  analysis: home.analysis,
}))(({ analysis }) => {
  const { userId } = qs.parse(window.location.search.replace(/^\?+/, ''));
  const [userInfo, setUserInfo] = useState(null);

  const goDetail = useCallback(() => {
    window.open(`/dashboard/${analysis.user_id}/accounts/${analysis.account.account_id}`);
  });

  useEffect(() => {
    if (analysis.account && userId === analysis.user_id) {
      setUserInfo({ ...analysis.account, ...analysis.accountTrading });
    }
  }, [analysis.account, analysis.user_id, userId]);

  if (!userInfo) {
    return <div></div>;
  }

  return <div className={styles.avatar_cart}>
    <InlineDialog
      isOpen={false}
      placement="auto-start"
      content={
        <AccountCard style={{ width: '300px' }}
                     account={userInfo} />}
      delay={500}>
        <div className={styles.avatar}
             onClick={goDetail}>
            {analysis.account.header_url ? <img
                className={styles.avatar_img}
                src={analysis.account.header_url}
                alt="" /> :
              <div className={styles.default_avatar}>
                <span>
                  {(analysis.account.nickname).slice(0, 1)}
                </span>
              </div>}
          </div>
    </InlineDialog>
    <div className={styles.username}>
       <InlineDialog
         isOpen={false}
         placement="auto-start"
         content={
           <AccountCard style={{ width: '300px' }}
                        account={userInfo} />}
         delay={500}>
          <Button
            css={{ maxWidth: '100%', overflow: 'hidden' }}
            onClick={goDetail}
            appearance="inline">
            <EllipsisText text={userInfo.nickname}>
              {userInfo.nickname}
            </EllipsisText>
          </Button>
       </InlineDialog>
    </div>

    <div className={styles.funs_no}>
      {formatMessage({ id: 'home.user.Follows' },
        { count: userInfo.no_of_followers, s: userInfo.no_of_followers > 1 ? 's' : '' })}
    </div>

    <div className={styles.follow_box}>
      <FollowButton
        userId={userInfo.user_id}
        is_followed={userInfo.is_followed}
        account_id={userInfo.account_id} />
    </div>

    <ul className={styles.info_box}>
      <Button
        onClick={goDetail}
        appearance="subtle"
        shape="square"
        css={css}>
        <li>
          <div className={styles.info_no}>{new Big(userInfo.ror || 0).times(100).toFixed(2)}%</div>
          <div className={styles.info_key}>{formatMessage({ id: 'home.user.Ror' })}</div>
        </li>
      </Button>

      <Button
        onClick={goDetail}
        appearance="subtle"
        shape="square"
        css={css}>
        <li>
          <div className={styles.info_no}>{userInfo.positions_no || 0}</div>
          <div className={styles.info_key}>
            {formatMessage({ id: 'home.user.PositionsNo' })}
          </div>
        </li>
      </Button>

      <Button
        isDisabled={!userInfo.analysis_no || userInfo.analysis_no === '0'}
        onClick={() => {
          window.open(`/home/stream/?${qs.stringify({
            postType: 'ANALYSIS',
            accountId: analysis.account_id,
          })}`);
        }}
        appearance="subtle"
        shape="square"
        css={css}>
        <li>
          <div className={styles.info_no}>
            {userInfo.analysis_no || 0}
          </div>
          <div className={styles.info_key}>
            {formatMessage({ id: 'home.user.Analysis' })}
          </div>
        </li>
      </Button>

      <Button
        isDisabled={!userInfo.tweets_no || userInfo.tweets_no === '0'}
        onClick={() => {
          window.open(`/home/stream/?${qs.stringify({
            postType: 'TWEET',
            accountId: analysis.account_id,
          })}`);
        }}
        appearance="subtle"
        shape="square"
        css={css}>
        <li>
          <div className={styles.info_no}>{userInfo.tweets_no || 0}</div>
          <div className={styles.info_key}>{formatMessage({ id: 'home.user.Posts' })}</div>
        </li>
      </Button>
    </ul>

  </div>;
});
