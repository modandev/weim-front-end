import styles from './styles.less';
import { formatMessage } from 'umi-plugin-react/locale';
import NoData from '@/assets/icon/ic_no_data.png';

export default () => {
  return <div className={styles.no_data}>
      <img src={NoData} className={styles.slid_no_data_img}/>
    <span> {formatMessage({id: 'home.NoData'})}</span>
  </div>
}
