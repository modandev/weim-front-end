import PropTypes from 'prop-types';
import classnames from 'classnames';
import { formatMessage } from 'umi-plugin-react/locale';


import styles from './index.less';


const UserParty = ({
  header,
  footer,
  children,
  description
}) => {
  
  return (
    <div className={classnames(styles.user_party, "weim-user-party")}>
      {header && (
        <div className={classnames(styles.user_party__header, 'weim-user-party-header')}>
          {header}
        </div>
      )}
      <div className={classnames(styles.user_party__body, 'weim-user-party-body')}>
        {description && (
          <h2 className={classnames(styles.user_party__description, "weim-user-party-description")}>
            {description}
          </h2>
        )}
        {children}
      </div>
      {footer && (
        <div className={classnames(styles.user_party__footer, "weim-user-party-footer")}>
          {footer}
        </div>
      )}
    </div>
  );
};

UserParty.defaultProps = {
  
};
UserParty.propTypes = {
  title: PropTypes.any,
  header: PropTypes.element,
  footer: PropTypes.element,
};

export default UserParty;
