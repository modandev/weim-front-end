import { cloneElement, Children } from 'react';
import classnames from 'classnames';
import Panel from './Panel';

import styles from './index.less';

const Collapse = ({ activeKey, children }) => {
  return (
    <div
      className={
        classnames(
          styles.collapse,
          'weim-collapse'
        )
      }
    >
      {Children.map(children, child => (
        cloneElement(child, {
          active: activeKey
            ? (Array.isArray(activeKey) ? activeKey.includes(child.key) : activeKey === child.key)
            : false
        })
      ))}
    </div>
  );
};

Collapse.Panel = Panel;

export default Collapse;
