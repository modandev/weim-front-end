/* eslint-disable react-hooks/exhaustive-deps */
import { createRef, useState, useEffect } from 'react';
import classnames from 'classnames';
import Icon from '../Icon';

import styles from './index.less';

export default ({ active, header, children }) => {
  const content = createRef();
  const [maxHeight, setMaxHeight] = useState(0);
  const [isUnfold, setIsUnfold] = useState(!!active);

  const onToggle = () => setIsUnfold(!isUnfold);

  useEffect(() => {
    setMaxHeight(content.current.children[0].offsetHeight + 20);
  }, []);

  return (
    <div
      className={
        classnames(
          styles.collapse_item,
          { [styles.fold]: !isUnfold },
          'weim-collapse-item'
        )
      }
    >
      <div
        onClick={onToggle}
        className={
          classnames(
            styles.collapse_header,
            'clearfix',
            'weim-collapse-header'
          )
        }
      >
        <span
          className={
            classnames(
              styles.label,
              styles.aggravate,
              'weim-collapse-header-label'
            )
          }
        >
          {header}
        </span>
        <Icon type="right-circle" />
      </div>
      <div
        ref={content}
        className={
          classnames(
            styles.collapse_content,
            'weim-collapse-content',
            { 'weim-collapse-active': isUnfold }
          )
        }
        style={{ ...(isUnfold ? { maxHeight } : {}) }}
      >
        {children}
      </div>
    </div>
  )
};
