import React from 'react';
import Tooltip from '../Tooltip';
import styles from './styles.less';
import { formatMessage, getLocale } from 'umi-plugin-react/locale';

export const copy = (copyStr) => {
  // 创建input标签存放需要复制的文字
  const oInput = document.createElement('input');
  // 把文字放进input中，供复制
  oInput.value = copyStr;
  document.body.appendChild(oInput);
  // 选中创建的input
  oInput.select();
  // 执行复制方法， 该方法f返回bool类型的结果，告诉我们是否复制成功
  const copyResult = document.execCommand('copy');
  // 操作中完成后 从Dom中删除创建的input
  document.body.removeChild(oInput);
  // 根据返回的复制结果 给用户不同的提示
  if (copyResult) {
    // message.success('DDL已复制到粘贴板')
  } else {
    // message.error('复制失败')
  }
};

export default (props) => <Tooltip delay={2000}
                                   hideTooltipOnClick={true}
                                   content={formatMessage({ id: 'home.copy' })}
                                   position="top">
    <span className={styles.copyText}
          style={props.style}
          onClick={() => copy(props.text)}>
    {props.children || props.text}
  </span>
</Tooltip>
