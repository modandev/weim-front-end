import { createRef } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { formatMessage } from 'umi-plugin-react/locale';
import { getUrlProperty } from 'shared/locationParser';
import thirdPartyConfig from 'configs/thirdParty';
import Icon from '../Icon';
import Button from '../Button';
import EmailAccount from '../EmailAccount';

import styles from './index.less';

const thirdPartyRef = createRef();
const { urls: oauthUrls } = thirdPartyConfig.oauth;

const ThirdParty = ({
  header,
  footer,
  useFor,
  signUp,
  signIn,
  remarks,
  buttons,
  onSelect,
  inactive,
  closeable,
  description,
  onRequestClose,
  buttonTxtAffix,
  buttonTxtPrefix,
}) => {
  const onClose = () => {
    if (typeof onRequestClose === 'function') {
      onRequestClose();
    } else {
      const { current } = thirdPartyRef;

      current && current.classList.add('hidden');
    }
  };
  const onChoose = thirdParty => {
    if (inactive) {
      return;
    }

    onSelect(thirdParty);

    switch (useFor) {
      case 'oauth':
        window.open(`${getUrlProperty('origin')}/user/oauth/${thirdParty}`, '_blank');
        break;

      case 'connect':
        // 关联
        break;

      case 'share':
        break;

      default:
        break;
    }
  };

  return (
    <div ref={thirdPartyRef} className={classnames(styles.third_party, "weim-third-party")}>
      {closeable && <Icon as="button" type="close-gray" onClick={onClose} />}
      {header && (
        <div className={classnames(styles.third_party__header, 'weim-third-party-header')}>
          {header}
        </div>
      )}
      <div className={classnames(styles.third_party__body, 'weim-third-party-body')}>
        {description && (
          <h2 className={classnames(styles.third_party__description, "weim-third-party-description")}>
            {description}
          </h2>
        )}
        <EmailAccount useFor={signIn ? 'signIn' : 'signUp'} />
        {buttons ? buttons : Object.keys(oauthUrls).map(key => (
          <Button
            block
            key={key}
            shape="square"
            disabled={inactive}
            icon={`${key}-white`}
            className={styles[key]}
            onClick={() => onChoose(key)}
          >
            {!!buttonTxtPrefix && `${buttonTxtPrefix} `}
            {formatMessage({ id: `component.thirdParty.${key}` })}
            {!!buttonTxtAffix && ` ${buttonTxtAffix}`}
          </Button>
        ))}
        {remarks && <p className={styles.remarks}>{remarks}</p>}
      </div>
      {footer && (
        <div className={classnames(styles.third_party__footer, "weim-third-party-footer")}>
          {footer}
        </div>
      )}
    </div>
  );
};

ThirdParty.defaultProps = {
  buttonTxtAffix: '',
  buttonTxtPrefix: '',
};
ThirdParty.propTypes = {
  title: PropTypes.any,
  header: PropTypes.element,
  footer: PropTypes.element,
  closeable: PropTypes.bool,
  buttonTxtAffix: PropTypes.string,
  buttonTxtPrefix: PropTypes.string,
};

export default ThirdParty;
