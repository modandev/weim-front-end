import PropTypes from 'prop-types';
import classnames from 'classnames';
import { formatMessage } from 'umi-plugin-react/locale';
import Icon from '../Icon';
import Button from '../Button';

import styles from './index.less';
import { useHistory } from 'react-router';

const EmailAccount = ({
  useFor,
  buttonTxtPrefix,
  buttonTxtAffix
}) => {
  const history = useHistory();

  const onClick = () => {
    history.push(`/${useFor.toLowerCase()}`)
  }
  

  return (  
    <div className={classnames(styles.email_account)}>
      <Button
        block
        shape="square"
        className={styles[useFor]}
        onClick={onClick}
      >
        {!!buttonTxtPrefix && `${buttonTxtPrefix} `}
        {formatMessage({ id: `component.emailAccount.${useFor || 'signIn'}` })}
        {!!buttonTxtAffix && ` ${buttonTxtAffix}`}
      </Button>
    </div>
  );
};

EmailAccount.defaultProps = {
  buttonTxtAffix: '',
  buttonTxtPrefix: '',
};
EmailAccount.propTypes = {
  buttonTxtAffix: PropTypes.string,
  buttonTxtPrefix: PropTypes.string,
};

export default EmailAccount;
