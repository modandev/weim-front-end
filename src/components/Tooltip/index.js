import React, { useState, useCallback, useEffect, useRef } from 'react';
import ReactDom from 'react-dom';
import styles from './styles.less';

const Tooltip = ({ children, content, disable = false }) => {
  const outer = useRef(null);

  const [isVisible, setIsVisible] = useState(false);

  const onMouseEnter = useCallback(() => {
    setIsVisible(true);
  });
  const onMouseLeave = useCallback(() => {
    setIsVisible(false);
  });

  return <div className={styles.tooltip}
              ref={outer}
              onMouseEnter={onMouseEnter}
              onClick={onMouseLeave}
              onMouseLeave={onMouseLeave}>
    {isVisible && !disable && ReactDom.createPortal(
      <Popup content={content}
             outer={outer} />
      , document.body)}
    {children}
  </div>;
};

const Popup = ({ content, outer }) => {
  const [position, setPosition] = useState({});

  useEffect(() => {
    if (outer) {
      const { top, left, height } = outer.current.getBoundingClientRect();
      setPosition({
        left: left + 'px',
        top: top + height + 5 + 'px',
      });
    }
  }, [outer]);


  return <div className={styles.popup_outer}
              style={position}>
    {content}
  </div>;
};

export default Tooltip;
