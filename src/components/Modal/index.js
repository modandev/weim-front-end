/* 直接作为组件使用将根据最近非 static 定位的父元素进行定位，如想令 modal 显示在 body 下，使用 modal.[apiName] */
/* eslint-disable react-hooks/exhaustive-deps */
import { memo, useRef, useEffect, useState } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { formatMessage } from 'umi-plugin-react/locale';
// import BodyBabyFactory from 'shared/BodyBabyFactory';

import styles from './index.less';

const Modal = ({ visible, fixed, mask, title, onOk, onCancel, okText, cancelText, description }) => {
  const prevVisible = useRef(visible);
  const [visibility, setVisibility] = useState(false);
  const [shown, setShown] = useState(false);

  const show = () => {
    setVisibility(true);
    setTimeout(() => setShown(true), 100);
  };

  const hide = () => {
    setShown(false);
    setTimeout(() => setVisibility(false), 300);
  };

  useEffect(() => {
    visible && show();
  }, []);

  useEffect(() => {
    if (visible !== prevVisible.current) {
      prevVisible.current = visible;

      visible ? show() : hide();
    }
  }, [visible]);

  return (
    <div
      className={
        classnames(
          styles.modal,
          {
            [styles.shown]: shown,
            [styles.visibility]: visibility,
            [styles.fixed]: fixed,
            [styles.mask]: mask
          },
          'weim-modal'
        )
      }
    >
      <div className={classnames(styles.content, 'weim-modal-content')}>
        {title && (<h3>{title}</h3>)}
        <div className={classnames(styles.description, 'weim-modal-description')}>
          {description && description.includes('\n') ? description.split('\n').map((item, idx) => <p key={idx}>{item}</p>) : description}
        </div>
        <div className={classnames(styles.footer, 'weim-modal-footer')}>
          <button className={styles.negative} onClick={onCancel}>
            {cancelText || formatMessage({ id: 'common.cancel' })}
          </button>
          <button className={styles.positive} onClick={onOk}>
            {okText || formatMessage({ id: 'common.confirm' })}
          </button>
        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  visible: PropTypes.bool,
  onOk: PropTypes.func,
  onCancel: PropTypes.func,
  okText: PropTypes.string,
  cancelText: PropTypes.string,
};

export default memo(Modal);
