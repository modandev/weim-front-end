import styles from './styles.less';

const countryCodeColor = new Map([
  ['US','rgb(0, 43, 127)'],
  ['HK', '#6B20CE'],
  ['SH', '#DF2A54'],
]);

export default ({ countryCode }) => {
  return <div className={styles.label}
              style={{ backgroundColor: countryCodeColor.get(countryCode) || '#f17139'}}>
    {countryCode}
  </div>;
}
