import React from 'react';
import ReactDOM from 'react-dom';
import styles from './styles.less';
import PropTypes from 'prop-types';

export default class InlineDialog extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: this.props.isOpen,
    };
  }

  onMouseOver = () => {
    this.timeout && clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.setState({
        isOpen: true,
      });
    }, this.props.delay);
  };

  onMouseLeave = () => {
    this.timeout && clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.setState({
        isOpen: false,
      });
    }, 500);
  };

  componentWillUnmount() {
    this.timeout && clearTimeout(this.timeout);
  }

  render() {
    return (
      <div ref={(ref) => this.outer = ref}
           onMouseOver={this.onMouseOver}
           onMouseLeave={this.onMouseLeave}
           className={styles.inline_dialog}>
        {this.state.isOpen ?
          ReactDOM.createPortal(
            <InlineDialogCenter
              content={this.props.content}
              outer={this.outer} />, document.body)

          : null}
        {this.props.children}
    </div>);
  }
}

InlineDialog.defaultProps = {
  placement: 'auto',
  delay: 300,
};

InlineDialog.propTypes = {
  delay: PropTypes.number,
  placement: PropTypes.oneOf([
    'auto-start',
    'auto',
    'auto-end',
    'top-start',
    'top',
    'top-end',
    'right-start',
    'right',
    'right-end',
    'bottom-end',
    'bottom',
    'bottom-start',
    'left-end',
    'left',
    'left-start',
  ]),
};


class InlineDialogCenter extends React.PureComponent {
  constructor(props) {
    super(props);
    this.outer = this.props.outer;
    this.state = {};
    this.position = {};
  }

  getPosition = () => {
    const outerRect = this.outer.getBoundingClientRect();
    const contentRect = this.content.getBoundingClientRect();
    this.position = {};
    if ('auto'.indexOf(this.props.placement)) {
      this.setAutoPosition(outerRect, contentRect);
    }
  };

  setAutoPosition = (outerRect, contentRect) => {
    if (outerRect.left + outerRect.width / 2 > window.innerWidth / 2) {
      this.setLeft(outerRect, contentRect);
    } else {
      this.setRight(outerRect, contentRect);
    }

    if (outerRect.top < window.innerHeight / 3) {
      this.verticalStart(outerRect, contentRect);
    } else if (outerRect.bottom > window.innerHeight * 2 / 3) {
      this.verticalEnd(outerRect, contentRect);
    } else {
      this.verticalCenter(outerRect, contentRect);
    }

    this.setState({
      position: this.position,
    });
  };

  setLeft = (outerRect, contentRect) => {
    this.position.left = outerRect.left;
    this.position.transform = [`translateX(-100%)`];
  };

  setRight = (outerRect, contentRect) => {
    this.position.left = outerRect.right;
  };

  verticalStart(outerRect) {
    this.position.top = outerRect.top;
  }

  verticalCenter(outerRect, contentRect) {
    this.position.top = outerRect.top + outerRect.height / 2;
    this.position.transform = [`translateY(-50%) ${this.position.transform || ''}`];
  }

  verticalEnd(outerRect, contentRect) {
    this.position.top = outerRect.bottom - contentRect.height;
    this.position.transform = [`translateY(-100%) ${this.position.transform || ''}`];
  }

  componentDidMount() {
    if (!this.outer) {
      return;
    }
    this.getPosition();
    document.addEventListener('scroll', this.getPosition);
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.getPosition);
  }

  render() {

    return (
      <div
        ref={(ref) => this.content = ref}
        className={styles.inline_dialog_content_outer}
        style={this.state.position}>
        <div className={styles.inline_dialog_content}>
            {this.props.content}
        </div>
      </div>
    );
  }
}
