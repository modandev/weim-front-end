import React, { useEffect, useState, useCallback } from 'react';
import styles from './styles.less';
import { formatMessage, FormattedMessage } from 'umi-plugin-react/locale';
import Button from '@/components/Buttons';
import { router } from 'umi';
import qs from 'querystring';
import { EllipsisText } from '@/components';

const css = {
  flex: 1,
  padding: '0',
  height: '50px',
  display: 'flex',
  justifyContent: 'center',
  alignItem: 'center',
};


export default ({ user, style = {}, followingNo, accounts }) => {
  const [userInfo, setUserInfo] = useState(null);
  const goDetail = useCallback(() => {
    window.open(`/dashboard/${user.uid}/accounts/`);
  });
  const goDashboardDetail = useCallback((accountId) => {
    window.open(`/dashboard/${user.uid}/accounts/${accountId}`);
  });

  useEffect(() => {
    setUserInfo(user);
  }, [user, user.uid]);

  if (!userInfo) {
    return <div></div>;
  }

  return <div style={style}
              className={styles.avatar_cart}>
    <div className={styles.content}>
      <div className={styles.avatar}>
        {user.avatar ? <img
            className={styles.avatar_img}
            src={user.avatar}
            alt="" /> :
          <div className={styles.default_avatar}>
            <span>
              {(user.nickname).slice(0, 1)}
            </span>
          </div>}
      </div>

      <div className={styles.direction}>
          <div
            style={{ fontSize: '20px', width: 'fit-content', fontWeight: 'bold' }}
          >
            <EllipsisText text={userInfo.nickname}>
              {userInfo.nickname}
            </EllipsisText>
          </div>
          <div className={styles.user_fun}>{formatMessage({ id: 'home.user.Following' }, {
            count: followingNo,
            s: followingNo > 1 ? 's' : '',
          })}
          </div>
      </div>
    </div>

    {accounts.length ? <ul className={styles.foot}>
      {accounts.map((item, index) => <li key={item.account_id}

                                         className={styles.account_item}>
        {formatMessage({ id: 'home.Account' })}{index + 1}:&nbsp;
        <span onClick={goDashboardDetail.bind(null, item.account_id)}>
          <EllipsisText text={item.nickname}>
            {item.nickname}
          </EllipsisText>
        </span>
      </li>)}
    </ul> : null}
  </div>;
};



