import React, { useCallback, useState, useRef, useEffect } from 'react';
import Button from '@atlaskit/button';
import Icon from '../Icon';
import PropTypes from 'prop-types';
import Popup from '../Popup';
import styles from './styles.less';

const Select = (props) => {
  const [isOpen, setIsOpen] = useState(props.isOpen);
  const [option, setOption] = useState(props.defaultOption);

  const onChange = useCallback((item, event) => {
    setOption(item);
    props.onChange && props.onChange(item.value, item.data);
    setIsOpen(false);
  });

  const changeIsOpen = useCallback((event) => {
    event.nativeEvent.stopImmediatePropagation();
    setIsOpen(!isOpen);
  });

  const close = useCallback((event) => {
    setIsOpen(false);
  });

  const content = () => {
    return <ul className={styles.select_option}>
      {props.options.map((item, index) => <li key={index}>
        <Button
          isSelected={item.value === (option ? option.value : '')}
          css={{
            width: '100%',
            height: '100%',
            borderRadius: 0,
            boxSizing: 'border-box',
            paddingRight: '10px',
            paddingLeft: '10px',
            textAlign: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onClick={onChange.bind(this, item)}
          appearance={'subtle'}>
          {item.label}
        </Button>
      </li>)
      }
    </ul>;
  };

  return <Popup isOpen={isOpen}
                content={content}
                onClose={close}>

      <Button appearance={props.appearance || 'subtle'}
              css={props.style}
              onClick={changeIsOpen}
              iconAfter={<Icon className={isOpen ? styles.icon : ''}
                               type={'down-circle'} />}>
        {option ? option.label : props.placeholder}
      </Button>
  </Popup>;
};

Select.defaultProps = {
  isOpen: false,
  options: [],
  defaultValue: {},
};

Select.propsTypes = {
  appearance: PropTypes.oneOf([
    'default',
    'danger',
    'link',
    'primary',
    'subtle',
    'subtle-link',
    'warning',
  ]),
  value: PropTypes.oneOfType([
    PropTypes.object, PropTypes.number, PropTypes.string,
  ]),
  defaultOption: {
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.node]),
  },
  onChange: PropTypes.func,
  onInputChange: PropTypes.func,
  menuIsOpen: PropTypes.bool,
  onMenuOpen: PropTypes.func,
  onMenuClose: PropTypes.func,
  options: [
    {
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.node]),
    },
  ],
  placeholder: PropTypes.string,
};


export default Select;
