import React, { useState, useEffect, useRef } from 'react';
import styles from './styles.less';
import PropTypes from 'prop-types';

const Popup = function({ children, content, isOpen, onClose }) {
  const outer = useRef(null);
  return <div ref={outer}
              className={styles.popup}>
    {children}
    {isOpen ? <Content content={content}
                       onClose={onClose} /> : null}
  </div>;
};

const Content = ({ onClose, content }) => {
  useEffect(() => {
    document.addEventListener('click', onClose, false,
    );
    return () => document.removeEventListener('click', onClose);
  }, [onClose]);
  return <>
    <div className={styles.arrow_top}></div>
  <div className={styles.popup_content}>
    {content()}
  </div></>;
};

Popup.defaultProps = {
  content: () => <div className={styles.default_content}> </div>,
};

Popup.propTypes = {
  content: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};


export default Popup;
