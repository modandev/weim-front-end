import { createRef, memo } from 'react';
import { connect } from 'dva';
import classnames from 'classnames';
import Icon from '../Icon';
import { LETTER_REGEXP } from 'shared/regexps';

import styles from './index.less';

export default memo(connect()(function Avatar({
                                                theme = 'colored',
                                                src,
                                                className,
                                                small,
                                                medium,
                                                large, middle,
                                                backup,
                                                dispatch,
                                                uploadable,
                                                onUploadSuccess,
                                                onUploadFail,
                                              }) {
  const classes = classnames({
    [styles.avatar_small]: small,
    [styles.avatar_medium]: medium,
    [styles.avatar_large]: large,
    [styles[theme]]: !src,
  }, className);
  const avatar = createRef();
  const onAvatarChange = async ({ target: { validity, files: [file] } }) => {
    if (validity.valid) {
      file = new File([file], new Date().getTime() + `.${file.name.split('.')[1]}`, { type: file.type });

      const { success, data } = await dispatch({ type: 'global/createFile', payload: { file } });

      if (avatar.current) {
        avatar.current.value = null;
      }

      if (success) {
        typeof onUploadSuccess === 'function' && onUploadSuccess(data);
      } else {
        // TODO: 提示用户
        typeof onUploadFail === 'function' && onUploadFail();
      }
    }
  };

  return (
    <div className={classnames(styles.avatar, { [styles.upload]: uploadable })}>
      {!!uploadable && (
        <>
          <div className={classnames(classes, styles.layer)}>
            <Icon type="camera" />
          </div>
          <input
            title=""
            type="file"
            ref={avatar}
            onChange={onAvatarChange}
            // accept="image/jpeg,image/jpg,image/png"
          />
        </>
      )}
      <div className={classnames(classes, styles.avatar_image)}>
        {src ? (
          <img
            src={src}
            alt="avatar"
          />
        ) : (
          <span>
            {(() => {
              if (medium) {
                return <Icon type="default_avatar" />;
              }
              if (!backup) {
                return 'W';
              } else {
                const pinyin = require('pinyin');

                if (LETTER_REGEXP.test(backup)) {
                  return backup.slice(0, 1).toUpperCase();
                } else {
                  const result = pinyin(backup, { style: pinyin.STYLE_FIRST_LETTER });

                  return result ? result[0][0].toUpperCase() : backup.slice(0, 1).toUpperCase();
                }
              }
            })()}
          </span>
        )}
      </div>
    </div>
  );
}));
