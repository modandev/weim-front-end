import React, { useEffect, useState, useCallback } from 'react';
import styles from './styles.less';
import { formatMessage, FormattedMessage } from 'umi-plugin-react/locale';
import { useFollow } from '@/components/FollowButton';
import { EllipsisText, Icon } from '@/components';
import Button from '@/components/Buttons';
import { router } from 'umi';
import qs from 'querystring';
import { connect } from 'dva';
import Spinner from '@atlaskit/spinner';
import Big from 'big.js'
const css = {
  flex: 1,
  padding: '0',
  height: '50px',
  display: 'flex',
  justifyContent: 'center',
  alignItem: 'center',
};

export default ({ account, style = {} }) => {
  const [userInfo, setUserInfo] = useState(null);
  const [isFollowed, setIsFollowed] = useState(account.is_followed);
  const goDetail = useCallback(() => {
    window.open(`/dashboard/${account.user_id}/accounts/${account.account_id}`);
  });
  const goFans = useCallback(() => {
    router.push(`/followers/${account.user_id}/accounts/${account.account_id}`);
  });
  useEffect(() => {
    setUserInfo(account);
  }, [account]);

  if (!userInfo) {
    return <div></div>;
  }

  const onFollowStateChange = () => {
    setIsFollowed(!isFollowed);
  };

  return (
    <div style={style}
         className={styles.avatar_cart}>
    <div className={styles.top}>
      <div className={styles.header}>
        <div className={styles.avatar}
             onClick={goDetail}>
        {account.header_url ? <img
            className={styles.avatar_img}
            src={account.header_url}
            alt="" /> :
          <div className={styles.default_avatar}>
            <span>
              {(account.nickname).slice(0, 1)}
            </span>
          </div>}
        </div>
        <div className={styles.username}>
          <Button
            mode={'dark'}
            css={{ maxWidth: '100%', overflow: 'hidden' }}
            onClick={goDetail}
            appearance="inline">
             <EllipsisText text={userInfo.nickname}>
              {userInfo.nickname}
             </EllipsisText>
          </Button>

          <div
            onClick={goFans}
            className={styles.user_fun}>
            {formatMessage({ id: 'home.user.Follows' },
              {
                count: account.no_of_followers || 0,
                s: account.no_of_followers > 1 ? 's' : '',
              })}
          </div>
        </div>
        <FollowButton shape="square"
                      userId={account.user_id}
                      is_followed={isFollowed}
                      onChange={onFollowStateChange}
                      account_id={account.account_id} />
      </div>

      <ul className={styles.info_box}>
        <Button
          onClick={goDetail}
          appearance="subtle"
          shape="square"
          css={css}>
          <li>
            <div className={styles.info_key}>{formatMessage({ id: 'home.user.Ror' })}</div>
            <div className={styles.info_no}>{new Big(userInfo.ror || 0).times(100).toFixed(2)}%</div>
          </li>
        </Button>

        <div className={styles.space} />

        <Button
          onClick={goDetail}
          appearance="subtle"
          shape="square"
          css={css}>
          <li>
            <div className={styles.info_key}>
              {formatMessage({ id: 'home.user.PositionsNo' })}
            </div>
            <div className={styles.info_no}>{userInfo.positions_no || 0}</div>
          </li>
        </Button>

        <div className={styles.space} />

        <Button
          isDisabled={!userInfo.analysis_no || userInfo.analysis_no === '0'}
          onClick={() => {
            window.open(`/home/stream/?${qs.stringify({
              postType: 'ANALYSIS',
              accountId: account.account_id,
            })}`);
          }}
          appearance="subtle"
          shape="square"
          css={css}>
          <li>
            <div className={styles.info_key}>
              {formatMessage({ id: 'home.user.Analysis' })}
            </div>
            <div className={styles.info_no}>
              {userInfo.analysis_no || 0}
            </div>
          </li>
        </Button>

        <div className={styles.space} />

        <Button
          isDisabled={!userInfo.tweets_no || userInfo.tweets_no === '0'}
          onClick={() => {
            window.open(`/home/stream/?${qs.stringify({
              postType: 'TWEET',
              accountId: account.account_id,
            })}`);
          }}
          appearance="subtle"
          shape="square"
          css={css}>
          <li>
            <div className={styles.info_key}>{formatMessage({ id: 'home.user.Posts' })}</div>
            <div className={styles.info_no}>{userInfo.tweets_no || 0}</div>
          </li>
        </Button>

      </ul>
    </div>

      {userInfo.description ? <p className={styles.direction}>
      <span>{userInfo.description}</span>
    </p> : null}

  </div>);
};

const FollowButton = connect(({ user }) => ({
  selfInfo: user.currentUser,
}))(({ is_followed, account_id, selfInfo, onChange, userId }) => {
  const [loading, changeAccountStatus] = useFollow({ is_followed, account_id, onChange });

  if (selfInfo.uid == userId) {
    return null;
  }
  return <div onClick={changeAccountStatus}
              size="small"
              className={styles.follow_button}>
    {loading ? <Spinner /> :
      <span>
        {!is_followed ? <>
          <Icon type="add"></Icon>&nbsp;
        </> : null}
        <FormattedMessage id={is_followed ? 'home.user.cancelFollow' : 'home.user.follow'} />
    </span>
    }
  </div>;
});


