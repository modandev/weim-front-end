import React from 'react';
import Button from '@atlaskit/button';
import PropTypes from 'prop-types';
import { colors } from '@atlaskit/theme';
import { getButtonStyles, getSpinnerStyles } from './getStyles';

const Buttons = ({ children, css, ...rest }) => {
  const props = {
    theme: (current, props) => {
      const { buttonStyles, spinnerStyles } = current(props);
      buttonStyles.background = buttonStyles.background === '#0052CC' ? colors.G500 : buttonStyles.background;

      return {
        buttonStyles: {...getButtonStyles(props), justifyContent: 'center', ...css, },
        spinnerStyles,
      };
    },
  };

  return <Button {...rest} {...props}>{children}</Button>;
};

Buttons.defaultProps = {
  shape: 'round',
  autoFocus: false,
  css: {},
  mode: 'light',
};

Buttons.propTypes = {
  size: PropTypes.oneOf(['small', 'large']),
  shape: PropTypes.oneOf(['square', 'round']),
  appearance: PropTypes.oneOf([
    'default',
    'danger',
    'link',
    'primary',
    'subtle',
    'subtle-link',
    'warning',
    'ghost',
    'inline'
  ]),
  mode: PropTypes.oneOf(['dark', 'light']),

  buttonStyles: PropTypes.object,
  onClick: PropTypes.func,
  css: PropTypes.object,
  autoFocus: PropTypes.bool,
  className: PropTypes.string,
  href: PropTypes.string,
  iconAfter: PropTypes.node,
  iconBefore: PropTypes.node,
};

export default Buttons;
