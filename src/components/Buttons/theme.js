import { colors, createTheme } from '@atlaskit/theme';
// import { createTheme } from '@atlaskit/theme/components';
import { getButtonStyles, getSpinnerStyles } from './getStyles';

export function hex2rgba(hex, alpha = 1) {
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    let colorArr = hex.substring(1).split('');
    if (colorArr.length === 3) {
      colorArr = [
        colorArr[0],
        colorArr[0],
        colorArr[1],
        colorArr[1],
        colorArr[2],
        colorArr[2],
      ];
    }
    const color = `0x${colorArr.join('')}`;
    const r = (color >> 16) & 255;
    const g = (color >> 8) & 255;
    const b = color & 255;
    return `rgba(${[r, g, b].join(',')}, ${alpha})`;
  }
  throw new Error('Bad Hex');
}

export const fallbacks = {
  background: { light: colors.N20A, dark: colors.DN70 },
  color: { light: colors.N400, dark: colors.DN400 },
  textDecoration: { light: 'none', dark: 'none' },
};

export const baseTheme = {
  // Default appearance
  background: {
    default: {
      default: { light: colors.N20A, dark: colors.DN70 },
      hover: { light: colors.N30A, dark: colors.DN60 },
      active: { light: hex2rgba(colors.G75, 0.6), dark: colors.G75 },
      disabled: { light: colors.N20A, dark: colors.DN70 },
      selected: { light: colors.N700, dark: colors.DN0 },
      focusSelected: { light: colors.N700, dark: colors.DN0 },
    },
    primary: {
      default: { light: colors.G400, dark: colors.G100 },
      hover: { light: colors.G300, dark: colors.G75 },
      active: { light: colors.G500, dark: colors.G200 },
      disabled: { light: colors.N20A, dark: colors.DN70 },
      selected: { light: colors.N700, dark: colors.DN0 },
      focusSelected: { light: colors.N700, dark: colors.DN0 },
    },
    warning: {
      default: { light: colors.Y300, dark: colors.Y300 },
      hover: { light: colors.Y200, dark: colors.Y200 },
      active: { light: colors.Y400, dark: colors.Y400 },
      disabled: { light: colors.N20A, dark: colors.DN70 },
      selected: { light: colors.Y400, dark: colors.Y400 },
      focusSelected: { light: colors.Y400, dark: colors.Y400 },
    },
    danger: {
      default: { light: colors.R400, dark: colors.R400 },
      hover: { light: colors.R300, dark: colors.R300 },
      active: { light: colors.R500, dark: colors.R500 },
      disabled: { light: colors.N20A, dark: colors.DN70 },
      selected: { light: colors.R500, dark: colors.R500 },
      focusSelected: { light: colors.R500, dark: colors.R500 },
    },
    link: {
      default: { light: 'none', dark: 'none' },
      selected: { light: colors.N700, dark: colors.N20 },
      focusSelected: { light: colors.N700, dark: colors.N20 },
    },
    inline: {
      default: { light: 'none', dark: 'none' },
      selected: { light: 'none', dark: 'none' },
      focusSelected: { light: colors.N700, dark: colors.N20 },
    },
    subtle: {
      default: { light: 'none', dark: 'none' },
      hover: { light: colors.N30A, dark: colors.DN60 },
      active: { light: hex2rgba(colors.G75, 0.6), dark: colors.G75 },
      disabled: { light: 'none', dark: 'none' },
      selected: { light: colors.N700, dark: colors.DN0 },
      focusSelected: { light: colors.N700, dark: colors.DN0 },
    },
    'subtle-link': {
      default: { light: 'none', dark: 'none' },
      selected: { light: colors.N700, dark: colors.N20 },
      focusSelected: { light: colors.N700, dark: colors.N20 },
    },
    ghost: {
      default: { light: 'none', dark: 'none' },
      selected: { light: 'none', dark: 'none' },
      focusSelected: { light: 'none', dark: 'none' },
    },
  },
  boxShadowColor: {
    default: {
      focus: { light: colors.G100, dark: colors.G75 },
      focusSelected: {
        light: colors.G100,
        dark: colors.G75,
      },
    },
    primary: {
      focus: { light: colors.G100, dark: colors.G75 },
      focusSelected: {
        light: colors.G100,
        dark: colors.G75,
      },
    },
    warning: {
      focus: { light: colors.Y500, dark: colors.Y500 },
      focusSelected: { light: colors.Y500, dark: colors.Y500 },
    },
    danger: {
      focus: { light: colors.R100, dark: colors.R100 },
      focusSelected: { light: colors.R100, dark: colors.R100 },
    },
    link: {
      focus: { light: colors.G100, dark: colors.G75 },
      focusSelected: {
        light: colors.G100,
        dark: colors.G75,
      },
    },
    inline: {
      focus: { light: colors.G100, dark: colors.G75 },
      focusSelected: {
        light: colors.G100,
        dark: colors.G75,
      },
    },
    subtle: {
      focus: { light: colors.G100, dark: colors.G75 },
      focusSelected: {
        light: colors.G100,
        dark: colors.G75,
      },
    },
    'subtle-link': {
      focus: { light: colors.G100, dark: colors.G75 },
      focusSelected: {
        light: colors.G100,
        dark: colors.G75,
      },
    },
    ghost: {
      focus: { light: colors.G100, dark: colors.G75 },
      focusSelected: {
        light: colors.G100,
        dark: colors.G75,
      },
    },
  },
  color: {
    default: {
      default: { light: colors.N500, dark: colors.DN400 },
      active: { light: colors.G400, dark: colors.G400 },
      disabled: { light: colors.N70, dark: colors.DN30 },
      selected: { light: colors.N20, dark: colors.DN400 },
      focusSelected: { light: colors.N20, dark: colors.DN400 },
    },
    primary: {
      default: { light: colors.N0, dark: colors.DN30 },
      disabled: { light: colors.N70, dark: colors.DN30 },
      selected: { light: colors.N20, dark: colors.DN400 },
      focusSelected: { light: colors.N20, dark: colors.DN400 },
    },
    warning: {
      default: { light: colors.N800, dark: colors.N800 },
      disabled: { light: colors.N70, dark: colors.DN30 },
      selected: { light: colors.N800, dark: colors.N800 },
      focusSelected: { light: colors.N800, dark: colors.N800 },
    },
    danger: {
      default: { light: colors.N0, dark: colors.N0 },
      disabled: { light: colors.N70, dark: colors.DN30 },
      selected: { light: colors.N0, dark: colors.N0 },
      focusSelected: { light: colors.N0, dark: colors.N0 },
    },
    link: {
      default: { light: colors.G400, dark: colors.G100 },
      hover: { light: colors.G300, dark: colors.G75 },
      active: { light: colors.G500, dark: colors.G200 },
      disabled: { light: colors.N70, dark: colors.DN100 },
      selected: { light: colors.N20, dark: colors.N700 },
      focusSelected: { light: colors.N20, dark: colors.N700 },
    },
    inline: {
      default: { light: colors.N500, dark: colors.N0 },
      hover: { light: colors.G300, dark: colors.G75 },
      active: { light: colors.G500, dark: colors.G200 },
      disabled: { light: colors.N70, dark: colors.DN100 },
      selected: { light: colors.G500, dark: colors.G200 },
      focusSelected: { light: colors.N20, dark: colors.N700 },
    },
    subtle: {
      default: { light: colors.N500, dark: colors.N0 },
      active: { light: colors.G400, dark: colors.G400 },
      disabled: { light: colors.N70, dark: colors.DN100 },
      selected: { light: colors.N20, dark: colors.DN400 },
      focusSelected: { light: colors.N20, dark: colors.DN400 },
    },
    'subtle-link': {
      default: { light: colors.G500, dark: colors.G400 },
      hover: { light: colors.G300, dark: colors.G50 },
      active: { light: colors.G300, dark: colors.DN300 },
      disabled: { light: colors.N70, dark: colors.DN100 },
      selected: { light: colors.N20, dark: colors.DN400 },
      focusSelected: { light: colors.N20, dark: colors.DN400 },
    },
    ghost: {
      default: { light: colors.G500, dark: colors.G500 },
      hover: { light: colors.G400, dark: colors.G400 },
      active: { light: colors.G400, dark: colors.G400 },
      disabled: { light: colors.N70, dark: colors.DN100 },
      selected: { light: colors.N20, dark: colors.DN400 },
      focusSelected: { light: colors.N20, dark: colors.DN400 },
    },
  },
  borderColor: {
    default: {
      default: { light: colors.N20A, dark: colors.DN70 },
      hover: { light: colors.N30A, dark: colors.DN60 },
      active: { light: hex2rgba(colors.G75, 0.6), dark: colors.G75 },
      disabled: { light: colors.N20A, dark: colors.DN70 },
      selected: { light: colors.N700, dark: colors.DN0 },
      focusSelected: { light: colors.N700, dark: colors.DN0 },
    },
    primary: {
      default: { light: colors.G400, dark: colors.G100 },
      hover: { light: colors.G300, dark: colors.G75 },
      active: { light: colors.G500, dark: colors.G200 },
      disabled: { light: colors.N20A, dark: colors.DN70 },
      selected: { light: colors.N700, dark: colors.DN0 },
      focusSelected: { light: colors.N700, dark: colors.DN0 },
    },
    warning: {
      default: { light: colors.Y300, dark: colors.Y300 },
      hover: { light: colors.Y200, dark: colors.Y200 },
      active: { light: colors.Y400, dark: colors.Y400 },
      disabled: { light: colors.N20A, dark: colors.DN70 },
      selected: { light: colors.Y400, dark: colors.Y400 },
      focusSelected: { light: colors.Y400, dark: colors.Y400 },
    },
    danger: {
      default: { light: colors.R400, dark: colors.R400 },
      hover: { light: colors.R300, dark: colors.R300 },
      active: { light: colors.R500, dark: colors.R500 },
      disabled: { light: colors.N20A, dark: colors.DN70 },
      selected: { light: colors.R500, dark: colors.R500 },
      focusSelected: { light: colors.R500, dark: colors.R500 },
    },
    link: {
      default: { light: 'transparent', dark: 'transparent' },
      selected: { light: 'transparent', dark: 'transparent' },
      focusSelected: { light: 'transparent', dark: 'transparent' },
    },
    inline: {
      default: { light: 'transparent', dark: 'transparent' },
      selected: { light: 'transparent', dark: 'transparent' },
      focusSelected: { light: 'transparent', dark: 'transparent' },
    },
    subtle: {
      default: { light: 'transparent', dark: 'transparent' },
      hover: { light: 'transparent', dark: 'transparent' },
      active: { light: 'transparent', dark: 'transparent' },
      disabled: { light: 'transparent', dark: 'transparent' },
      selected: { light: 'transparent', dark: 'transparent' },
      focusSelected: { light: 'transparent', dark: 'transparent' },
    },
    'subtle-link': {
      default: { light: 'none', dark: 'none' },
      selected: { light: colors.N700, dark: colors.N20 },
      focusSelected: { light: colors.N700, dark: colors.N20 },
    },
    ghost: {
      default: { light: colors.G500, dark: colors.G500 },
      hover: { light: colors.G300, dark: colors.G300 },
      selected: { light: colors.G300, dark: colors.G300 },
      focusSelected: { light: 'none', dark: 'none' },
    },
  },
};

export function applyPropertyStyle(property, { appearance = 'default', state = 'default', mode = 'light' }, theme) {
  const propertyStyles = theme[property];
  if (!propertyStyles) {
    return 'initial';
  }
  // Check for relevant fallbacks
  if (!propertyStyles[appearance]) {
    if (!propertyStyles['default']) {
      return fallbacks[property][mode] ? fallbacks[property][mode] : 'initial';
    }
    appearance = 'default';
  }
  // If there is no 'state' key (ie, 'hover') defined for a given appearance,
  // return the 'default' state of that appearance.
  if (!propertyStyles[appearance][state]) {
    state = 'default';
  }
  const appearanceStyles = propertyStyles[appearance];
  const stateStyles = appearanceStyles[state];
  if (!stateStyles) {
    return 'inherit';
  }
  return stateStyles[mode] || appearanceStyles.default[mode];
}

export const Theme = createTheme(themeProps => ({
  buttonStyles: getButtonStyles(themeProps),
  spinnerStyles: getSpinnerStyles(),
}));
//# sourceMappingURL=theme.js.map
