import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { formatMessage } from 'umi-plugin-react/locale';
import { setStringToClipboard } from 'shared/helpers';
import Icon from '../Icon';

import styles from './index.less';

const Button = ({
  icon,
  type,
  size,
  shape,
  ghost,
  block,
  action,
  payload,
  children,
  disabled,
  onClick,
  className
}) => {
  const [hint, setHint] = useState(null);
  const onButtonClick = e => {
    if (disabled) {
      return false;
    }

    switch (action) {
      case 'copy':
        if (!hint) {
          setStringToClipboard(payload);
          setHint(formatMessage({ id: 'common.copied' }));
        }
        break;

      default:
        typeof onClick === 'function' && onClick(e);
        break;
    }
  };

  useEffect(() => {
    if (hint !== null) {
      setTimeout(() => {
        setHint(null);
      }, 3000);
    }
  }, [hint]);

  return (
    <button
      className={
        classnames(
          styles.button,
          styles[size],
          styles[type],
          styles[shape],
          {
            [styles.ghost]: ghost,
            [styles.block]: block,
            [styles.disabled]: disabled,
            disabled,
          },
          "weim-button",
          className
        )
      }
      onClick={onButtonClick}
    >
      {icon && (
        <span
          className={
            classnames(
              styles.button__icon,
              { [styles.button__space]: !!children },
              "weim-button-icon"
            )
          }
        >
          <Icon type={icon} />
        </span>
      )}
      {children && (
        <span
          className={
            classnames(
              styles.button__text,
              "weim-button-text"
            )
          }
        >
          {hint || children}
        </span>
      )}
    </button>
  );
};

Button.defaultProps = {
  type: 'primary',
};
Button.propTypes = {
  icon: PropTypes.string,
  type: PropTypes.oneOf(['primary', 'danger']),
  size: PropTypes.oneOf(['small', 'large']),
  shape: PropTypes.oneOf(['square']),
  ghost: PropTypes.bool,
  block: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  loading: PropTypes.bool,
};

export default Button;
