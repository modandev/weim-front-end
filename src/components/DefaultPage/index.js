import React, {} from 'react';
import styles from './styles.less';
import classnames from 'classnames';

export const DefaultHeader = ({ size = 'small' }) =>
  <div className={`${styles.default_avatar}  ${styles.default_page} ${styles[size]}`} />;

export const DefaultTextLine = ({ width }) =>
  <div style={{ width: width }}
       className={`${styles.default_text_line} ${styles.default_page}  `} />;

export const DefaultTextPic = ({ height }) =>
  <div style={{ height: height }}
       className={`${styles.default_pic} ${styles.default_page}  `} />;

export const DefaultTextPage = ({ lines = 1, style }) => {
  const array = new Array(lines).fill(0);
  return (
    <div style={style}>
      {
        array.map((item, index) => {
          if (index + 1 === lines && index !== 0) {
            return <DefaultTextLine width={'80%'}
                                    key={index} />;
          }
          return <DefaultTextLine key={index} />;
        })
      }
    </div>);
};

export const DefaultPage = () => {
  return <div className={styles.default_page_outer}>
    <div className={styles.row}>
       <DefaultHeader size="middle" />
       <div style={{ flex: 1 }}>
         <DefaultTextPage lines={2} />
       </div>
    </div>
    <DefaultTextPic height={'200px'} />
    <DefaultTextPage lines={5} />
  </div>;
};

export const StockPage = () => {
  return <div className={styles.default_page_outer}>
    <div className={styles.center}>
       <div style={{ width: '30%', marginBottom: '20px' }}>
         <DefaultTextPage lines={1} />
       </div>
    </div>
    <div style={{ width: '20%' }}>
         <DefaultTextPage lines={1} />
    </div>
    <DefaultTextPic height={'200px'} />
    <DefaultTextPage lines={4} />
  </div>;
};

export const DefaultFans = () => {
  return <div className={styles.default_page_outer}
              style={{ width: '390px', height: '110px' }}>
    <div className={styles.row}>
       <DefaultHeader size="middle" />
       <div style={{ flex: 1 }}>
         <DefaultTextPage lines={2} />
       </div>
    </div>
  </div>;
};
