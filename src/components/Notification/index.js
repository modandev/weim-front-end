/* eslint-disable react-hooks/exhaustive-deps */
import { memo, useRef, useEffect, useState } from 'react';
import { G300, R400} from '@atlaskit/theme/colors';
import classnames from 'classnames';
import SuccessIcon from '@atlaskit/icon/glyph/check-circle';
import ErrorIcon from '@atlaskit/icon/glyph/error';
import Spinner from '@atlaskit/spinner';
import Flag from '@atlaskit/flag';

import styles from './index.less';


const Notification = ({ visible, text, actions, type }) => {  
  const prevVisible = useRef(visible);
  const [visibility, setVisibility] = useState(false);
  const [shown, setShown] = useState(false);

  const show = () => {
    setVisibility(true);
    setTimeout(() => setShown(true), 100);
  };

  const hide = () => {
    setShown(false);
    setTimeout(() => setVisibility(false), 100);
  };

  useEffect(() => {
    visible && show();
  }, []);

  useEffect(() => {
    if (visible !== prevVisible.current) {
      prevVisible.current = visible;

      visible ? show() : hide();
    }
  }, [visible]);

  const Icon = type === 'success' ? SuccessIcon : ErrorIcon;
  const color = type === 'success' ? G300 : R400;

  return (
    <div
      className={
        classnames(
          styles.notification,
          {
            [styles.shown]: shown,
            [styles.visibility]: visibility
          },
          'weim-notification'
        )
      }
    >
      <div className={classnames(styles.content, 'weim-notification-content', { [styles.loading]: type === 'loading' })}>
        {
          type === 'loading' ? <Spinner size="large" /> : <Flag
            appearance={type}
            icon={<Icon primaryColor={color} label="Info" />}
            actions={actions}
            title={text}
          />
        }
      </div>
    </div>
  );
};

export const notify = {
  loading (timeout = 30000) {
    document.dispatchEvent(new CustomEvent(`notify`, {
      detail: {
        visible: true,
        timeout,
        type: 'loading',
      }
    }));
  },

  warning (text, timeout) {
    return this.show(text, 'warning', timeout)
  },

  show (text, type = 'warning', timeout = 1500) {
    document.dispatchEvent(new CustomEvent(`notify`, {
      detail: {
        visible: true,
        timeout,
        text,
        type,
      }
    }))
  },

  hide () {
    document.dispatchEvent(new CustomEvent(`notify`, {
      detail: {
        visible: false,
      }
    }))
  }
}

export default memo((props) => {
  const [notify, setNotify] = useState({
    text: '', 
    visible: false, 
    type: 'warning'
  })

  useEffect(() => {
    const onNotify = (event) => {
      const { timeout, ...rest } = event.detail;

      setNotify({
        ...notify,
        ...rest
      });

      if (timeout && rest.visible) {
        setTimeout(() => {
          setNotify({
            ...notify,
            visible: !rest.visible
          })
        }, timeout)
      }
    }
    
    document.addEventListener('notify', onNotify);

    return () => {
      document.removeEventListener('notify', onNotify);
    }
  })

  return <Notification {...notify} />
});
