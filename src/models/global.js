import {
  search,
  searchHistory,
  deleteSearchHistory,
  searchHot,
  commitSearchWord,
  createFile,
} from 'services/global';

export default {
  namespace: 'global',

  state: {
    activeMenu: '', // active 菜单
    searchResult: {}, // 搜索结果
    searchHistory: [], // 搜索历史
    searchHot: [], // 热门搜索
    notices: [],
  },

  subscriptions: {
    routing({ dispatch, history }) {
      history.listen(({ pathname }) =>
        dispatch({
          type: 'updateCurrentState',
          payload: { activeMenu: pathname }
        }),
      );
    }
  },

  effects: {
    // 搜索历史
    *searchHistory(_, { call, put }) {
      const response = yield call(searchHistory);

      yield put({
        type: 'updateCurrentState',
        payload: {
          searchHistory: response
        }
      });
    },
    // 清空搜索历史
    *deleteSearchHistory(_, { call, put }) {
      yield put({
        type: 'updateCurrentState',
        payload: {
          searchHistory: []
        }
      });
      yield call(deleteSearchHistory);
    },
    // 热门搜索
    *searchHot(_, { call, put }) {
      const response = yield call(searchHot);

      yield put({
        type: 'updateCurrentState',
        payload: {
          searchHot: response
        }
      });
    },
    // 记录搜索词
    *commitSearchWord({ payload }, { call, put }) {
      yield call(commitSearchWord, payload);
      yield put({ type: 'searchHistory' });
    },
    // 头部搜索
    *search({ payload }, { call, put }) {
      const response = yield call(search, payload);
      yield put({
        type: 'updateCurrentState',
        payload: {
          searchResult: response
        }
      });
      return response
    },
    // 上传文件
    *createFile({ payload }, { call, put }) {
      try {
        const response = yield call(createFile, payload);

        return { success: true, data: response };
      } catch(error) {
        return { success: false };
      }
    }
  },

  reducers: {
    addNotice(state, { payload }) {
      const { type, unique } = payload;
      const { notices } = state;

      if (unique && notices.find(notice => type === notice.type)) {
        return { ...state };
      }

      return {
        ...state,
        notices: [...state.notices, payload],
      };
    },

    removeNotice(state, { payload }) {
      const { notices } = state;
      const index = notices.findIndex(({ id, type }) =>{
        return  id === payload || type === payload
      })

      index !== -1 && notices.splice(index, 1);

      return {
        ...state,
        notices
      };
    },
    updateCurrentState(state, { payload }) {
      return {
        ...state,
        ...payload
      };
    },
  },
};
