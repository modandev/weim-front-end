import { STREAM } from '@/shared/graphql/gql/tweet';
import { TICKERACCOUNTS } from '@/shared/graphql/gql/ticker';
import { client } from '@/shared/graphql/client';

const assetStreamNames = [
  'assetAccount',
  'assetAnalysis',
  'assetPosts',
];

export default {
  namespace: 'assets',
  state: {
    assetAccount: [],
    assetAnalysis: [],
    assetPosts: [],
  },

  effects: {
    * tickerAccounts({ variables = {} }, { call, put }) {
      try {
        const { data } = yield call(client.query, {
          query: TICKERACCOUNTS,
          variables,
        });
        if (variables.cursor) {
          yield put({
            type: 'pushStreamList',
            stream: data.tickerAccounts,
            dataType: 'assetAccount',
          });
        } else {
          yield put({
            type: 'updateCurrentState', payload: {
              assetAccount: data.tickerAccounts.hits,
            },
          });
        }
        return {
          cursor: data.stream.cursor,
          isNoMore: data.stream.hits.length === 0,
        };
      } catch (e) {
        return {
          isNoMore: true,
        };
      }
    },

    * stream({ variables = {}, dataType }, { call, put }) {
      try {
        const { data } = yield call(client.query, {
          query: STREAM,
          variables,
        });
        if (variables.cursor) {
          yield put({
            type: 'pushStreamList',
            stream: data.stream,
            dataType,
          });
        } else {
          yield put({
            type: 'updateCurrentState', payload: {
              ['asset' + dataType]: data.stream.hits,
              cursor: data.stream.cursor,
              noMore: false,
            },
          });
        }
        return {
          cursor: data.stream.cursor,
          isNoMore: data.stream.hits.length === 0,
        };
      } catch (e) {
        return {
          isNoMore: true,
        };
      }
    },
  },

  reducers: {
    updateCurrentState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },

    pushStreamList(state, { stream, dataType }) {
      if (!stream.hits.length) {
        return {
          ...state,
        };
      }
      return {
        ...state,
        ['asset' + dataType]: [...state['asset' + dataType], ...stream.hits],
      };
    },

    changeWatchStatus(state, { ticker }) {
      const assetStreams = {};
      assetStreamNames.map(item => {
        assetStreams[item] = _changeWatchStatus(state[item], ticker);
      });
      return {
        ...state,
        ...assetStreams,
      };
    },

    changeFollowStatus(state, { account_id }) {
      const assetStreams = {};
      assetStreamNames.map(item => {
        assetStreams[item] = _changeFollowStatus(state[item], account_id);
      });

      return {
        ...state,
        ...assetStreams,
      };
    },
  },
};

// 收藏
const _changeWatchStatus = (list, ticker) => {
  return list.map(
    item => {
      if (item.ticker && item.ticker.key === ticker) {
        return {
          ...item, ticker: {
            ...item.ticker,
            is_watch: !item.ticker.is_watch,
          },
        };
      }
      return item;
    },
  );
};

// 关注
const _changeFollowStatus = (list, account_id) => {
  return list.map(item => {
    if (item.account && item.account.account_id === account_id) {
      return {
        ...item,
        account: {
          ...item.account,
          is_followed: !item.account.is_followed,
          no_of_followers: item.account.no_of_followers + (item.account.is_followed ? -1 : 1),
        },
      };
    }
    return item;
  });
};
