import { watchlist, trades } from '@/services/trade';

export default {
  namespace: 'trade',

  state: {
    watchList: {},
  },

  effects: {
    * watchlist(_, { call, put }) {
      const response = yield call(watchlist);
    },

    * trades({payload}, { call }) {
      const res = yield call(trades,payload);
    },
  },

  reducers: {
    updateCurrentState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
