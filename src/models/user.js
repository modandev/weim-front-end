import router from 'umi/router';
import {
  oauth,
  getUserByUid,
  accounts,
  createAccount,
  updateAccount,
  updateUser,
  nav,
  position,
  summary,
  orders,
  watchlist,
  ticker,
  tickers,
  statsOfTicker,
  tickerMarketData,
  analysis,
  analysisDetail,
  publishDraft,
  newDraft,
  saveDraft,
  deleteDraft,
  accountFollowers,
  stream,
  logOut,
  logIn,
  resetPassword,
  changePassword
} from '@/services/user';
import { formatMessage } from 'umi-plugin-react/locale';
import { getQueries } from 'shared/locationParser';
import appConfig from 'configs/app';
import { randomColor, wholeTens } from 'shared/helpers';
import { client } from '@/shared/graphql/client';
import { viewer, DISPLAY_BANNER } from '@/shared/graphql/gql/user';
import { TICKERS_INFO } from '@/shared/graphql/gql/ticker';
import { ACCOUNT_ROR } from '@/shared/graphql/gql/account';

const { colors } = appConfig;
const { positionStartColor, positionEndColor } = colors;

export default {
  namespace: 'user',

  state: {
    currentUser: {}, // 当前用户

    accounts: [], // 账号列表

    watchlist: [], // 自选列表

    user: {}, // 某个用户
    navData: [], // 资产净值
    positions: { data: [], colors: [] }, // 仓位
    tickerMarketOrder: [], // 股票行情

    analysis: [], // 投资研究文章

    userStatus: {},

    liquidatedTickerData: {
      data: [], colors: [],
    },

    returnData: [],
  },

  effects: {
    // 三方验证
    * oauth({ payload }, { call, put }) {
      const response = yield call(oauth, payload);

      localStorage.setItem('token', response.sessionToken);
      localStorage.setItem('user_id', response.id);

      yield put({ type: 'viewer' });
      yield put({
        type: 'accounts',
        payload: { redirectToIBConnectIfNeeded: true },
      });
    },

    // 邮件登录
    * logIn({ payload }, { call, put }) {
      const response = yield call(logIn, payload);

      localStorage.setItem('token', response.sessionToken);
      localStorage.setItem('user_id', response.id);

      yield put({ type: 'viewer' });
      yield put({
        type: 'accounts',
        payload: { redirectToIBConnectIfNeeded: true },
      });

      return response;
    },

    // 发送重置密码邮件
    * resetPassword({ payload }, { call, put }) {
      const response = yield call(resetPassword, payload);

      return response;
    },

    // 发送重置密码邮件
    * changePassword({ payload }, { call, put }) {
      const response = yield call(changePassword, payload);

      return response;
    },

    // 个人信息
    * viewer(_, { call, put }) {
      try {
        const { data: response } = yield call(client.query, {
          query: viewer,
        });
        let { userStatus } = response;

        yield put({
          type: 'updateCurrentState',
          payload: {
            currentUser: response.viewer,
          },
        });

        yield put({
          type: 'updateCurrentState',
          payload: {
            userStatus: {
              ...userStatus,
              accountId: userStatus.account_bind_failure ? userStatus.account_bind_failure.account_id : userStatus.account_bind_in_progress ? userStatus.account_bind_in_progress.account_id : userStatus.account_pending_info ? userStatus.account_pending_info.account_id : '',
            },
          },
        });

        // 用于关联失败顶部提示词条
        if (userStatus.active && (userStatus.signup_redirect === 'ACCT_CONNECT_FAIL' || userStatus.signup_redirect === 'ACCT_PENDING_INFO' || userStatus.signup_redirect === 'BINDING_IN_PROGRESS')) {
          // 获取accountId
          const accountId = userStatus.account_bind_failure ? userStatus.account_bind_failure.account_id : userStatus.account_bind_in_progress ? userStatus.account_bind_in_progress.account_id : userStatus.account_pending_info.account_id;
          // 获取本地存储的Tip ID
          const accountStatus = localStorage.getItem('accountStatus') ? JSON.parse(localStorage.getItem('accountStatus')) : [];
          // 拼接tip ID
          const id = userStatus.signup_redirect + ':' + accountId;
          // 如果本地已存在的Tip ID 跳过弹出该词条
          if (!accountStatus.find(item => item === id)) {
            yield put({
              type: 'global/addNotice',
              payload: {
                type: 'ACCOUNT_STATUS',
                unique: true,
                id,
                message: 'user.connect.' + userStatus.signup_redirect,
                linkMessage: 'user.connect.' + userStatus.signup_redirect + '_Link',
                onPressClose: () => {
                  accountStatus.push(id);
                  localStorage.setItem('accountStatus', JSON.stringify(accountStatus));
                },
                onPress: () => {
                  router.push(`${userStatus.signup_redirect === 'ACCT_PENDING_INFO' ? '/connect_account/account_profile/' : '/connect_account/status/'}${accountId}`);
                },
              },
            });
          }
        }

        // if (response.userStatus.show_banner_account_binding) {
        //   yield put({
        //     type: 'global/addNotice', payload: {
        //       type: 'TIPS',
        //       unique: true,
        //       message: formatMessage({ id: 'user.account.connectingTip' }),
        //       onPressClose: () => {
        //         client.mutate(
        //           {
        //             mutation: DISPLAY_BANNER,
        //           },
        //         );
        //       },
        //     },
        //   });
        // }
        return { success: true, userStatus: response.userStatus };
      } catch (error) {
        return { success: false };
      }
    },
    // 某个用户信息
    * user({ payload }, { call, put }) {
      const response = yield call(getUserByUid, payload);

      yield put({
        type: 'updateCurrentState',
        payload: {
          user: response.edges[0].node,
        },
      });
    },
    // 所有账户
    * accounts({ payload }, { call, put }) {
      const { redirectToIBConnectIfNeeded } = payload || {};
      const response = yield call(accounts, payload);

      if (redirectToIBConnectIfNeeded) {
        // Login successfully
        const urlParams = new URL(window.location.href);
        let { redirect } = getQueries();

        if (redirect) {
          const redirectUrlParams = new URL(redirect);

          if (redirectUrlParams.origin === urlParams.origin) {
            redirect = redirect.substr(urlParams.origin.length);

            if (redirect.match(/^\/.*#/)) {
              redirect = redirect.substr(redirect.indexOf('#') + 1);
            }
          } else {
            window.location.href = '/';
            return;
          }
        }

        localStorage.setItem('c_s_o_u', 1);
        router.replace(response.length ? redirect || '/' : '/manager_intro');
      } else {
        yield put({
          type: 'updateCurrentState',
          payload: {
            accounts: response,
          },
        });
      }

      return response;
    },
    // 添加账户
    * createAccount({ payload }, { call, put }) {
      try {
        const data = yield call(createAccount, payload);
        yield put({
          type: 'global/addNotice',
          payload: {
            type: 'TIPS',
            unique: true,
            message: formatMessage({ id: 'user.account.connectingTip' }),
            onPressClose: () => {
              client.mutate(
                {
                  mutation: DISPLAY_BANNER,
                },
              );
            },
          },
        });

        return {
          success: true,
          accountId: data.account_id,
        };
      } catch (error) {
        return { success: false, error };
      }
    },
    // 更新账户
    * updateAccount({ payload }, { call, put }) {
      try {
        yield call(updateAccount, payload);
      } catch (error) {
        return { success: false, error };
      }

      yield put({ type: 'accounts', payload: { status: 'ALL' } });
      return { success: true };
    },
    // 用户信息更新
    * updateUser({ payload }, { call, put }) {
      const response = yield call(updateUser, payload);

      yield put({
        type: 'updateCurrentState',
        payload: {
          currentUser: response,
        },
      });
    },
    * nav({ payload }, { call, put }) {
      let response = [];

      try {
        response = yield call(nav, payload);
      } catch (error) {
      }

      if (response.length) {
        const sorted = response.sort((a, b) => a.nav - b.nav);
        let min = +sorted[0].nav;
        let max = +sorted[sorted.length - 1].nav;

        min = wholeTens(min, { round: 'down' });
        max = wholeTens(max);
        response.min = min;
        response.max = max;
      }

      yield put({
        type: 'updateCurrentState',
        payload: {
          navData: response,
        },
      });
    },

    * getReturn({ variables }, { call, put }) {

      try {
        let { data: { accountRor = [], nav = [] } = {} } = yield call(client.query, {
          query: ACCOUNT_ROR,
          variables,
          fetchPolicy: 'network-only',
        });
        const returnData = [];

        accountRor.reduce((previous, current, index) => {
          let currentNavData = nav.find(item => item.date === current.date);
          let currentNav = currentNavData ? parseFloat(currentNavData.nav) : 0;

          returnData.push({
            ...current,
            ror: +current.ror,
            rise: currentNav > previous,
            volumn: currentNav,
          });

          return currentNav;
        }, 0);

        yield put({
          type: 'updateCurrentState',
          payload: {
            returnData,
          },
        });
      } catch (error) {
        console.log(error);
      }
    },


    * position({ payload }, { call, put }) {
      let positions = {};
      const positionData = yield call(position, payload);

      if (!payload.pure) {
        const summaryData = yield call(summary, { accountId: payload.accountId });
        const { cash } = summaryData;
        const sum = positionData.reduce((prev, next) => prev + +next.market_value, +cash);
        let dataSource = positionData.map(({ ticker, market_value, ...rest }) => ({
          ...rest,
          ticker,
          item: ticker.split('.')[1],
          id: ticker.split('.')[1] + 'position',
          count: +market_value,
          percent: Number((market_value / sum).toFixed(2)),
        }));

        positions.data = dataSource.sort((a, b) => b.percent - a.percent);

        if (dataSource.length) {
          dataSource.push({ item: 'CASH', count: cash, percent: Number((cash / sum).toFixed(2)) });
        }

        if (dataSource.length) {
          positions.colors = randomColor({
            orderly: true,
            count: dataSource.length,
            start: positionStartColor,
            end: positionEndColor,
          });
        }

      } else {
        positions = positionData;
      }

      yield put({
        type: 'updateCurrentState',
        payload: {
          positions,
        },
      });
    },

    * liquidatedTicker({ variables }, { call, put }) {
      const { data: { tickersDetail } } = yield call(client.query, { query: TICKERS_INFO, variables });

      const data = tickersDetail.map(({ ticker, tickerInfo }) => ({
        ticker,
        tickerInfo,
        item: ticker.split('.')[1],
        id: ticker.split('.')[1] + 'liquidated',
      }));
      yield put({
        type: 'updateCurrentState',
        payload: {
          liquidatedTickerData: {
            data,
          },
        },
      });
    },

    * orders({ payload }, { call, put }) {
      let response = [];

      try {
        response = yield call(orders, payload);
      } catch (error) {
      }

      return response;
    },

    * tickerMarketData({ payload }, { call, put }) {
      let response = [];

      try {
        response = yield call(tickerMarketData, payload);
      } catch (error) {
      }

      return response;
    },

    * watchlist(_, { call, put }) {
      let response = [];

      try {
        const watchlistData = yield call(watchlist);

        response = watchlistData.map(({ ticker, statsOfTicker, tickerInfo, earningOfTicker }, index) => ({
          ...tickerInfo,
          ...earningOfTicker,
          ticker,
          current_price: +statsOfTicker.current_price || 0,
          account_holders_no: +statsOfTicker.account_holders_no || 0,
          currency: statsOfTicker.currency || '',
        })).sort((a, b) => b.current_price - a.current_price);
      } catch (error) {
      }

      yield put({ type: 'updateCurrentState', payload: { watchlist: response } });
    },

    * tickers({ payload }, { call, put }) {
      let data = [];
      const response = yield call(tickers, payload);

      if (response && response.length) {
        const promises = response.map(item => ticker({ ticker: item }));

        data = yield call(() => Promise.all(promises));
      }

      return data;
    },

    * analysis({ payload }, { call, put }) {
      const response = yield call(analysis, payload);

      yield put({ type: 'updateCurrentState', payload: { analysis: response ? response.hits : [] } });
      return response;
    },
    * analysisDetail({ payload }, { call, put }) {
      const response = yield call(analysisDetail, payload);

      return response;
    },
    * publishDraft({ payload }, { call, put }) {
      const response = yield call(publishDraft, payload);
    },
    * newDraft({ payload }, { call, put }) {
      const response = yield call(newDraft, payload);
      const { skipUpdateAnalysis, fields } = payload;
      const { accountId, ticker } = fields;

      if (!skipUpdateAnalysis) {
        yield put({ type: 'analysis', payload: { accountId, ticker } });
      }

      return response;
    },
    * saveDraft({ payload }, { call, put }) {
      yield call(saveDraft, payload);
    },
    * deleteDraft({ payload }, { call, put }) {
      const { analysisId, accountId, ticker } = payload;

      yield call(deleteDraft, { analysisId });
      yield put({ type: 'analysis', payload: { accountId, ticker } });
    },
    * accountFollowers({ payload }, { call, put }) {
      const response = yield call(accountFollowers, payload);

      return response;
    },
    * stream({ payload }, { call, put }) {
      const response = yield call(stream, payload);

      return response && response.hits;
    },
    // 登出
    * logOut({ payload }, { call, put }) {
      yield call(logOut, payload);
      localStorage.removeItem('token');
      localStorage.removeItem('user_id');
      localStorage.removeItem('skipAccountProfile');
      localStorage.setItem('c_s_o_u', 0);
      localStorage.removeItem('accountStatus');
      yield put({ type: 'updateCurrentState', payload: { currentUser: {} } });
      window.location.reload();
    },
  },

  reducers: {
    updateCurrentState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
