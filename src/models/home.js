import { trades, watchlist } from '@/services/trade';
import { HOT_ASSET, RANKING, STREAM, CREATE_TWEET, TWEET } from '@/shared/graphql/gql/tweet';
import { ANALYSIS } from '@/shared/graphql/gql/depth';

import { client } from '@/shared/graphql/client';
import { FOLLOW, UN_FOLLOW } from '@/shared/graphql/gql/follow';

export default {
  namespace: 'home',
  state: {
    hotListOfBuy: [],
    hotListOfSell: [],
    accountsRank: [],
    streamList: [],
    cursor: '',
    tweet: {},
    analysis: {},
    noMore: false,
    streamQuery: {
      followedOnly: false,
      watchedOnly: false,
      postType: '' // ANALYSIS or TWEET ro ''
    },
  },

  effects: {
    * getAnalysisDetail({ variables }, { call, put }) {
      try {
        const { data } = yield call(client.query, {
          query: ANALYSIS,
          variables,
        });
        yield put({
          type: 'updateCurrentState', payload: {
            analysis: data.analysisDetail,
          },
        });
      } catch (e) {
      }
    },

    * createTweet({ variables }, { call, put }) {
      try {
        const { data } = yield call(client.mutate, { variables, mutation: CREATE_TWEET });

        yield put({ type: 'addNewTweet', tweet: data.createTweet });
      } catch (e) {
      }
    },

    * getTweet({ variables }, { call, put }) {
      try {
        const { data } = yield call(client.query, {
          query: TWEET,
          variables,
        });
        yield put({
          type: 'updateCurrentState', payload: {
            tweet: data.tweet,
          },
        });
      } catch (e) {
      }
    },

    * stream({ variables = {} }, { call, put }) {
      try {
        const { data } = yield call(client.query, {
          query: STREAM,
          fetchPolicy: 'network-only',
          variables,
        });
        if (variables.cursor) {
          yield put({
            type: 'pushStreamList', stream: data.stream,
          });
        } else {
          yield put({
            type: 'updateCurrentState', payload: {
              streamList: data.stream.hits,
              cursor: data.stream.cursor,
              noMore: false,
            },
          });
        }
      } catch (e) {
      }
    },

    * hotList({ variables, key }, { call, put }) {
      try {
        const { data } = yield call((variables) => {
          return client.query({
            query: HOT_ASSET,
            variables,
          });
        }, variables);


        yield put({
          type: 'updateCurrentState',
          payload: {
            [key]: data.hotAsset,
          },
        });
      } catch (e) {
      }
    },

    * rankList({ variables }, { call, put }) {
      try {
        const { data } = yield call((variables) => {
          return client.query({
            query: RANKING,
            variables,
          });
        }, variables);

        yield put({
          type: 'updateCurrentState',
          payload: {
            accountsRank: data.ranking,
          },
        });
      } catch (e) {
      }
    },

    * fetchFollow({ account_id, is_followed }, { call, put }) {
      try {
        yield call((variables) => {
          return client.mutate({
            mutation: is_followed ? UN_FOLLOW : FOLLOW,
            variables,
          });
        }, { accountId: account_id });

        yield put({ type: 'changeFollowStatus', account_id });
        yield put({ type: 'assets/changeFollowStatus', account_id });
      } catch (e) {
      }
    },
  },

  reducers: {
    addNewTweet(state, { tweet }) {
      return {
        ...state,
        streamList: [{ ...tweet, type: 'TWEET' }, ...state.streamList],
      };
    },

    setStreamQuery(state, { streamQuery }) {
      return {
        ...state,
        streamQuery: { ...state.streamQuery, ...streamQuery },
      };
    },

    updateCurrentState(state, { payload }) {
      // console.log('payload', payload);
      return {
        ...state,
        ...payload,
      };
    },

    pushStreamList(state, { stream }) {
      if (!stream.hits.length) {
        return {
          ...state,
          noMore: true,
        };
      }
      return {
        ...state,
        noMore: false,
        streamList: [...state.streamList, ...stream.hits],
        cursor: stream.cursor,
      };
    },

    changeWatchStatus(state, { ticker }) {
      const hotListOfBuy = _changeWatchStatus(state.hotListOfBuy, ticker);
      const hotListOfSell = _changeWatchStatus(state.hotListOfSell, ticker);
      const streamList = _changeWatchStatus(state.streamList, ticker);
      const tweet = _changeWatchStatus([state.tweet], ticker)[0];
      return {
        ...state,
        hotListOfBuy,
        hotListOfSell,
        streamList,
        tweet,
      };
    },

    changeFollowStatus(state, { account_id }) {
      const accountsRank = _changeFollowStatus(state.accountsRank, account_id);
      const streamList = _changeFollowStatus(state.streamList, account_id);
      const tweet = _changeFollowStatus([state.tweet], account_id)[0];
      const analysis = _changeFollowStatus([state.analysis], account_id)[0];
      return {
        ...state,
        accountsRank,
        streamList,
        tweet,
        analysis,
      };
    },

    changeAnalysisLike(state, { no }) {
      if (!state.analysis.account) {
        return state;
      }
      const analysis = {
        ...state.analysis,
        account: {
          ...state.analysis.account,
          analysis_likes_no: parseInt(state.analysis.account.analysis_likes_no) + (no),
        },
      };
      return {
        ...state,
        analysis,
      };
    },

    addAnalysisCommentNo(state) {
      if (!state.analysis.account) {
        return state;
      }
      const analysis = {
        ...state.analysis,
        account: {
          ...state.analysis.account,
          analysis_comments_no: parseInt(state.analysis.account.analysis_comments_no) + 1,
        },
      };
      return {
        ...state,
        analysis,
      };
    },
  },
};

// 收藏
const _changeWatchStatus = (list, ticker) => {
  return list.map(
    item => {
      if (item.ticker && item.ticker.key === ticker) {
        return {
          ...item, ticker: {
            ...item.ticker,
            is_watch: !item.ticker.is_watch,
          },
        };
      }
      return item;
    },
  );
};

// 关注
const _changeFollowStatus = (list, account_id) => {
  return list.map(item => {
    if (item.account && item.account.account_id === account_id) {
      return {
        ...item,
        account: {
          ...item.account,
          is_followed: !item.account.is_followed,
          no_of_followers: item.account.no_of_followers + (item.account.is_followed ? -1 : 1),
        },
      };
    }
    return item;
  });
};
