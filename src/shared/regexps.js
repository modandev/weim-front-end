import curry from 'lodash.curry';

export const GLOBAL_HEADER_SEARCH_KEYWORD_REGEXP = /^[0-9a-zA-Z\p{Unified_Ideograph}]{1,20}$/u;

export const ACCOUNT_NAME_REGEXP = /^[\p{Unified_Ideograph}a-zA-Z0-9]{3,20}$/u;

export const ACCOUNT_NAME_PATTERN_REGEXP = /^[\p{Unified_Ideograph}a-zA-Z0-9]{0,20}$/u;


export const USER_NICKNAME_REGEXP = /^[0-9a-zA-Z\p{Unified_Ideograph}]{1,16}$/u;

export const IB_UID_REGEXP = /^[0-9a-zA-Z]*$/;

export const EMAIL_REGEXP = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

// 作字母头像用
export const LETTER_REGEXP = /^[a-z]/i;


function check(reg, str) {
  return reg.test(str);
}

const checkCurry = curry(check);

export const checkEmail = checkCurry(EMAIL_REGEXP)
