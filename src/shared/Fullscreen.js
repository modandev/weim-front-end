class Fullscreen {
  constructor(targetElement, callback) {
    this.targetElement = targetElement;
    this.callback = callback;
    this.isSupported && this.init();
  }

  get isSupported() {
    let isSupport = false;
    const { documentElement } = document;

    if (
      window.ActiveXObject ||
      documentElement.requestFullscreen ||
      documentElement.msRequestFullscreen ||
      documentElement.webkitRequestFullScreen ||
      documentElement.mozRequestFullScreen
    ) {
      isSupport = true;
    }

    return isSupport;
  }

  get isFullscreen() {
    return window.screen.height <= this.targetElement.clientHeight;
  }

  init = () => {
    window.addEventListener('keydown', this.onKeydown, false);
    window.addEventListener('resize', this.onResize, false);
  }

  destroy = () => {
    window.removeEventListener('keydown', this.onKeydown, false);
    window.removeEventListener('resize', this.onResize, false);
  };

  onKeydown = e => {
    if (e.keyCode === 122 && !this.isFullscreen) {
      e.preventDefault();
      this.enterFullscreen();
    }
  };

  onResize = () => {
    const { userAgent } = navigator;

    if (userAgent.includes('Safari') && !userAgent.includes('Chrome')) {
      setTimeout(() => {
        this.callback(this.isFullscreen);
      }, 200);
    } else {
      this.callback(this.isFullscreen);
    }
  };

  enterFullscreen = () => {
    const requestFullscreen =
      this.targetElement.requestFullscreen ||
      this.targetElement.msRequestFullscreen ||
      this.targetElement.webkitRequestFullScreen ||
      this.targetElement.mozRequestFullScreen;

    if (requestFullscreen) {
      requestFullscreen.call(this.targetElement);
    } else if (window.ActiveXObject) {
      const wscript = new window.ActiveXObject('WScript.Shell');

      wscript != null && wscript.SendKeys('{F11}');
    }
  }

  exitFullscreen = () => {
    const exitFullscreen =
      document.exitFullscreen ||
      document.msExitFullscreen ||
      document.webkitExitFullscreen ||
      document.mozExitFullScreen;

    if (exitFullscreen) {
      exitFullscreen.call(document);
    } else if (window.ActiveXObject) {
      const wscript = new window.ActiveXObject('WScript.Shell');

      wscript != null && wscript.SendKeys('{F11}');
    }
  }
}

export default Fullscreen;
