import EditorJS from '@editorjs/editorjs';
import List from '@editorjs/list';
import Header from '@editorjs/header';
import Table from '@editorjs/table';
import SimpleImage from '@editorjs/simple-image';

export default (options = {}) =>
  new EditorJS({
    tools: {
      header: {
        class: Header,
        config: {
          levels: [1, 2, 3],
        }
      },
      list: List,
      image: {
        class: SimpleImage,
        inlineToolbar: true,
        config: {
          placeholder: 'Paste image URL'
        }
      },
      table: Table,
    },
    ...options
  });
