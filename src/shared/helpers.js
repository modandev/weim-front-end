export function firstLetterUpperCase(str) {
  if (!str) {
    return '';
  }

  str = typeof str === 'string' ? str : String(str);

  return str.replace(/^[a-z]/i, l => l.toUpperCase());
}

export function hex2rgb(hexColor) {
  const { length } = hexColor;
  let targetColor = hexColor;

  if (length === 4) {
    // e.g. #000
    targetColor = '#';

    for (let i = 1; i < length; i++) {
      const cur = hexColor[i];

      targetColor += `${cur}${cur}`;
    }
  }

  return `rgb(${parseInt('0x' + targetColor.slice(1, 3))}, ${parseInt('0x' + targetColor.slice(3, 5))}, ${parseInt('0x' + targetColor.slice(5))})`;
}

export const validator = {
  email (email) {
    return /^[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/g.test(email)
  },
  verification (verification) {
    return /\w{4}/g.test(verification);
  },
  password (password) {
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/g.test(password);
  }
}

export function rgb2hex(rgbColor) {
  return '#' + [...rgbColor.matchAll(/\d+/g)].map(([v]) => `0${parseInt(v).toString(16)}`.slice(-2)).join('');
}

export function randomColor(config = {}) {
  let { start, end, count = 1, orderly, deepening } = config;
  const result = [];
  const times = Array.from({ length: 3 });

  const _generator = (basedColor, step) => {
    const { random, floor } = Math;
    const regexp = /\d+/g;
    const temp = times.map((_, index) => {
      if (basedColor) {
        let val = +(regexp.exec(basedColor)[0]);

        if (deepening) {
          val -= step;

          if (val < 0) {
            val = 0;
          }
        } else {
          val += step;

          if (val > 255) {
            val = 255;
          }
        }

        return val;
      } else {
        return floor(random() * 256);
      }
    });

    return `rgb(${temp.join(',')})`;
  };
  const _repeater = (startColor, endColor) => {
    result.push(rgb2hex(startColor || _generator()));

    if (count === 1) {
      return;
    }

    if (!orderly) {
      for (let i = count - 1; i--;) {
        result.push(rgb2hex(_generator()));
      }
    } else {
      const [sr, sg, sb] = [...startColor.matchAll(/\d+/g)];
      const [er, eg, eb] = [...endColor.matchAll(/\d+/g)];
      const maxS = Math.max(sr[0], sg[0], sb[0]);
      const maxE = Math.max(er[0], eg[0], eb[0]);
      const step = Math.ceil(Math.abs(maxE - maxS) / (count - 1));
      let prevColor = startColor;

      for (let i = count - 2; i--;) {
        prevColor = _generator(prevColor, step);
        result.push(rgb2hex(prevColor));
      }

      result.push(rgb2hex(endColor));
    }
  };

  if (orderly) {
    // 有规律的系列色值
    if (start && end) {
      if (start.includes('#')) {
        start = hex2rgb(start);
      }

      if (end.includes('#')) {
        end = hex2rgb(end);
      }

      _repeater(start, end);
    }
  } else {
    _repeater();
  }

  return result;
}

export function semicolon(number) {
  let result = null;

  if (!number) {
    result = '--';
  } else {
    const _string = String(number);
    const [firstLetter] = _string; // 解构
    let _symbol = '';

    if (['+', '-'].includes(firstLetter)) {
      _symbol = firstLetter;
      result = _string.slice(1);
    } else {
      result = _string;
    }

    if (isNaN(Number(result))) {
      result = '--';
    } else {
      const [int, decimal] = result.split('.');

      if (int.length > 3) {
        result = `${_symbol}${(+int).toLocaleString('en-US')}${decimal ? `.${decimal}` : ''}`;
      } else {
        result = `${_symbol}${result}`;
      }
    }
  }

  return result;
}

export function wholeTens(number, options = {}) {
  const { round = 'up' } = options;

  if (isNaN(Number(number)) || !['up', 'down'].includes(round)) {
    return number;
  }

  let power = 0;
  let mid = null;
  const { floor, pow, ceil } = Math;

  if (number < 10) {
    return round === 'up' ? 10 : 0;
  }

  number = String(number).split('.')[0];
  mid = floor(number.length / 2);

  while (String(number).split('.')[0].length > mid) {
    number /= 10;
    power += 1;
  }

  return (round === 'up' ? ceil : floor)(number) * pow(10, power);
}

export function setStringToClipboard(text) {
  if (!text) return;

  const temp = document.createElement('input');

  temp.value = String(text);
  document.body.appendChild(temp);
  temp.select();
  document.execCommand('Copy');
  document.body.removeChild(temp);
}

export function stringLength(str) {
  let length = 0;

  for (let i = 0, len = str.length; i < len; i++) {
    if ((str.charCodeAt(i) & 0xff00) !== 0) {
      length += 1;
    }

    length += 1;
  }

  return length;
}

export function detectZoom() {
  var isMac = /macintosh|mac os x/i.test(navigator.userAgent);
  var ratio = 0,
    screen = window.screen,
    ua = navigator.userAgent.toLowerCase();

  if (window.devicePixelRatio !== undefined) {
    ratio = window.devicePixelRatio;
  } else if (~ua.indexOf('msie')) {
    if (screen.deviceXDPI && screen.logicalXDPI) {
      ratio = screen.deviceXDPI / screen.logicalXDPI;
    }
  } else if (window.outerWidth !== undefined && window.innerWidth !== undefined) {
    ratio = window.outerWidth / window.innerWidth;
  }

  if (ratio) {
    ratio = Math.round(ratio * 100 / (isMac ? 2 : 1));
  }
  return ratio;
}
