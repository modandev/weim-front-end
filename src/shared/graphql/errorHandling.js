import { onError } from '@apollo/client/link/error';
import { notify } from '@/components';

const onTokenExpired = () => {
  const currentStatusOfUser = localStorage.getItem('c_s_o_u');

  if (currentStatusOfUser !== -1) {
    localStorage.removeItem('token');
    localStorage.removeItem('user_id');
    localStorage.setItem('c_s_o_u', -1);
  }
};

export default onError(({ response, operation, graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    const { extensions: { response: { body }, query } } = graphQLErrors[0];
    const { code, error, message } = body;

    if ([209, 401].includes(code)) {
      // token expired
      onTokenExpired();
    } else {
      if (error || message) {
        notify.warning(error || message)
      }
    }
  }

  if (networkError) console.dir(networkError);
});
