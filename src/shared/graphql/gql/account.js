import { gql } from '@apollo/client';

export const ACCOUNT_ROR = gql`  
  query accountRor($type: RorType!
    $accountId: String!
    $startDate: String
    $endDate: String
    $scope: String ) {
    accountRor (type: $type
      accountId:$accountId
      startDate: $startDate
      endDate: $endDate
      scope: $scope) {
      date
      ror
    }
    nav(accountId: $accountId, scope: $scope) {
      date
      nav
    }
  }
`


