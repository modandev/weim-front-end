import { gql } from '@apollo/client';

export const oauth = gql`
  mutation oauth($type: OAuthType!, $code: String!) {
    oauth (type: $type, code: $code) {
      sessionToken
      id
    }
  }
`;

export const logIn = gql`
  mutation logIn($fields: LogInFieldsInput) {
    logIn (fields: $fields) {
      sessionToken
      id
    }
  }
`;

export const resetPassword = gql`
  mutation resetPassword($email: String!, $code: String!) {
    resetPassword (email: $email, code: $code) {
      code
    }
  }
`;

export const changePassword = gql`
  mutation changePassword($username: String!, $token: String!, $password: String!) {
    changePassword (username: $username, token: $token, password: $password) {
      code
    }
  }
`;

export const viewer = gql`
  query viewer {
    viewer {
      sessionToken
      username
      uid
      id
      createdAt
      updatedAt
      email
      emailVerified
      authData
      isLinked
      firstName
      lastName
      nickname
      avatar
    }
    userStatus{
      active,
      signup_redirect,
      account_bind_failure{
        account_id
        reason_en
        reason_zh
      },
      account_bind_in_progress{
        account_id
      },
      account_pending_info{
        account_id
      },
      show_banner_account_binding
    }
  }
`;

export const user = gql`
  query user ($id: ID!) {
    user (id: $id) {
      username
      uid
      id
      objectId
      createdAt
      updatedAt
      email
      emailVerified
      authData
      isLinked
      firstName
      lastName
      nickname
      avatar
    }
  }
`;

export const users = gql`
  query users (
    $where: UserWhereInput
    $order: [UserOrder!]
    $skip: Int
    $limit: Int
    $options: ReadOptionsInput
  ) {
    users (
      where: $where
      order: $order
      skip: $skip
      limit: $limit
      options: $options
    ) {
      count
      results {
        username
        uid
        id
        createdAt
        updatedAt
        email
        emailVerified
        authData
        isLinked
        firstName
        lastName
        nickname
        avatar
      }
    }
  }
`;

export const accounts = gql`
  query accounts ($uid: ID, $status: AccountStatus) {
    accounts (uid: $uid, status: $status) {
      nickname
      header_url
      description
      uid
      account_id
      firstname
      lastname
      broker
      email
      status
      nickname_update_counter
      no_of_followers
      is_followed
    }
  }
`;

export const account = gql`
  query account ($accountId: String!) {
    account (accountId: $accountId) {
      nickname
      header_url
      description
      uid
      account_id
      firstname
      lastname
      broker
      email
      status
    }
  }
`;

export const createAccount = gql`
  mutation createAccount($fields: AccountFields) {
    createAccount(fields: $fields){
      account_id
    }
  }
`;

export const updateAccount = gql`
  mutation updateAccount($accountId: String!, $fields: UpdateAccountFields!) {
    updateAccount(accountId: $accountId, fields: $fields) {
      code
      message
    }
  }
`;

export const updateUser = gql`
  mutation updateUser($id:ID! ,$fields: UpdateUserFieldsInput!) {
    updateUser(id:$id, fields: $fields) {
      username
      uid
      id
      createdAt
      updatedAt
      email
      emailVerified
      authData
      isLinked
      firstName
      lastName
      nickname
      avatar
    }
  }
`;

export const nav = gql`
  query nav($accountId: String!, $scope: String!) {
    nav(accountId: $accountId, scope: $scope) {
      date
      nav
    }
  }
`;

export const position = gql`
  query position($accountId: String!, $currency: String) {
    position(accountId: $accountId, currency: $currency) {
      avg_cost
      ticker
      tickerInfo
      side
      quantity
      currency
      percentage
      market_value
    }
  }
`;

export const summary = gql`
  query summary($accountId: String!) {
    summary(accountId: $accountId) {
      cash
    }
  }
`;

export const trade = gql`
  query trade($accountId: String!, $start: Date!, $end: Date!, $ticker: String!, $scope: String!) {
    trade(accountId: $accountId, start: $start, end: $end, ticker: $ticker, scope: $scope) {
      order_id
      ticker
      side
      quantity
      price
      time
      trade_id
      account_id
    }
  }
`;

export const orders = gql`
  query orders($accountId: String!, $start: String, $end: String, $ticker: String, $scope: String) {
    orders(accountId: $accountId, start: $start, end: $end, ticker: $ticker, scope: $scope) {
      order_id
      time
      ticker
      symbol
      avg_price
      quantity
      side
      account_id
      currency
    }
  }
`;

export const tickerMarketData = gql`
  query tickerMarketData($ticker: String!, $start: Date!, $end: Date!) {
    tickerMarketData(ticker: $ticker, start: $start, end: $end) {
      open
      close
      date
    }
  }
`;

export const logOut = gql`
  mutation logOut {
    logOut{
      id
    }
  }
`;

export const watchlist = gql`
  query watchlist {
    watchlist {
      ticker
      statsOfTicker
      tickerInfo
      earningOfTicker
    }
  }
`;

export const ticker = gql`
  query ticker($ticker: String!) {
    ticker(ticker: $ticker) {
      key
      name
      symbol
      exchange
      is_watch
      country_code
    }
  }
`;

export const tickers = gql`
  query tickers($type: TickerType, $accountId: String!) {
    tickers(type: $type, accountId: $accountId)
  }
`;

export const statsOfTicker = gql`
  query statsOfTicker($ticker: String!) {
    statsOfTicker(ticker: $ticker) {
      current_price
      account_holders_no
      currency
    }
  }
`;

export const analysis = gql`
  query analysis($accountId: String!, $ticker: String!) {
    analysis(accountId: $accountId, ticker: $ticker) {
      cursor
      hits {
        post_id
        title
        created_at
        views_no
        likes_no
        comments_no
        status
        prediction_price
        prediction_days
        prediction_side
        content
        summary
      }
    }
  }
`;

export const analysisDetail = gql`
  query analysisDetail($analysisId: String!) {
    analysisDetail(analysisId: $analysisId) {
      likes_no
      comments_no
      content
      account_id
      is_like
      ticker
      user_id
      views_no
      prediction_price
      prediction_days
      prediction_side
      title
      summary
    }
  }
`;

export const publishDraft = gql`
  mutation publishDraft($analysisId: String!) {
    publishDraft(analysisId: $analysisId) {
      code
      message
    }
  }
`;

export const newDraft = gql`
  mutation newDraft($fields: NewDraftFields!) {
    newDraft(fields: $fields) {
      post_id
    }
  }
`;

export const saveDraft = gql`
  mutation saveDraft($analysisId: String!, $fields: SaveDraftFields!) {
    saveDraft(analysisId: $analysisId, fields: $fields) {
      code
      message
    }
  }
`;

export const deleteDraft = gql`
  mutation deleteDraft($analysisId: String!) {
    deleteDraft(analysisId: $analysisId) {
      code
      message
    }
  }
`;

export const accountFollowers = gql`
  query accountFollowers($accountId: String) {
    accountFollowers(accountId: $accountId) {
      cursor
      hits {
        username
        uid
        id
        firstName
        lastName
        nickname
        avatar
      }
    }
  }
`;

export const stream = gql`
  query stream (
    $accountId: String,
    $ticker: String,
    $market: String,
    $size: String,
    $cursor: String
    $postType: HitsType
    $orderId: String
    $startTime: String
    $endTime: String
  ){
    stream (accountId: $accountId, ticker: $ticker,market: $market, size: $size, cursor: $cursor, postType:$postType, orderId: $orderId, startTime: $startTime, endTime: $endTime) {
      cursor
      hits{
        created_at
        post_id
        type
        user_id
        account_id
        content
        likes_no
        comments_no
        dislikes_no
        order_id
        ticker
        trade_id
        trade
        is_like
        is_dislike
        summary
        prediction_days
        prediction_side
        prediction_price
        title
        account{
          nickname
          is_followed
          header_url
          description
          account_id
          no_of_followers
          is_followed
          tweets_no
          analysis_no
        }
        accountTrading{
          positions_no
          ror
        }
        order{
          order_id
          time
          ticker
          symbol
          avg_price
          quantity
          side
          currency
          avg_price
        }
      }
    }
  }
`;

export const ACCOUNT = gql`query account ($accountId: String!) {
  account (accountId: $accountId){
    nickname
    header_url
    description
    uid
    account_id
    firstname
    lastname
    broker
    email
    status
    nickname
    no_of_followers
    is_followed
  }
}`;

export const USER_STATUS = gql`
  query userStatus {
    userStatus{
      active,
      signup_redirect,
      account_bind_failure{
        account_id
        reason_en
        reason_zh
      },
      account_bind_in_progress{
        account_id
      },
      account_pending_info{
        account_id
      },
      show_banner_account_binding
    }
  }
`;

export const DISPLAY_BANNER = gql`
  mutation displayBanner{
    displayBanner{message}
  }
`;
