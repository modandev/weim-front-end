import { gql } from '@apollo/client';

export const CREATE_TWEET = gql`
  mutation createTweet ($fields: TweetFields!) {
  createTweet(fields: $fields){
    post_id
    likes_no
    dislikes_no
    comments_no
    content
    account_id
    order_id
    order
    account
    ticker
    user_id
    is_like
    is_dislike
    accountTrading
  }
}`;


export const STREAM = gql`
  query stream (
    $accountId: String,
    $ticker: String,
    $market: String,
    $size: String,
    $cursor: String
    $postType: HitsType
    $orderId: String
    $startTime: String
    $endTime: String
    $followedOnly: Boolean,
    $watchedOnly: Boolean
  ){
    stream (accountId: $accountId, ticker: $ticker,market: $market, size: $size, cursor: $cursor, postType:$postType, orderId: $orderId, startTime: $startTime, endTime: $endTime,followedOnly: $followedOnly,watchedOnly: $watchedOnly) {
      cursor
      hits{
        created_at
        post_id
        type
        user_id
        account_id
        content
        likes_no
        comments_no
        dislikes_no
        order_id
        ticker
        trade_id
        trade
        is_like
        is_dislike
        summary
        prediction_days
        prediction_side
        prediction_price
        title
        account{
          nickname
          is_followed
          header_url
          description
          account_id
          no_of_followers
          is_followed
          tweets_no
          analysis_no
          user_id
        }
        accountTrading{
          positions_no
          ror
        }
        order{
          order_id
          time
          ticker
          symbol
          avg_price
          quantity
          side
          currency
          avg_price
        }
      }
    }
  }
`;

export const LICK_TWEET = gql`
  mutation likeTweet ($tweetId: String!,$type:LikeType) {
    likeTweet(tweetId: $tweetId,type:$type) {
      code
    }
  }
`;

export const DISLICK_TWEET = gql`
  mutation dislikeTweet  ($tweetId: String!,$type:LikeType) {
    dislikeTweet(tweetId: $tweetId, type:$type) {
      code
    }
  }
`;

export const COMMENT = gql`
  mutation comment ($id: ID!,$content: String!,$type:CommentType!){
    comment (id: $id, content: $content, type: $type){
      user
      userFollowing
      accounts
      likes_no
      comments_no
      is_like
    }
  }
`;

export const LIKE_COMMENT = gql`
  mutation likeComment ($commentId: String!,
    $type: LikeType) {
    likeComment (commentId:$commentId, type:$type) {
      code
    }
  }
`;

export const HOT_ASSET = gql`
  query hotAsset ($side: Side!,$scope: String!) {
    hotAsset(side: $side, scope: $scope ) {
      ticker
      tradeNo:trades_no
      accountNo: accounts_no
    }
  }
`;

export const TWEET = gql`
  query tweet ($tweetId: String!) {
    tweet (tweetId: $tweetId) {
      post_id
      likes_no
      user_id
      created_at
      dislikes_no
      comments_no
      content
      account_id
      account
      order_id
      order
      ticker
      is_like
      is_dislike
      accountTrading
    }
  }
`;

export const COMMENTS = gql`
  query comments ($tweetId: String!){
    comments(tweetId: $tweetId) {
      user_id
      content
      created_at
      comment_id
      user
      likes_no
      comments_no
      accounts
      is_like
      userFollowing
      comments{
        user_id
        content
        created_at
        content
        comment_id
        userFollowing
        accounts
        user
        is_like
        likes_no
      }

    }
  }
`;

export const RANKING = gql`
  query ranking($type: Side!,$scope: String!,$pageSize: Int!){
    ranking(type:$type, scope: $scope, pageSize: $pageSize){
      account_id
      account
      accountTrading
      followers
      rate_of_return
    }
  }
`;
