import { gql } from '@apollo/client';

export const TRADES = gql`
  query trades ($start: Date!,$end: Date!,$symbol: String,$scope: String!) {
    trades (start:$start,end:$end,symbol: $symbol, scope: $scope) {
      order_id
      symbol
      side
      quantity
      price
      time
      trade_id
      account_id
    }
  }
`;
export const ORDERS = gql`
  query orders ($start: String,$end: String,$scope: String, $accountId: String!,$ticker: String) {
    orders (start:$start,end:$end, scope: $scope, accountId: $accountId, ticker: $ticker) {
      order_id
      ticker
      side
      quantity
      avg_price
      time
      account{
        nickname
        header_url
        account_id
      }
    }
  }

`;

