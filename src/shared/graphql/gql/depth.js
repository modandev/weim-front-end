import { gql } from '@apollo/client';

export const LICK_ANALYSIS = gql`
  mutation likeAnalysis ($analysisId: String!,$type:LikeType) {
    likeAnalysis(analysisId: $analysisId,type:$type) {
      code
    }
  }
`;

export const ANALYSIS_COMMENT = gql`
  mutation analysisComment ($id: ID!,$content: String!,$type:AnalysisCommentType!){
    analysisComment (id: $id, content: $content, type: $type){
      code

    }
  }
`;

export const ANALYSIS_LIKE_COMMENT = gql`
  mutation likeAnalysisComment ($commentId: String!,
    $type: AnalysisLikeType) {
    likeAnalysisComment (commentId:$commentId, type:$type) {
      code
    }
  }
`;

export const ANALYSIS = gql`
  query analysisDetail ($analysisId: String!) {
    analysisDetail (analysisId: $analysisId) {
      post_id
      likes_no
      user_id
      comments_no
      content
      account_id
      account
      ticker
      views_no
      title
      is_like
      summary
      prediction_price
      prediction_days
      prediction_side
      created_at
      published_at
      accountTrading
    }
  }
`;

export const ANALYSIS_COMMENTS = gql`
  query analysisComments ($analysisId: String!, $sort: AnalysisCommentSortType, $cursor: String){
    analysisComments(analysisId: $analysisId, sort: $sort, cursor: $cursor) {
      user_id
      content
      created_at
      comment_id
      user
      userFollowing
      accounts
      likes_no
      comments_no
      is_like
      comments{
        user_id
        accounts
        content
        created_at
        content
        comment_id
        likes_no
        is_like
        user
        userFollowing
      }
    }
  }
`;

export const HOST_ANALYSIS = gql`
  query hostAnalysis ( $accountId: String!, $ticker: String!$size: String, $cursor: String) {
    hostAnalysis (size: $size, cursor: $cursor, accountId: $accountId, ticker: $ticker){
      hits{
        post_id
        time
        title
        views_no
        likes_no
        comments_no
        status
        prediction_side
        summary
      }
      cursor
    }
  }
`;

export const TRENDING_ANALYSIS = gql`
  query trendingAnalysis {
    trendingAnalysis {
      post_id
      detail{
        title
        post_id
        likes_no
        comments_no
        account_id
        user_id
        prediction_side
        ticker
        account
      }
    }
  }`;
