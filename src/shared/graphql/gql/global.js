import { gql } from '@apollo/client';

export const search = gql`
  query search($q: String!) {
    search (q: $q) {
      accounts {
        account_id
        nickname
        user_id
      }
      tickers {
        ticker
        tickerDetail
      }
      users {
        nickname
        uid
        id
      }
    }
  }
`;

export const searchHistory = gql`
  query searchHistory {
    searchHistory {
      word
    }
  }
`;

export const deleteSearchHistory = gql`
  mutation deleteSearchHistory {
    deleteSearchHistory {
      code
      message
    }
  }
`;

export const searchHot = gql`
  query searchHot {
    searchHot {
      word
    }
  }
`;

export const commitSearchWord = gql`
  mutation commitSearchWord($word: String!) {
    commitSearchWord(word: $word) {
      code
      message
    }
  }
`;

export const createFile = gql`
  mutation createFile($file: Upload!) {
    createFile(upload: $file) {
      
        name
        url
    }
  }
`;


