import { gql } from '@apollo/client';

export const TICKER = gql`
  query ticker($ticker: String!) {
    ticker(ticker: $ticker) {
      key
      name
      symbol
      exchange
      is_watch
      country_code
      currency
    }
  }
`;

export const TICKERS = gql`
  query tickers($type: TickerType, $accountId: String!) {
    tickers(type: $type, accountId: $accountId)
  }
`;

export const TICKERS_INFO = gql`
  query tickersDetail($type: TickerType, $accountId: String!) {
    tickersDetail(type: $type, accountId: $accountId){
      ticker
      tickerInfo{
        currency
      }
    }
  }
`;

export const TICKERDETAIL = gql`
  query tickerDetail ($ticker: String!) {
    tickerHighLight (ticker: $ticker){
      pe_ratio
      dividend_yield
      high
      low
      close
      open
      high_52_wk
      low_52_wk
      mkt_cap
    }
    ticker(ticker: $ticker) {
      key
      name
      symbol
      exchange
      is_watch
      country_code
    }

  }
`

export const TICKERACCOUNTS = gql`
  query tickerAccounts($ticker: String!){
    tickerAccounts(ticker: $ticker) {
      cursor
      hits{
        account_id
        accountTickerInfo{
          positions_no
          ror
          market_value
          nav
          unrealized_pl
          analysis_no
          tweets_no
        }
        account{
          nickname
          header_url
          description
          uid
          account_id
          firstname
          lastname
          broker
          email
          status
          user_id
          nickname_update_counter
          no_of_followers
          is_followed
        }
      }
    }
  } 
`
