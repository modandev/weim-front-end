import { gql } from '@apollo/client';

export const FOLLOWS = gql`
  query followers ($userId: String, $cursor: String, $size: String){
    followers (userId: $userId, cursor: $cursor, size: $size) {
      cursor
      hits{
        account_id
        account{
          account_id
          no_of_followers
          nickname_update_counter
          header_url
          description
          status
          is_followed
          nickname
          uid
          user_id
          tweets_no
          analysis_no
        }
        accountTrading{
          positions_no
          ror
        }
      }
    }
  }
`;

export const FOLLOW = gql`
  mutation follow ($accountId: String!) {
    follow (accountId: $accountId) {
      code
    }
  }`;
export const UN_FOLLOW = gql`
  mutation unfollow ($accountId: String!) {
    unfollow (accountId: $accountId) {
      code
    }
  }
`;

export const COMMIT_WATCH_LIST = gql`
  mutation commitWatchlist ($ticker: String) {
    commitWatchlist (ticker: $ticker) {
      code
    }
  }`;


export const DELETE_WATCH_LIST = gql`
  mutation deleteWatchlist ($ticker: String) {
    deleteWatchlist(ticker: $ticker) {
      code
    }
  }
`;

export const WATCHLIST = gql`
  query watchlist {
    watchlist {
      ticker
    }
  }
`;

export const FANS = gql`
  query fans ($accountId: String!,$cursor: String, $size: String) {
    fans (accountId: $accountId, cursor: $cursor, size: $size) {
      cursor
      hits{
        user_id
        userInfo
        accounts{
          nickname
          account_id
        }
        userFollowing{
          following_no
        }
      }
    }
  }
`;
