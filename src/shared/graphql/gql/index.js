import * as UserGQL from './user';
import * as GlobalGQL from './global';
import * as TradeGQL from './trade';

export default {
  ...UserGQL,
  ...GlobalGQL,
  ...TradeGQL,
};
