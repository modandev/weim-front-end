import React from 'react';
import { Redirect } from 'umi';
import { connect } from 'dva';

export default connect(
  ({ user }) => ({
    currentUser: user.currentUser,
  }),
)(({ currentUser, children }) => {

  if (currentUser && currentUser.uid) {
    return <div>{children}</div>;
  } else {
    return <Redirect to="/login?" />;
  }
});
