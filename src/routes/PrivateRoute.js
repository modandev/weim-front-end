import React from 'react';
import { Redirect } from 'umi';
import { connect } from 'dva';

const defaultRoutes = {
  'accountConnected': connect(
    ({ user }) => ({
      active: user.userStatus.active,
      isLogin: user.userStatus && user.userStatus.uid,
      signUpRedirect: user.userStatus.signup_redirect,
      userStatus: user.userStatus,
    }))(({ active, children, signUpRedirect, userStatus, isLogin }) => {
    if (isLogin) {
      return <Redirect to={`/manager_intro`} />;
    }

    if (active) {
      return children;
    }

    if (signUpRedirect === 'CONNECT_UID') {
      return <Redirect to="/connect_account/add_IB" />;
    }

    if (signUpRedirect === 'ACCT_PENDING_INFO') {
      if (localStorage.getItem('skipAccountProfile')) {
        return children;
      }
      return <Redirect to={`/connect_account/account_profile/${userStatus.account_pending_info.account_id}`} />;
    }

    if (signUpRedirect === 'ACCT_CONNECT_FAIL' || signUpRedirect === 'BINDING_IN_PROGRESS') {
      const AccountId = userStatus.account_bind_in_progress ? userStatus.account_bind_in_progress.account_id : userStatus.account_bind_failure.account_id;
      return <Redirect to={`/connect_account/status/${AccountId}`} />;
    }

    return <Redirect to={`/manager_intro`} />;
  }),
};

export default ({ children, route }) => {
  const { authority = [] } = route;
  for (let key of authority) {
    const Component = defaultRoutes[key];
    if (Component) {
      return <Component>{children}</Component>;
    }
  }
  return children;
}
