import routes from './';

const PARAMS_SYMBOL = ':';
const PATH_SEPARATOR = '/';

const routeParser = (Routes, routesList = {}) => {
  Routes.forEach(({ path = '/404', routes: childRoutes, component, ...rest }) => {
    if (childRoutes && childRoutes.length) {
      routeParser(childRoutes, routesList);
    } else {
      if (path.includes(PARAMS_SYMBOL)) {
        routesList[
          path
           .split(PATH_SEPARATOR)
           .filter(i => !i.includes(PARAMS_SYMBOL))
           .join(PATH_SEPARATOR)
        ] = {
          ...rest,
          originPath: path
        }
      } else {
        routesList[path] = rest;
      }
    }
  });

  return routesList;
}

const getRoutes = () => routeParser(routes);

export default getRoutes;
