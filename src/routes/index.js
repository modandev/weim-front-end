// 注意，相对路径基于 /pages
module.exports = [
  {
    path: '/user',
    routes: [
      {
        name: 'oauth',
        path: `/user/oauth/:thirdParty(linkedin|google|facebook|twitter|wechat)`,
        component: './user/Auth',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/',
    component: '../layouts/SecurityLayout',
    routes: [
      {
        path: '/',
        component: '../layouts/BasicLayout',
        routes: [
          { exact: true, path: '/', redirect: '/home/stream' },
          {
            path: '/about-us',
            component: './AboutUs',
          },
          {
            path: '/privacy-notice',
            component: './PrivacyNotice',
            authority: ['user', 'accountConnected'],
            Routes: ['./src/routes/PrivateRoute.js'],
          },
          {
            path: '/user-terms',
            component: './UserTerms',
            authority: ['user', 'accountConnected'],
            Routes: ['./src/routes/PrivateRoute.js'],
          },
          {
            name: 'log-in',
            path: '/login',
            component: './user/SignIn',
          },
          {
            name: 'sign-up',
            path: '/join',
            component: './user/SignUp',
          },
          {
            name: 'email-sign-up',
            path: '/signup',
            component: './user/Email/SignUp',
          },
          {
            name: 'email-sign-in',
            path: '/signin',
            component: './user/Email/SignIn'
          },
          {
            name: 'email-reset-password',
            path: '/reset-password',
            component: './user/Email/ResetPassword'
          },
          {
            name: 'email-reset-password-tips',
            path: '/reset-password/:type/:status',
            component: './user/Email/ResetPasswordTips'
          },
          {
            name: 'change-password',
            path: '/parse/apps/:id/:method',
            component: './user/Email/ChangePassword'
          },
          {
            path: '/dashboard',

            routes: [
              {
                name: 'user details',
                path: '/dashboard/:uid/:subdirectories?/:subdirectoriesId?',
                authority: ['user', 'accountConnected'],
                Routes: ['./src/routes/PrivateRoute.js'],
                component: './user/Panel',
              },
              {
                component: './404',
              },
            ],
          },
          {
            path: '/u',
            name: 'user',
            routes: [
              {
                component: './404',
              },
            ],
          },
          {
            name: 'connect IB',
            path: '/connect-IB',
            authority: ['user'],
            component: './user/ConnectIB',
          },
          {
            name: 'manager intro',
            path: '/manager_intro',
            component: './ManagerIntro',
          },
          {
            name: 'account connect IB',
            path: '/connect_account',
            authority: ['user'],
            component: './ConnectAccount',
            routes:[
              { path: '/connect_account/add_IB',
                component: './ConnectAccount/ConnectIB'
              },
              { path: '/connect_account/status/:accountId',
                component: './ConnectAccount/Status',
              },
              { path: '/connect_account/account_profile/:accountId',
                component: './ConnectAccount/AccountProfile',
              },
            ]
          },

          // 用于展示当前所有图标的路由
          {
            path: '/icons',
            component: './Icons',
          },

          {
            name: 'personal',
            path: '/personal/:subdirectories?',
            authority: ['user', 'accountConnected'],
            Routes: ['./src/routes/PrivateRoute.js'],
            component: './user/Me',
          },

          {
            name: 'single fans',
            path: '/followers/:uid/:subdirectories?/:subdirectoriesId?',
            authority: ['user', 'accountConnected'],
            Routes: ['./src/routes/PrivateRoute.js'],
            component: './user/Fans',
          },

          {
            name: 'analysis',
            path: '/analysis',
            authority: ['user', 'accountConnected'],
            Routes: ['./src/routes/PrivateRoute.js'],
            routes:[{
              name: 'analysis',
              path: '/analysis/box/:accountId?/:ticker?/:analysisId?',
              component: './user/Analysis',
            }]
          },

          {
            path: '/analyses',
            name: 'depthAnalysis',
            authority: ['user', 'accountConnected'],
            Routes: ['./src/routes/PrivateRoute.js'],
            wrappers: [
              '@/wrappers/auth',
            ],
            routes: [
              {
                path: '/analyses/edit',
                exact: true,
                component: './user/Analysis',
              },
              {
                path: '/analyses/:id',
                name: 'depthanalysisdetail',
                Routes:[ './src/layouts/DepthAnalysisLayout'],
                component: './Home/DepthAnalysis',
              },
              {
                name: 'analysis',
                path: '/analyses/edit/:accountId?/:ticker?/:analysisId?',
                component: './user/Analysis',
                exact: true
              },
              {
                name: 'analysis preview',
                path: '/analyses/preview/:analysisId',
                authority: ['user', 'accountConnected'],
                Routes: ['./src/routes/PrivateRoute.js'],
                exact: true,
                component: './user/Analysis/Preview',
              },

            ],
          },

          {
            name: 'asset',
            path: '/asset/:symbol',
            authority: ['user', 'accountConnected'],
            redirect: '/asset/:symbol/account',
            Routes: ['./src/routes/PrivateRoute.js'],
          },

          {
            name: 'asset',
            path: '/asset/:symbol/:tabType',
            authority: ['user' , 'accountConnected'],
            component: './Asset',
            Routes: ['./src/routes/PrivateRoute.js'],
          },

          {
            name: 'contactus',
            path: '/contactus',
            component: './ContactUs',
          },

          {
            path: '/home',
            component: '../layouts/HomeLayout/Home',
            Routes: ['./src/routes/PrivateRoute.js'],
            authority: ['user', 'accountConnected'],
            routes: [
              { path: '/home',
                redirect: '/home/stream',
              },
              {
                path: '/home/stream',
                name: 'homeStream',
                component: './Home/Stream',
              },
              {
                path: '/home/tweetdetail/:id',
                name: 'tweet-detail',
                component: './Home/TweetDetail',
              },
              {
                component: './404',
              },
            ],
          },

          {
            component: './404',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    component: './404',
  },
];
