import React from 'react';
import { ApolloProvider } from '@apollo/client';
import { client } from 'shared/graphql/client';

export const dva = {
  config: {
    onError(err) {
      err.preventDefault();
    },
  },
};

export function rootContainer(container) {
  return React.createElement(ApolloProvider, { client }, container);
}
