import styles from './styles.less';
import avatar from '@/assets/common/ic_touxiang.png';
import icImage from '@/assets/common/ic_size_of_fund.png';
import cumImage1 from '@/assets/common/img_cum1.png';
import cumImage2 from '@/assets/common/img_cum2.png';


export default () => {
  return <div className={styles.manager_intro}>
    <section className={styles.ad}></section>

    <section className="type-area">
      <div className={styles.action}>
        <sup>“</sup>
        <p>
           WEIM provides a high quality community for each member contributes to as well as benefits from such transparent and free flow of ideas
        </p>
        <sub>”</sub>
    </div>
    </section>

    <section className={styles.program}>
      <div className={`type-area ${styles.contain}`}>

        <div className={styles.title}>
          Qualified Manager Program
        </div>

        <div className={styles.sub_title}>
          WEIM is looking for talented, open-minded and ambitious individuals that can build partnerships with.
        </div>

        <div className={styles.row}>
          <div className={styles.message}>
            <div>
              -- $100k will be provided for the qualified manager.
            </div>
            <div style={{ color: '#2F7354', marginTop: '25px' }}>
              *All returns belongs to the Qualified Manager at this stage
            </div>
          </div>

          <div className={styles.space}></div>

          <div className={styles.message}
               style={{ paddingLeft: '55px' }}>
            <div className={styles.title}>
             How to become a Qualified Manager
            </div>
            <div>
              * Resume <br />
              * Past Investment performance <br />
              * Investment philosophy <br />
              * 3 sample analyses <br />
              * Youtube, LinkedIn, Facebook and other social media profiles
            </div>
          </div>
        </div>
      </div>
    </section>

    <section className={styles.weather}>
      <div className={`type-area ${styles.contain}`}>
        <p>
          <div className={styles.title}>
            Wealth Manager Program
          </div>
          <div className={styles.sub_title}>
          *Qualified Managers can be upgraded to Wealth Management <br />
            Manager based on their performance
          </div>
          <div className={styles.detail}>
            -- $1M will be provided for the wealth manager.
            <br />
            <br />
           *WEIM provides instant access to capital from family offices around the world run by open-minded teams
          </div>
        </p>

        <img src={require('@/assets/common/img_wealth_manager.png')}
             alt="" />



      </div>
    </section>

    <section className={styles.case}>
      <div className={`${styles.contain} type-area`}>
        <div className={styles.title}>
          Discover the Top Alpha Generators
        </div>

        <div className={styles.sub_title}>
          -- Transparent data of each manager  <br />
          -- Objective, return based ranking
        </div>

        <ul className={styles.main}>
          <li>
            <div className={styles.avatar}>
              <img src={avatar}
                   alt="" />
                   <div className={styles.name}>
                     Account Name
                   </div>
            </div>

            <div className={styles.row}>
              <div className={styles.chart}>
                 size of fund
                <img src={cumImage1}
                     className={styles.main_img}
                     alt="" />
              </div>

              <div className={styles.size_fund}>
                size of fund
                <img src={icImage}
                     alt="" />
                <span>
                  06/22/20
                </span>
              </div>
            </div>

            <p>
              Risk control philosophy Risk control philosophy Risk control philosophy Risk control philosophyRisk control philosophy Risk control philosophy Risk control philosophy Risk control philosophy
            </p>
          </li>


          <li>
            <div className={styles.avatar}>
              <img src={avatar}
                   alt="" />
              <div className={styles.name}>
                Account Name
              </div>
            </div>
            <div className={styles.row} style={{justifyContent: 'flex-end'}}>
              <div className={styles.size_fund}>
                size of fund
                <img src={icImage}
                     alt="" />
                <span>
                  06/22/20
                </span>
              </div>
              <div className={styles.chart}>
                 size of fund
                <img src={cumImage2}
                     className={styles.main_img}
                     alt="" />
              </div>

            </div>
            <p>
              Risk control philosophy Risk control philosophy Risk control philosophy Risk control philosophyRisk control philosophy Risk control philosophy Risk control philosophy Risk control philosophy
            </p>
          </li>
        </ul>
      </div>
    </section>

    <section className={styles.partners}>
      <div className={`type-area ${styles.contain}`}>
        <div className={styles.title}>
          Our Partners
        </div>

        <div className={styles.img_box}>
          <img src={require('../../assets/common/img_futu.png')}
               alt="" />
          <img src={require('../../assets/common/img_laohu.png')}
               alt="" />
          <img src={require('../../assets/common/img_yintou.png')}
               alt="" />
        </div>
      </div>
    </section>

  </div>;
}
