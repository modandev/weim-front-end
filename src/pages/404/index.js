import { formatMessage } from 'umi-plugin-react/locale';

import styles from './index.less';

export default () => (
  <section className={styles.wrapper}>
    <div className={styles.content}>
      <img src={require('../../assets/common/logo.png')} alt="404 not found" />
      <div className={styles.message}>
        <div className={styles.code}>404</div>
        <div className={styles.text}>{formatMessage({ id: 'common.notFound' })}</div>
      </div>
    </div>
  </section>
);
