import React, { useState, useCallback } from 'react';
import styles from './style.less';
import { formatMessage } from 'umi-plugin-react/locale';
import TextArea from '@atlaskit/textarea';
import Textfield from '@atlaskit/textfield';
import Button from '@/components/Buttons';
import { Icon } from '@/components';
import vc from '@/assets/common/wechat-official-account.png';
import cu from '@/assets/common/customer-service.png';
import { connect } from 'dva';
import { checkEmail } from '@/shared/regexps';

export default connect(
  ({ user }) => {
    return {
      user: user.currentUser,
    };
  },
)(({ user }) => {
  const [content, setContent] = useState('');
  const contentOnChange = useCallback((d) => {
    setContent(d.target.value);
  });
  const [email, setEmail] = useState(user.email);
  const [isEmailFail, setIsEmailFail] = useState(false);
  const [weimId, setId] = useState(user.uid);
  const [isWeimIdFail, setIsWeimIdFail] = useState(false);

  const onEmailChange = useCallback((e) => {
    setEmail(e.target.value);
    setIsEmailFail(!checkEmail(e.target.value) || !e.target.value);
  });

  const onWeimIdChange = useCallback((e) => {
    setId(e.target.value);
    setIsWeimIdFail(!e.target.value);
  });

  return <div className={styles.connect_us}>
    <div className={styles.title}>
      {formatMessage({ id: 'connect.page.title' })}
    </div>

    <div className={styles.form_item}>
      <div className={styles.label}>
        {formatMessage({ id: 'connect.page.Email_address' })}
        <span className={styles.require} />
      </div>
      <div className={`${styles.input_outer} ${isEmailFail ? styles.error : ''}`}>
        <Textfield
          defaultValue={email}
          onChange={onEmailChange}
          local={'en'}
          maxLength={30}
          appearance="none"
          placeholder={''}
        />
      </div>
      <div className={styles.direction}>
        {formatMessage({ id: 'connect.page.Email_address_description' })}
      </div>
    </div>

    <div className={styles.form_item}>
      <div className={styles.label}>
        WEIM ID
        <span className={styles.require} />
      </div>
      <div className={`${styles.input_outer} ${isWeimIdFail ? styles.error : ''}`}>
        <Textfield
          defaultValue={weimId}
          onClick={onWeimIdChange}
          maxLength={30}
          appearance="none"
        />
      </div>
    </div>

    <div className={styles.form_item}>
      <div className={styles.label}>
        {formatMessage({ id: 'connect.page.Description' })}
      </div>
      <div className={styles.input_outer}>
        <TextArea
          appearance="none"
          minimumRows={5}
          maxLength={1000}
          onChange={contentOnChange}
        >
        </TextArea>
        <div className={styles.last}>
        {content ? content.length : 0}/1000
      </div>
      </div>
      <div className={styles.direction}>
        {formatMessage({ id: 'connect.page.Description_description' })}
      </div>
    </div>


    <div className={styles.label}>
        {formatMessage({ id: 'connect.page.Attachments' })}
    </div>

    <ul className={styles.connect_by}>

      <li>
        <div className={styles.add_fill_box}>
          <Icon type="add"
                style={{ width: '20px' }}></Icon>
          <span>{formatMessage({ id: 'connect.page.add_file' })}</span>
        </div>
        <Button
          css={{ width: '144px', height: '40px' }}
          appearance="primary"
          shape="square">
          {formatMessage({ id: 'connect.page.Submit' })}
        </Button>
      </li>

      <li>
        <img
          className={styles.Qr_code}
          src={vc}
          alt="" />
        <div className={styles.label}>
           {formatMessage({ id: 'connect.page.Wechat_official_account' })}
        </div>
      </li>


      <li>
         <img
           className={styles.Qr_code}
           src={cu}
           alt="" />
        <div className={styles.label}>
           {formatMessage({ id: 'connect.page.Customer_service' })}
        </div>
      </li>
    </ul>

  </div>;
});
