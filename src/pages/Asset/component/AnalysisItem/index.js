import React, { useState, useEffect, useCallback, useContext, Fragment, PureComponent, memo } from 'react';
import TweetUser from '@/pages/Home/components/TweetUser';
import StockInfo from '@/pages/Home/components/StockInfo';
import style from './styles.less';
import NewsInBrief from '@/pages/Home/components/NewsInBrief';
import { InteractForDepth } from '@/pages/Home/components/Interact';
import Chart from '@/components/DepthChart';
import AnalysisComments from '@/pages/Home/components/AnalysisComments';
import { stringify } from 'querystring';
import MessageTitle from '@/pages/Home/components/MessageTitle';

export default memo(({ data }) => {
  const [analysis, assetAnalysis] = useState(data);
  const [comments_no, setComments_no] = useState(data.comments_no);
  useEffect(() => {
    assetAnalysis(data);
  }, [data]);
  useEffect(() => {
  }, [isShowComment]);

  const [isShowComment, setIsShowComment] = useState(false);

  const addCommit = useCallback(
    (no) => {
      setComments_no(comments_no + 1);
    },
  );

  const goDetail = useCallback(() => window.open(`/analyses/${analysis.post_id}?${stringify({
    userId: analysis.user_id,
    accountId: analysis.account_id,
    symbol: analysis.ticker.key,
  })}`));

  return (
    <div
      key={analysis.post_id}
      className={style.analysis_item}>

      <div className={style.analysis_main}>
        <div className={style.user_info}>
          <div className={style.flex_content}>
            <TweetUser account={analysis.account}
                       user_id={analysis.user_id} />
          </div>
          <div className={style.line} />
          <div className={style.flex_content}>
            <StockInfo ticker={analysis.ticker} />
          </div>

        </div>

        <div className={style.chart}>
         <Chart analysis={analysis}
                pagetype={'stream'}
                width={627}
                height={226}
                padding={[40, 60, 30, 60]}
                ticker={analysis.ticker.key} />
        </div>

      </div>

      <div style={{ padding: '0 40px' }}>
        <MessageTitle created_at={analysis.created_at}
                      tilte={analysis.title} />
        <NewsInBrief tradeId={analysis.trade_id}
                     onShowMore={goDetail}
                     created_at={analysis.created_at || '2020-08-11T14:21:41.917747Z'}
                     page={analysis.summary} />
      </div>

      <div className={style.line}></div>
      <InteractForDepth data={analysis}
                        comments_no={comments_no}
                        onCommentPress={() => setIsShowComment(!isShowComment)}
                        tweetId={analysis.post_id} />
      {isShowComment ?
        <AnalysisComments
          goDetail={goDetail}
          addCommit={addCommit}
          type='stream'
          post_id={analysis.post_id}
          user_id={analysis.user_id}
          account={analysis.account} /> : null
      }
    </div>);
});


