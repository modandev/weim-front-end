import React, { useCallback, useEffect, memo } from 'react';
import {
  Axis,
  Area,
  Chart,
  Line,
  Annotation,
  Point,
  LineAdvance,
  Tooltip,
} from 'bizcharts';
import { semicolon } from '@/shared/helpers';
import { tickerMarketData } from '@/shared/graphql/gql/user';
import { useQuery } from '@apollo/react-hooks';
import styles from './styles.less';
import { formatMessage } from 'umi-plugin-react/locale';
import { Icon } from '@/components';
import dayjs from 'dayjs';
import Spinner from '@atlaskit/spinner';

const { DataMarker } = Annotation;

export default memo(({ ticker, post_id }) => {

  const { data: { tickerMarketData: tickerData = [] } = {} } = useQuery(tickerMarketData,
    {
      skip: !ticker,
      variables: {
        ticker: ticker,
        start: dayjs().subtract(3, 'month').format('YYYY-MM-DDT00:00:00+0000'),
        end: dayjs().format('YYYY-MM-DDT00:00:00+0000'),
      },
    });

  useEffect(() => {
  }, [tickerData, ticker]);

  return <div className={styles.chart_box}>
    {tickerData.length ?
      <Chart
        id={post_id}
        errorContent=""
        autoFit
        defaultInteractions={[]}
        padding={[70, 60, 50, 60]}
        animate={false}
        height={320}
        scale={{
          close: {
            type: 'linear',
            nice: 'true',
          },
          date: {
            type: 'time',
            nice: true,
            range: [0, 0.95],
            tickCount: 6,
          },
        }}
        placeholder={(
          <div className={styles.empty}>
            <h6>{formatMessage({ id: 'message.chart.empty' })}</h6>
          </div>
        )}
        onGetG2Instance={g2Chart => {
          g2Chart.animate(false);

          // 缩放交互
          g2Chart.interaction('drag', {
            type: 'XY',
          }).interaction('zoom', {
            type: 'XY',
          });
        }}
        data={tickerData}>
        <Axis
          name="date"
          label={{
            formatter(text) {
              const [year, month, day] = text.split('-');

              return `${+year}-${+month}-${+day}`;
            },
          }}
        />
        <Axis
          name="close"
          label={{
            formatter(text) {
              return semicolon(text);
            },
          }}
        />

		<LineAdvance animate={false}
                 area
                 position="date*close"
    />

    <Tooltip
      title="date"
      showCrosshairs
      marker={{ background: 'transparent' }}
    >
        {(title, [item]) => (<Tol title={title}
                                  item={item}></Tol>
        )}
      </Tooltip>
	</Chart> : <Spinner />}
    </div>;
});

const Tol = memo(({ title, item }) => {
  return <div className={styles.tooltip}>
              <div className={styles.title}>{title.split('T')[0]}</div>
              <div className={styles.value}>{semicolon((+item.value).toFixed(2))}</div>
            </div>;
});


