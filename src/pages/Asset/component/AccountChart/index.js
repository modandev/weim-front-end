import React from 'react';
import {
  Chart,
  PieChart,
  Axis,
  Coordinate,
  Interaction,
  Legend,
} from 'bizcharts';




export default ({ nav, markerValue }) => {
  const cols = {
    percent: {
      formatter: val => {
        val = val * 100 + '%';
        return val;
      },
    },
  };

  const data = [
    {
      type: 'markerValue',
      value: Math.max( 0.01 ,markerValue/nav )* nav,
    },
    {
      type: 'nav',
      value: nav - markerValue,
    },
  ];


  return (
    <PieChart
      data={data}
      radius={1}
      width={250}
      height={250}
      angleField='value'
      colorField='type'
      color={['rgba(81, 133, 110, 1)', 'rgba(227, 245, 237, 1)']}
      legend={{ visible: false }}
      tooltip={{ visible: false }}
      label={{ visible: false }}
    >

    </PieChart>
  );
}

