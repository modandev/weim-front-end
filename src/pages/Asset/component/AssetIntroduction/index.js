import React, {} from 'react';
import style from './style.less';
import { TICKERDETAIL } from '@/shared/graphql/gql/ticker';
import { useQuery } from '@apollo/react-hooks';
import { FormattedMessage, formatMessage } from 'umi-plugin-react/locale';
import TickerChart from '../TickerChart';
import { StockPage } from '@/components/DefaultPage';
import { semicolon } from '@/shared/helpers';

const tickerColumn = [
  {
    title: 'Open',
    key: 'open',
  },
  {
    title: 'MktCap',
    key: 'mkt_cap',
  },
  {
    title: 'PrevClose',
    key: 'close',
  },
  {
    title: 'High',
    key: 'high',
  },
  {
    title: 'PERatio',
    key: 'pe_ratio',
  },

  {
    title: '52-WkHigh',
    key: 'high_52_wk',
  },
  {
    title: 'Low',
    key: 'low',
  },
  {
    title: 'DivYield',
    key: 'dividend_yield',
  },
  {
    title: '52-WkLow',
    key: 'low_52_wk',
  },
];

export default (props) => {
  const { data: { tickerHighLight = {}, ticker = {} } = {}, loading } = useQuery(TICKERDETAIL, {
    variables: {
      ticker: props.match.params.symbol,
    },
  });

  if (loading) {
    return <StockPage />;
  }

  return <div className={style.asset_introduction}>
     <>
      <div className={style.title}>{ticker.name}</div>
      <div className={style.subtitle}> </div>
      <div className={style.chart_outer}>
        <TickerChart ticker={props.match.params.symbol} />
      </div>
    </>
    <ul className={style.ticker_info_box}>
      {tickerColumn.map(item => {
        return (
          <li key={item.key}>
            <div className={style.ticker_info_key}>
              {formatMessage({ id: 'stock.' + item.title })}:
            </div>

            <div className={style.ticker_info_value}>
              {tickerHighLight[item.key] < 0 ? 'N/A' : semicolon(tickerHighLight[item.key])}
            </div>
        </li>);
      })}

    </ul>

  </div>;
}
