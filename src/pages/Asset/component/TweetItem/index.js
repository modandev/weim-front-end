import React, { useState, useEffect, useCallback, useContext, Fragment, PureComponent, memo } from 'react';
import TweetUser from '@/pages/Home/components/TweetUser';
import StockInfo from '@/pages/Home/components/StockInfo';
import style from './styles.less';
import NewsInBrief from '@/pages/Home/components/NewsInBrief';
import { InteractForStream } from '@/pages/Home/components/Interact';
import Chart from '@/pages/Home/components/PieChart';
import TweetComments from '@/pages/Home/components/TweetComments';
import { router } from 'umi';
import MessageTitle from '@/pages/Home/components/MessageTitle';

export default memo(({ data }) => {
  const [tweet, setTweet] = useState(data);
  const [comments_no, setComments_no] = useState(data.comments_no);
  useEffect(() => {
    setTweet(data);
  }, [data]);
  const [isShowComment, setIsShowComment] = useState(false);
  useEffect(() => {
  }, [isShowComment]);

  const addCommit = useCallback(
    (no) => {
      setComments_no(comments_no + 1);
      // setTweet({ ...tweet, comments_no: tweet.comments_no + 1 });
    },
  );
  const goDetail = useCallback(() => router.push(`/home/tweetdetail/${tweet.post_id}?userId=${tweet.user_id}`));
  return (
    <div
      key={tweet.post_id}
      className={style.analysis_item}>

      <div className={style.analysis_main}>

        <div className={style.user_info}>

          <div className={style.flex_content}>
             <TweetUser account={tweet.account}
                        user_id={tweet.user_id} />
          </div>
          <div className={style.line} />
          <div className={style.flex_content}>
            <StockInfo ticker={tweet.ticker} />
          </div>

        </div>
        <div className={style.chart}>
          <Chart order={tweet.order}
                 post_id={tweet.post_id}
                 height={226}
                 padding={[40, 30, 30, 60]}/>
        </div>
      </div>

      <div style={{ padding: '10px 40px' }}>
        <MessageTitle created_at={tweet.created_at} />
        <NewsInBrief tradeId={tweet.trade_id}
                     created_at={tweet.created_at || '2020-08-11T14:21:41.917747Z'}
                     page={tweet.content} />
      </div>

      <div className={style.line}></div>
      <InteractForStream data={tweet}
                         comments_no={comments_no}
                         onCommentPress={() => setIsShowComment(!isShowComment)}
                         tweetId={tweet.post_id} />
      {isShowComment ?
        <TweetComments
          goDetail={goDetail}
          addCommit={addCommit}
          type='stream'
          tweetId={tweet.post_id}
          user_id={tweet.user_id}
          account={tweet.account} /> : null
      }
    </div>);
});


