import React, { useEffect, useState, useCallback } from 'react';
import styles from './styles.less';
import { connect } from 'dva';
import { formatMessage } from 'umi-plugin-react/locale';
import FollowButton from '@/components/FollowButton';
import Button from '@/components/Buttons';
import { router } from 'umi';
import qs from 'querystring';
import { STREAM } from '@/shared/graphql/gql/tweet';
import { useQuery } from '@apollo/react-hooks';
import { AccountCard, InlineDialog } from '@/components';

const css = {
  flex: 1,
  padding: '0',
  height: '50px',
  display: 'flex',
  justifyContent: 'center',
  alignItem: 'center',

};

export default ({ account }) => {
  const {data, loading} = useQuery(STREAM);

  const [userInfo, setUserInfo] = useState(null);

  const goDetail = useCallback(() => {
    window.open(`/dashboard/${account.user_id}/accounts/${account.account_id}`);
  });

  useEffect(() => {
    setUserInfo(account);
  }, [account]);

  if (!userInfo) {
    return <div></div>;
  }

  return <div className={styles.avatar_cart}>
    <InlineDialog
      isOpen={false}
      placement="auto-start"
      content={<AccountCard style={{ width: '300px' }}
                            account={account} /> }
      delay={500}>
        <div className={styles.avatar}
             onClick={goDetail}>
            {userInfo.header_url ? <img
                className={styles.avatar_img}
                src={userInfo.header_url}
                alt="" /> :
              <div className={styles.default_avatar}>
                <span>
                  {userInfo.nickname ? (userInfo.nickname).slice(0, 1) : null}
                </span>
              </div>}
          </div>
    </InlineDialog>
    <div className={styles.username}>
      <InlineDialog
        isOpen={false}
        placement="auto-start"
        content={<AccountCard style={{ width: '300px' }}
                              account={account} /> }
        delay={500}>
        <Button
          css={{ maxWidth: '100%', overflow: 'hidden' }}
          onClick={goDetail}
          appearance="inline">
          {userInfo.nickname}
        </Button>
      </InlineDialog>
    </div>

    <div className={styles.funs_no}>
      {formatMessage({ id: 'home.user.Follows' },
        { count: userInfo.no_of_followers, s: userInfo.no_of_followers > 1 ? 's' : '' })}
    </div>

    <div className={styles.follow_box}>
      <FollowButton
        userId={userInfo.user_id}
        is_followed={userInfo.is_followed}
        account_id={userInfo.account_id} />
    </div>

  </div>;
};
