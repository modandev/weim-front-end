import React, { useState, useCallback, useEffect, useRef } from 'react';
import style from './style.less';


const Tab = ({ tab, currentIndex = 0, onChange }) => {
  const [activeIndex, setActiveIndex] = useState(currentIndex);
  const [isTop, setIsTop] = useState(false);
  const onPress = useCallback((index) => {
    setActiveIndex(index);
    onChange && onChange(index);
  });
  const space = useRef(null);


  useEffect(() => {

    const onScroll = () => {
      const bottom = space.current.getBoundingClientRect().bottom;
      setIsTop(bottom < 0);
    };

    document.addEventListener('scroll', onScroll);

    return () => {
      document.removeEventListener('scroll', onScroll);
    };
  });


  return (
    <>
      <div ref={space}
           className={style.space}></div>
      <nav className={`${style.tab} ${isTop ? style.over_screen : ''}`}>
        <ul className={style.nav}>
          {tab.map((item, index) => <li key={item.title}
                                        className={`${style.nav_item} ${index === activeIndex ? style.active : ''}`}>
            <span onClick={onPress.bind(null, index)}
                  className={style.button}>{item.title}</span>
            {index === activeIndex ? <div className={style.action_slider} /> : null}
          </li>)}
        </ul>
      </nav>
      </>
  );
};

const Slider = ({ length, index }) => {

};

Tab.defaultProps = {
  tab: [],
};

export default Tab;
