import React, { useState, useCallback, useEffect, memo } from 'react';
import debounce from 'debounce';
import { useDispatch } from 'dva';

export default ({ variables, type, dispatchVariables = {} }) => {
  const dispatch = useDispatch();
  const [cursor, setCursor] = useState('');
  const [loading, setLoading] = useState(true);
  const [isNoMore, seIsNoMore] = useState(false);

  const fetchData = useCallback(async () => {
    setLoading(true);
    const params = {
      size: '5',
      ...variables,
    };
    if (cursor) params.cursor = cursor;
    const data = await dispatch({
      ...dispatchVariables,
      type,
      variables: params,
    });

    seIsNoMore(data.isNoMore)
    setCursor(data.cursor);
    setLoading(false);
  });


  const loadMore = useCallback(
    debounce(
      () => {
        fetchData();
      }, 200,
    ),
  );

  const onScroll = (e) => {
    if (!loading && !isNoMore && e.target.scrollingElement.scrollHeight <= document.documentElement.clientHeight + e.target.scrollingElement.scrollTop) {
      loadMore();
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(
    () => {
      document.addEventListener('scroll', onScroll);
      return () => {
        document.removeEventListener('scroll', onScroll);
      };
    });

  return [loading, isNoMore];
}
