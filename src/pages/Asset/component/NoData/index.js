import noAccount from '@/assets/common/img_no_account.png';
import onAnalysis from '@/assets/common/img_no_analysis.png';
import noPost from '@/assets/common/img_no_post.png';
import styles from './styles.less';
import { formatMessage } from 'umi-plugin-react/locale';


export default ({ type }) => {

  return <div className={styles.no_data_box}>
    <img src={type === 'account' ? noAccount : type === 'post' ? noPost : onAnalysis} />
    <span>
      {formatMessage({ id: 'home.no' + type })}
    </span>
  </div>;
}
