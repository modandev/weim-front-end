import React from 'react';
import useFetchData from '@/pages/Asset/component/useFetchData';
import style from './style.less';
import Spinner from '@atlaskit/spinner';
import { connect } from 'dva';
import AnalysisItem from '../component/AnalysisItem';
import NoData from '@/pages/Asset/component/NoData';

export default connect(
  ({ assets }) => {
    return {
      assetAnalysis: assets.assetAnalysis,
    };
  },
)(({ assetAnalysis, match }) => {
  let { symbol } = match.params;
  const [loading, isNoMore] = useFetchData({
    variables: {
      ticker: symbol,
      postType: 'ANALYSIS',
      size: '3',
    },
    type: 'assets/stream',
    dispatchVariables: {
      dataType: 'Analysis',
    },
  });

  return <div className={style.analysis}>
    {assetAnalysis.map(item => <AnalysisItem data={item}
                                             key={item.post_id} />)}
    {loading ? <div className={style.spinner_box}><Spinner /> </div> : null}
    {
      !loading && !assetAnalysis.length ? <NoData type="analysis" /> : null
    }
  </div>;
});
