import React, { useCallback } from 'react';
import AssetIntroduction from './component/AssetIntroduction';
import Tab from './component/Tab';
import { router } from 'umi';
import Account from './Account';
import Analysis from './Analysis';
import Posts from './Posts';
import { formatMessage } from 'umi-plugin-react/locale';


const tabColumn = [
  { title: formatMessage({id:'home.Account'}), key: 'account', component: Account },
  { title: formatMessage({id:'home.user.Analysis'}), key: 'analysis', component: Analysis },
  { title: formatMessage({id:'home.user.Posts'}), key: 'post', component: Posts },
];
export default (props) => {
  const { currentUser, match } = props;
  let { symbol, tabType } = match.params;

  const onChange = useCallback((index) => {
    router.replace(`/asset/${symbol}/${tabColumn[index].key}`);
  });

  const getCurrentIndex = useCallback(() => {
    const currentIndex = tabColumn.findIndex(item => item.key === tabType);
    return currentIndex === -1 ? 0 : currentIndex;
  });


  return <>
    <AssetIntroduction {...props} />
    <Tab tab={tabColumn}
         onChange={onChange}
         currentIndex={getCurrentIndex()} />
    {tabColumn.map(
      ({ key, component: Component }) => {
        return tabType === key ? <Component key={key} {...props}
                                            tabType={tabType} /> : null;
      },
    )}
  </>;
}
