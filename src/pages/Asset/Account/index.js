import React, { useEffect, useCallback, useState } from 'react';
import style from './style.less';
import AvatarCart from '../component/AvatarCart';
import AccountChart from '../component/AccountChart';
import { formatMessage } from 'umi-plugin-react/locale';
import useFetchData from '../component/useFetchData';
import { connect } from 'dva';
import Spinner from '@atlaskit/spinner';
import { semicolon } from '@/shared/helpers';
import NoData from '../component/NoData';

export default connect(
  ({ assets }) => {
    return {
      accounts: assets.assetAccount,
    };
  },
)(({ match, accounts, dispatch }) => {
  let { symbol } = match.params;
  const [loading] = useFetchData({ variables: { ticker: symbol }, type: 'assets/tickerAccounts' });

  return <div className={style.account}>
    {accounts.map(item => <div
      key={item.account_id}
      className={style.account_item}>

      <div className={style.user_info}>
        <AvatarCart account={item.account} />
      </div>

      <div className={style.chart}>
        <AccountChart
          nav={item.accountTickerInfo.nav}
          markerValue={item.accountTickerInfo.market_value}
        />
         <UserTickerInfo {...item} />
      </div>
    </div>)}
    {loading ? <div className={style.spinner_box}><Spinner /> </div> : null}
    {!loading && !accounts.length ? <NoData type="account" /> : null}
  </div>;
});

const userTickerInfoColumn = [
  {
    title: 'mktVI', key: 'market_value', render: (text, data) => {
      return (data.nav ? (data.market_value / data.nav * 100).toFixed(2) : 0) + '%';
    },
  },
];

const positionTickerInfoColumn = [
  {
    title: 'Unrlzd', key: 'unrealized_pl', render: (text, data) => {
      return semicolon((text * 100).toFixed(2)) + '%';
    },
  },
  { title: 'Analyses', key: 'analysis_no' },
  { title: 'Posts', key: 'tweets_no' },
];

const UserTickerInfo = ({ tickerHighLight, accountTickerInfo }) => {
  const getValue = useCallback(({ key, render }) => {
    if (render) {
      return render(accountTickerInfo[key], accountTickerInfo);
    }
    return semicolon(accountTickerInfo[key]);
  });

  return <div className={style.info}>
    <div className={style.line}>
      <div className={style.circle}></div>
    </div>
    {userTickerInfoColumn.map(item => <Item key={item.key} {...item}
                                            value={getValue(item)} />)}

    <div style={{ width: '100%', height: '16px' }} />
    {positionTickerInfoColumn.map(item => <Item key={item.key} {...item}
                                                value={getValue(item)}
    />)}
  </div>;
};

const Item = ({ title, value }) => {
  return <div className={style.account_info_box}>
    <div className={style.account_info_key}>
      {formatMessage({ id: 'assets.account.' + title })}:
    </div>

     <div className={style.account_info_value}>
      {value}
    </div>

  </div>;
};
