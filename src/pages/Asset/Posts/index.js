import React from 'react';
import useFetchData from '@/pages/Asset/component/useFetchData';
import style from './style.less';
import Spinner from '@atlaskit/spinner';
import { connect } from 'dva';
import TweetItem from '../component/TweetItem'
import NoData from '@/pages/Asset/component/NoData';

export default connect(
  ({ assets }) => {
    return {
      assetPosts: assets.assetPosts,
    };
  },
)(({ assetPosts, match }) => {
  let { symbol } = match.params;
  const [loading, isNoMore] = useFetchData({
    variables: {
      ticker: symbol,
      postType: 'TWEET',
      size: '3',
    },
    type: 'assets/stream',
    dispatchVariables: {
      dataType: 'Posts',
    },
  });

  return <div className={style.analysis}>
    {assetPosts.map(item => <TweetItem data={item} key={item.post_id}/>)}
    {loading ? <div className={style.spinner_box}><Spinner /> </div> : null}
    {!loading && !assetPosts.length ? <NoData type="post" /> : null}

  </div>;
});
