import { useEffect } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';
import Subtitle from './Subtitle';
import List from './List';

import styles from './index.less';

export default () => {
  useEffect(() => {
    window.scrollTo({ top: 0, left: 0 });
  }, []);

  return (
    <section className={styles.wrapper}>
      <h1>{formatMessage({ id: 'privacyNotice.title' })}</h1>
      <Subtitle serial="I" text={formatMessage({ id: 'privacyNotice.introduction.title' })} />
      {formatMessage({ id: 'privacyNotice.introduction.content.paragraph' }).split('\n').map((p, i) => (
        <p className={styles.para} key={i}>{p}</p>
      ))}
      <Subtitle serial="II" text={formatMessage({ id: 'privacyNotice.informationWeCollect.title' })} />
      <p className={styles.para}>{formatMessage({ id: 'privacyNotice.informationWeCollect.content.paragraph' })}</p>
      <List
        data={[
          {
            title: formatMessage({ id: 'privacyNotice.informationWeCollect.content.list.informationProvidedToWEIM.title' }),
            content: formatMessage({ id: 'privacyNotice.informationWeCollect.content.list.informationProvidedToWEIM.items' })
          },
          {
            title: formatMessage({ id: 'privacyNotice.informationWeCollect.content.list.informationCollectedThroughUse.title' }),
            content: formatMessage({ id: 'privacyNotice.informationWeCollect.content.list.informationCollectedThroughUse.items' })
          },
          {
            title: formatMessage({ id: 'privacyNotice.informationWeCollect.content.list.thirdParties.title' }),
            content: formatMessage({ id: 'privacyNotice.informationWeCollect.content.list.thirdParties.items' })
          },
          {
            title: formatMessage({ id: 'privacyNotice.informationWeCollect.content.list.whenRequiredByLaw.title' }),
            content: formatMessage({ id: 'privacyNotice.informationWeCollect.content.list.whenRequiredByLaw.items' })
          },
          {
            title: formatMessage({ id: 'privacyNotice.informationWeCollect.content.list.socialMedia.title' }),
            content: formatMessage({ id: 'privacyNotice.informationWeCollect.content.list.socialMedia.items' })
          },
        ]}
      />
      <Subtitle serial="III" text={formatMessage({ id: 'privacyNotice.howWeUseUrInformation.title' })} />
      <p className={styles.para}>{formatMessage({ id: 'privacyNotice.howWeUseUrInformation.content.paragraph' })}</p>
      <List
        data={[
          {
            title: formatMessage({ id: 'privacyNotice.howWeUseUrInformation.content.list.provideProductsAndServices.title' }),
            content: formatMessage({ id: 'privacyNotice.howWeUseUrInformation.content.list.provideProductsAndServices.items' })
          },
          { title: formatMessage({ id: 'privacyNotice.howWeUseUrInformation.content.list.improveServices.title' }) },
          {
            title: formatMessage({ id: 'privacyNotice.howWeUseUrInformation.content.list.marketingAndCustomizingServices.title' }),
            content: formatMessage({ id: 'privacyNotice.howWeUseUrInformation.content.list.marketingAndCustomizingServices.items' })
          },
          {
            title: formatMessage({ id: 'privacyNotice.howWeUseUrInformation.content.list.researchAndAnalytics.title' }),
            content: formatMessage({ id: 'privacyNotice.howWeUseUrInformation.content.list.researchAndAnalytics.items' })
          },
          {
            title: formatMessage({ id: 'privacyNotice.howWeUseUrInformation.content.list.asRequiredByLaw.title' }),
            content: formatMessage({ id: 'privacyNotice.howWeUseUrInformation.content.list.asRequiredByLaw.items' })
          },
        ]}
      />
      <Subtitle serial="IV" text={formatMessage({ id: 'privacyNotice.sharingInformation.title' })} />
      <p className={styles.para}>{formatMessage({ id: 'privacyNotice.sharingInformation.content.paragraph' })}</p>
      <List
        data={[
          { content: formatMessage({ id: 'privacyNotice.sharingInformation.content.list.items' }) },
        ]}
      />
      <Subtitle serial="V" text={formatMessage({ id: 'privacyNotice.howWeSafeguardYourInformation.title' })} />
      <List
        data={[
          { content: formatMessage({ id: 'privacyNotice.howWeSafeguardYourInformation.content.list.items' }) },
        ]}
      />
      <Subtitle serial="VI" text={formatMessage({ id: 'privacyNotice.cookiePolicy.title' })} />
      <List
        data={[
          { content: formatMessage({ id: 'privacyNotice.cookiePolicy.content.list.items' }) },
        ]}
      />
      <Subtitle serial="VII" text={formatMessage({ id: 'privacyNotice.yourRightsAndChoices.title' })} />
      <List
        data={[
          {
            title: formatMessage({ id: 'privacyNotice.yourRightsAndChoices.content.list.marketing.title' }),
            content: formatMessage({ id: 'privacyNotice.yourRightsAndChoices.content.list.marketing.items' })
          },
          {
            title: formatMessage({ id: 'privacyNotice.yourRightsAndChoices.content.list.correctionAndErasure.title' }),
            content: formatMessage({ id: 'privacyNotice.yourRightsAndChoices.content.list.correctionAndErasure.items' })
          },
          {
            title: formatMessage({ id: 'privacyNotice.yourRightsAndChoices.content.list.accessAndPortability.title' }),
            content: formatMessage({ id: 'privacyNotice.yourRightsAndChoices.content.list.accessAndPortability.items' })
          },
          {
            title: formatMessage({ id: 'privacyNotice.yourRightsAndChoices.content.list.restrictionOfProcessingToStorageOnly.title' }),
            content: formatMessage({ id: 'privacyNotice.yourRightsAndChoices.content.list.restrictionOfProcessingToStorageOnly.items' })
          },
          {
            title: formatMessage({ id: 'privacyNotice.yourRightsAndChoices.content.list.withdrawalOfConsentAndObjectionOfProcessing.title' }),
            content: formatMessage({ id: 'privacyNotice.yourRightsAndChoices.content.list.withdrawalOfConsentAndObjectionOfProcessing.items' })
          },
        ]}
      />
      <Subtitle serial="VIII" text={formatMessage({ id: 'privacyNotice.EUDataProcessingRetentionAndTransfers.title' })} />
      <List
        data={[
          { content: formatMessage({ id: 'privacyNotice.EUDataProcessingRetentionAndTransfers.content.list.items' }) },
        ]}
      />
    </section>
  );
};
