import styles from './index.less';

const Subtitle = ({ serial, text }) => (
  <h4>
    <span className={styles.serial}>{serial}.</span>
    {text}
  </h4>
);

export default Subtitle;
