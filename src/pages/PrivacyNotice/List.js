import styles from './index.less';

export default ({ data }) => (
  <div className={styles.list}>
    {data.map(({ title, content }, idx) => (
      <div className={styles.listItem}>
        {title && <p>{idx + 1}. {title}</p>}
        {content && (
          <ul>
            {content.split('\n').map((item, idx) => (
              <li key={idx}>- {item}</li>
            ))}
          </ul>
        )}
      </div>
    ))}
  </div>
);
