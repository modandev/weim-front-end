import { createRef } from 'react';
import { Redirect } from 'umi';
import { Icon } from 'components';
import { getQueries } from 'shared/locationParser';

import styles from './index.less';

export default () => {
  const textarea = createRef();
  const i = getQueries().i;

  if (!i) {
    return <Redirect to="/" />;
  }

  const onClick = type => {
    textarea.current.value = type;
    textarea.current.select();
    document.execCommand('Copy');
  };

  return (
    <div className={styles.wrap}>
      <textarea ref={textarea} />
      {i.split(',').map(type => (
        <div
          key={type}
          className={styles.item}
          onClick={() => onClick(type)}
        >
          <Icon type={type} />
          <span id={type}>{type}</span>
          <span className={styles.hint}>点击复制</span>
        </div>
      ))}
    </div>
  );
};
