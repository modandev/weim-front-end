import { PureComponent } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';
import classnames from 'classnames';

import styles from './index.less';

export default class extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      error: false,
    };
  }

  _validateSelf = () => {
    const { rule } = this.props;
    const { value } = this.state;

    if (!value || !value.trim() || (rule && !rule.test(value))) {
      this.setState({ error: true });
      return false;
    }

    return value;
  };

  onTextInput = e => {
    const { onInput, pattern } = this.props;
    const value = e.target.value;
    if (pattern) {
      if (!pattern.test(value)) {
        return;
      }
    }

    onInput(value);
    this.setState({ value, error: false });
  };

  render() {
    const { value, error } = this.state;
    const { name, label, className, require, message } = this.props;

    return (
      <div className={classnames(styles.input, className)}>
        {label ?
          <div className={styles.label}> {require ? <div className={styles.require}></div> : ''} {label}</div> : null}
        <input
          name={name}
          value={value}
          onChange={this.onTextInput}
          className={classnames({ [styles.error]: error })}
          placeholder={this.props.placeholder}
        />
        <div
          className={
            classnames(
              styles.hint,
              { [styles.visible]: this.props.errorMessage || error },
            )
          }
        >
          {this.props.errorMessage? this.props.errorMessage:error ? formatMessage({ id: value ? 'message.format.error' : 'message.required' }) : ''}
        </div>
        <div className={styles.message}>
          {message}
        </div>

      </div>
    );
  };
};
