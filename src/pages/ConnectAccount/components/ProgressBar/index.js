import styles from './styles.less';
import { connect } from 'dva';

export default connect(({ user }) => ({
  userStatus: user.userStatus,
}))(({ userStatus, match }) => {
  return <div className={styles.bar_box}>
    <div className={`${styles.bar} ${styles.active}`}></div>
    <div className={`${styles.bar} ${userStatus.signup_redirect !== 'BINDING_IN_PROGRESS'
    && (match.params.accountId === userStatus.accountId) ? styles.active : ''}`}>
    </div>
  </div>;
});
