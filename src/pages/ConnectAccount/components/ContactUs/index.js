import styles from './styles.less';
import { formatMessage } from 'umi-plugin-react/locale';
import classnames from 'classnames';

export default () => (
  <section className={classnames(styles.contactUs, styles.primary_bg)}>
      <div className={styles.qrcode}>

        <div className={styles.flex_center}>
          <div className={styles.contact_box}>
             <img src={require('../../../../assets/common/wechat-official-account.png')}
                  alt="@wechat-official-account" />
          </div>
          <p>{formatMessage({ id: 'aboutUs.contactUs.wechat.officialAccount' })}</p>
        </div>

        <div className={styles.flex_center}>
          <div className={styles.contact_box}>
             <img src={require('../../../../assets/common/customer-service.png')}
                  alt="@wechat-customer-service" />
          </div>
          <p>{formatMessage({ id: 'aboutUs.contactUs.wechat.customerService' })}</p>
        </div>

      </div>
  </section>)
