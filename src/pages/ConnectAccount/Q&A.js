import { formatMessage } from 'umi-plugin-react/locale';

import styles from './index.less';

export default () => (
  <section className={styles.qa}>
    <div className={styles.qa_item}>
      <h2 className={styles.question}>
        <span className={styles.serial}>1</span>
        {formatMessage({ id: 'user.connectIB.questionOne' })}
      </h2>
      <div className={styles.answer}>
        {formatMessage({ id: 'user.connectIB.answerOne' }).split('\n').map((paragraph, index) => (
          <p key={index}>{paragraph}</p>
        ))}
      </div>
    </div>
    <div className={styles.qa_item}>
      <h2 className={styles.question}>
        <span className={styles.serial}>2</span>
        {formatMessage({ id: 'user.connectIB.questionTwo' })}
      </h2>
      <div className={styles.answer}>
        {formatMessage({ id: 'user.connectIB.answerTwo' }).split('\n').map((paragraph, index) => (
          <p key={index}>{paragraph}</p>
        ))}
      </div>
    </div>
    <div className={styles.qa_item}>
      <h2 className={styles.question}>
        <span className={styles.serial}>3</span>
        {formatMessage({ id: 'user.connectIB.questionThree' })}
      </h2>
      <div className={styles.answer}>
        {formatMessage({ id: 'user.connectIB.answerThree' }).split('\n').map((paragraph, index) => (
          <p key={index}>{paragraph}</p>
        ))}
      </div>
    </div>
    <div className={styles.qa_item}>
      <h2 className={styles.question}>
        <span className={styles.serial}>4</span>
        {formatMessage({ id: 'user.connectIB.questionFour' })}
      </h2>
      <div className={styles.answer}>
        {formatMessage({ id: 'user.connectIB.answerFour' }).split('\n').map((paragraph, index) => (
          <p key={index}>{paragraph}</p>
        ))}
      </div>
    </div>
    <div className={styles.mail_us}>
      {formatMessage({ id: 'user.connectIB.moreQuestion.help' })}{` `}
      <a href="mailto:manager@weim.com">manager@weim.com</a>
    </div>
  </section>
);
