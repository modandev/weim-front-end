import { useState, createRef } from 'react';
import { connect } from 'dva';
import router from 'umi/router';
import { formatMessage } from 'umi-plugin-react/locale';
import { IB_UID_REGEXP, EMAIL_REGEXP, USER_NICKNAME_REGEXP } from '@/shared/regexps';
import { Tips, Icon } from '@/components';
import Button from '@/components/Buttons';
import Input from '../components/Input';
import styles from './styles.less';
import ContactUs from '../components/ContactUs';
import { Redirect } from 'umi';


const Index = ({ dispatch, userStatus }) => {
  const [state, setState] = useState({});
  const forms = Array.from(
    {
      0: 'uid',
      1: 'lastname',
      2: 'firstname',
      3: 'email',
      length: 4,
    },
    name => ({
      name,
      value: '',
      ref: createRef(),
    }),
  );
  const [UID, LASTNAME, FIRSTNAME, EMAIL] = forms;
  const onInput = () => state && setState({});
  const onAdd = async () => {
    const result = forms.map(({ ref }) => ref.current._validateSelf());
    if (result.every(val => !!val)) {
      const { error, accountId } = await dispatch({
        type: 'user/createAccount',
        payload: {
          fields: {
            uid: result[0],
            lastname: result[1],
            firstname: result[2],
            email: result[3],
            broker: 'IBKR',
          },
        },
      });

      if (error) {
        const { message } = error.graphQLErrors;
        setState({ success: false, errorMessage: message });
      } else {
        setState({ success: true });
        setTimeout(() => {
          router.push('/connect_account/status/' + accountId);
        }, 2000);
      }
    }
  };

  if (userStatus.active === false && userStatus.signup_redirect !== 'CONNECT_UID') {
    return <Redirect to={'/connect_account/status/' + userStatus.accountId}></Redirect>;
  }

  if (userStatus.active === true && userStatus.signup_redirect === 'ACCT_PENDING_INFO') {
    return <Redirect to={'/connect_account/account_profile/' + userStatus.accountId} />;
  }

  if (userStatus.active === true && userStatus.signup_redirect === 'BINDING_IN_PROGRESS') {
    return <Redirect to={'/connect_account/status/' + userStatus.accountId} />;
  }

  return (
    <section className={styles.connect}>
      <div className={styles.title}>
        {formatMessage({ id: 'user.connect.title' })}
      </div>

      <div className={styles.sub_title}>
        {formatMessage({ id: 'user.connect.sub_title' })}
      </div>

      <Tips
        visible={'success' in state && state.success}
        type={state.success ? 'safe' : 'danger'}
        header={
          <>
            <Icon type={state.success > 0 ? 'check-circle-outline' : 'tips'} />
            {state.success > 0 ? formatMessage({ id: 'user.connectIB.subaccount.connect.done' }) : formatMessage({ id: 'user.connectIB.subaccount.connect.connected' })}
          </>
        }
        description={formatMessage({ id: `user.connectIB.subaccount.connect.${state.success > 0 ? 'notify' : 'help'}` })}
      >
        <Input
          require
          ref={UID.ref}
          name={UID.name}
          onInput={onInput}
          pattern={IB_UID_REGEXP}
          errorMessage={'success' in state && !state.success ? formatMessage({ id: 'user.connectIB.subaccount.connect.connected' }) : ''}
          label={formatMessage({ id: 'user.connectIB.subaccount.uid' })}
          message={formatMessage({ id: 'user.connect.uid_message' })}
        />

        <Input
          require
          onInput={onInput}
          ref={LASTNAME.ref}
          name={LASTNAME.name}
          className={styles.margin}
          rule={USER_NICKNAME_REGEXP}
          label={formatMessage({ id: 'user.connectIB.subaccount.lastname' })}
        />

        <Input
          require
          onInput={onInput}
          ref={FIRSTNAME.ref}
          name={FIRSTNAME.name}
          rule={USER_NICKNAME_REGEXP}
          label={formatMessage({ id: 'user.connectIB.subaccount.firstname' })}
        />

        <Input
          require
          ref={EMAIL.ref}
          name={EMAIL.name}
          onInput={onInput}
          rule={EMAIL_REGEXP}
          className={styles.margin}
          label={formatMessage({ id: 'user.connectIB.subaccount.email' })}
        />

        <Button icon="add-white"
                appearance="primary"
                shape="square"
                size="large"
                css={{ width: '144px', height: '40px' }}
                onClick={onAdd}>
            {formatMessage({ id: 'user.settings.connect' })}
        </Button>

        <div className={styles.post_message}>
          {formatMessage({ id: 'user.connect.email_action' })}
        </div>
      </Tips>
    </section>
  );
};

export default connect(({ user }) => ({
  userStatus: user.userStatus,
}))(Index);
