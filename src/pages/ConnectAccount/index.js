import { useEffect } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';
import styles from './index.less';
import ProgressBar from './components/ProgressBar';
import ContactUs from '@/pages/ConnectAccount/components/ContactUs';
import { Redirect } from 'umi';
import { connect } from 'dva';

export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))((props) => {
  useEffect(() => {
    window.scrollTo({ top: 0, left: 0 });
  }, []);

  if (!props.currentUser || !props.currentUser.uid) {
    return <Redirect to="/login" />;
  }
  return (
    <section className={styles.connect_IB}>
        <div className={`type-area ${styles.content}`}>
          <div className={styles.left}></div>
          <section className={styles.others}>
            <ProgressBar match={props.match}/>
            <div className={styles.title}>
              {formatMessage({ id: 'use.connectMessageTitle' })}
            </div>
            {
              props.children
            }
          </section>
          <ContactUs />

        </div>
    </section>
  );
});
