import { formatMessage } from 'umi-plugin-react/locale';

import styles from './index.less';

export default () => (
  <>
    <section className={styles.how}>
      <div className={styles.title}>
        <h2>{formatMessage({ id: 'user.connectIB.howToBeManager' })}</h2>
      </div>
      <div className={styles.steps}>
        {formatMessage({ id: 'user.connectIB.toBeManagerSteps' }).split('\n').map((paragraph, index) => (
          <p key={index}>{paragraph}</p>
        ))}
      </div>
    </section>
    <section className={styles.follow}>
      <div className={styles.official_account}>
        <img src={require('../../assets/common/wechat-official-account.png')} alt="@wechat-official-account" />
        <div className={styles.desc}>
          {formatMessage({ id: 'user.follow.wechat.official.account' })}
        </div>
      </div>
      <div className={styles.customer_service}>
        <img src={require('../../assets/common/customer-service.png')} alt="@wechat-customer-service" />
        <div className={styles.desc}>
          {formatMessage({ id: 'user.consult.wechat.custom.service' })}
        </div>
      </div>
    </section>
  </>
);
