import { useRef, useState, useCallback, useEffect } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';
import styles from './styles.less';
import { Tips, Icon, Avatar, Modal } from '@/components';
import Button from '@/components/Buttons';
import { useDispatch } from 'dva';
import Input from '../components/Input';
import { ACCOUNT_NAME_REGEXP, ACCOUNT_NAME_PATTERN_REGEXP } from '@/shared/regexps';
import { router } from 'umi';
import { connect } from 'dva';

export default connect(({ user }) => ({
  active: user.userStatus.active,
}))((props) => {
  const { active, match } = props;
  const dispatch = useDispatch();
  const nameInput = useRef(null);
  const [isVisible, setIsVisible] = useState(false);
  const [accountId] = useState(match.params.accountId);
  const [url, setUrl] = useState('');
  const [nickname, setNickname] = useState('');
  const [description, setDescription] = useState('');

  const onSaveAvatar = (accountId, { url }) => {
    setUrl(url);
  };

  const onClose = () => {
    const payload = `ACCT_PENDING_INFO:${accountId}`;
    const accountStatus = localStorage.getItem('accountStatus') ? JSON.parse(localStorage.getItem('accountStatus')) : [];
    dispatch({
      type: 'global/removeNotice',
      payload,
    });
    accountStatus.push(payload);
    localStorage.setItem('accountStatus', JSON.stringify(accountStatus));
    window.location.reload()
  };

  const onChangeName = useCallback((value) => {
    setNickname(value);
  });

  const onChange = useCallback(e => setDescription(e.target.value));

  const onSubmit = useCallback(() => {
    let name = nameInput.current._validateSelf();

    if (name && description && url) {
      setIsVisible(true);
    }
  });

  const onHide = useCallback(() => {
    setIsVisible(false);

  });

  const onOk = useCallback(() => {
    setIsVisible(false);
    onSave({ accountId, fields: { header_url: url, description, nickname } });
  });

  const onSave = async payload => {
    const { success } = await dispatch({ type: 'user/updateAccount', payload });
    console.log('success', success)
    if (success) {
      await dispatch({ type: 'user/viewer', payload });
      router.replace('/');
      onClose();
    }
  };

  const skip = useCallback(() => {
    localStorage.setItem('skipAccountProfile', '1');
    router.push('/');
  });

  return (
    <div className={styles.account_profile}>
      <div className={styles.title}>
        {formatMessage({ id: 'user.connect.Creating your account profile' })}
      </div>

      <Avatar
        medium
        uploadable
        src={url}
        onUploadSuccess={file => onSaveAvatar(accountId, file)}
      />

      <div className={styles.name_editor}>
        <Input
          maxLength={20}
          require
          ref={nameInput}
          onInput={onChangeName}
          pattern={ACCOUNT_NAME_PATTERN_REGEXP}
          rule={ACCOUNT_NAME_REGEXP}
          placeholder={formatMessage({ id: 'user.me.account.name.placeholder' })}
        />
        <p className={styles.message}>
          {formatMessage({ id: 'user.connect.userNicknameAction' })}
        </p>
      </div>

      <div className={styles.editor}>
        <textarea
          maxLength={320}
          onChange={onChange}
          value={description}
          placeholder={formatMessage({ id: 'user.me.aboutAccount.placeholder' })}
        />
          <div className={styles.count}>
            {description ? description.trim().length : 0} / 320
          </div>
      </div>

      <Button
        onClick={onSubmit}
        shape="square"
        appearance="primary"
        css={{ width: '144px', height: '40px' }}>
        {formatMessage({ id: 'user.connect.submitButton' })}
      </Button>
      <br />
      {active ? <Button
        css={{ padding: 0, marginTop: '15px' }}
        onClick={skip}
        appearance="link">
        {formatMessage({ id: 'user.connect.nextTime' })}
      </Button> : null}

      <Modal
        onOk={onOk}
        onCancel={onHide}
        visible={isVisible}
        okText={formatMessage({ id: 'common.save' })}
        description={formatMessage({ id: 'user.me.account.name.modify.tip' })}
      />
</div>);
});
