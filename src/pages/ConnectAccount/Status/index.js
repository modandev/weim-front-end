import React, { useEffect } from 'react';
import styles from './styles.less';
import { formatMessage, getLocale } from 'umi-plugin-react/locale';
import { connect } from 'dva';

export default connect(({ user }) => ({
  userStatus: user.userStatus,
}))(
  (props) => {
    useEffect(() => {
      localStorage.removeItem('loginJust');
    });

    const { userStatus } = props;

    return (
      <div className={styles.status}>
      {userStatus.signup_redirect === 'BINDING_IN_PROGRESS' || (props.match.params.accountId !== userStatus.accountId) ?
        <div className={styles.title}>
        {formatMessage({ id: 'use.connect.status.success.title' })}
      </div> :
        <div className={`${styles.title} ${styles.error}`}>
        {formatMessage({ id: 'use.connect.status.error.title' })}
      </div>
      }
        <p className={styles.message}>
        {userStatus.signup_redirect !== 'ACCT_CONNECT_FAIL' || (props.match.params.accountId !== userStatus.accountId) ? formatMessage({ id: 'use.connect.status.success.message' }) : /^zh+/.test(getLocale()) ? userStatus.account_bind_failure.reason_zh : userStatus.account_bind_failure.reason_en}
        </p>
    </div>
    );
  },
);
