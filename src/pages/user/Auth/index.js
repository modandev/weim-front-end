import thirdPartyConfig from 'configs/thirdParty';

export default props => {
  const { match, location } = props;
  const { thirdParty } = match.params;

  if (!Object.getOwnPropertyNames(location.query).length) {
    window.location.href = thirdPartyConfig.oauth.urls[thirdParty];
  } else {
    const { code } = location.query;

    switch (thirdParty) {
      case 'linkedin':
        code && localStorage.setItem(thirdParty, code);
        window.close();
        break;

      default:
        break;
    }
  }

  return null;
};
