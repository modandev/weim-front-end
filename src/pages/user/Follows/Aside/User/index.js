/* eslint-disable react-hooks/exhaustive-deps */
import { formatMessage } from 'umi-plugin-react/locale';
import Panel from '../../components/Panel';
import { Avatar, Button, Skeleton } from 'components';
import mail from 'assets/icon/mail.svg';

import styles from './index.less';

function UserDetail ({ user, loading }) {
  return (
    <Panel className={styles.wrapper} title={formatMessage({ id: 'user.panel.investment.profile' })}>
      <div className={styles.user}>
        <Skeleton
          loading={loading}
          avatar
          title={{ rows: 2, width: 180 }}
        >
          {/* 1.0 屏蔽 */}
          {/* <Button className={styles.button} ghost size="small">
            <img className={styles.button_icon} src={mail} />
            <span>发消息</span>
          </Button> */}
          <Avatar className={styles.avatar} src={user.avatar} backup={user.lastName} />
          <div className={styles.nickname}>
            {user.nickname}
            <i></i>
          </div>
          {/* 1.0 屏蔽 */}
          {/* <div className={styles.follow}>
            100人正在关注
          </div> */}
          <div className={styles.desc}>

          </div>
        </Skeleton>
      </div>
    </Panel>
  );
}

export default UserDetail;
