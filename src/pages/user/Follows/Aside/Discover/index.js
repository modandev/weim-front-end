import Picked from './Picked';
import Accounts from './Accounts';
import Watchlist from './Watchlist';

import styles from './index.less';

export default props => {
  return (
    <aside className={styles.discover}>
      <Accounts {...props} />
      {/* 1.0 屏蔽 */}
      {/* <Picked /> */}
      {props.isMe && (
        <Watchlist {...props} />
      )}
    </aside>
  );
};
