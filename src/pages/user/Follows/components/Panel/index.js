import classnames from 'classnames';

import styles from './index.less';

export function Content ({ children }) {
  return (
    <div className={styles.panel_content}>
      {children}
    </div>
  );
}

export function Header ({ title }) {
  return (
    <div className={styles.panel_header}>
      <h3>{title}</h3>
    </div>
  )
}

export default function Panel ({ children, title, className }) {
  return (
    <div className={classnames(styles.panel, className)}>
      <Header title={title} />
      <Content>{children}</Content>
    </div>
  )
}
