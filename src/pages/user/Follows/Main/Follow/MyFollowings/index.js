import React, { useState, useCallback, useEffect } from 'react';
import styles from './styles.less';
import Spinner from '@atlaskit/spinner';
import { AccountCard } from '@/components';
import { FOLLOWS } from '@/shared/graphql/gql/follow';
import { useQuery } from '@apollo/react-hooks';
import { FormattedMessage } from 'umi-plugin-react/locale';
import debounce from 'debounce';

export default (props) => {
  const {
    refetch, loading, data: { followers: { hits, cursor } = {} } = {},
  } = useQuery(FOLLOWS,
    {
      variables: {
        size: '4',
        userId: props.match.params.uid,
      },
    },
  );
  const [followers, setFollowers] = useState([]);
  const loadMore = useCallback(
    debounce(
      () => {
        refetch({
          cursor: cursor,
          size: '4',
          userId: props.match.params.uid,
        })
      }, 200,
    ),
  );

  useEffect(() => {
    if (hits) setFollowers([...followers, ...hits]);
  }, [hits]);

  useEffect(
    () => {
      const onScroll = (e) => {
        if (!loading && cursor && e.target.scrollingElement.scrollHeight <= document.documentElement.clientHeight + e.target.scrollingElement.scrollTop) {
          loadMore();
        }
      };

      document.addEventListener('scroll', onScroll);
      return () => {
        document.removeEventListener('scroll', onScroll);
      };
    },
    [cursor, loadMore, loading]);

  return (<div className={styles.followings}>
    {followers.map((item) =>
      <div
        key={item.account_id}
        style={{ marginBottom: '16px' }}>
        <AccountCard
          style={{ width: '391px' }}
          account={{ ...item.account, ...item.accountTrading }}
        />
      </div>,
    )}

    {loading ? <div className={styles.spinner_box}><Spinner /> </div> : null}

    {!loading && !cursor ? <div className={styles.no_more}><FormattedMessage id="home.NoMore" /></div> : null}

  </div>);
};
