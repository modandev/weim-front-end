import PageTitle from './PageTitle';
import MyFollowings from './MyFollowings';
export default props => (
  <>
      <PageTitle {...props} />
      <MyFollowings {...props}/>
  </>
);
