import { connect } from 'dva';
import { Redirect } from 'umi';
import WindowEvent from 'shared/WindowEvent';
import thirdPartyConfig from 'configs/thirdParty';

const THIRD_PARTY_MAPPING = {
  'linkedin': 'LinkedIn',
};

export default WrapperComponent =>
  connect(({ user }) => ({
    currentUser: user.currentUser,
    signUpRedirect: user.userStatus.signup_redirect,
    userStatus: user.userStatus,
  }))(props => {
    const { signUpRedirect, userStatus } = props;
    if (signUpRedirect === 'CONNECT_UID') {
      return <Redirect to="connect_account/add_IB" />;
    }

    if (signUpRedirect === 'ACCT_PENDING_INFO') {
      return <Redirect to={`/connect_account/account_profile/${userStatus.account_pending_info.account_id}`} />;
    }

    if (signUpRedirect === 'ACCT_CONNECT_FAIL' || signUpRedirect === 'BINDING_IN_PROGRESS') {
      const AccountId = userStatus.account_bind_in_progress ? userStatus.account_bind_in_progress.account_id : userStatus.account_bind_failure.account_id;
      return <Redirect to={`/connect_account/status/${AccountId}`} />;
    }

    if (props.currentUser.uid) {
      return <Redirect to="/" />;
    }

    let _timer = null;
    let _thirdParty = null;

    const onOauth = () => {
      const code = localStorage.getItem(_thirdParty);
      const { dispatch } = props;

      localStorage.removeItem(_thirdParty);
      _timer && clearTimeout(_timer);
      code && dispatch({
        type: 'user/oauth',

        payload: {
          type: THIRD_PARTY_MAPPING[_thirdParty],
          code,
        },
      });
    };

    const onSelect = thirdParty => {
      _thirdParty = thirdParty;
      _timer && clearTimeout(_timer);
      _timer = setTimeout(() => {
        WindowEvent.off('storage', onOauth);
        _timer && clearTimeout(_timer);
      }, thirdPartyConfig.oauth.timeout);
      WindowEvent.onlyOnce('storage', onOauth);
    };

    return (
      <WrapperComponent
        {...props}
        onSelect={onSelect}
      />
    );
  });
