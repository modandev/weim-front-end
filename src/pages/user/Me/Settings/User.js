import { useState } from 'react';
import { connect } from 'dva';
import { formatMessage } from 'umi-plugin-react/locale';
import { Tips, Avatar, ThirdParty, Icon } from 'components';
import { USER_NICKNAME_REGEXP } from 'shared/regexps';
import { firstLetterUpperCase } from 'shared/helpers';

import styles from './index.less';

const User = ({ dispatch, currentUser }) => {
  const currentNickname = currentUser.nickname ? currentUser.nickname : `${firstLetterUpperCase(currentUser.firstName) + firstLetterUpperCase(currentUser.lastName)}`;
  const [nickname, setNickname] = useState('');
  const [editing, setEditing] = useState(false);
  const [tipsVisible, setTipsVisible] = useState(false);

  const onSaveAvatar = file => onUpdateUser({ avatar: file.url });
  const onNicknameChange = e => {
    tipsVisible && setTipsVisible(false);
    setNickname(e.target.value);
  };
  const onEdit = currentName => {
    setEditing(!editing);
    setNickname(currentName);
  };
  const onSave = () => {
    if (!USER_NICKNAME_REGEXP.test(nickname)) {
      setTipsVisible(true);
      return;
    }

    setEditing(!editing);
    onUpdateUser({ nickname });
  };
  const onCancel = () => {
    setNickname('');
    setEditing(!editing);
  };
  const onUpdateUser = fields => {
    dispatch({
      type: 'user/updateUser',
      payload: {  id: currentUser.id, fields  }
    });
  };
  const onSelect = () => {};

  return (
    <div className={styles.user}>
      <h3>{formatMessage({ id: 'user.settings.user' })}</h3>
      <div className={styles.options}>
        <div className={styles.data}>
          <div className={styles.avatar}>
            <Avatar
              src={currentUser.avatar}
              large
              backup={currentUser.lastName}
              uploadable
              onUploadSuccess={onSaveAvatar}
            />
          </div>
          <div className={styles.weim}>
            <Tips
              floated
              type="danger"
              visible={tipsVisible}
              header={formatMessage({ id: 'message.username.format.error' })}
            >
              <input
                maxLength={16}
                disabled={!editing}
                onChange={onNicknameChange}
                value={editing ? nickname : currentNickname}
                placeholder={formatMessage({ id: 'user.settings.nickname' })}
              />
            </Tips>
            {editing ? (
              <>
                <button onClick={onSave}>
                  {formatMessage({ id: 'common.save' })}
                </button>
                <button onClick={onCancel}>
                  {formatMessage({ id: 'common.cancel' })}
                </button>
              </>
            ) : (
              <Icon type="edit" as="button" onClick={() => onEdit(currentNickname)} />
            )}
            <p>WEIM ID: {currentUser.uid}</p>
          </div>
        </div>
        <div className={styles.associated}>
          <ThirdParty
            useFor="connect"
            onSelect={onSelect}
            remarks={currentUser.email}
            inactive={currentUser.isLinked}
            buttonTxtPrefix={formatMessage({ id: `user.settings.connect${currentUser.isLinked ? 'ed' : ''}` })}
          />
        </div>
      </div>
    </div>
  );
};

export default connect(({ user }) => ({
  currentUser: user.currentUser
}))(User);
