import User from './User';
import Favorite from './Favorite';

import styles from './index.less';

export default () => (
  <div className={styles.settings}>
    <User />
    <Favorite />
  </div>
);
