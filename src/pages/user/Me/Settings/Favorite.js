import { useState } from 'react';
import { setLocale, getLocale, formatMessage } from 'umi-plugin-react/locale';
import classnames from 'classnames';
import { SUPPORTED_LOCALES, RISE_FALL_STYLE_KEY, SUPPORTED_RISE_FALL_STYLES } from 'shared/constants';

import styles from './index.less';

export default () => {
  const locale = getLocale();

  const [riseFallStyle, setRiseFallStyle] = useState(localStorage.getItem(RISE_FALL_STYLE_KEY));

  const onSetLocale = value => setLocale(value, true);
  const onSetRiseFallStyle = value => {
    setRiseFallStyle(value);
    localStorage.setItem(RISE_FALL_STYLE_KEY, value);
  };

  return (
    <div className={styles.favorite}>
      <h3>{formatMessage({ id: 'user.settings.favorite' })}</h3>
      <div className={styles.options}>
        <ul className="clearfix">
          <li>{formatMessage({ id: 'user.settings.locale' })}</li>
          {SUPPORTED_LOCALES.map(({ key, value }) => (
            <li
              key={key}
              onClick={() => onSetLocale(value)}
              className={classnames({ [styles.active]: locale === value })}
            >
              {formatMessage({ id: `user.settings.locale.${key}` })}
            </li>
          ))}
        </ul>
        {/* <ul className="clearfix">
          <li>{formatMessage({ id: 'user.settings.riseAndFall' })}</li>
          {SUPPORTED_RISE_FALL_STYLES.map(({ key, value }) => (
            <li
              key={key}
              onClick={() => onSetRiseFallStyle(value)}
              className={
                classnames({
                  [styles.active]: (riseFallStyle || SUPPORTED_RISE_FALL_STYLES[0].value) === value
                })
              }
            >
              {formatMessage({ id: `user.settings.riseAndFall.${key}` })}
            </li>
          ))}
        </ul> */}
      </div>
    </div>
  );
};
