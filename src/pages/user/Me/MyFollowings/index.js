import React, { useState, useCallback, useEffect, memo } from 'react';
import styles from './styles.less';
import Spinner from '@atlaskit/spinner';
import { AccountCard } from '@/components';
import { useQuery } from '@apollo/react-hooks';
import { FOLLOWS } from '@/shared/graphql/gql/follow';
import { FormattedMessage } from 'umi-plugin-react/locale';
import debounce from 'debounce';
import noFollowingImg from '@/assets/common/img_no_flowing.png';

const getLines = (word) => {
  if (word) {
    const space = word.length - word.replace(/\s+/gi, '').length;
    const ch = word.replace(/[^\u4e00-\u9fa5]/gi, '').length;
    const other = word.length - space - ch;
    return Math.ceil((ch * 17 + other * 13 + space * 4) / 264);
  }
  return 0;
};

export default memo(() => {
  const [followers, setFollowers] = useState({
    left: { hits: [], height: 0 },
    center: { hits: [], height: 0 },
    right: { hits: [], height: 0 },
  });
  const [isEnd, setIsEnd] = useState(false);

  const {
    refetch, loading, data: { followers: { hits, cursor } = {} } = {},
  } = useQuery(FOLLOWS,
    {
      variables: {
        size: '20',
      },
    },
  );

  const loadMore = useCallback(
    debounce(
      () => {
        cursor && refetch({ cursor: cursor });
      }, 200,
    ),
  );


  useEffect(() => {

    if (hits) {
      let leftHeight = followers.left.height;
      let leftCenter = followers.center.height;
      let leftRight = followers.right.height;

      hits.forEach((item) => {
        const linesNo = getLines(item.account.description);
        const height = linesNo ? 20 + linesNo * 25 : 0;
        if (leftHeight <= leftCenter && leftHeight <= leftRight) {
          followers.left.hits.push(item);
          leftHeight = leftHeight + 180 + height;
        } else if (leftCenter <= leftHeight && leftCenter <= leftRight) {
          followers.center.hits.push(item);
          leftCenter = leftCenter + 180 + height;
        } else {
          followers.right.hits.push(item);
          leftRight = leftRight + 180 + height;
        }
      });

      followers.left.height = leftHeight;
      followers.center.height = leftCenter;
      followers.right.height = leftRight;
      setFollowers({ ...followers });

      if (!hits.length) {
        setIsEnd(true);
      }
    }
  }, [hits]);

  useEffect(
    () => {
      const onScroll = (e) => {
        if (!loading && !isEnd && e.target.scrollingElement.scrollHeight <= document.documentElement.clientHeight + e.target.scrollingElement.scrollTop) {
          loadMore();
        }
      };

      document.addEventListener('scroll', onScroll);
      return () => {
        document.removeEventListener('scroll', onScroll);
      };
    });

  return (<div className={styles.followings}>
      <div className={styles.column}>
        {followers.left.hits.map((follower) => <Item follower={follower} />)}
      </div>
      <div className={styles.column}>
        {followers.center.hits.map((follower) => <Item follower={follower} />)}
      </div>
      <div className={styles.column}>
        {followers.right.hits.map((follower) => <Item follower={follower} />)}
      </div>


    {loading ? <div className={styles.spinner_box}><Spinner /> </div> : null}

    {!loading && !followers.left.hits.length && !followers.center.hits.length && !followers.right.hits.length ?
      <div className={styles.no_more}>

      <img src={noFollowingImg}
           alt="" />
      <FormattedMessage id="home.NoFollowing" />
    </div> : null}
  </div>);
});

const Item = ({ follower }) => <div
  key={follower.account_id}
  className={styles.column}
  style={{ marginBottom: '16px' }}>
        <AccountCard
          style={{ width: '300px' }}
          account={{ ...follower.account, ...follower.accountTrading }}
        />
      </div>;
