import { formatMessage } from 'umi-plugin-react/locale';
import { Redirect } from 'umi';
import Tabs from './components/Tabs';
import MyAccount from './MyAccount';
import Settings from './Settings';
import MyFollowings from './MyFollowings';
import { connect } from 'dva';
import styles from './index.less';

const { TabBlock, TabPane } = Tabs;
const tabs = ['analysis', 'accounts', 'following','settings', 'notifications'];
const AuthorityListOfConnectedAccount = ['settings','accounts'];

export default  connect(
    ({ user }) => ({
      active: user.userStatus.active,
    }),
  )(({ match, active }) => {
  const { subdirectories = tabs[0] } = match.params;

  if (!tabs.includes(subdirectories)) {
    return <Redirect to="/personal" />;
  }

  if (subdirectories === tabs[0]) {
    return <Redirect to="/analyses/edit" />;
  }

  if (!active && !AuthorityListOfConnectedAccount.includes(subdirectories)){
    return <Redirect to={`/personal/accounts`} />
  }

  return (
    <div className={styles.user_me}>
      <Tabs activeKey={subdirectories}>
        <TabBlock tab={formatMessage({ id: 'user.accountManagement' })}>
          <TabPane tab={formatMessage({ id: 'user.accountManagement.myAnalysis' })} key={tabs[0]}>

          </TabPane>
          <TabPane tab={formatMessage({ id: 'user.accountManagement.myAccounts' })} key={tabs[1]}>
            <MyAccount />
          </TabPane>
        </TabBlock>
        <TabBlock tab={formatMessage({ id: 'user.userManagement' })}>
          <TabPane tab={formatMessage({ id: 'user.userManagement.following' })} key={tabs[2]}>
            <MyFollowings />
          </TabPane>
          <TabPane tab={formatMessage({ id: 'common.settings' })} key={tabs[3]}>
            <Settings />
          </TabPane>
          <TabPane tab={formatMessage({ id: 'common.notification' })} key={tabs[4]}>

          </TabPane>
        </TabBlock>
      </Tabs>
    </div>
  );
});
