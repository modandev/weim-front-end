import { useRef, useState, useEffect } from 'react';
import useSplit from './useSplit';
import Nav from './Nav';
import Content from './Content';

import styles from './index.less';

const Tabs = ({ activeKey: activeTab, children }) => {
  const activeTabRef = useRef(activeTab);
  const [activeKey, setActiveKey] = useState(activeTab);
  const [activated, setActivated] = useState([activeTab]);
  const [nav, content] = useSplit(children);

  useEffect(() => {
    if (activeTab !== activeTabRef.current && activeTab !== activeKey) {
      activeTabRef.current = activeTab;
      setActiveKey(activeTab);
      !activated.includes(activeTab) && setActivated([...activated, activeTab]);
    }
  }, [activeTab, activeKey, activated]);

  return (
    <div className={styles.tabs}>
      <Nav data={nav} active={activeKey} />
      <Content data={content} active={activeKey} activated={activated} />
    </div>
  );
};

Tabs.TabPane = () => null;
Tabs.TabBlock = () => null;

export default Tabs;
