import classnames from 'classnames';

import styles from './index.less';

export default ({ data, active, activated }) => {
  return (
    <div className={styles.content}>
      {data.map(({ key, element }) => {
        if (!activated.includes(key)) {
          return null;
        }

        return (
          <div
            key={key}
            className={
              classnames(
                styles.pane,
                { [styles.active]: active === key }
              )
            }
          >
            {element}
          </div>
        );
      })}
    </div>
  );
};
