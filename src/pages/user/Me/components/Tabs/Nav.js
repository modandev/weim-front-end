import { Link } from 'umi';
import classnames from 'classnames';
import { Collapse } from 'components';

import styles from './index.less';

const { Panel } = Collapse;

export default ({ data, active }) => {
  let activeKey = null;
  const Panels = data.map(({ header, children }, index) => (
      <Panel header={header} key={index}>
        <ul>
          {children.map(({ key, label }) => {
            const isActive = key === active;

            if (isActive) {
              activeKey = `${index}`;
            }

            return (
              <li
                key={key}
                className={classnames({ [styles.active]: isActive })}
              >
                <Link to={`/personal/${key}`}>{label}</Link>
              </li>
            );
          })}
        </ul>
      </Panel>
    )
  );

  return (
    <aside className={styles.nav}>
      {/* 默认全都展开 */}
      <Collapse activeKey={/* activeKey */['0', '1']}>
        {Panels}
      </Collapse>
    </aside>
  )
};
