import { Children } from 'react';

export default tabsChildren => {
  const nav = [];
  const content = [];

  Children.forEach(tabsChildren, tabBlock => {
    const currentNav = { header: tabBlock.props.tab, children: [] };

    tabBlock.props.children.forEach(tabPane => {
      const key = tabPane.key;

      currentNav.children.push({ key, label: tabPane.props.tab });
      content.push({ key, element: tabPane.props.children });
    });

    nav.push(currentNav);
  });

  return [nav, content];
};
