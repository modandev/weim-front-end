import { useState } from 'react';
import { Link } from 'umi';
import { formatMessage } from 'umi-plugin-react/locale';
import { Icon, Button } from 'components';

import styles from './index.less';

export default ({ followers, status, description, id, uid, onSave }) => {
  description = description || '';

  const [value, setValue] = useState('');
  const [isEdit, setIsEdit] = useState(false);

  const onEdit = () => {
    setValue(description);
    setIsEdit(true);
  };
  const onCancel = () => {
    setValue('');
    setIsEdit(false);
  };

  const onSaveEdit = payload => {
    const { description: newDescription } = payload.fields;

    if (newDescription !== description) {
      onSave(payload);
    }

    onCancel();
  };

  const onChange = e => setValue(e.target.value);

  return (
    <div className={styles.cardBody}>
      <div className={styles.info}>
        <Link to={`/followers/${uid}/accounts/${id}`}>
          {formatMessage({ id: 'common.follower' })}:{` `}
          <span className={styles.dark}>{followers || 0}</span>
        </Link>
        {/* <div>
          {formatMessage({ id: 'user.me.accountStatus' })}:{` `}
          <span className={styles[status]}>
            {formatMessage({ id:  `user.me.accountStatus.${status}` })}
          </span>
          <Icon type="question-circle" as="button" />
        </div> */}
      </div>
      <div className={styles.editor}>
        <textarea
          maxLength={150}
          disabled={!isEdit}
          onChange={onChange}
          value={isEdit ? value : description}
          placeholder={formatMessage({ id: 'user.me.aboutAccount.placeholder' })}
        />
        {isEdit && (
          <div className={styles.count}>
            {value ? value.trim().length : 0} / 150
          </div>
        )}
      </div>
      <div className={styles.operation}>
        {isEdit ? (
          <div className={styles.group}>
            <Button
              size="large"
              shape="square"
              onClick={onCancel}
            >
              {formatMessage({ id: 'common.cancel' })}
            </Button>
            <Button
              size="large"
              shape="square"
              disabled={!value.trim().length}
              onClick={() => onSaveEdit({ accountId: id, fields: { description: value } })}
            >
              {formatMessage({ id: 'common.save' })}
            </Button>
          </div>
        ) : (
          <Button size="large" shape="square" onClick={onEdit}>
            {formatMessage({ id: 'common.edit' })}
          </Button>
        )}
      </div>
    </div>
  );
};
