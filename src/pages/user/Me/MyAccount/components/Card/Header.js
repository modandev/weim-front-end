import { useRef, useState, useCallback } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';
import { Tips, Icon, Avatar, Modal } from 'components';
import { ACCOUNT_NAME_REGEXP } from 'shared/regexps';

import styles from './index.less';

export default ({ avatar, lastname, id, nickname, uid, editable, onSave }) => {
  nickname = nickname || formatMessage({ id: 'user.account' });

  const isSavingRef = useRef(false);
  const [value, setValue] = useState('');
  const [params, setParams] = useState(null);
  const [isEdit, setIsEdit] = useState(false);
  const [tipsMessage, setTipsMessage] = useState('');

  const onSaveAvatar = (accountId, file) => onSave({ accountId, fields: { header_url: file.url } });

  const onEdit = () => {
    setValue(nickname);
    setIsEdit(true);
  };
  const onCancel = useCallback(() => {
    return (() => {
      setValue('');
      setIsEdit(false);
      tipsMessage && setTipsMessage('');
    })();
  }, [tipsMessage]);

  const onSaveEdit = payload => {
    const { nickname: newNickname } = payload.fields;

    if (newNickname !== nickname) {
      ACCOUNT_NAME_REGEXP.test(newNickname)
        ? setParams(payload)
        : setTipsMessage(formatMessage({ id: 'message.account.nickname.format.error' }));
    } else {
      onCancel();
    }
  };
  const onHide = useCallback(() => setParams(null), []);
  const onOk = useCallback(() => {
    async function save () {
      const { success, error } = await onSave(params);
      onHide();
      isSavingRef.current = false;
      !success
        ? setTipsMessage(error.graphQLErrors[0].extensions.response.body.error)
        : onCancel();
    }

    if (!isSavingRef.current) {
      isSavingRef.current = true;

      return save();
    }
  }, [onHide, onCancel, onSave, params]);

  const onChange = e => {
    const { value } = e.target;

    tipsMessage && setTipsMessage('');
    setValue(value);
  };

  return (
    <div className={styles.cardHeader}>
      <Avatar
        uploadable
        src={avatar}
        backup={lastname}
        onUploadSuccess={file => onSaveAvatar(id, file)}
      />
      <div className={styles.info}>
        {isEdit ? (
          <div className={styles.editor}>
            <Tips
              floated
              type="danger"
              header={tipsMessage}
              visible={!!tipsMessage}
            >
              <input
                value={value}
                maxLength={20}
                onChange={onChange}
                placeholder={formatMessage({ id: 'user.me.account.name.placeholder' })}
              />
            </Tips>
            <button className={styles.save} onClick={() => onSaveEdit({ accountId: id, fields: { nickname: value } })}>
              {formatMessage({ id: 'common.save' })}
            </button>
            <button className={styles.cancel} onClick={onCancel}>
              {formatMessage({ id: 'common.cancel' })}
            </button>
          </div>
        ) : (
          <div className={styles.nickname}>
            <div className={styles.text}>
              <span>{nickname}</span>
              {editable && <Icon as="button" type="edit" onClick={onEdit} />}
            </div>
            {editable && <span className={styles.tips}>{formatMessage({ id: 'user.me.account.name.tips' })}</span>}
          </div>
        )}
        <h3>IB: {uid}</h3>
      </div>
      <Modal
        onOk={onOk}
        onCancel={onHide}
        visible={!!params}
        okText={formatMessage({ id: 'common.save' })}
        description={formatMessage({ id: 'user.me.account.name.modify.tip' })}
      />
    </div>
  );
};
