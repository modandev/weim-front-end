import CardHeader from './Header';
import CardBody from './Body';

import styles from './index.less';

export default ({ uid, data, onSave }) => {
  const { uid: IBUid, account_id } = data;

  return (
    <div className={styles.card}>
      <CardHeader
        uid={IBUid}
        id={account_id}
        onSave={onSave}
        avatar={data.header_url}
        nickname={data.nickname}
        lastname={data.lastname}
        editable={!data.nickname_update_counter}
      />
      <CardBody
        uid={uid}
        id={account_id}
        onSave={onSave}
        status={data.status}
        description={data.description}
        followers={data.no_of_followers}
      />
    </div>
  );
};
