/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from 'react';
import { connect } from 'dva';
import router from 'umi/router';
import { formatMessage } from 'umi-plugin-react/locale';
import { Button } from 'components';
import Card from './components/Card';

import styles from './index.less';

const MyAccount = ({ dispatch, accounts, currentUser }) => {
  const onSave = async payload => await dispatch({ type: 'user/updateAccount', payload });

  const onAddAccount = () => router.push('/connect_account/add_IB');

  useEffect(() => {
    async function getAccounts() {
      await dispatch({ type: 'user/accounts', payload: { status: 'ALL' } });
    }

    getAccounts();
  }, []);

  return (
    <div className={styles.my_account}>
      {accounts.map((account, index) => (
        <Card
          key={index}
          data={account}
          onSave={onSave}
          uid={currentUser.uid}
        />
      ))}
      <Button block ghost shape="square" icon="add" onClick={onAddAccount}>
        {formatMessage({ id: 'user.account' })}
      </Button>
    </div>
  );
};

export default connect(({ user }) => ({
  accounts: user.accounts,
  currentUser: user.currentUser
}))(MyAccount);
