
import { Link } from 'umi';
import { Fragment, useState } from 'react';
import Textfield from '@atlaskit/textfield';
import { formatMessage } from 'umi-plugin-react/locale';
import { UserParty, Button, notify } from '@/components';
import { validator } from '../../../../shared/helpers';
import oauth from '../../oauth';

import styles from './index.less';

const SignIn = ({ dispatch, location, history }) => {
  const [ fields, setFields ] = useState({
    username: null,
    password: null
  });
  const onClick = async () => {
    if (!validator.email(fields.username)) {
      return notify.warning(formatMessage({ id: `component.userParty.email.message` }));
    } else if (!validator.password(fields.password)) {
      return notify.warning(formatMessage({ id: `component.userParty.password.message` }));
    }

    notify.loading();

    const resp = await dispatch({
      type: 'user/logIn',
      payload: {
        fields
      },
    });
    
    notify.hide();
    if (resp.code === 0) {
      history.replace(`/`)
    }
  }

  const disabled = !(fields.password && fields.username)

  return (
    <div className={styles.wrapper}>
       <UserParty
        description={formatMessage({ id: 'component.userParty.signin.title' })}
        footer={
          <div className={styles.footer_wrapper}>
            <Link to={'/reset-password'} className={styles.reset_password}>
              {formatMessage({ id: 'component.userParty.forgotPassword' })}
            </Link>
          </div>
        }
       >
         <Fragment>
           <div
             className={styles.username}
           >
             <label className={styles.label}>{formatMessage({ id: 'component.userParty.email.label' })}</label>
             <Textfield
               onChange={(event) => setFields({ ...fields, username: event.target.value })}
               placeholder={formatMessage({ id: 'component.userParty.email.placeholder' })}
             />
           </div>
           <div
             className={styles.password}
           >
             <label className={styles.label}>{formatMessage({ id: 'component.userParty.newPassword.label' })}</label>
             <Textfield
               onChange={(event) => setFields({ ...fields, password: event.target.value })}
               placeholder={formatMessage({ id: 'component.userParty.password.placeholder' })}
               type="password"
             />
           </div>

           <div className={styles.code}>
            {/* <div className={styles.code_field}>
              <Textfield 
                placeholder={formatMessage({ id: 'component.userParty.code.placeholder' })}
              />
            </div>
            <img 
              src="/captcha"
              className={styles.code_image} 
              onClick={onImageCodeClick}
            /> */}
           </div>
              
         </Fragment>
          <Button 
            block
            disabled={disabled}
            shape="square"
            onClick={onClick}
          >
            {formatMessage({ id: 'component.userParty.button.signin' })}
          </Button>
      </UserParty>
    </div>
  );
};

export default oauth(SignIn);
