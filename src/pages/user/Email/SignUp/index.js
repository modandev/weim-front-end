import classnames from 'classnames';
import { formatMessage } from 'umi-plugin-react/locale';
import UserParty from '@/components';

import oauth from '../../oauth';
import styles from './index.less';

export default oauth(() => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.body}>
        <UserParty>
          
        </UserParty>
      </div>
    </div>
  );
});
