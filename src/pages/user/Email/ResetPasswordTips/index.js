
import { Link } from 'umi';
import { Fragment } from 'react';
import Textfield from '@atlaskit/textfield';
import Form, { Field, FormFooter, HelperMessage } from '@atlaskit/form';
import { formatMessage } from 'umi-plugin-react/locale';
import { UserParty, Button } from '@/components';
import oauth from '../../oauth';


import styles from './index.less';

const ResetPasswordTips = ({ match }) => {
  const params = match.params;
  const { type, status } = params;
 
  return (
    <div className={styles.wrapper}>
       <UserParty>
         <div className={styles.tips}>{formatMessage({ id: `component.userParty.${type}.${status}.title` })}</div>
      </UserParty>
    </div>
  );
};

export default oauth(ResetPasswordTips);
