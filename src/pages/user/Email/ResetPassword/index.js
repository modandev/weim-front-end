
import { Link } from 'umi';
import { Fragment, useState } from 'react';
import Textfield from '@atlaskit/textfield';
import { formatMessage } from 'umi-plugin-react/locale';
import { UserParty, Button, notify } from '@/components';
import oauth from '../../oauth';

import { validator } from '../../../../shared/helpers';

import styles from './index.less';


const ResetPassword = ({ dispatch, history }) => {
  const [ fields, setFields ] = useState({
    email: null,
    code: null
  });
  const [captcha, setCaptcha] = useState(`/captcha/RESET_PASSWORD?t=${Date.now()}`);

  const onClick = async () => {
    if (!validator.email(fields.email)) {
      return notify.warning(formatMessage({ id: `component.userParty.email.message` }));
    } else if (!validator.verification(fields.code)) {
      return notify.warning(formatMessage({ id: `component.userParty.code.message` }));
    }

    notify.loading();

    const resp = await dispatch({
      type: 'user/resetPassword',
      payload: fields,
    });

    notify.hide();

    if (resp.code === 0) {
      history.replace(`/reset-password/forgot/success`)
    } else {
      history.replace(`/reset-password/forgot/fail`)
    }
  }

  const onImageCodeClick = () => {
    setCaptcha(`/captcha/RESET_PASSWORD?t=${Date.now()}`)
  }

  const disabled = !(fields.code && fields.email)

  return (
    <div className={styles.wrapper}>
       <UserParty
        description={formatMessage({ id: 'component.userParty.forgotPassword.title' })}
        footer={
          <div className={styles.footer_wrapper}>
            <Link to={'/signin'} className={styles.reset_password}>
              {formatMessage({ id: 'component.userParty.forgotPassword.footer.title' })}
            </Link>
          </div>
        }
       >
         <Fragment>
           <div
             className={styles.username}
           >
            <label className={styles.label}>{formatMessage({ id: 'component.userParty.email.label' })}</label>
            <Textfield
              onChange={(event) => setFields({ ...fields, email: event.target.value })}
              placeholder={formatMessage({ id: 'component.userParty.email.placeholder' })}
            />
           </div>

           <div className={styles.code}>
            <div className={styles.code_field}>
              <Textfield 
                onChange={(event) => setFields({ ...fields ,code: event.target.value })}
                placeholder={formatMessage({ id: 'component.userParty.code.placeholder' })}
              />
            </div>
            <img 
              src={captcha}
              className={styles.code_image} 
              onClick={onImageCodeClick}
            />
           </div>
              
         </Fragment>
          <Button 
            block
            disabled={disabled}
            shape="square"
            onClick={onClick}
          >
            {formatMessage({ id: 'component.userParty.button.resetPassword' })}
          </Button>
      </UserParty>
    </div>
  );
};

export default oauth(ResetPassword);
