
import { Link } from 'umi';
import { Fragment, useState } from 'react';
import Textfield from '@atlaskit/textfield';
import { formatMessage } from 'umi-plugin-react/locale';
import { UserParty, Button, notify } from '@/components';
import { validator } from '../../../../shared/helpers';
import oauth from '../../oauth';


import styles from './index.less';

const ChangePassword = ({ location, match, history, dispatch }) => {
  const [ fields, setFields ] = useState({
    password: null,
    confirmPassword: null
  });
  

  const onClick = async () => {
    if (!validator.password(fields.password)) {
      return notify.warning(formatMessage({ id: `component.userParty.newPassword.message` }));
    } else if (fields.password !== fields.confirmPassword) {
      return notify.warning(formatMessage({ id: `component.userParty.newPassword.confirm.message` }));
    }

    notify.loading();

    const resp = await dispatch({
      type: 'user/changePassword',
      payload: {
        username: location.query.username,
        token: location.query.token,
        password: fields.password
      }
    });

    if (resp.code === 0) {
      // notify.warning(formatMessage({ id: 'component.userParty.changePassword.success' }), `success`);

      setTimeout(() => {
        notify.hide();
        history.replace('/signin');
      }, 2500);
    }
  }

  return (
    <div className={styles.wrapper}>
       <UserParty
        description={formatMessage({ id: 'component.userParty.forgotPassword.title' })}
       >
         <Fragment>
           <div
             className={styles.password}
           >
            <label className={styles.label}>{formatMessage({ id: 'component.userParty.newPassword.label' })}</label>
            <Textfield
              type="password"
              onChange={(event) => setFields({ ...fields ,password: event.target.value })}
              placeholder={formatMessage({ id: 'component.userParty.newPassword.placeholder' })}
            />
           </div>

           <div
             className={styles.confirm_password}
           >
            <label className={styles.label}>{formatMessage({ id: 'component.userParty.newPassword.confirm.label' })}</label>
            <Textfield
              type="password"
              onChange={(event) => setFields({ ...fields ,confirmPassword: event.target.value })}
              placeholder={formatMessage({ id: 'component.userParty.newPassword.confirm.placeholder' })}
            />
           </div>
              
         </Fragment>
          <Button 
            block
            shape="square"
            onClick={onClick}
          >
            {formatMessage({ id: 'component.userParty.button.resetPassword' })}
          </Button>
      </UserParty>
    </div>
  );
};

export default oauth(ChangePassword);
