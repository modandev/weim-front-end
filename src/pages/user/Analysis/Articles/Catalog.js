import { connect } from 'dva';
import classnames from 'classnames';
import { Link } from 'umi';
import dayjs from 'dayjs';
import { formatMessage } from 'umi-plugin-react/locale';
import { Icon, Button, Skeleton } from 'components';
import { getQueries } from 'shared/locationParser';

import styles from '../index.less';

export default connect()(({ loading, notes, params, titles, onAdd }) => {
  const { accountId, ticker, analysisId } = params;
  const { organization } = getQueries();

  return (
    <div className={styles.catalog}>
      <div className={classnames(styles.list, { [styles.gap]: loading })}>
        <Skeleton
          loading={loading}
          title={{ rows: 2 }}
          paragraph={{ rows: 4 }}
        >
          {notes.map(({ title, post_id, created_at, views_no, likes_no, comments_no, status }, index) => {
            const isDraft = status === 'DRAFT';

            title = titles[post_id] || title;

            return (
              <div className={styles.listItem} key={post_id}>
                <Link
                  className={
                    classnames(
                      styles.content,
                      {
                        [styles.focused]: analysisId ? (analysisId === post_id) : (index === 0)
                      }
                    )
                  }
                  to={`/analyses/edit/${accountId}/${ticker}/${post_id}?organization=${decodeURIComponent(organization)}`}
                >
                  <div className={styles.header}>
                    <h3 title={title} className={classnames({ [styles.placeholder]: !title })}>
                      {isDraft && (
                        <span className={styles.draft}>
                          {formatMessage({ id: 'user.analysis.investmentResearch.draft' })}
                        </span>
                      )}
                      {title || formatMessage({ id: 'user.analysis.investmentResearch.title.placeholder' })}
                    </h3>
                    <span className={styles.time}>{dayjs(created_at).format('YYYY-MM-DD')}</span>
                  </div>
                  {!isDraft && (
                    <div className={styles.bar}>
                      <span><Icon type="eye" /> {views_no}</span>
                      <span><Icon type="thumbs-up-gray" /> {likes_no}</span>
                      <span><Icon type="comment-gray" /> {comments_no}</span>
                    </div>
                  )}
                </Link>
              </div>
            )
          })}
        </Skeleton>
      </div>
      <Button
        ghost
        icon="add"
        shape="square"
        onClick={onAdd}
      >
        {formatMessage({ id: 'user.analysis.investmentResearch' })}
      </Button>
    </div>
  );
});
