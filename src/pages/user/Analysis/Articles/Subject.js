/* eslint-disable react-hooks/exhaustive-deps */
import { useRef, useState, useEffect } from 'react';
import classnames from 'classnames';
import { Skeleton } from 'components';
import Fullscreen from 'shared/Fullscreen';
import Editor from './Editor';

import styles from '../index.less';

export default ({ loading, notes, params, onTitleEdit }) => {
  const { analysisId } = params;
  const editorRef = useRef(null);
  const [fullscreen, setFullscreen] = useState(false);

  const onFullscreenToggle = () =>
    editorRef.current[fullscreen ? 'exitFullscreen' : 'enterFullscreen']();
  const onFullscreenChange = isFullscreen => setFullscreen(isFullscreen);

  useEffect(() => {
    editorRef.current = new Fullscreen(
      document.getElementById('editor'),
      onFullscreenChange
    );

    return () => {
      editorRef.current.destroy();
    };
  }, []);

  return (
    <div className={classnames(styles.editor, { [styles.gap]: loading })} id="editor">
      <Skeleton
        loading={loading}
        title={{ rows: 2 }}
        paragraph={{ rows: 6 }}
        chart
      >
        {notes.map((note, index) => (
          <div
            key={note.post_id}
            className={
              classnames(
                styles.area,
                {
                  [styles.visible]: analysisId ? (note.post_id === analysisId) : (index === 0)
                }
              )
            }
          >
            <Editor
              note={note}
              params={params}
              onTitleEdit={onTitleEdit}
              fullscreen={fullscreen}
              onFullscreenToggle={onFullscreenToggle}
            />
          </div>
        ))}
      </Skeleton>
    </div>
  );
};
