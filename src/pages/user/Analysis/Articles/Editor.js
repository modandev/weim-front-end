/* eslint-disable react-hooks/exhaustive-deps */
import { createRef, Component } from 'react';
import classnames from 'classnames';
import { connect } from 'dva';
import router from 'umi/router';
import { formatMessage } from 'umi-plugin-react/locale';
import debounce from 'lodash.debounce';
import { Icon, Button, Modal, StockAreaCode } from '@/components';
import EditorJS from 'shared/Editor';
import { getQueries } from 'shared/locationParser';
import { stringLength } from 'shared/helpers';
import { stringify } from 'querystring';

import styles from '../index.less';

class Editor extends Component {
  constructor(props) {
    super(props);

    const { note } = props;

    this.state = {
      form: {
        title: note.title || '',
        content: note.content || '',
        summary: note.summary || '',
        prediction_days: note.prediction_days || null,
        prediction_price: note.prediction_price || null,
        prediction_side: note.prediction_side || 'LONG',
      },
      saving: false,
      modalProps: null,
    };
    this.saveForm = debounce(this.onSave, 3000);
  }

  _selector = createRef();
  _modalType = null;
  _editor = null;

  componentDidMount() {
    const { note } = this.props;

    if (note) {
      const { post_id, content } = note;

      this._editor = EditorJS({
        holder: `investment-research-${post_id}`,
        placeholder: formatMessage({ id: 'user.analysis.investmentResearch.subject.placeholder' }),
        onChange: () => this.onFieldChange({ name: 'content' }),
        ...(content && content.includes('blocks') ? { data: JSON.parse(content) } : {}),
      });
    }
  }

  updateForm = async ({ name, value }) => {
    switch (name) {
      case 'prediction_side':
        break;

      case 'content':
        const data = await this._editor.save();

        value = data.blocks.length ? JSON.stringify(data) : '';
        break;

      case 'title':
        const { onTitleEdit, note } = this.props;

        onTitleEdit({ analysisId: note.post_id, title: value });
        break;

      default:
        break;
    }

    this.setState(prevState => ({ form: { ...prevState.form, [name]: value.trim() } }));
  };

  onSave = ({ name, value }) => {
    if (name === 'prediction_price' && +value === 0) {
      this.updateForm({ name, value: '' });
    } else {
      this.setState({ saving: true }, async () => {
        const { dispatch, note, params } = this.props;
        let analysisId = note.post_id;
        const fields = this.state.form;
        const { ticker, accountId } = params;

        if (note.temp) {
          ({ post_id: analysisId } = await dispatch({
            type: 'user/newDraft',
            payload: {
              fields: {
                title: '',
                summary: '',
                content: '',
                ticker,
                accountId,
              },
              skipUpdateAnalysis: true,
            },
          }));
        }

        if (name === 'content') {
          const editorData = await this._editor.save();

          fields.content = JSON.stringify(editorData);
        }

        await dispatch({ type: 'user/saveDraft', payload: { analysisId, fields } });
        note.temp && await dispatch({ type: 'user/analysis', payload: { accountId, ticker } });
        this.setState({ saving: false });
      });
    }
  };

  validateField = ({ name, value }) => {
    let valid = true;

    if (name === 'prediction_days' || name === 'prediction_price') {
      const regexp = name === 'prediction_days' ? /^[1-9](\d+)?$/ : /^\d+(\.\d{0,2})?$/;

      if (value && !regexp.test(value)) {
        valid = false;
      }
    } else if (name === 'title' || name === 'summary') {
      const maxLength = name === 'title' ? 50 : 320;

      if (stringLength(value) > maxLength) {
        valid = false;
      }
    }

    return valid;
  };

  onFieldChange = field => {
    if (this.props.note.status === 'DRAFT') {
      if (this.validateField(field)) {
        this.updateForm(field);
        this.saveForm(field);
      }
    }
  };

  onDiscard = () => {
    this._modalType = 'DISCARD';
    this.setState({
      modalProps: {
        title: formatMessage({ id: 'user.analysis.investmentResearch.discard' }),
        okText: formatMessage({ id: 'user.analysis.investmentResearch.discard' }),
        description: formatMessage({ id: 'user.analysis.investmentResearch.discard.tips' }),
      },
    });
  };

  onPreview = () =>
    window.open(
      `/analyses/preview/${this.props.note.post_id}?organization=${getQueries().organization}`,
      '_blank',
    );

  onPublish = () => {
    this.onSave({});
    this._modalType = 'PUBLISH';
    this.setState({
      modalProps: {
        title: formatMessage({ id: 'user.analysis.investmentResearch.publish' }),
        okText: formatMessage({ id: 'user.analysis.investmentResearch.publish' }),
        description: formatMessage({ id: 'user.analysis.investmentResearch.publish.tips' }),
      },
    });
  };

  onConfirm = async () => {
    const { dispatch, note, params, currentUser } = this.props;

    switch (this._modalType) {
      case 'DISCARD':
        await dispatch({
          type: 'user/deleteDraft',
          payload: { ...params, analysisId: note.post_id },
        });
        this.onCancel();
        router.replace(`/analyses/edit/${params.accountId}/${params.ticker}`);
        break;

      case 'PUBLISH':
        this.saveForm.cancel();
        await dispatch({ type: 'user/publishDraft', payload: { analysisId: note.post_id } });
        router.replace(`/analyses/${note.post_id}?${stringify({
          userId: currentUser.uid,
          accountId: params.accountId,
          symbol: params.ticker,
          published: 1,
        })}`);
        break;

      default:
        break;
    }
  };

  onCancel = () => {
    this._modalType = null;
    this.setState({ modalProps: null });
  };

  render() {
    const { saving, form, modalProps } = this.state;
    const { note, fullscreen, params, onFullscreenToggle } = this.props;
    const isDraft = note.status === 'DRAFT';
    const isTemp = note.temp;
    const { title, summary, prediction_side } = form;
    const { organization , countryCode} = getQueries();
    const { ticker, tickerInfo } = params;

    return (
      <>
        <div className={styles.header}>
          <div className={styles.hints}>
            {isDraft && (
              <span className={styles.draft}>{formatMessage({ id: 'user.analysis.investmentResearch.draft' })}</span>
            )}
            <span
              className={classnames(styles.saved, { [styles.shown]: saving })}
            >
              {formatMessage({ id: 'user.analysis.investmentResearch.saving' })}
            </span>
          </div>
          <div className={styles.actions}>
            {isDraft && !isTemp && <Icon type="discard"
                                         as="button"
                                         onClick={this.onDiscard} />}
            {isDraft && !isTemp && <Icon type="preview"
                                         as="button"
                                         onClick={this.onPreview} />}
            <Icon type={fullscreen ? 'exit-fullscreen' : 'fullscreen'}
                  as="button"
                  onClick={onFullscreenToggle} />
            {isDraft && (
              <Button
                icon="publish"
                onClick={this.onPublish}
                disabled={Object.values(form).some(item => !item)}
              >
                {formatMessage({ id: 'user.analysis.investmentResearch.publish' })}
              </Button>
            )}
          </div>
        </div>
        <div className={styles.body}>
          <div className={styles.forecast}>
            {organization && ticker && (
              <div className={styles.company}>
                <div className={styles.name}>
                  <Icon type="organization" />
                  {decodeURIComponent(organization)}
                </div>
                <div className={styles.symbol}>
                  {formatMessage({ id: 'common.symbol' })}:&nbsp;
                  <StockAreaCode countryCode={countryCode} />&nbsp;
                {ticker.split('.')[1]}
                </div>
              </div>
            )}
            <div className={styles.value}>
              {formatMessage({ id: 'user.analysis.investmentResearch.PriceForecasting' })}&nbsp;
              <div className={styles.price}>
                <input
                  disabled={!isDraft}
                  maxLength={6}
                  value={form[`prediction_price`] || ''}
                  onChange={e => this.onFieldChange({ name: `prediction_price`, value: e.target.value })}
                />
              </div>；&nbsp;&nbsp;

              {formatMessage({ id: 'user.analysis.investmentResearch.PredictedArrivalTime' })}&nbsp;
              <div className={styles.days}>
                <input
                  disabled={!isDraft}
                  value={form[`prediction_days`] || ''}
                  onChange={e => this.onFieldChange({ name: `prediction_days`, value: e.target.value })}
                />
                <span className={styles.unit}>
                  {formatMessage({ id: 'user.analysis.investmentResearch.Day' })}
                </span>
              </div>；&nbsp;&nbsp;

              <div className={styles.button_group}>
                <div
                  onClick={() => this.onFieldChange({ name: 'prediction_side', value: 'LONG' })}
                  className={classnames(styles.guss_button, styles.bullish, prediction_side === 'LONG' ? styles.active : '')}>
                  {formatMessage({ id: 'user.analysis.investmentResearch.forecast.LONG' })}
                </div>
                <div
                  onClick={() => this.onFieldChange({ name: 'prediction_side', value: 'SHORT' })}
                  className={classnames(styles.guss_button, styles.bearish, prediction_side === 'SHORT' ? styles.active : '')}>
                  {formatMessage({ id: 'user.analysis.investmentResearch.forecast.SHORT' })}
                </div>
              </div>
            </div>
          </div>
          <div className={styles.essentials}>
            <div className={styles.title}>
              <input
                value={title}
                disabled={!isDraft}
                onChange={e => this.onFieldChange({ name: 'title', value: e.target.value })}
                placeholder={formatMessage({ id: 'user.analysis.investmentResearch.title.placeholder' })}
              />
              {isDraft && <span className={styles.limit}>{stringLength(title)}/50</span>}
            </div>
            <div className={styles.summary}>
              <textarea
                value={summary}
                disabled={!isDraft}
                onChange={e => this.onFieldChange({ name: 'summary', value: e.target.value })}
                placeholder={formatMessage({ id: 'user.analysis.investmentResearch.summary.placeholder' })}
              />
              {isDraft && <span className={styles.limit}>{stringLength(summary)}/320</span>}
            </div>
          </div>
          <div className={styles.separator}>
            <span className={styles.label}>{formatMessage({ id: 'user.analysis.investmentResearch.body' })}</span>
            <i className={styles.line} />
          </div>
          <div className={styles.subject}>
            <div id={`investment-research-${note.post_id}`} />
            {!isDraft && <div className={styles.mask} />}
          </div>
        </div>
        <Modal
          fixed
          mask
          onOk={this.onConfirm}
          onCancel={this.onCancel}
          visible={!!modalProps}
          {...modalProps}
        />
      </>
    );
  }
}

export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(Editor);
