/* eslint-disable react-hooks/exhaustive-deps */
import { useRef, useState, useEffect } from 'react';
import { connect } from 'dva';
import router from 'umi/router';

import Catalog from './Catalog';
import Subject from './Subject';

import styles from '../index.less';

export default connect(({ user }) => ({
  analysis: user.analysis
}))(({ analysis, params, dispatch }) => {
  const backup = useRef([]);
  const { accountId, ticker } = params;
  const [loading, setLoading] = useState(true);
  const [titles, setTitles] = useState({});

  const onAdd = async () => {
    const { post_id } = await dispatch({
      type: 'user/newDraft',
      payload: {
        fields: {
          title: '',
          summary: '',
          content: '',
          ticker,
          accountId
        }
      }
    });

    router.push(`/analyses/edit/${accountId}/${ticker}/${post_id}${window.location.search}`);
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
  };

  const onTitleEdit = ({ title, analysisId }) => {
    const _titles = { ...titles };

    _titles[analysisId] = title;
    setTitles(_titles);
  };

  useEffect(() => {
    async function fetchAnalysis() {
      const analysis = await dispatch({ type: 'user/analysis', payload: { accountId, ticker } });

      if (analysis && analysis.hits.length === 0) {
        const now = new Date().valueOf();

        backup.current = [{
          post_id: now.toString(),
          created_at: now,
          status: 'DRAFT',
          temp: true
        }];
      }

      setLoading(false);
    }

    if (accountId && ticker) {
      setLoading(true);
      fetchAnalysis();
    }
  }, [accountId, ticker]);

  return (
    <div className={styles.main}>
      <Catalog
        notes={analysis.length ? analysis : backup.current}
        loading={loading}
        params={params}
        onAdd={onAdd}
        titles={titles}
      />
      <Subject
        notes={analysis.length ? analysis : backup.current}
        loading={loading}
        params={params}
        onTitleEdit={onTitleEdit}
      />
    </div>
  );
});
