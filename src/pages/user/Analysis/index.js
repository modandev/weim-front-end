import Accounts from './Accounts';
import Articles from './Articles';

import styles from './index.less';

const Analysis = ({ match: { params } }) => {
  return (
    <section className={styles.wrapper}>
      <Accounts params={params} />
      <Articles params={params} />
    </section>
  );
};

export default Analysis;
