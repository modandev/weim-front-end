/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';
import { connect } from 'dva';
import router from 'umi/router';
import { Icon, Modal, Button, Skeleton } from 'components';
import DepthChart from '@/components/DepthChart'
import EditorJS from 'shared/Editor';
import { getQueries } from 'shared/locationParser';
import { stringify } from 'querystring';
import styles from './index.less';

const EDITOR_HOLDER = 'article-content';

export default connect(({ user }) => ({
  currentUser: user.currentUser
}))(({ match, dispatch, currentUser }) => {
  const { organization } = getQueries();

  const [article, setArticle] = useState({});
  const [visible, setVisible] = useState(false);

  const onPublish = () => setVisible(true);
  const onCancel = () => setVisible(false);
  const onConfirm = async () => {
    const { analysisId, accountId } = match.params;

    await dispatch({ type: 'user/publishDraft', payload: { analysisId } });
    router.replace(`/analyses/${analysisId}?${stringify({
      userId:currentUser.uid,
      accountId: accountId,
      symbol: article.ticker,
      published: 1
    })}`);
  };

  useEffect(() => {
    const { analysisId } = match.params;

    async function getAnalysisDetail() {
      const data = await dispatch({ type: 'user/analysisDetail', payload: { analysisId } });
      EditorJS({
        holder: EDITOR_HOLDER,
        data: JSON.parse(data.content.includes('blocks') ? data.content : '{}'),
      });
      setArticle(data);
    }

    getAnalysisDetail();
  }, []);

  return (
    <section className={styles.preview}>
      <div className={styles.header}>
        <h4>{formatMessage({ id: 'user.analysis.investmentResearch.previewPage.title' })}</h4>
        <Button icon="publish" onClick={onPublish}>
          {formatMessage({ id: 'user.analysis.investmentResearch.publish' })}
        </Button>
      </div>
      <div className={styles.content}>
        <Skeleton
          loading={!Object.getOwnPropertyNames(article).length}
          title={{ row: 2 }}
          chart
          paragraph={{ rows: 5 }}
        >
          <div className={styles.title}>
            <h2>{article.title}</h2>
          </div>
          {organization && article.ticker?.key && (
            <div className={styles.company}>
              <div className={styles.name}>
                <Icon type="organization" />
                {decodeURIComponent(organization)}
              </div>
              <div className={styles.symbol}>
                {formatMessage({ id: 'common.symbol' })}: {article.ticker?.key.split('.')[1]}
              </div>
            </div>
          )}
          {article.ticker && (
            <div className={styles.chart}>
              <DepthChart
                analysis={article}
                ticker={article.ticker.key}
              />
            </div>
          )}
          <div className={styles.text}>
            <p>{article.summary}</p>
            <div id={EDITOR_HOLDER} />
            <div className={styles.mask} />
          </div>
        </Skeleton>
      </div>
      <Modal
        fixed
        mask
        onOk={onConfirm}
        visible={visible}
        onOk={onConfirm}
        onCancel={onCancel}
        title={formatMessage({ id: 'user.analysis.investmentResearch.publish' })}
        okText={formatMessage({ id: 'user.analysis.investmentResearch.publish' })}
        description={formatMessage({ id: 'user.analysis.investmentResearch.publish.tips' })}
      />
    </section>
  );
});
