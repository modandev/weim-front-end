/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-loop-func */
import { useRef, useState, useEffect } from 'react';
import { connect } from 'dva';
import router from 'umi/router';
import classnames from 'classnames';
import { Link } from 'umi';
import { Skeleton, Collapse, EllipsisText } from 'components';
import { getQueries } from 'shared/locationParser'

import styles from './index.less';
import Button from '@/components/Buttons';
import { StockAreaCode } from '@/components';

const { Panel } = Collapse;

export default connect()((props) => {
  const { dispatch, params } = props;
  let { accountId, ticker } = params;
  const defaultActiveKey = useRef();
  const [loading, setLoading] = useState(true);
  const [accounts, setAccounts] = useState([]);

  useEffect(() => {
    let redirect = false;
    let { organization, countryCode } = getQueries();

    async function fetchData() {
      const response = await dispatch({ type: 'user/accounts' });

      if (response.length) {
        if (!accountId) {
          const account = response[0];

          if (account) {
            redirect = true;
            accountId = account.account_id;
          }
        }

        const tickers = await Promise.all(
          response.map(({ account_id }, index) => {
            if (account_id === accountId) {
              defaultActiveKey.current = `${index}`;
            }

            return dispatch({ type: 'user/tickers', payload: { accountId: account_id } });
          })
        );

        if (!ticker) {
          redirect = true;
          ({ name: organization, key: ticker, country_code:countryCode } = tickers[0][0]);

        } else {
          if (!organization) {
            redirect = true;

            for (let i = 0, tickersLen = tickers.length; i < tickersLen; i++) {
              const curTicker = tickers[i];

              if (Array.isArray(curTicker)) {
                const matched = curTicker.find(({ key }) => key === ticker);

                if (matched) {
                  organization = matched.name;
                  countryCode = matched.country_code
                  break;
                }
              } else {
                if (curTicker.key === ticker) {
                  organization = curTicker.name;
                  countryCode = curTicker.country_code

                  break;
                }
              }
            }
          }
        }

        redirect && router.replace(`/analyses/edit/${accountId}/${ticker}?organization=${encodeURIComponent(organization)}&countryCode=${countryCode}`);
        setAccounts(response.map((item, index) => ({ ...item, tickers: tickers[index] })));
        setLoading(false);
      }
    }

    fetchData();
  }, []);

  return (
    <aside className={classnames(styles.aside, { [styles.gap]: loading })}>
      <Skeleton
        loading={loading}
        title
        paragraph
      >
        <Collapse activeKey={defaultActiveKey.current}>
          {accounts.map(({ account_id, nickname, tickers = [] }, index) => (
            <Panel header={ <EllipsisText text={nickname}>{nickname}</EllipsisText>} key={index}>
              <ul>
                {tickers.map(({ name, symbol, key, exchange, country_code }) => (
                  <li key={key} className={classnames({ [styles.active]: ticker === key })}>
                    <Link to={`/analyses/edit/${account_id}/${key}?organization=${encodeURIComponent(name)}&countryCode=${country_code}`}>
                      <div className={styles.company}>{name}</div>
                      <div className={styles.info}>
                        <StockAreaCode countryCode={country_code}/>&nbsp;
                        <span className={styles.code}>{symbol}</span>
                      </div>
                    </Link>
                  </li>
                ))}
              </ul>
            </Panel>
          ))}
        </Collapse>
      </Skeleton>
    </aside>
  );
});
