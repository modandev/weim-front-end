import { useState, createRef } from 'react';
import { connect } from 'dva';
import router from 'umi/router';
import { formatMessage } from 'umi-plugin-react/locale';
import { IB_UID_REGEXP, EMAIL_REGEXP } from 'shared/regexps';
import { Tips, Icon, Button } from 'components';
import Input from './components/Input';

import styles from './index.less';

const Connect = ({ dispatch }) => {
  const [state, setState] = useState({});
  const forms = Array.from(
    {
      0: 'uid',
      1: 'lastname',
      2: 'firstname',
      3: 'email',
      length: 4
    },
    name => ({
      name,
      value: '',
      ref: createRef(),
    })
  );
  const [UID, LASTNAME, FIRSTNAME, EMAIL] = forms;

  const onInput = () => state && setState({});
  const onAdd = async () => {
    const result = forms.map(({ ref }) => ref.current._validateSelf());

    if (result.every(val => !!val)) {
      const { error } = await dispatch({
        type: 'user/createAccount',
        payload: {
          fields: {
            uid: result[0],
            lastname: result[1],
            firstname: result[2],
            email: result[3],
            broker: 'IBKR'
          }
        },
      });

      if (error) {
        const { message } = error.graphQLErrors[0];

        setState({ success: false, errorMessage: message });
      } else {
        setState({ success: true });
        setTimeout(() => {
          router.push('/personal/accounts');
        }, 2000);
      }
    }
  };

  return (
    <section className={styles.connect}>
      <Tips
        visible={'success' in state}
        type={state.success ? 'safe' : 'danger'}
        header={
          <>
            <Icon type={state.success > 0 ? 'check-circle-outline' : 'tips'} />
            {state.success > 0 ? formatMessage({ id: 'user.connectIB.subaccount.connect.done' }) : formatMessage({ id: 'user.connectIB.subaccount.connect.connected' })}
          </>
        }
        description={formatMessage({ id: `user.connectIB.subaccount.connect.${state.success > 0 ? 'notify' : 'help'}` })}
      >
        <Input
          ref={UID.ref}
          name={UID.name}
          onInput={onInput}
          pattern={IB_UID_REGEXP}
          label={formatMessage({ id: 'user.connectIB.subaccount.uid' })}
        />
        <div className={styles.row}>
          <Input
            onInput={onInput}
            ref={LASTNAME.ref}
            name={LASTNAME.name}
            className={styles.margin}
            label={formatMessage({ id: 'user.connectIB.subaccount.lastname' })}
          />
          <Input
            onInput={onInput}
            ref={FIRSTNAME.ref}
            name={FIRSTNAME.name}
            label={formatMessage({ id: 'user.connectIB.subaccount.firstname' })}
          />
        </div>
        <div className={styles.row}>
          <Input
            ref={EMAIL.ref}
            name={EMAIL.name}
            onInput={onInput}
            rule={EMAIL_REGEXP}
            className={styles.margin}
            label={formatMessage({ id: 'user.connectIB.subaccount.email' })}
          />
          <Button icon="add-white" size="large" onClick={onAdd}>
            {formatMessage({ id: 'user.account' })}
          </Button>
        </div>
      </Tips>
    </section>
  );
};

export default connect()(Connect);
