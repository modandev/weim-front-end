import { useEffect } from 'react';
import QA from './Q&A';
import Connect from './Connect';
import BeManager from './BeManager';

import styles from './index.less';

export default () => {
  useEffect(() => {
    window.scrollTo({ top: 0, left: 0 });
  }, []);

  return (
    <section className={styles.connect_IB}>
      <QA />
      <section className={styles.others}>
        <Connect />
        <BeManager />
      </section>
    </section>
  );
};
