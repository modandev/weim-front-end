import { PureComponent } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';
import classnames from 'classnames';

import styles from './index.less';

export default class extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      error: false,
    }
  }

  _validateSelf = () => {
    const { rule } = this.props;
    const { value } = this.state;

    if (!value || !value.trim() || (rule && !rule.test(value))) {
      this.setState({ error: true });
      return false;
    }

    return value;
  };

  onTextInput = e => {
    const { onInput, pattern } = this.props;
    const value = e.target.value;

    if (pattern) {
      if (!pattern.test(value)) {
        return;
      }
    }

    onInput(value);
    this.setState({ value, error: false });
  };

  render() {
    const { value, error } = this.state;
    const { name, label, className } = this.props;

    return (
      <div className={classnames(styles.input, className)}>
        <div className={styles.label}>{label}</div>
        <input
          name={name}
          value={value}
          onChange={this.onTextInput}
          className={classnames({ [styles.error]: error })}
        />
        <div
          className={
            classnames(
              styles.hint,
              { [styles.visible]: error }
            )
          }
        >
          {error ? formatMessage({ id: value ? 'message.format.error' : 'message.required' }) : ''}
        </div>
      </div>
    );
  };
};
