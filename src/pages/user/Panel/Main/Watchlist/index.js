/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from 'react';
import classnames from 'classnames';
import { connect } from 'dva';
import { Skeleton } from 'components';
import THead from './THead';
import TBody from './TBody';

import styles from './index.less';
import noWatching from '@/assets/common/img_no_watching.png';
import { formatMessage } from 'umi-plugin-react/locale';

export default connect(({ user }) => ({
  watchlist: user.watchlist,
}))(({ dispatch, watchlist }) => {
  const [loading, setLoading] = useState(true);
  const [sortableWatchlist, setSortableWatchlist] = useState([]);

  const onSort = ({ key, order }, list = sortableWatchlist) => {
    if (key && order) {
      setSortableWatchlist(
        [...list].sort((a, b) => {
          const itemA = a[key];
          const itemB = b[key];

          if (typeof itemA === 'string') {
            return order === 'asc' ? (itemA.localeCompare(itemB)) : (itemB.localeCompare(itemA));
          } else if (typeof itemA === 'number') {
            return order === 'asc' ? (itemA - itemB) : (itemB - itemA);
          }

          return 0;
        }),
      );
    } else {
      setSortableWatchlist(watchlist);
    }
  };

  const onExchangeSelect = (exchangeKey, sorter) =>
    onSort(sorter, exchangeKey  ? watchlist.filter(({ exchange }) => (exchangeKey === exchange || exchangeKey === 'All')) : watchlist);

  useEffect(() => {
    if (watchlist.length && !sortableWatchlist.length) {
      setSortableWatchlist(watchlist);
    }
  }, [watchlist]);

  useEffect(() => {
    (async () => {
      await dispatch({ type: 'user/watchlist' });
      setLoading(false);
    })();
  }, []);

  return (
    <div className={classnames(styles.watchlist, { [styles.gap]: loading })}>
      <Skeleton
        title
        loading={loading}
        paragraph={{ rows: 10 }}
      >
        <table>
          <THead
            onSort={onSort}
            onExchangeSelect={onExchangeSelect}
            exchange={['All', ...new Set(watchlist.map(({ exchange }) => exchange))]}
          />
          <TBody data={sortableWatchlist.length ? sortableWatchlist : []} />
        </table>
        {sortableWatchlist.length === 0 ? <div className={styles.no_watching}>
                      <img src={noWatching}
                           alt="" />
            <span>{formatMessage({ id: 'home.NoWatching' })}</span>
          </div> : null}
      </Skeleton>
    </div>
  );
});
