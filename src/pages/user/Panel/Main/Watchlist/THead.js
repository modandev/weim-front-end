/* eslint-disable default-case */
import { memo, useState, createRef } from 'react';
import classnames from 'classnames';
import { formatMessage } from 'umi-plugin-react/locale';

import styles from './index.less';

const DEFAULT_SORTER = {
  key: 'current_price',
  order: 'desc'
};

export default memo(({ exchange, onSort, onExchangeSelect }) => {
  const sorter = [
    { label: formatMessage({ id: 'user.watchlist.name' }), key: 'name', sortable: true, width: '8%' },
    { label: formatMessage({ id: 'user.watchlist.symbol' }), key: 'symbol', sortable: true, width: '8%' },
    {
      label: formatMessage({ id: 'user.watchlist.exchange' }),
      key: 'exchange',
      sortable: false,
      selectable: true,
      width: '10%',
      ref: createRef(),
      dropdownData: exchange
    },
    { label: formatMessage({ id: 'user.watchlist.currentPrice' }), key: 'current_price', sortable: true, width: '10%' },
    { label: formatMessage({ id: 'user.watchlist.accountsHolding' }), key: 'account_holders_no', sortable: true, width: '15%' },
    { label: formatMessage({ id: 'user.watchlist.earningsCalendar' }), key: 'report_date', sortable: true, width: '15%' },
  ];

  const [sortOrder, setSortOrder] = useState(DEFAULT_SORTER);
  const [exchangeKey, setExchangeKey] = useState('All');

  const onSortClick = ({ key, sortable }) => {
    if (sortable) {
      const newState = {};

      if (sortOrder.key && sortOrder.key === key) {
        switch (sortOrder.order) {
          case 'asc':
            Object.assign(newState, { key, order: 'desc' });
            break;

          case 'desc':
            break;
        }
      } else {
        Object.assign(newState, { key, order: 'asc' });
      }

      setSortOrder(newState);
      onSort(newState);
    }
  };

  const onOptionClick = (key, ref) => {
    ref.current.blur();

    if (exchangeKey !== key) {
      setExchangeKey(key);
      onExchangeSelect(key, sortOrder);
    } else {
      setExchangeKey(null);
      onExchangeSelect(null, sortOrder);
    }
  };

  return (
    <thead>
      <tr>
        {sorter.map(({ label, key, sortable, selectable, width, dropdownData, ref }) => (
          <th key={key} width={width}>
            <div
              onClick={() => onSortClick({ key, sortable })}
              tabIndex="-1"
              className={
                classnames(
                  styles.tableCell,
                  {
                    [styles.tableSorter]: sortable,
                    [styles.tableSelector]: selectable,
                    [styles[sortOrder.order]]: sortOrder.key === key,
                    [styles.selected]: selectable && !!exchangeKey
                  }
                )
              }
              {...(ref ? { ref } : {})}
            >
              <span>
                {label}
              </span>
              {selectable && (
                <div className={styles.selector}>
                  <ul>
                    {dropdownData.map(item => (
                      <li
                        key={item}
                        onClick={() => onOptionClick(item, ref)}
                        className={classnames({ [styles.active]: exchangeKey === item })}
                      >
                        {formatMessage({id:item})}
                      </li>
                    ))}
                  </ul>
                </div>
              )}
            </div>
          </th>
        ))}
      </tr>
    </thead>
  );
});
