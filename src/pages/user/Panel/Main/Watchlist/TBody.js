import { Link } from 'umi';
import { semicolon } from 'shared/helpers';
import { Icon } from '@/components';
import styles from './index.less';

export default ({ data }) => {
  return (
    <tbody>
      {data.map(({ key, name, symbol, exchange, currency, current_price, account_holders_no, report_date, report_time }, index) => (
        <tr key={key}>
          <td className={styles.hilite}>
            <Link to={`/asset/${key}/account`}>
              {name}
            </Link>
          </td>
          <td className={styles.hilite}>
            <Link to={`/asset/${key}/account`}>
              {symbol}
            </Link>
          </td>
          <td>{exchange}</td>
          <td className={styles.strong}>{`${currency} ${semicolon(current_price.toFixed(3))}`}</td>
          <td className={styles.strong}>{account_holders_no}</td>
          <td className={styles.strong}>
            <div className="flex-center">
              {report_date}&nbsp;<Icon type={report_time === ' PRE_MARKET' ? ' pre_market' : report_time === 'AFTER_MARKET' ? 'after_market' : 'none_market'} />
            </div>
          </td>
        </tr>
      ))}
    </tbody>
  );
};
