/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import classnames from 'classnames';
import { formatMessage } from 'umi-plugin-react/locale';
import router from 'umi/router';
import { Icon, Skeleton, Avatar } from 'components';

import styles from './index.less';

export default ({ match, dispatch }) => {
  const { subdirectoriesId: accountId } = match.params;

  const [loading, setLoading] = useState(true);
  const [list, setList] = useState([]);

  useEffect(() => {
    (async () => {
      setLoading(true);

      const data = await dispatch({ type: 'user/accountFollowers', payload: { accountId } });

      setList(data.hits || []);
      setLoading(false);
    })();
  }, [accountId]);

  return (
    <div className={styles.followers}>
      <div className={styles.header}>
        <Icon type="back" as="button" onClick={router.goBack} />
        {formatMessage({ id: 'user.followersList' })}
      </div>
      <div className={classnames(styles.list, { [styles.pending]: loading })}>
        <Skeleton
          title
          paragraph
          loading={loading}
        >
          {list.map(({ avatar, lastName, firstName, uid }) => (
            <div className={styles.item} key={uid}>
              <div className={styles.content}>
                <Avatar src={avatar} theme="white" backup={lastName} />
                <span className={styles.name}>{lastName}{firstName}</span>
              </div>
            </div>
          ))}
        </Skeleton>
      </div>
    </div>
  );
};
