import React, {useCallback, useState, useEffect } from 'react'
import { formatMessage } from 'umi-plugin-react/locale';
import { useMutation } from '@apollo/client';
import Button from 'components/Buttons';
import { Avatar } from 'components';
import { FOLLOW, UN_FOLLOW } from 'shared/graphql/gql/follow';
import FollowButton from '@/components/FollowButton'
import styles from './index.less';

function Basic ({ uid, account, onUpdateAccounts }) {
  const { header_url, lastName, nickname, no_of_followers, is_followed, account_id: accountId } = account;
  const [isFollowed, setIsFollowed] = useState(is_followed)
  const [noOfFollowers, setNoOfFollowers] = useState(no_of_followers)

  useEffect(() => {
    setIsFollowed(is_followed);
    setNoOfFollowers(no_of_followers);
  }, [
    is_followed,no_of_followers
  ])

  const followOnChange = useCallback(() => {
    setIsFollowed(!isFollowed);
    setNoOfFollowers(noOfFollowers + (isFollowed ? +1 : -1));
  })

  // const [doFollow, { loading: followLoading }] = useMutation(FOLLOW, {
  //   variables: { accountId },
  //   onCompleted() {
  //     onUpdateAccounts({ uid });
  //   },
  // });
  // const [doUnfollow, { loading: unfollowLoading }] = useMutation(UN_FOLLOW, {
  //   variables: { accountId },
  //   onCompleted() {
  //     onUpdateAccounts({ uid });
  //   },
  // });


  return (
    <div className={styles.user}>
      <Avatar className={styles.user_avatar} src={header_url} backup={lastName} />
      <div className={styles.user_content}>
        <div className={styles.user_nickname}>{nickname}</div>
        <span className={styles.user_follow}>
          {formatMessage({ id: 'common.follower' })}:{` `}
          {noOfFollowers || 0}
        </span>
      </div>

      <div className={styles.user_follow}>
        <FollowButton account_id={accountId} is_followed={isFollowed} userId={account.user_id} onChange={followOnChange}></FollowButton>
      </div>

    </div>
  );
}

export default Basic;
