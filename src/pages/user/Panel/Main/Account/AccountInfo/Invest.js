import styles from './index.less';

function InvestRow ({ name, value }) {
  return (
    <div className={styles.invest_row}>
      <div className={styles.invest_row_name}>
        {name}
      </div>
      <div className={styles.invest_row_value}>
        {value}
      </div>
    </div>
  )
}

export default function Invest ({ account }) {
  if (!account.description) {
    return null;
  }

  return (
    <div className={styles.invest}>
      {/* <div className={styles.invest_info}>
        <InvestRow name="平均持仓时间:" value="35.67天" />
        <InvestRow name="平均收益:" value="32.09%" />
        <InvestRow name="分析报告:" value="39" />
        <InvestRow name="现有仓位:" value="4" />
        <InvestRow name="已平仓:" value="1" />
      </div> */}
      <div className={styles.invest_desc}>
        {account.description}
      </div>
    </div>
  );
}
