import { formatMessage } from 'umi-plugin-react/locale';
import { Skeleton } from '@/components';
import Panel from '../../../components/Panel';
import Invest from './Invest';
import Basic from './Basic';

import styles from './index.less';

export default function AccountDetail ({ loading, match, accounts, onUpdateAccounts }) {
  const { uid, subdirectoriesId: accountId } = match.params;
  let currentAccount = {};

  if (accountId) {
    currentAccount = accounts.find(({ account_id }) => accountId === account_id);
  }

  return (
    <section className={styles.intro}>
      <Panel className={styles.wrapper} title={formatMessage({ id: 'user.panel.account.profile' })}>
        <Skeleton
          loading={loading}
          avatar
          title={{ rows: 2 }}
          chart
        >
        <Basic account={currentAccount} onUpdateAccounts={onUpdateAccounts} uid={+uid} />

          <Invest account={currentAccount} />
        </Skeleton>
      </Panel>
    </section>
  );
}
