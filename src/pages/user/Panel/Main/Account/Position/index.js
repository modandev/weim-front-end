/* eslint-disable react-hooks/exhaustive-deps */
import { useRef, useState, useEffect } from 'react';
import { connect } from 'dva';
import router from 'umi/router';
import { formatMessage } from 'umi-plugin-react/locale';
import { Button } from 'components';
import LineChart from './LineChart';

import Panel from '../../../components/Panel';
import styles from './index.less';


export function Position({ isMe, match, positions, title }) {
  const { subdirectoriesId: accountId } = match.params;
  const positionsConfig = useRef([]);
  const visiblePositions = useRef([]);
  const [activeKeys, setActiveKey] = useState([]);
  const positionsData = positions.data.filter(({ ticker }) => !!ticker);
  const onScroll = () => {
    if (positionsConfig.current.length) {
      for (let i = 0; i < positionsConfig.current.length; i++) {
        const current = positionsConfig.current[i];

        if (
          current.offset <= window.scrollY + window.innerHeight &&
          !visiblePositions.current.includes(current.id)
        ) {
          positionsConfig.current.splice(i, 1);
          visiblePositions.current = [...visiblePositions.current, current.id];
          setActiveKey(visiblePositions.current);
          break;
        }
      }
    }
  };

  useEffect(() => {
    document.addEventListener('scroll', onScroll, false);

    return () => {
      document.removeEventListener('scroll', onScroll, false);
    };
  }, []);

  useEffect(() => {
    if (positionsData.length) {
      positionsConfig.current = positionsData.map(({ item , id}) => ({
        ticker: item,
        id: id,
        offset: document.getElementById(id).getBoundingClientRect().top,
      }));
    }
  }, [positionsData]);

  return (
    <section className={styles.position}>
      <Panel title={title}>
        {positionsData.map(({ ticker, item, tickerInfo = {}, id, currency}) => (
          <div key={ticker}
               className={styles.block}
               id={id}>
            <div className={styles.chart}>
              <div className={styles.header}>
                <h3>{item}</h3>
                <ul>
                  <li className={styles.buy}>{formatMessage({ id: 'user.panel.position.trade.buy' })}</li>
                  <li className={styles.sell}>{formatMessage({ id: 'user.panel.position.trade.sell' })}</li>
                  <li className={styles.analysis}>{formatMessage({ id: 'user.panel.position.trade.analysis' })}</li>
                </ul>
              </div>
              <LineChart accountId={accountId}
                         ticker={ticker}
                         visible={activeKeys.includes(id)} />
              <div className={styles.unit}>
                {formatMessage({ id: 'user.priceInUSD' },{
                  country: tickerInfo.currency
                })}
              </div>

            </div>
            {isMe && (
              <Button icon="add"
                      ghost
                      onClick={() => router.push(`/analyses/edit/${accountId}/${ticker}`)}>
                {formatMessage({ id: 'user.panel.position.addAnalysis2Asset' })}
              </Button>
            )}
          </div>
        ))}
      </Panel>
    </section>
  );
}

export default connect(({ user }) => ({
  positions: user.positions,
  title: formatMessage({ id: 'user.panel.position.title' }),
}))(Position);
