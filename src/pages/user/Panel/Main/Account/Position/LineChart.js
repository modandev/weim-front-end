/* eslint-disable react-hooks/exhaustive-deps */
import { useRef, memo, useState, useEffect } from 'react';
import { connect } from 'dva';
import { Link } from 'umi';
import { formatMessage } from 'umi-plugin-react/locale';
import dayjs from 'dayjs';
import {
  Chart,
  Line,
  Axis,
  Point,
  Tooltip,
  Legend,
  Interaction,
  registerInteraction,
} from 'bizcharts';
import { Skeleton } from 'components';

import styles from './index.less';

registerInteraction('locked-tooltip', {
  start: [
    {
      trigger: 'plot:click',
      action: context => {
        const locked = context.view.isTooltipLocked();

        locked
          ? context.view.unlockTooltip()
          : context.view.lockTooltip();
      },
    },
    { trigger: 'plot:mousemove', action: 'tooltip:show' },
  ],
  end: [{ trigger: 'plot:mouseleave', action: 'tooltip:hide' }],
});

registerInteraction('active-region-click', {
  start: [{ trigger: 'plot:click', action: 'active-region:show' }],
  end: [{ trigger: 'tooltip:mouseleave', action: 'active-region:hide' }],
});

const FILL_MAPPING = {
  BUY: '#26B633',
  SELL: '#FF2C2C',
  ANALYSIS: '#0081FF',
};

const start = dayjs().subtract(3, 'month').format('YYYY-MM-DD');
const end = dayjs().format('YYYY-MM-DD');

const translateAnalysisData = (analysis) => {
  const resultData = [];
  analysis.map(item => {
    const currentAnalysisTime = item.created_at.split('T')[0];
    const HasIndex = resultData.findIndex(analysis => analysis.date === currentAnalysisTime);
    if (HasIndex !== -1) {
      resultData[HasIndex].analysis.push(item);
    } else {
      resultData.push({
        date: currentAnalysisTime,
        side: 'ANALYSIS',
        analysis: [item],
      });
    }
  });
  return resultData;
};

export default memo(connect()(({ ticker, visible, dispatch, accountId, pageReady }) => {
  let visibleRef = useRef(visible);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const fetchData = async () => {
    let tickerMarketData = await dispatch({ type: 'user/tickerMarketData', payload: { ticker, start, end } });

    if (tickerMarketData && tickerMarketData.length) {
      const orders = await dispatch({ type: 'user/orders', payload: { accountId, ticker, start, end } });
      const analysis = await dispatch({
        type: 'user/stream',
        payload: { ticker, postType: 'ANALYSIS', startTime: `${start}T00:00:00Z`, endTime: `${end}T00:00:00Z` },
      });

      if (orders.length || analysis.length) {
        const tickerLen = tickerMarketData.length;
        const analysisData = translateAnalysisData(analysis); // 将相同日期的analysis整理到一个对象中

        for (let i = 0; i < tickerLen; i++) {
          const currentTicker = tickerMarketData[i];
          const date = currentTicker.date.split('T')[0];

          currentTicker.close = +currentTicker.close;

          for (let k = 0; k < analysisData.length; k++) {
            const currentAnalysis = analysisData[k];

            if (date === currentAnalysis.date) {
              analysisData.splice(k, 1);
              k = 1;

              if (!currentTicker.analysis) {
                currentTicker.analysis = [];
              }

              currentTicker.side = 'ANALYSIS';
              currentTicker.analysis = currentAnalysis.analysis;
            } else {

            }
          }


          for (let j = 0; j < orders.length; j++) {
            const currentOrder = orders[j];
            const currentOrderTime = currentOrder.time;
            const time = currentOrderTime.split('T')[0];

            if (date === time) {
              if (currentTicker.lastTradeTime) {
                if (
                  currentTicker.side !== currentOrder.side &&
                  currentTicker.lastTradeTime < new Date(currentOrderTime).getTime()
                ) {
                  currentTicker.side = currentOrder.side;
                  currentTicker.lastTradeTime = new Date(currentOrderTime).getTime();
                }
              } else {
                currentTicker.lastTradeTime = new Date(currentOrderTime).getTime();
                currentTicker.side = currentOrder.side;
              }

              if (!currentTicker.orders) {
                currentTicker.orders = [];
              }

              currentTicker.orders.push(currentOrder);
              orders.splice(j, 1);
              j -= 1;
            }
          }

          if (!orders.length && currentTicker.orders) {
            const tweetsReq = currentTicker.orders.map(({ account_id: accountId, order_id: orderId }) => {
              return dispatch({ type: 'user/stream', payload: { accountId, orderId, postType: 'TWEET' } });
            });

            Promise.all(tweetsReq).then(tweets => {
              tweets.forEach((tweet, index) => {
                currentTicker.orders[index].tweet = tweet;
              });
            });
            break;
          }
        }

        if (analysisData.length) {
          tickerMarketData = tickerMarketData.concat(analysisData);
        }
      }

      const tickerMarketSortData = tickerMarketData.sort((a, b) => (+new Date(a.date)) - (+new Date(b.date)));

      const data = [];
      // 将数据中缺少close的对象 以前一个数据为基准补气
      for (let index in tickerMarketSortData) {
        const item = tickerMarketSortData[index];
        if (item.close === undefined) {
          item.close = tickerMarketSortData[index - 1] ? tickerMarketSortData[index - 1].close : '';
        }
        data.push(item);
      }
      setData(data);
    }

    setLoading(false);
  };

  useEffect(() => {
    visible && fetchData();
  }, []);

  useEffect(() => {
    if (!visibleRef.current && visibleRef.current !== visible) {
      visibleRef.current = true;
      fetchData();
    }
  }, [visible]);

  return (
    <Skeleton
      loading={loading}
      chart
    >
      <Chart
        height={200}
        data={data}
        padding={[40, 20, 50, 40]}
        scale={{
          close: {
            type: 'linear',
            // tickCount: 5,
          },
        }}
        placeholder={(
          <div className={styles.empty}>
            <h6>{formatMessage({ id: 'message.chart.empty' })}</h6>
          </div>
        )}
        onGetG2Instance={g2Chart => {
          g2Chart.animate(false);

          // 缩放交互
          g2Chart.interaction('drag', {
            type: 'XY',
          }).interaction('zoom', {
            type: 'XY',
          });
        }}
        forceFit
      >
        <Axis name="close" />
        <Axis
          name="date"
          label={{
            formatter(text) {
              if (text.includes('T')) {
                text = text.slice(0, text.indexOf('T'));
              }

              const [year, month, day] = text.split('-');

              return `${+month}-${+day}`;
            },
          }}
        />
        <Line position="date*close" />

        <Point
          position="date*close*side"
          style={[
            'date*close*side',
            (date, close, side) => {
              const fill = FILL_MAPPING[side];

              return fill ? { fill } : {};
            },
          ]}
          size={['side', side => {
            return !!side && typeof side === 'string' ? 3 : 0;
          }]}
        />

        <Legend visible={false} />
        <Tooltip
          shared
          enterable
          title="date"
          showCrosshairs
        >
          {(title, [item]) => {
            const { data, value } = item;
            const { orders, analysis } = data;
            return (
              <div className={styles.tooltip}>
                {analysis && analysis.map(({ title, created_at, post_id, user_id, account_id, ticker }) => (
                  <div className={styles.item}
                       key={post_id}>
                    <Link
                      to={`/analyses/${post_id}?userId=${user_id}&accountId=${account_id}&symbol=${ticker.key}`}
                      className={styles.title}
                    >
                      {title}
                    </Link>
                    <div className={styles.value}>{value}</div>
                    <div className={styles.date}>
                      {dayjs(created_at).format('YYYY-MM-DD HH:mm')}
                    </div>
                  </div>
                ))}
                {orders && orders.map(({ time, side, quantity, tweet }) => (
                  <div className={styles.item}
                       key={time}>
                    {tweet ? (
                      Array.isArray(tweet) ? tweet.map(({ title, post_id, user_id }) => (
                        <Link
                          key={post_id}
                          className={styles.title}
                          to={`/home/tweetdetail/${post_id}?userId=${user_id}`}
                        >
                          {title || formatMessage({ id: 'home.user.Posts' })}
                        </Link>
                      )) : (
                        <Link
                          className={styles.title}
                          to={`/home/tweetdetail/${tweet.post_id}?userId=${tweet.user_id}`}
                        >
                          {tweet.title || formatMessage({ id: 'home.user.Posts' })}
                        </Link>
                      )
                    ) : null}
                    <div className={styles.value}>{quantity}</div>
                    <div className={styles.date}>{time.split('T')[0]}</div>
                  </div>
                ))}

                {!analysis && !orders && (
                  <div className={styles.item}>
                    <div className={styles.title}>{title.split('T')[0]}</div>
                    <div className={styles.value}>{value}</div>
                  </div>
                )}
                <Interaction type="active-region-click" />
              </div>
            );
          }}
        </Tooltip>
        <Interaction type="locked-tooltip" />
        <Interaction type="active-region" />
      </Chart>
    </Skeleton>
  );
}));
