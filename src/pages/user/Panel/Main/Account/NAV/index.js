/* eslint-disable react-hooks/exhaustive-deps */
import { useRef, useState, useEffect, useCallback } from 'react';
import { connect } from 'dva';
import { formatMessage } from 'umi-plugin-react/locale';
import { Skeleton, StockAreaCode } from '@/components';
import classnames from 'classnames';
import PieChart from './PieChart';
import LineChart from './LineChart';
import appConfig from 'configs/app';
import ReturnChart from './ReturnChart';
import styles from './index.less';

const { NAVScreening } = appConfig;


const NAV = ({ match, dispatch, navData, returnData, positions: { data: positionsData, colors } }) => {
  const { subdirectories, subdirectoriesId: accountId } = match.params;
  const selectedKeyRef = useRef();
  const [scope, setScope] = useState(NAVScreening[0]);
  const [selectedKey, setSelectedKey] = useState(null);
  const [panelLoading, setPanelLoading] = useState(true);
  const [navLoading, setNavLoading] = useState(false);
  const [returnType, setReturnType] = useState('TIME_WEIGHTED');
  const [chartLoading, setChardLoading] = useState(false);
  const getPositions = async () =>
    await dispatch({
      type: 'user/position',
      payload: { accountId, currency: 'USD' },
    });

  const getLiquidated = async () => {
    await dispatch({
      type: 'user/liquidatedTicker',
      variables: { accountId, type: 'CLOSED' },

    });
  };

  const getAccountNAV = async () => {
    await dispatch({
      type: 'user/getReturn',
      variables: { accountId, scope, type: returnType },
    });
    setNavLoading(false);
  };

  const onChoose = key => {
    if (key !== scope) {
      setScope(key);
    }
  };

  const onChooseReturn = useCallback((key) => {
    if (key !== returnType) {
      setReturnType(key);
    }
  });

  const onIntervalClick = ({ data }) => {
    const { item } = data.data;

    if (item === selectedKeyRef.current) {
      setSelectedKey(null);
      selectedKeyRef.current = null;
    } else {
      setSelectedKey(item);
      selectedKeyRef.current = item;
    }
  };

  useEffect(() => {
      setChardLoading(true);
      accountId && getAccountNAV();
    },
    [scope, returnType, accountId],
  );

  useEffect(() => {
    setChardLoading(false);
  }, [returnData])

  useEffect(() => {
    async function getData() {
      await getPositions();
      await getLiquidated();
      setPanelLoading(false);
    }

    if (subdirectories === 'accounts' && accountId) {
      setPanelLoading(true);
      getData();
    }
  }, [accountId]);

  return (
    <section className={styles.NAV}>
      <Skeleton
        loading={panelLoading}
        chart
        avatar={{ size: 'large' }}
        title={{ rows: 5 }}
        paragraph={{ rows: 1, width: 300 }}
      >
        {positionsData.length ? (
          <div className={styles.pieChart}>
            <PieChart
              data={positionsData}
              onIntervalClick={onIntervalClick}
            />
            <ul className={styles.legend}>
              {positionsData.map(({ item, percent, tickerInfo }, index) => (
                <li key={item}
                    className={classnames({ [styles.selected]: selectedKey === item })}>
                  <i style={{ backgroundColor: colors[index] }} />
                  {tickerInfo ? <><StockAreaCode countryCode={tickerInfo.country_code} />&nbsp;&nbsp;</> : null}
                  <span>{item}</span>
                  <span>{parseInt(percent * 100)}%</span>
                </li>
              ))}
            </ul>
          </div>
        ) : null}
        <div className={styles.lineChart}>
          <Skeleton
            loading={navLoading}
            paragraph={{ rows: 1, width: 300 }}
            chart
          >
            <ul className={`clearfix ${styles.navs_tab}`}>
              {NAVScreening.map(item => (
                <li
                  key={item}
                  onClick={() => onChoose(item)}
                  className={classnames({ [styles.active]: scope === item })}
                >
                  {item}
                </li>
              ))}
            </ul>

             <ReturnChart
              data={returnData}
              navData={navData}
              chartLoading={chartLoading}
              onChooseReturn={onChooseReturn}
              returnType={returnType} />
          </Skeleton>
        </div>
      </Skeleton>
    </section>
  );
};

export default connect(({ user }) => ({
  navData: user.navData,
  positions: user.positions,
  returnData: user.returnData,
}))(NAV);
