import {
  Chart,
  Axis,
  Tooltip,
  LineAdvance,
  View,
  Interval
} from 'bizcharts';
import { formatMessage } from 'umi-plugin-react/locale';
import { semicolon } from 'shared/helpers';
import styles from './index.less';

export default ({ data }) => {

  return (

  <Chart
    height={200}
    data={data}
    forceFit
    padding={[10, 20, 40, 55]}
    scale={{
      date: {
        type: 'timeCat',
        range: [0, 1],
        tickCount: 6
      },
      nav: {
        type: 'linear',
        tickCount: 5,
      }
    }}
    placeholder={(
      <div className={styles.empty}>
        <h6>{formatMessage({ id: 'message.chart.empty' })}</h6>
      </div>
    )}
  >
      <Axis
        name="date"
        label={{
          formatter(text) {
            const [year, month, day] = text.split('-');

            return `${+month}-${+day}`;
          }
        }}
      />
      <Axis
        name="nav"
        label={{
          formatter(text) {
            return semicolon(text);
          }
        }}
      />
      <Tooltip title="date" showCrosshairs>
        {(title, [item]) => (
          <div className={styles.tooltip}>
            <div className={styles.value}>{semicolon((+item.value).toFixed(2))}</div>
            <div className={styles.date}>{title}</div>
          </div>
        )}
      </Tooltip>
    <LineAdvance
      area
      position="date*nav"
    />

  </Chart>
)};
