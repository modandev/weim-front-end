import React, { useState, useEffect } from 'react';
import {
  Chart,
  Point,
  View,
  Tooltip,
  RangeColumnChart,
  LineAdvance,
  Axis,
  Interval,
  Text,
} from 'bizcharts';
import styles from '@/pages/user/Panel/Main/Account/NAV/index.less';
import { formatMessage } from 'umi-plugin-react/locale';
import { semicolon } from 'shared/helpers';
import Spanner from '@atlaskit/spinner';

const ReturnScreening = [
  { key: 'TIME_WEIGHTED', title: 'Time-weighted Return' },
  { key: 'MONEY_WEIGHTED', title: 'Money-weighted Return' },
];

export default ({ data, onChooseReturn, returnType, chartLoading }) => {
  return (<div className={styles.return_chart}>

    <div className={styles.line_chart}>
      <ul className={styles.return_tab}>
        {ReturnScreening.map(({ key, title }) => (
          <li
            key={key}
            onClick={() => onChooseReturn(key)}
            className={`${styles.return_button} ${returnType === key ? styles.active : ''}`}>
            {formatMessage({ id: 'user.' + title })}
          </li>
        ))}
    </ul>
      <Chart
        height={300}
        padding={[10, 40, 40, 40]}
        data={data}
        autoFit
        scale={{
          date: {
            type: 'timeCat',
            range: [0, 1],
            tickCount: 4,
          },
        }}
      >
    <Tooltip
      showTitle={false}
      showMarkers={true}
      itemTpl={'<li class="g2-tooltip-list-item" data-index={index}>'
      + '<span style="background-color:{color};" class="g2-tooltip-marker"></span>'
      + '{name}{value}</li>'}
    />

      <View
        data={data}
        region={{
          start: { x: 0, y: 0 },
          end: { x: 1, y: 1 },
        }}
      >
      <Axis name="date"
            tickLine={null}
            label={null} />
      <Axis name="ror"
            label={{
              formatter: val => {
                return semicolon((+val).toFixed(2));
              },
            }}
      />
      <LineAdvance
        position={'date*ror'}
        tooltip={[]}
      />
      </View>


  </Chart>


    </div>

    <div className={styles.title}>
      {formatMessage({ id: 'user.panel.NAV' })}
    </div>

    <div className={styles.line_chart}>
      <Chart
        height={180}
        padding={[20, 40, 20, 40]}
        data={data}
        autoFit
      >

      <View
        data={data}
        scale={{
          date: {
            type: 'timeCat',
            range: [0, 1],
            tickCount: 4,
          },
          volumn: {
            type: 'time',
            nice: true,
            formatter: val => {
              return semicolon(val.toFixed(2));
            },
          }

        }}
        region={{
          start: { x: 0, y: 0 },
          end: { x: 1, y: 1 },
        }}
      >
        <Interval
          position={'date*volumn'}
          style={['rise', val => {
            return {
              stroke: val ? '#2fc25b' : '#f04864',
              fill: val ? '#2fc25b' : '#f04864',
            };
          },

          ]}
          size={2}
          tooltip={['time*volumn', (time, volumn) => {
            return {
              name: time,
              value: '<br/>nav:<span style="padding-left: 16px">' + volumn + '</span><br/>',
            };
          }]}
        />
        </View>
      </Chart>
      <div className={styles.from_yahoo}>
        {formatMessage({ id: 'user.DataFromYahoo' })}
      </div>
    </div>

    {chartLoading ? <div className={styles.chart_loading}>
      <Spanner />
    </div> : null}

  </div>);
}

