import { Component } from 'react';
import {
  Axis,
  Chart,
  Legend,
  Tooltip,
  Interval,
  Coordinate,
  Interaction,
} from 'bizcharts';
import isEqual from 'lodash.isequal';
import appConfig from 'configs/app';

const { colors } = appConfig;

export default class extends Component {
  shouldComponentUpdate(nextProps) {
    return false;
  }

  componentWillReceiveProps(nextProps) {
    if (!isEqual(nextProps.data, this.props.data)) {
      this.forceUpdate();
    }
  }

  render() {
    let { data, onIntervalClick } = this.props;

    return (
      <Chart
        width={200}
        height={200}
        data={data}
        padding={10}
        onIntervalClick={onIntervalClick}
      >
        <Coordinate type="theta" />
        <Axis visible={false} />
        <Tooltip visible={false} />
        <Legend visible={false} position="right" />
        <Interval
          position="percent"
          adjust="stack"
          color={['item', `${colors.positionStartColor}-${colors.positionEndColor}`]}
        />
        <Interaction type='element-active' />
        <Interaction type='element-single-selected' />
      </Chart>
    );
  }
};
