import AccountInfo from './AccountInfo';
import NAV from './NAV';
import Position from './Position';
import Liquidated from './Liquidated'
import {Hr} from '@/components'
export default props => (
  <>
    {!props.isMe ? (
      <AccountInfo {...props} />
    ) : null}
    <NAV {...props} />
    <Position {...props} />
    <div style={{height: '20px'}}/>
    <Liquidated {...props} />
  </>
);
