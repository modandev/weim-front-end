import { Position } from '@/pages/user/Panel/Main/Account/Position';
import { connect } from 'dva';
import { formatMessage } from 'umi-plugin-react/locale';

export default connect(({ user }) => ({
  positions: user.liquidatedTickerData,
  title: formatMessage({ id: 'user.Liquidated' }),
}))(Position);
