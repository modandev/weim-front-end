import { Collapse, Skeleton } from 'components';
import { formatMessage } from 'umi-plugin-react/locale';
import { Link } from 'umi';
import classnames from 'classnames';
import styles from './index.less';

const Panel = Collapse.Panel;

const Watchlist = ({ match, loading }) => {
  const { uid, subdirectoriesId: watchlistId } = match.params;

  return (
    <section className={styles.watchlist}>
      <Skeleton
        loading={loading}
        title={{ rows: 2 }}
        avatar={{ shape: 'square' }}
      >
        <Collapse activeKey="1">
          <Panel header={formatMessage({ id: 'user.watchlist.title' })} key="1">
            <ul>
              <li
                className={classnames({ [styles.active]: watchlistId === 'all' })}
              >
                <Link to={`/dashboard/${uid}/watchlist/all`}>
                  <span>{formatMessage({ id: 'user.watchlist.filter.all' })}</span>
                </Link>
              </li>
            </ul>
          </Panel>
        </Collapse>
      </Skeleton>
    </section>
  );
}

export default Watchlist;
