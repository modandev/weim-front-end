import { Collapse, Skeleton } from 'components';
import { formatMessage } from 'umi-plugin-react/locale';
import { Link } from 'umi';
import classnames from 'classnames';

import styles from './index.less';

const Panel = Collapse.Panel;

let presubdirectories = null;

const Accounts = ({ accounts, match, loading }) => {
  let { uid: userUID, subdirectories, subdirectoriesId: accountId } = match.params;

  if (subdirectories !== 'watchlist' && presubdirectories !== subdirectories) {
    presubdirectories = subdirectories;
  }

  return (
    <section className={styles.accounts}>
      <Skeleton
        loading={loading}
        title={{ rows: 2 }}
        avatar={{ shape: 'square' }}
      >
        {
          accounts.length > 0 && (
            <Collapse activeKey="1">
              <Panel header={formatMessage({ id: 'user.panel.my.account.title' })} key="1">
                <ul>
                  {
                    accounts.map(({ nickname, account_id }) => (
                      <li
                        key={account_id}
                        className={classnames({ [styles.active]: account_id === accountId })}
                      >
                        <Link to={`/dashboard/${userUID}/${presubdirectories}/${account_id}`}>
                          <span>{nickname || account_id}</span>
                        </Link>
                      </li>
                    ))
                  }
                </ul>
              </Panel>
            </Collapse>
          )
        }
      </Skeleton>
    </section>
  );
}

export default Accounts;
