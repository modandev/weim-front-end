/* eslint-disable eqeqeq */
import { Component } from 'react';
import { connect } from 'dva';
import router from 'umi/router';
import Aside from './Aside';
import Main from './Main';

import styles from './index.less';

// subdirectories: accounts / watchlist

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMe: true,
      pageReady: false,
    };
  }

  _currentUid = null;

  componentDidMount() {
    const { uid } = this.props.match.params;

    this._currentUid = uid;
    this.fetchData(uid);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { uid: nextUid } = nextProps.match.params;

    if (nextUid !== this._currentUid) {
      this._currentUid = nextUid;
      this.fetchData(nextUid);
    }
  }

  fetchData = uid =>
    this.setState({ pageReady: false }, async () => {
      const { currentUser, match } = this.props;
      let { subdirectories, subdirectoriesId } = match.params;
      const accountsPayload = {};
      const newState = {};
      let autoCompleteAccountId = false;

      if (uid != currentUser.uid) {
        // 访问他人 dashboard
        newState.isMe = false;
        accountsPayload.uid = +uid;
        this.fetchUser(+uid);
      } else {
        // 自己的 dashboard
        newState.isMe = true;
      }

      const accounts = await this.fetchAccounts(accountsPayload);

      if (!subdirectories) {
        autoCompleteAccountId = true;
      } else {
        if (['accounts', 'followers'].includes(subdirectories) && !subdirectoriesId) {
          autoCompleteAccountId = true;
        }
      }

      if (autoCompleteAccountId) {
        const account = accounts[0];

        if (account) {
          subdirectoriesId = account.account_id;
          router.replace(`/dashboard/${uid}/${subdirectories}/${subdirectoriesId}`);
        }
      }

      newState.pageReady = true;
      this.setState(newState);
    });

  fetchAccounts = async payload => await this.props.dispatch({ type: 'user/accounts', payload });

  fetchUser = async equalTo =>
    await this.props.dispatch({
      type: 'user/user',
      payload: {
        uid: {
          equalTo
        }
      }
    });

  render() {
    const { isMe, pageReady } = this.state;

    return (
      <div className={styles.user_panel}>
        <Aside {...this.props} loading={!pageReady} isMe={isMe} />
        <Main {...this.props} loading={!pageReady} isMe={isMe} onUpdateAccounts={this.fetchAccounts} />
      </div>
    );
  }
};

export default connect(({ user }) => ({
  user: user.user,
  accounts: user.accounts,
  currentUser: user.currentUser
}))(Dashboard);
