import { Link } from 'umi';
import { formatMessage } from 'umi-plugin-react/locale';
import { ThirdParty } from '@/components';
import oauth from '../oauth';

import styles from './index.less';

const SignIn = ({ onSelect }) => {
  return (
    <div className={styles.wrapper}>
      
      <ThirdParty
        useFor="oauth"
        signIn
        onSelect={onSelect}
        buttonTxtAffix={formatMessage({ id: 'common.signIn' }).toLowerCase()}
        description={formatMessage({ id: 'user.weim.signIn' })}
        footer={
          <Link to="/join" className={styles.sign_up}>
            {formatMessage({ id: 'common.signUp' })}
          </Link>
        }
      />
    </div>
  );
};

export default oauth(SignIn);
