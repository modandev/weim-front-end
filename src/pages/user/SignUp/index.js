import classnames from 'classnames';
import { formatMessage } from 'umi-plugin-react/locale';
import { ThirdParty } from 'components';
import { EmailAccount } from 'components';

import oauth from '../oauth';

import styles from './index.less';

export default oauth(({ onSelect }) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.body}>
        <ThirdParty
          signUp
          useFor="oauth"
          onSelect={onSelect}
          buttonTxtAffix={formatMessage({ id: 'common.signUp' }).toLowerCase()}
          description={formatMessage({ id: 'user.weim.signUp' })}
        />
        <div className={styles.supplement}>
          <div className={styles.about_sign_up}>
            <p className={styles.required}>
              {formatMessage({ id: 'user.signUp.required' })}
            </p>
            <p className={styles.subscribe}>
              {formatMessage({ id: 'user.signUp.subscribe' })}
            </p>
          </div>
          <i className={styles.separator} />
          <div className={styles.qrcode}>
            <div className={classnames(styles.wechat_official_account, "clearfix")}>
              <img src={require('../../../assets/common/wechat-official-account.png')} alt="@wechat-official-account" />
              <span>{formatMessage({ id: 'user.follow.wechat.official.account' })}</span>
            </div>
            <div className={classnames(styles.wechat_customer_service, "clearfix")}>
              <img src={require('../../../assets/common/customer-service.png')} alt="@wechat-customer-service" />
              <span>{formatMessage({ id: 'user.consult.wechat.custom.service' })}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});
