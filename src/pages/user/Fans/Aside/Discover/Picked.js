import { useCallback, useState } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';
import { Icon, Collapse } from 'components';

import styles from './index.less';

const Panel = Collapse.Panel;

const Picked = () => {
  const [picked, setPicked] = useState(Array.from({ length: 9 }, (_, i) => ({ label: `MSFT-${i}`, id: i })));

  const onRemove = useCallback(targetId => {
    const _picked = [...picked];

    _picked.splice(_picked.findIndex(({ id }) => targetId === id), 1);
    setPicked(_picked);
  }, [picked]);

  return (
    <section className={styles.picked}>
      <Collapse>
        <Panel header={formatMessage({ id: 'user.panel.my.picked.title' })}>
          <ul className="clearfix">
            {picked.map(({ label, id }) => (
              <li key={id}>
                <span>{label}</span>
                <Icon type="close-circle" onClick={() => onRemove(id)} />
              </li>
            ))}
          </ul>
        </Panel>
      </Collapse>
    </section>
  );
}

export default Picked;
