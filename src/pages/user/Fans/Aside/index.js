/* eslint-disable eqeqeq */
import { memo } from 'react';

import User from './User';
import Discover from './Discover';

import styles from './index.less';

export default memo(props => {
  return (
    <aside className={styles.aside}>
      <User {...props} />
      <Discover {...props} />
    </aside>
  );
});
