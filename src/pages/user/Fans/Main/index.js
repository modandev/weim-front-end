import { useRef, useState, useEffect } from 'react';
import classnames from 'classnames';
import styles from './index.less';

import MyFans from './MyFans';
import Watchlist from '../../Panel/Main/Watchlist';

const SUBDIRECTORIES = [
  { key: 'accounts', component: MyFans },
  { key: 'watchlist', component: Watchlist }
];

export default function Main (props) {
  const { subdirectories } = props.match.params;
  const activeRef = useRef(subdirectories);
  const [activated, setActivated] = useState([subdirectories]);

  useEffect(() => {
    if (activeRef.current !== subdirectories) {
      !activated.includes(subdirectories) && setActivated([...activated, subdirectories]);
    }
  }, [subdirectories, activated]);

  return (
    <main className={styles.main}>
      {SUBDIRECTORIES.map(({ key, component: Component }) => {
        if (!activated.includes(key)) {
          return null;
        }

        return (
          <div
            key={key}
            className={
              classnames(
                styles.block,
                { [styles.active]: key === subdirectories }
              )
            }
          >
            <Component {...props} />
          </div>
        )
      })}
    </main>
  );
}
