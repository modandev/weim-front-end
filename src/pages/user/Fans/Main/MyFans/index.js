import PageTitle from './PageTitle';
import MyFans from './MyFans';
export default props => (
  <>
      <PageTitle {...props} />
      <MyFans {...props} />
  </>
);
