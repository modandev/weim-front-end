import React, { useState, useCallback, useEffect } from 'react';
import styles from './styles.less';
import Spinner from '@atlaskit/spinner';
import { FansCard } from '@/components';
import { useQuery } from '@apollo/react-hooks';
import { FANS } from '@/shared/graphql/gql/follow';
import debounce from 'debounce';
import { FormattedMessage } from 'umi-plugin-react/locale';
import { DefaultFans } from '@/components/DefaultPage';
import NoData from '@/components/NoData';

export default (props) => {
  const { subdirectoriesId } = props.match.params;
  const [fans, setFans] = useState({ [subdirectoriesId]: [] });
  const [cursorData, setCursorData] = useState({});
  const { refetch, loading, data: { fans: { hits, cursor } = {} } = {} } = useQuery(FANS, {
    variables: {
      accountId: subdirectoriesId,
      size: '20',
    },
  });

  const loadMore = useCallback(
    debounce(
      () => {
        refetch({
          cursor: cursorData[subdirectoriesId],
          size: '20',
        });
      }, 200,
    ),
  );

  useEffect(() => {
    cursorData[subdirectoriesId] = cursor;
  }, [cursor]);

  useEffect(() => {
    if (hits) setFans({
      [subdirectoriesId]: [...(fans[subdirectoriesId] || []), ...hits],
    });
  }, [hits]);

  useEffect(
    () => {
      const onScroll = (e) => {
        if (!loading && cursorData[subdirectoriesId] && e.target.scrollingElement.scrollHeight <= document.documentElement.clientHeight + e.target.scrollingElement.scrollTop) {
          loadMore();
        }
      };

      document.addEventListener('scroll', onScroll);
      return () => {
        document.removeEventListener('scroll', onScroll);
      };
    },
    [cursorData, loadMore, loading, subdirectoriesId]);


  return (<div className={styles.followings}>
    {loading && (!fans[subdirectoriesId] || fans[subdirectoriesId].length === 0) ? <>
      <DefaultFans />
      <DefaultFans />
      </> : null}

    {fans[subdirectoriesId] ?
      <>
        <div>
      {(fans[subdirectoriesId]).map((item, index) => {
        return index % 2 === 0 ? <FansCard
          style={{ width: '390px' }}
          accounts={item.accounts}
          user={item.userInfo}
          followingNo={item.userFollowing.following_no}
          userId={item.user_id}
          key={item.user_id} /> : null;
      })}
      </div>

        <div>
      {(fans[subdirectoriesId]).map((item, index) => {
        return index % 2 === 1 ? <FansCard
          style={{ width: '390px' }}
          accounts={item.accounts}
          user={item.userInfo}
          followingNo={item.userFollowing.following_no}
          userId={item.user_id}
          key={item.user_id} /> : null;
      })}
      </div>

      </>
      : loading ? null : <div className={styles.no_data}><NoData></NoData></div>}

    {loading ? <div className={styles.spinner_box}><Spinner /> </div> : null}

  </div>);
};
