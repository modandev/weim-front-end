import { formatMessage } from 'umi-plugin-react/locale';
import { Header } from '../../../components/Panel';
import Button from '@/components/Buttons';
import { Icon } from '@/components';
import styles from './index.less';
import {router} from 'umi'

const css = {
  display: 'flex',
  alignItem: 'center',
  justifyContent: 'center',
}
export default function AccountDetail({ loading, match, accounts, onUpdateAccounts }) {

  return (
    <section className={styles.intro}>
      <div
        onClick={router.goBack}
        className={styles.button}>
          <Icon type="back"></Icon>
      </div>
      <div className={styles.title}>
        <Header className={styles.wrapper}
                title={formatMessage({ id: 'home.title.Followers' })}>
      </Header>
      </div>
    </section>
  );
}
