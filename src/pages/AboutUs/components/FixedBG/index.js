/* eslint-disable react-hooks/exhaustive-deps */

import { createRef, useState, useEffect } from 'react';
import classnames from 'classnames';

import styles from './index.less';

export default ({ className }) => {
  const bg = createRef();
  const [top, setTop] = useState(0);

  useEffect(() => {
    let critical = null;
    const boundingTop = bg.current.getBoundingClientRect().top;
    const onScroll = () => {
      const { scrollY, innerHeight } = window;

      if (boundingTop + 230 - scrollY <= innerHeight) {
        if (!critical) {
          critical = scrollY;
        }

        setTop((scrollY - critical) / 10);
      }
    };

    document.addEventListener('scroll', onScroll);

    return () => {
      document.removeEventListener('scroll', onScroll);
    };
  }, []);

  return (
    <section
      ref={bg}
      className={classnames(styles.fixed_bg, className)}
      style={{ backgroundPosition: `50% -${top}px` }}
    />
  );
};
