/* eslint-disable jsx-a11y/anchor-is-valid */
import { formatMessage } from 'umi-plugin-react/locale';
import classnames from 'classnames';
import { Collapse } from 'components';
import FixedBG from './components/FixedBG';

import styles from './index.less';

const { Panel } = Collapse;

export default () => {
  return (
    <div className={styles.wrap}>
      <section className={classnames(styles.aboutName, styles.gold_bg)}>
        <div className={classnames(styles.content, 'type-area')}>
          <h3>{formatMessage({ id: 'aboutUs.our.name' })}：WEIM Wisdom Exchange Investment</h3>
          <p>{formatMessage({ id: 'aboutUs.our.name.meaning' })}</p>
        </div>
      </section>
      <section className={classnames(styles.principleAndConcept, styles.white_bg)}>
        <div className={classnames(styles.content, 'type-area')}>
          <div className={styles.text}>
            <h4 className={styles.section_title}>
              {formatMessage({ id: 'aboutUs.principle.and.concept.title' })}
            </h4>
            <div className="clearfix">
              <ul className={styles.principles}>
                {formatMessage({ id: 'aboutUs.principles' }).split('\n').map((p, i) => (
                  <li key={i}>{p}</li>
                ))}
              </ul>
              <p className={styles.concept}>{formatMessage({ id: 'aboutUs.concept' })}</p>
            </div>
          </div>
        </div>
      </section>

      <FixedBG className={styles.first_bg} />

      <section className={classnames(styles.vision, styles.white_bg)}>
        <div className={classnames(styles.content, 'type-area')}>
          <h4 className={styles.section_title}>
            {formatMessage({ id: 'aboutUs.vision.title' })}
          </h4>
          <p>{formatMessage({ id: 'aboutUs.vision.weThink' })}</p>
          <i className={styles.separator} />
          <p>
            <span>{formatMessage({ id: 'aboutUs.vision.personal' })}</span>
            <span>{formatMessage({ id: 'aboutUs.vision.personal.desc' })}</span>
          </p>
          <i className={styles.separator} />
          <p>
            <span>{formatMessage({ id: 'aboutUs.vision.organization' })}</span>
            <span>{formatMessage({ id: 'aboutUs.vision.organization.desc' })}</span>
          </p>
          <i className={styles.separator} />
          <p>{formatMessage({ id: 'aboutUs.vision.weBelieve' })}</p>
        </div>
      </section>

      <section className={classnames(styles.investmentGroup, styles.primary_bg)}>
        <div className={classnames(styles.content, 'type-area')}>
          <h4 className={styles.section_title}>
            {formatMessage({ id: 'aboutUs.investmentGroup.title' })}
          </h4>
          <p>{formatMessage({ id: 'aboutUs.whatIs.investmentGroup' })}</p>
          <p>{formatMessage({ id: 'aboutUs.investmentGroup.weThink' })}</p>
          <p>{formatMessage({ id: 'aboutUs.investmentGroup.different' })}</p>
          <p>{formatMessage({ id: 'aboutUs.investmentGroup.base' })}</p>
          <p>{formatMessage({ id: 'aboutUs.investmentGroup.example' })}</p>
          <p>
            <a className={styles.hilite}>{formatMessage({ id: 'aboutUs.investmentGroup.example.oneSideDoubling' })}</a>
            <a>{formatMessage({ id: 'aboutUs.investmentGroup.example.flyCutter' })}</a>
            <a>{formatMessage({ id: 'aboutUs.investmentGroup.example.smallMarketValueGrowth' })}</a>
          </p>
        </div>
      </section>

      <section className={classnames(styles.example, styles.gold_bg)}>
        <div className={classnames(styles.content, 'type-area')}>
          <h4 className={styles.section_title}>
            {formatMessage({ id: 'aboutUs.example.title' })}
          </h4>
          <p>{formatMessage({ id: 'aboutUs.example.meaning' })}</p>
          <i className={styles.separator} />
          <p>
            <span>{formatMessage({ id: 'aboutUs.example.elite' })}</span>
            <span>
              {formatMessage({ id: 'aboutUs.example.elite.desc' }).split('\n').map((p, i) => (
                <span key={i}>{p}</span>
              ))}
            </span>
          </p>
          <i className={styles.separator} />
          <p>
            <span>{formatMessage({ id: 'aboutUs.example.rm' })}</span>
            <span>
              {formatMessage({ id: 'aboutUs.example.rm.desc' }).split('\n').map((p, i) => (
                <span key={i}>{p}</span>
              ))}
            </span>
          </p>
          <i className={styles.separator} />
          <p>
            <span>{formatMessage({ id: 'aboutUs.example.thinkingFrame' })}</span>
            <span>{formatMessage({ id: 'aboutUs.example.thinkingFrame.desc' })}</span>
          </p>
          <i className={styles.separator} />
          <p>
            <span>{formatMessage({ id: 'aboutUs.example.revenueTarget' })}</span>
            <span>
              {formatMessage({ id: 'aboutUs.example.revenueTarget.desc' }).split('\n').map((p, i) => (
                <span key={i}>{p}</span>
              ))}
            </span>
          </p>
          <i className={styles.separator} />
          <p>
            <span>{formatMessage({ id: 'aboutUs.example.combination' })}</span>
            <span>{formatMessage({ id: 'aboutUs.example.combination.desc' })}</span>
          </p>
          <i className={styles.separator} />
          <p>{formatMessage({ id: 'aboutUs.example.why' })}</p>
        </div>
      </section>

      <FixedBG className={styles.second_bg} />

      <section className={classnames(styles.faq, styles.white_bg)}>
        <div className={classnames(styles.content, 'type-area')}>
          <h4>F A Q</h4>
          <Collapse>
            <Panel header={formatMessage({ id: 'aboutUs.faq.howToJoin' })}>
              <div className={styles.answer}>
                {formatMessage({ id: 'aboutUs.faq.howToJoin.answer' }).split('\n').map((p, i) => (
                  <p key={i}>{p}</p>
                ))}
              </div>
            </Panel>
            <Panel header={formatMessage({ id: 'aboutUs.faq.equityOfInvestmentManager' })}>
              <div className={styles.answer}>
                {formatMessage({ id: 'aboutUs.faq.equityOfInvestmentManager.answer' }).split('\n').map((p, i) => (
                  <p key={i}>{p}</p>
                ))}
              </div>
            </Panel>
            <Panel header={formatMessage({ id: 'aboutUs.faq.membershipInterests' })}>
              <div className={styles.answer}>
                {formatMessage({ id: 'aboutUs.faq.membershipInterests.answer' }).split('\n').map((p, i) => (
                  <p key={i}>{p}</p>
                ))}
              </div>
            </Panel>
            <Panel header={formatMessage({ id: 'aboutUs.faq.howToCreateInvestmentGroup' })}>
              <div className={styles.answer}>
                {formatMessage({ id: 'aboutUs.faq.howToCreateInvestmentGroup.answer' }).split('\n').map((p, i) => (
                  <p key={i}>{p}</p>
                ))}
              </div>
            </Panel>
            <Panel header={formatMessage({ id: 'aboutUs.faq.howToJoinInvestmentGroup' })}>
              <div className={styles.answer}>
                <p>{formatMessage({ id: 'aboutUs.faq.howToJoinInvestmentGroup.answer' })}</p>
              </div>
            </Panel>
            <Panel header={formatMessage({ id: 'aboutUs.faq.howToBuildingACollectivePortfolio' })}>
              <div className={styles.answer}>
                {formatMessage({ id: 'aboutUs.faq.howToBuildingACollectivePortfolio.answer' }).split('\n').map((p, i) => (
                  <p key={i}>{p}</p>
                ))}
              </div>
            </Panel>
          </Collapse>
        </div>
      </section>

      <section className={classnames(styles.contactUs, styles.primary_bg)}>
        <div className={classnames(styles.content, 'type-area')}>
          <h4>{formatMessage({ id: 'aboutUs.contactUs.title' })}</h4>
          <div className={styles.qrcode}>
            <div>
              <img src={require('../../assets/common/wechat-official-account.png')} alt="@wechat-official-account" />
              <p>{formatMessage({ id: 'aboutUs.contactUs.wechat.officialAccount' })}</p>
            </div>
            <div>
              <img src={require('../../assets/common/customer-service.png')} alt="@wechat-customer-service" />
              <p>{formatMessage({ id: 'aboutUs.contactUs.wechat.customerService' })}</p>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};
