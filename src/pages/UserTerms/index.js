import { useEffect } from 'react';
import { formatMessage } from 'umi-plugin-react/locale';

import styles from './index.less';

export default () => {
  useEffect(() => {
    window.scrollTo({ top: 0, left: 0 });
  }, []);

  return (
    <section className={styles.wrapper}>
      <h1>{formatMessage({ id: 'userTerms.title' })}</h1>
      {formatMessage({ id: 'userTerms.paragraphs' }).split('\n').map((p, i) => (
        <p key={i} className={styles.para}>{p}</p>
      ))}
      <div id="research"></div>
    </section>
  );
};
