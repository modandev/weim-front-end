import React, { useRef, useState, useCallback, useEffect, memo, PureComponent } from 'react';
import { connect } from 'dva';
import router from 'umi/router';
import { parse } from 'querystring';
import { formatMessage } from 'umi-plugin-react/locale';
import { Hr, Icon, ThirdParty, Button } from '@/components';
import StockInfo from '@/pages/Home/components/StockInfo';
import NewsInBrief from '@/pages/Home/components/NewsInBrief';
import { DefaultPage } from '@/components/DefaultPage';
import styles from './index.less';
import Chart from '@/components/DepthChart';
import AnalysisDetailComments from '../components/AnalysisDetailComments';
import MessageTitle from '../components/MessageTitle';
import InteractForDepthDetail from '../components/InteractDepthDetail';
import { getQueries } from 'shared/locationParser';
import BodyBabyFactory from 'shared/BodyBabyFactory';
import EditorJS from '@/shared/Editor';

export default connect(({ home }) => ({
  analysisStore: home.analysis,
}))(
  memo(({ match, history, dispatch, analysisStore }) => {
    const publishedModalRef = useRef();
    const analysisId = match.params.id;
    const isShare = parse(window.location.search)['?share'];
    const [analysis, setAnalysis] = useState(null);
    const [comments_no, setComment_no] = useState(0);

    const onClosePublishedModal = () => document.body.removeChild(publishedModalRef.current);

    useEffect(() => {
      dispatch({ type: 'home/getAnalysisDetail', variables: { analysisId } });
    }, [dispatch, analysisId]);

    useEffect(() => {
      if (analysisStore.post_id === analysisId) {
        setAnalysis(analysisStore);
      }
      analysisStore.comments_no > comments_no && setComment_no(analysisStore.comments_no);
    }, [comments_no, analysisId, analysisStore]);

    useEffect(() => {
      const queries = getQueries();
      const { published } = queries;

      if (published === '1') {
        delete queries.published;
        router.replace(
          `${window.location.pathname}${Object.entries(queries).reduce((prev, cur, idx) => `${prev}${cur[0]}=${cur[1]}&`, '?')}`,
        );
        publishedModalRef.current = new BodyBabyFactory({
          className: 'modal modal-published',
          cssText: 'position: fixed; top: 0; left: 0; right: 0; bottom: 0; background-color: rgba(0, 0, 0, .2); display: flex; align-items: center; justify-content: center;',
        }).exFactory(
          <ThirdParty
            closeable
            useFor="share"
            // onSelect={onSelect}
            header={
              <h2>{formatMessage({ id: 'user.analysis.published' })}</h2>
            }
            onRequestClose={onClosePublishedModal}
            description={formatMessage({ id: 'user.analysis.shareWithWorld' })}
            // buttonTxtPrefix={formatMessage({ id: 'component.thirdParty.shareTo' })}
            buttons={
              <Button
                block
                action="copy"
                shape="square"
                payload={window.location.href}
              >
              {formatMessage({ id: `component.thirdParty.copylink` })}
            </Button>
            }
          />,
        );
      }
    }, []);

    const addCommit = useCallback(
      (no) => {
        setComment_no(comments_no + 1);
      });


    if (!analysis) return <DefaultPage />;
    return (
      <section className={styles.newsletter_detail}>
        <section className={styles.article}>
          <MessageTitle created_at={analysis.published_at}
                        tilte={analysis.title} />
          <StockInfo ticker={analysis.ticker || {}}
                     hasAdd={false}
          />
          {analysis.prediction_side ? <div className={`${styles.direction} ${analysis.prediction_side ===
          'LONG' ? styles.long : styles.short}`}>
            {formatMessage({ id: `home.analysis.${analysis.prediction_side}` })}
          </div> : null}
          <Chart
            analysis={analysis}
            ticker={analysis.ticker.key}
          />
          <NewsInBrief
            tradeId={analysis.trade_id}
            page={analysis.summary || ''} />
            <div className={styles.edit_content}>
              <ContentComponent content={analysis.content} />
              <div className={styles.edit_content_mark}></div>
            </div>
        </section>
        <AnalysisDetailComments
          post_id={analysisId}
          account={analysis.account}
          addCommit={addCommit} />
        <InteractForDepthDetail
          data={analysisStore}
          comments_no={comments_no}
          onCommentPress={null}
          tweetId={analysisStore.post_id} />
      </section>
    );
  }));

class ContentComponent extends PureComponent {
  renderAnalysisDetail = (content) => {
    new EditorJS({
      holder: 'editContent',
      data: JSON.parse(content.includes('blocks') ? content : '{}'),
    });
  };

  componentDidMount() {
    this.props.content && this.renderAnalysisDetail(this.props.content);
  }

  render() {
    return <div id='editContent' />;
  }
}
