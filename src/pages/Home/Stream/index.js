import React, { useState, useEffect, useCallback, useContext, Fragment, PureComponent, memo } from 'react';
import styles from './styles.less';
import CreateNew from '../components/CreateNew';
import { FormattedMessage } from 'umi-plugin-react/locale';
import Spinner from '@atlaskit/spinner';
import debounce from 'debounce';
import { DefaultPage } from '@/components/DefaultPage';
import { connect } from 'dva';
import Tweet from '../components/Tweet';
import Depth from '../components/Depth';
import qs from 'querystring';
import { detectZoom } from '@/shared/helpers';

export default connect(({ home }) => ({
  streamList: home.streamList,
  cursor: home.cursor,
  noMore: home.noMore,
  streamQuery: home.streamQuery,
}))(
  ({ streamList, dispatch, cursor, noMore, route, streamQuery }) => {
    let variables = {
      ...streamQuery,
      size: '3',
    };
    if (!variables.postType) {
      delete variables.postType;
    }
    const [isLoading, setIsLoading] = useState(true);
    const search = window.location.search.replace(/^\?/, '');
    if (search) {
      const searchData = qs.parse(search);
      if (searchData.postType) variables.postType = searchData.postType;
      if (searchData.accountId) variables.accountId = searchData.accountId;
    }

    const reFetch = useCallback(async () => {
      setIsLoading(true);
      await dispatch({ type: 'home/stream', variables });
      setIsLoading(false);
    });

    useEffect(() => {
      reFetch();
    }, [streamQuery]);

    const loadMore = useCallback(debounce(async () => {
      if (cursor) {
        setIsLoading(true);
        await dispatch({ type: 'home/stream', variables: { ...variables, cursor } });
        setIsLoading(false);
      }
    }, 1000));


    return (
      <Stream isOver={noMore}
              isLoading={isLoading}
              streamList={streamList}
              dispatch={dispatch}
              loadMore={loadMore} />);
  });

class Stream extends PureComponent {

  onScroll = (e) => {
    const { isOver, isLoading } = this.props;
    if (!isOver && !isLoading && e.target.scrollingElement.scrollHeight <= document.documentElement.clientHeight + e.target.scrollingElement.scrollTop + 1) {
      this.props.loadMore();
    }
  };

  renderNoMessage() {
    return <div className={styles.no_message}>
      <img src={require('@/assets/common/img_no_post.png')}
           alt="" />
           <FormattedMessage id="home.No Information" />
    </div>;
  }

  componentDidMount() {
    document.addEventListener('scroll', this.onScroll);
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.onScroll);
  }

  render() {
    const { dispatch, streamList } = this.props;
    const { isOver, isLoading } = this.props;
    return <section className={styles.stream}>
      <CreateNew refresh={this.props.refresh}
                 dispatch={dispatch} />
      {streamList.length ? streamList.map((item, index) => (item.type === 'TWEET' ?
        <Tweet data={item}
               key={item.post_id}
        /> :
        <Depth
          data={item}
          key={item.post_id} />))
        : isLoading ? <DefaultPage /> : this.renderNoMessage()
      }
      <div className={styles.spinner}
           style={{ display: !isOver ? 'flex' : 'none' }}><Spinner /></div>
      {isOver ? <div className={styles.no_more}>
               <FormattedMessage id="home.NoMore" />
      </div>
        : null}
  </section>;
  }
}

