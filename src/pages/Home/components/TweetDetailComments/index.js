import React from 'react';
import styles from './index.less';
import { COMMENT, COMMENTS } from '@/shared/graphql/gql/tweet';
import { useQuery } from '@apollo/react-hooks';
import Comment from '@/pages/Home/components/Comment';
import CommentList from '../CommentList';
import Spinner from '@atlaskit/spinner';

export default ({ tweetId, account, addCommit, goDetail }) => {
  const { data: { comments: commentsList } = {}, refetch, loading } = useQuery(COMMENTS, {
    skip: !tweetId,
    variables: {
      tweetId,
    },
  });

  return (
    <section className={styles.comments}>
      <Comment tweetId={tweetId}
               comment={COMMENT}
               addCommit={addCommit}
               refresh={refetch}
               account={account || {}} />
      <div className={styles.comment_list}>
        {loading ?
          <div className={styles.spinner}>
            <Spinner />
          </div> :
          <CommentList
            tweetId={tweetId}
            account={account}
            refetch={refetch}
            commentsList={commentsList} />
        }
      </div>
    </section>
  );
};


