import React, { useCallback, useState, useContext, useEffect } from 'react';
import styles from './styles.less';
import { formatMessage } from 'umi-plugin-react/locale';
import { Icon } from '@/components';
import { copy } from '@/components/CopyText';
import Button from '@atlaskit/button';
import { LICK_TWEET, DISLICK_TWEET } from '@/shared/graphql/gql/tweet';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import router from 'umi/router';
import { HomeContext } from '@/store';
import { useDispatch } from 'dva';

const iconStyle= {marginRight: '4px'}

export const Like = (props) => {
  const { tweetId, refresh } = props;
  const [isLike, setIsLike] = useState(props.isLike);
  const [no, setNo] = useState(props.on);
  const [isShowAddOne, setIsShowAddOne] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    setIsLike(props.isLike);
    setNo(props.no);
  }, [props.isLike, props.no]);

  const [fetchLike] = useMutation(LICK_TWEET, {
    onCompleted() {
    },
  });

  const onPressLike = useCallback(() => {
    fetchLike({
      variables: {
        tweetId,
      },
    });
    dispatch({ type: 'home/changeAnalysisLike', no: 1 });
    setIsLike(true);
    setNo(no + 1);
    setIsShowAddOne(true);
    setTimeout(() => {
      setIsShowAddOne(false);
    }, 2000);
  });


  const cancelLikeOnPress = useCallback(() => {
    fetchLike({
      variables: {
        tweetId,
        type: 'CANCEL',
      },
    });
    dispatch({ type: 'home/changeAnalysisLike', no: -1 });
    setIsLike(false);
    setNo(no - 1);
  });

  return <>
    {isLike ? <Button
        onClick={cancelLikeOnPress}
        appearance="subtle"
        css={{
          width: '60px',
          height: '60px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
      <div className={styles.button_content}>
          <Icon type="thumbs-up-colored"
                style={{ width: '22px', height: '35px' }} />
          <span style={{ color: '#FD4E4E' }}>{no}</span>
        </div>
    </Button> :
      <Button appearance="subtle"
              css={{
                width: '60px',
                height: '60px',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onClick={onPressLike}>
        <div className={styles.button_content}>
          <Icon type="thumbs-up"
                style={{ width: '22px', height: '35px' }} />
          <span>{no}</span>
        </div>
  </Button>
    }
    {isShowAddOne ? <div className={styles.liking}>
      <Icon type="thumbs-up-effect"
            style={{ width: '50px' }} />
    </div> : null}
    </>;
};

export const Share = ({ tweetId }) => {
  const [isShow, setIsShow] = useState(false);
  const click = useCallback(() => {
    copy(window.location.host + '/home/tweetdetail/' + tweetId + '?share=1');
    setIsShow(true);
    setTimeout(() => {
      setIsShow(false);
    }, 1200);
  });

  return <Button appearance="subtle"
                 css={{
                   width: '60px',
                   height: '60px',
                   display: 'flex',
                   alignItems: 'center',
                   justifyContent: 'center',
                 }}
                 onClick={click}>
     <div className={styles.button_content}>
          <Icon type="share"
                style={{ width: '22px', height: '35px' }} />
          <span>{formatMessage({ id: 'common.share' })}</span>
     </div>
    {isShow ? <div className={styles.action}>
          {formatMessage({ id: 'home.copied' })}
        </div> : null
    }


  </Button>;
};

export default ({ data, tweetId, refresh, onCommentPress, comments_no }) => {
  return <div className={styles.interact_depth_detail}>
    <ul>
      <li>
        <Like no={data.likes_no}
              refresh={refresh}
              tweetId={tweetId}
              isLike={data.is_like} />
      </li>
      <li>
        <Share tweetId={tweetId} />
      </li>
      <li>
        <div className={styles.button_content}>
          <Icon
            style={{ width: '25px', height: '35px' }}
            type="wechat" />
        </div>
      </li>
    </ul>
  </div>;
};
