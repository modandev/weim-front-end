import React, { useState, useEffect, useCallback, useContext, Fragment, PureComponent, memo } from 'react';
import { Hr } from '@/components';
import TweetUser from '../TweetUser';
import StockInfo from '../StockInfo';
import styles from './styles.less';
import NewsInBrief from '../NewsInBrief';
import { InteractForStream } from '../Interact';
import Chart from '../PieChart';
import TweetComments from '../TweetComments';
import router from 'umi/router';
import MessageTitle from '@/pages/Home/components/MessageTitle';

export default memo(({ data }) => {
  const [tweet, setTweet] = useState(data);
  const [comments_no, setComments_no] = useState(data.comments_no);
  useEffect(() => {
    setTweet(data);
  }, [data]);
  const [isShowComment, setIsShowComment] = useState(false);
  useEffect(() => {
  }, [isShowComment]);

  const addCommit = useCallback(
    (no) => {
      setComments_no(comments_no + 1);
      // setTweet({ ...tweet, comments_no: tweet.comments_no + 1 });
    },
  );
  const goDetail = useCallback(() => router.push(`/home/tweetdetail/${tweet.post_id}?userId=${tweet.user_id}`));

  return (
    <div className={styles.content}>
      <TweetUser
        user_id={tweet.user_id}
        accountId={tweet.account_id}
        account={{
          ...(tweet.account || {}), ...tweet.accountTrading,
        }} />
      <Hr />
      <StockInfo
        ticker={tweet.ticker}
      />
      <Chart order={tweet.order}
             height={280}
             padding = {[80, 60, 50, 70]}
             post_id={tweet.post_id} />
      <MessageTitle created_at={tweet.created_at}
                    tilte={tweet.title} />
      <NewsInBrief tradeId={data.trade_id}
                   created_at={data.created_at || '2020-08-11T14:21:41.917747Z'}
                   page={data.content} />

      <Hr />
      <InteractForStream data={tweet}
                         comments_no={comments_no}
                         onCommentPress={() => setIsShowComment(!isShowComment)}
                         tweetId={tweet.post_id} />
      {isShowComment ?
        <TweetComments
          addCommit={addCommit}
          type='stream'
          goDetail={goDetail}
          user_id={tweet.user_id}
          tweetId={tweet.post_id}
          account={tweet.account} /> : null
      }
    </div>);
});


