import React, { useRef, useState, useCallback, useLayoutEffect } from 'react';
import styles from './styles.less';
import Button from '@/components/Buttons';
import { Icon } from '@/components';
import { FormattedMessage, formatMessage } from 'umi-plugin-react/locale';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

dayjs.extend(relativeTime);


const getTime = (time) => {
  const inTime = (dayjs() - dayjs(time)) / 86400000;
  if (inTime / 365 > 1) {
    return [parseInt(inTime / 365), 'year'];
  } else if (inTime / 30 > 1) {
    return [parseInt(inTime / 30), 'month'];
  } else if (inTime / 7 > 1) {
    return [parseInt(inTime / 7), 'week'];
  } else if (inTime > 1) {
    return [parseInt(inTime), 'day'];
  } else {
    return [1, 'day'];
  }
};

const NewsInBrief = ({ page, onShowMore }) => {
  const [isShowAll, setIsShowAll] = useState(false);
  const [isOverHeight, setIsOverHeight] = useState(false)
  const messageBox = useRef(null);
  const showMore = useCallback(() => {
    setIsShowAll(true);
  });

  useLayoutEffect(() => {
    const messageHeight = messageBox.current ? messageBox.current.offsetHeight : 0;
    setIsOverHeight(messageHeight > 168);
  },[page]);

  return <div className={styles.newsInBrief}>
    <p className={`${styles.message} ${isShowAll ? styles.noMore : ''}`}>
      <span ref={messageBox}>
        {page}
      </span>
    </p>

    {(isOverHeight && !isShowAll) || onShowMore ?
      <div className={styles.buttonBox}>
          <Button appearance="link"
                  onClick={onShowMore || showMore}
                  iconAfter={<Icon type="right" />}>
            <FormattedMessage id={'home.tweet.ViewFull'} />
          </Button>
    </div> : null}
  </div>;
};


export default NewsInBrief;
