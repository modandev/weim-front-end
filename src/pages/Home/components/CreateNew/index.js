import React, { useCallback, useEffect, useState } from 'react';
import styles from './styles.less';
import SelectButton from '@/components/SelectButton';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { ORDERS } from '@/shared/graphql/gql/trade';
import { connect, useDispatch } from 'dva';
import TextArea from '@atlaskit/textarea';
import Button from '@/components/Buttons';
import EditorDoneIcon from '@atlaskit/icon/glyph/editor/done';
import EditIcon from '@atlaskit/icon/glyph/edit';
import { FormattedMessage, getLocale, formatMessage } from 'umi-plugin-react/locale';
import Spinner from '@atlaskit/spinner';
import dayjs from 'dayjs';
import WarningIcon from '@atlaskit/icon/glyph/warning';
import router from 'umi/router';
import { Icon, EllipsisText } from '@/components';

export default ({ refresh }) => {
  const [isEdit, setIsEdit] = useState(false);
  const [tradeInfo, setTradeInfo] = useState({});
  const [content, setContent] = useState('');
  const onClick = useCallback(() => {
    setIsEdit(!isEdit);
  });

  const goEdit = useCallback(() => {
    router.push('/analyses/edit');
  });

  return isEdit ? <WriteNews
    tradeInfo={tradeInfo}
    setTradeInfo={setTradeInfo}
    setContent={setContent}
    content={content}
    setIsEdit={setIsEdit}
    refresh={refresh} /> : (
    <section className={styles.create_news}>
    <div className={styles.box}
         onClick={onClick}>
      <EditIcon />&nbsp;

      <span>
        <FormattedMessage id="home.main.newsletters.placeholder" />
      </span>
    </div>

      <button onClick={goEdit}
              className={styles.go_edit}>
        {formatMessage({ id: 'home.goEdit' })}
      </button>
  </section>);
}

const WriteNews = ({ setIsEdit, tradeInfo, setTradeInfo, setContent, content }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isSelect, setIsSelect] = useState(false);

  const { data: { orders = [] } = {}, refetch, loading: orderLoading } = useQuery(ORDERS, {
    variables: {
      'accountId': '',
      'scope': '36h',
    },
  });

  const onChange = useCallback((tradeId, item) => {
    setTradeInfo(item);
    setIsSelect(false);
  });

  const contentOnChange = useCallback((d) => {
    setContent(d.target.value);
  });

  const onSubmit = useCallback(async () => {
    if (!tradeInfo.order_id) {
      setIsSelect(true);
    }
    if (content && tradeInfo.order_id && content.trim()) {
      setLoading(true);
      await dispatch({
        type: 'home/createTweet',
        variables: {
          fields: {
            content,
            ticker: tradeInfo.ticker,
            order_id: tradeInfo.order_id,
            account_id: tradeInfo.account.account_id,
          },
        },
      });
      setLoading(false);
      setIsSuccess(true);
      setTimeout(() => {
        setContent('');
        setIsEdit('');
      }, 1000);
    }
  });

  const options = orders.map((item, index) => ({
    value: item.order_id,
    data: item,
    label: <ul key={index}
               className={styles.options}>
      <li style={{ flex: 3 }}>
        <p>
          <EllipsisText text={item.account.nickname}>
              {item.account.nickname}
          </EllipsisText>
      </p>
      </li>
      <li style={{ flex: 3 }}><p>{item.ticker}</p></li>
      <li style={{ flex: 3.5 }}><p>{dayjs(item.time).format('YYYY-MM-DD HH:mm ')}</p></li>
      <li style={{ flex: 1.5 }}><p>{item.quantity}</p></li>
    </ul>,
  }));
  return <div className={styles.create_header}>
    <div className={styles.header}>
      <Avatar tradeInfo={tradeInfo} />
      <div className={styles.avatar_box}>
        <div className={styles.user_name}>
          <FormattedMessage id="home.discover.transactionNewsletters" />
        </div>
        {orderLoading ? <div className={styles.order_spinner}><Spinner size={20} /></div> : options.length ?
          <SelectButton
            onChange={onChange}
            appearance={isSelect ? 'danger' : ''}
            placeholder={
              <div style={{ color: '#999' }}>{formatMessage({ id: 'home.tweet.selectOrder' })}</div>
            }

            defaultOption={options.find(item => item.value === tradeInfo.order_id)}
            options={options} /> :
          <div className={styles.no_options}>
          <WarningIcon /> {formatMessage({ id: 'home.tweet.There are noOrders' })}
        </div>}
      </div>
      <div onClick={() => setIsEdit('')}
           className={styles.return_button}>
        <Icon type="return" />
      </div>
    </div>

    <div className={styles.textarea_outer}>
      <TextArea
        appearance="none"
        isRequired={true}
        minimumRows={5}
        maxlength={500}
        isInvalid={tradeInfo.value && !content}
        onChange={contentOnChange}
        placeholder={formatMessage({ id: 'home.newsletter.comment.placeholder' })}>
      {content}
    </TextArea>
      <div className={styles.last}>
        {content ? content.length : 0}/500
      </div>
    </div>

    <div className={styles.button_box}>
      <Button appearance="primary"
              isDisabled={!tradeInfo.order_id || !content || !content.trim()}
              isLoading={loading}
              onClick={onSubmit}
              shape="square"
              css={{ width: '236px', height: '40px', display: 'flex', justifyContent: 'center' }}>{isSuccess ?
        <div style={{ height: '25px' }}>
          <EditorDoneIcon size="medium" />
        </div> :
        <FormattedMessage id="home.Publish" />}
      </Button>
    </div>
  </div>;
};

const Avatar = connect(({ user }) => ({
  user: user.currentUser,
}))(
  ({ user, tradeInfo }) => {
    const account = tradeInfo.account || {};
    return <div className={styles.avatar}>
        {account.header_url || user.avatar ?
          <img className={styles.avatar_img}
               src={account.header_url || user.avatar} /> :
          <div className={styles.default_avatar}>
            <span>
              {(user.nickname).slice(0, 1)}
            </span>
          </div>}
      </div>;
  },
);

