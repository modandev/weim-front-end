import React, { useCallback, useEffect, useRef } from 'react';
import Button from '@/components/Buttons';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import { formatMessage } from 'umi-plugin-react/locale';
import styles from './styles.less';
import { connect } from 'react-redux';
import { useDispatch } from 'dva';

export default ({ tweetId, comment, addCommit, refresh, type="TWEET" }) => {
  const dispatch = useDispatch()
  const [sendComment, { loading }] = useMutation(comment, {
    skip: !comment,
    onCompleted() {
      type ==='ANALYSIS' && dispatch({ type: 'home/addAnalysisCommentNo' });
      inputView.current.value = '';
      addCommit();
      refresh();
    },
  });

  const inputView = useRef(null);

  const sendMessage = useCallback(() => {
    const content = inputView.current.value;
    if (content && content.trim()) {
      sendComment({
        variables: {
          id: tweetId,
          content,
          type,
        },
      });
    }
  });

  return <div className={styles.comments_header}>
          <Avatar />
          <input ref={inputView}
                 autoFocus="autofocus"
                 placeholder={formatMessage({ id: 'home.newsletter.comment.placeholder' })} />
          <Button appearance="primary"
                  onClick={sendMessage}
                  isLoading={loading}
                  css={{ width: '100px', height: '40px' }}
                  size="large"
                  shape="square">
            {formatMessage({ id: 'common.publish' })}
          </Button>
        </div>;
}

const Avatar = connect(({ user }) => ({
  user: user.currentUser,
}))(
  ({ user }) => {
    return <div className={styles.avatar}>
        {user.avatar ? <img
          src={user.avatar} /> : <div className={styles.default_avatar}>
            <span>
              {(user.nickname || '').slice(0, 1)}
            </span>
          </div>}
      </div>;
  },
);
