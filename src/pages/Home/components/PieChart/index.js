import React, { useCallback, useEffect, memo } from 'react';
import {
  Axis,
  Area,
  Chart,
  Line,
  Annotation,
  Point,
  LineAdvance,
  Tooltip,
} from 'bizcharts';
import { semicolon } from '@/shared/helpers';
import { tickerMarketData } from '@/shared/graphql/gql/user';
import { useQuery } from '@apollo/react-hooks';
import styles from './styles.less';
import { formatMessage } from 'umi-plugin-react/locale';
import { Icon } from '@/components';
import dayjs from 'dayjs';
import Spinner from '@atlaskit/spinner';

const { DataMarker } = Annotation;

export default memo(({ order, post_id, padding = [70, 60, 50, 60], height = 320 }) => {

  const { data: { tickerMarketData: tickerData = [] } = {} } = useQuery(tickerMarketData,
    {
      skip: !order,
      variables: {
        ticker: order.ticker,
        start: order ? dayjs(order.time).subtract(6, 'day').format('YYYY-MM-DDT00:00:00+0000') : '',
        end: dayjs().format('YYYY-MM-DDT00:00:00+0000'),
      },
    });

  useEffect(() => {
  }, [tickerData, order]);

  const getTime = useCallback((order) => {
    return order ? dayjs(order.time).format('YYYY-MM-DD') : '';
  });

  const now = tickerData[tickerData.length - 1];
  const price = (tickerData.find(item => item.date === getTime(order)) || {}).close;

  return <div className={styles.chart_box}  style={{ height: height || 'auto' }}>
    <Rist now={now}
          order={order} />

    {order && tickerData.length ?
      <Chart
        id={post_id}
        errorContent=""
        autoFit
        defaultInteractions={[]}
        padding={padding}
        animate={false}
        height={height}
        scale={{
          close: {
            type: 'linear',
            nice: 'true',
          },
          date: {
            type: 'time',
            nice: true,
            range: [0, 0.95],
            tickCount: 6,
          },
        }}
        placeholder={(
          <div className={styles.empty}>
            <h6>{formatMessage({ id: 'message.chart.empty' })}</h6>
          </div>
        )}
        onGetG2Instance={g2Chart => {
          g2Chart.animate(false);

          // 缩放交互
          g2Chart.interaction('drag', {
            type: 'XY',
          }).interaction('zoom', {
            type: 'XY',
          });
        }}
        data={tickerData}>
        <Axis
          name="date"
          label={{
            formatter(text) {
              const [year, month, day] = text.split('-');

              return `${+year}-${+month}-${+day}`;
            },
          }}
        />
        <Axis
          name="close"
          label={{
            formatter(text) {
              return semicolon(text);
            },
          }}
        />

		<LineAdvance animate={false}
                 area
                 position="date*close"
    />

    <DataMarker position={[getTime(order), price]}
                autoAdjust={false}
                text={{
                  position: 'center',
                  content: (order.side === 'SELL' ? formatMessage({ id: 'home.Chart.Sell' }) : formatMessage({ id: 'home.Chart.Buy' })) + '\n' +
                    parseFloat(semicolon(order.avg_price)).toFixed(2),
                  style: {
                    fontSize: 12,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    fill: order.side === 'SELL' ? '#FF2C2C' : '#26B14F',
                  },
                }}
                region={
                  { backgroundColor: 'green' }
                }
                line={{
                  length: 10,
                  style: {
                    fill: 'transparent',
                    stroke: 'transparent',
                  },
                }}
                point={{
                  style: {
                    stroke: order.side === 'SELL' ? '#FF2C2C' : '#26B14F',
                    fill: order.side === 'SELL' ? '#FF2C2C' : '#26B14F',
                  },
                }}
                direction="upward"
    />

    <DataMarker position={[now.date, now.close]}
                autoAdjust={false}
                text={{
                  position: 'center',
                  content: (formatMessage({ id: 'home.Chart.Now' })) + '\n' +
                    parseFloat(semicolon(now.close)).toFixed(2),
                  style: {
                    textAlign: 'center',
                    fontSize: 12,
                    fontWeight: 'bold',
                    fill: '#FF9700',
                    stroke: 'transparent',
                  },
                }}
                region={
                  { backgroundColor: 'green' }
                }
                line={{
                  length: 10,
                  style: {
                    fill: 'transparent',
                    stroke: 'transparent',
                  },
                }}
                point={{
                  style: {
                    fill: now.date === getTime(order) ? 'transparent' : '#FF9700',
                    stroke: '#FF9700',
                  },
                }}
                direction={now.date === getTime(order) ? 'downward' : 'upward'}
    />

    <Tooltip
      title="date"
      showCrosshairs
      marker={{ background: 'transparent' }}
    >
        {(title, [item]) => (<Tol title={title}

                                  item={item}></Tol>
        )}
      </Tooltip>
	</Chart> : <Spinner />}
    </div>;
});

const Tol = memo(({ title, item }) => {
  return <div className={styles.tooltip}>
              <div className={styles.title}>{title.split('T')[0]}</div>
              <div className={styles.value}>{semicolon((+item.value).toFixed(2))}</div>
            </div>;
});

const Rist = memo(({ now, order }) => {
    const point = now ? ((((now.close || 0) - order.avg_price) / order.avg_price) * 100).toFixed(2) : '';
    return point === '' ? null : point > 0 ? <div className={styles.rise}>
      <Icon type="rise" />&nbsp;{point}%
    </div> : <div className={styles.down}>
      <Icon type="downwrap" />&nbsp;{point}%
    </div>;
  },
);
