import React, { useState, useCallback, useContext, useRef } from 'react';
import styles from './styles.less';
import { formatMessage } from 'umi-plugin-react/locale';
import { router } from 'umi';
import FollowButton from '@/components/FollowButton';
import { AccountCard, InlineDialog, EllipsisText } from '@/components';

export default ({ account, refresh, user_id }) => {
  const goFans = useCallback(() => {
    router.push(`/followers/${user_id}/accounts/${account.account_id}`);
  });

  const goDetails = useCallback(() => {
    window.open(`/dashboard/${user_id}/accounts/${account.account_id}`);
  });

  if (!account) return null;
  return (
    <div className={styles.tweetUser}>
      <div className={styles.avatar}>
        <InlineDialog
          isOpen={false}
          placement="auto-start"
          content={
            <AccountCard style={{ width: '300px' }}
                         account={account} />}
          delay={500}>
          {account.header_url ? <img
              onClick={goDetails}
              className={styles.avatar_img}
              src={account.header_url}
              alt="" /> :
            <div onClick={goDetails}
                 className={styles.default_avatar}>
              <span>
                {(account.nickname).slice(0, 1)}
              </span>
            </div>
          }
        </InlineDialog>

      </div>
      <div className={styles.avatar_box}>
        <InlineDialog
          isOpen={false}
          placement="auto-start"
          content={<AccountCard style={{ width: '300px' }}
                                account={account} />}
          delay={500}>
          <div className={styles.user_name}
               onClick={goDetails}>
            <EllipsisText text={account.nickname}>
              {account.nickname}
            </EllipsisText>
          </div>
        </InlineDialog>

        <div
          onClick={goFans}
          className={styles.user_fun}>{formatMessage({ id: 'home.user.Follows' }, {
          count: account.no_of_followers || 0,
          s: account.no_of_followers > 1 ? 's' : '',
        })}
        </div>

      </div>

      <FollowButton is_followed={account.is_followed}
                    userId={account.user_id}
                    account_id={account.account_id} />
  </div>);
}

