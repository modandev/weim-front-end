import React, { useState, useCallback, useRef, useEffect, Fragment } from 'react';
import styles from './index.less';
import { ANALYSIS_COMMENT, ANALYSIS_COMMENTS } from '@/shared/graphql/gql/depth';
import { useQuery } from '@apollo/react-hooks';
import Comment from '@/pages/Home/components/Comment';
import CommentList from '../CommentList';
import Spinner from '@atlaskit/spinner';
import Button from '@/components/Buttons';
import { formatMessage } from 'umi-plugin-react/locale';

export default ({ post_id, account, addCommit, goDetail }) => {
  const { data: { analysisComments: commentsList } = {}, refetch, loading } = useQuery(ANALYSIS_COMMENTS, {
    skip: !post_id,
    variables: {
      analysisId: post_id,
    },
  });

  const setOrder = useCallback((sort) => {
    refetch({
      analysisId: post_id,
      sort,
    });
  });

  return (
    <section className={styles.comments}>
      <Order setOrder={setOrder} />

      <Comment tweetId={post_id}
               addCommit={addCommit}
               refresh={refetch}
               type="ANALYSIS"
               comment={ANALYSIS_COMMENT}
               account={account || {}} />
      <div className={styles.comment_list}>
        {loading ?
          <div className={styles.spinner}>
            <Spinner />
          </div> :
          <CommentList
            tweetId={post_id}
            account={account}
            type="ANALYSIS"
            refetch={refetch}
            commentsList={commentsList} />
        }
      </div>
    </section>
  );
};

export const Order = ({ setOrder }) => {
  const [sort, setSort] = useState('LATEST');
  const orderList = [
    { value: 'LATEST', text: formatMessage({ id: 'home.analysis.Newest' }) },
    { value: 'MOST_LIKES', text: formatMessage({ id: 'home.analysis.MostReply' }) },
    { value: 'MOST_COMMENTS', text: formatMessage({ id: 'home.analysis.MostComments' }) },
  ];

  const onPress = useCallback((nextSort) => {
    if (sort !== nextSort) {
      setSort(nextSort);
      setOrder(nextSort);
    }
  });

  return <ul className={styles.order}>
    {
      orderList.map((item, index) => {
        return <Fragment key={index}>
        <li  key={index}>
          <Button appearance={'inline'}
                  onClick={onPress.bind(null, item.value)}
                  isSelected={sort === item.value}>
            {item.text}
          </Button>
        </li>
          {index + 1 < orderList.length ? <div className={styles.hr} /> : null}
        </Fragment>;
      })
    }
  </ul>;
};

