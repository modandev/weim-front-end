import React, { useCallback, useState, useContext, useEffect } from 'react';
import styles from './styles.less';
import { formatMessage } from 'umi-plugin-react/locale';
import { Icon } from '@/components';
import { copy } from '@/components/CopyText';
import Button from '@atlaskit/button';
import { LICK_TWEET, DISLICK_TWEET } from '@/shared/graphql/gql/tweet';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import router from 'umi/router';
import { HomeContext } from '@/store';

const iconStyle= {marginRight: '6px'}

export const Lick = (props) => {
  const { tweetId, refresh } = props;
  const [isLike, setIsLike] = useState(props.isLike);
  const [no, setNo] = useState(props.on);
  const [isShowAddOne, setIsShowAddOne] = useState(false);
  const { dispatch } = useContext(HomeContext);

  useEffect(() => {
    setIsLike(props.isLike);
    setNo(props.no);
  }, [props.isLike, props.no]);

  const [fetchLick] = useMutation(LICK_TWEET, {
    onCompleted() {
      dispatch({ type: 'setLike', id: tweetId });
    },
  });

  const onPressLike = useCallback(() => {
    fetchLick({
      variables: {
        tweetId,
      },
    });
    setIsLike(true);
    setNo(no + 1);
    setIsShowAddOne(true);
    setTimeout(() => {
      setIsShowAddOne(false);
    }, 1000);
  });


  const cancelLikeOnPress = useCallback(() => {
    fetchLick({
      variables: {
        tweetId,
        type: 'CANCEL',
      },
    });
    setIsLike(false);
    setNo(no - 1);
  });

  return <>
    {isLike ? <Button
        onClick={cancelLikeOnPress}
        appearance="subtle"
        iconBefore={<Icon type="thumbs-up-colored" style={iconStyle} />}>
        <span style={{ color: '#FD4E4E' }}>{no}</span>
      <div className={styles.add_one}>+1</div>
        {isShowAddOne ? <div className={styles.add_one}>+1</div> : null}
    </Button> :
      <Button appearance="subtle"
              onClick={onPressLike}
              iconBefore={<Icon type="thumbs-up" style={iconStyle}/>}>
      <span>{no}</span>
        <div className={styles.add_one}>+1</div>
  </Button>
    }
    </>;
};

export const UnLick = (props) => {
  const { tweetId, refresh } = props;
  const [isDisLike, setIsDisLike] = useState(props.isDisLike);
  const [no, setNo] = useState(props.on);
  const { dispatch } = useContext(HomeContext);
  const [isShowAddOne, setIsShowAddOne] = useState(false);
  const [fetchLick] = useMutation(DISLICK_TWEET, {
    onCompleted() {
      dispatch({ type: 'setDisLike', id: tweetId });
    },
  });

  useEffect(() => {
    setIsDisLike(props.isDisLike);
    setNo(props.no);
  }, [props.isDisLike, props.no]);

  const disLickOnPress = useCallback(() => {
    fetchLick({
      variables: {
        tweetId,
      },
    });
    setIsDisLike(true);
    setNo(no + 1);
    setIsShowAddOne(true);
    setTimeout(() => {
      setIsShowAddOne(false);
    }, 1000);
  });

  const cancelDisLickOnPress = useCallback(() => {
    fetchLick({
      variables: {
        tweetId,
        type: 'CANCEL',
      },
    });
    setIsDisLike(false);
    setNo(no - 1);
  });

  return isDisLike ? <Button appearance="subtle"
                             onClick={cancelDisLickOnPress}
                             iconBefore={<Icon type={'thumbs-down-color'} style={iconStyle}/>}>
    <span style={{ color: '#3B82FF' }}>{no}</span>
      {isShowAddOne ? <div className={styles.add_one}>+1</div> : null}
  </Button> :
    <Button appearance="subtle"
            onClick={disLickOnPress}
            iconBefore={<Icon type={'thumbs-down'} style={iconStyle}/>}>
    <span>{no}</span>
  </Button>;
};

export const Comment = ({ no, tweetId, onPress }) => {
  return <Button appearance="subtle"
                 onClick={onPress}
                 iconBefore={<Icon type="comment" style={iconStyle}/>}>
    <span>{no}</span>
  </Button>;
};

export const Share = ({ tweetId }) => {
  const [isShow, setIsShow] = useState(false);
  const click = useCallback(() => {
    copy(window.location.host + '/home/tweetdetail/' + tweetId + '?share=1');
    setIsShow(true);
    setTimeout(() => {
      setIsShow(false);
    }, 1200);
  });
  return <Button appearance="subtle"
                 onClick={click}
                 iconBefore={<Icon type="share" style={iconStyle}/>}>
        {isShow ? <div className={styles.action}>
          {formatMessage({ id: 'home.copied' })}
        </div> : null}
    {formatMessage({ id: 'common.share' })}

  </Button>;
};

const Interact = ({ data, tweetId, refresh, comments_no }) => {
  return <div className={styles.interact}>
    <ul className="clearfix">
      <li>
        <Lick no={data.likes_no}
              refresh={refresh}
              tweetId={tweetId}
              isLike={data.is_like} />
      </li>
      <li>
        <UnLick no={data.dislikes_no}
                tweetId={tweetId}
                refresh={refresh}
                isDisLike={data.is_dislike} />
      </li>
      <li>
        <Comment
          onPress={null}
          tweetId={tweetId}
          no={comments_no} />
      </li>
      <li>
        <Share tweetId={tweetId} />
      </li>
    </ul>
  </div>;
};

export const InteractForStream = ({ data, tweetId, refresh, onCommentPress, comments_no }) => {
  return <div className={styles.interact}>
    <ul className="clearfix">
      <li>
        <Lick no={data.likes_no}
              refresh={refresh}
              tweetId={tweetId}
              isLike={data.is_like} />
      </li>
      <li>
        <UnLick no={data.dislikes_no}
                tweetId={tweetId}
                refresh={refresh}
                isDisLike={data.is_dislike} />
      </li>
      <li>
        <Comment
          tweetId={tweetId}
          onPress={onCommentPress}
          no={comments_no} />
      </li>
      <li>
        <Share tweetId={tweetId} />
      </li>
    </ul>
  </div>;
};

export const InteractForDepth = ({ data, tweetId, refresh, onCommentPress, comments_no }) => {
  return <div className={styles.interact}>
    <ul className="clearfix">
      <li>
        <Lick no={data.likes_no}
              refresh={refresh}
              tweetId={tweetId}
              isLike={data.is_like} />
      </li>
      <li>
        <Comment
          tweetId={tweetId}
          onPress={onCommentPress}
          no={comments_no} />
      </li>
      <li>
        <Share tweetId={tweetId} />
      </li>
    </ul>
  </div>;
};

export default Interact;
