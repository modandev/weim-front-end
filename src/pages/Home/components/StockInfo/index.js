import React, { useCallback, useContext, useState } from 'react';
import styles from './styles.less';
import Button from '@/components/Buttons';
import { Icon, CopyText, Hr, StockAreaCode } from '@/components';
import { DELETE_WATCH_LIST, COMMIT_WATCH_LIST } from '@/shared/graphql/gql/follow';
import { useMutation } from '@apollo/react-hooks';
import { FormattedMessage } from 'umi-plugin-react/locale';
import { useDispatch } from 'dva';
import EditorDoneIcon from '@atlaskit/icon/glyph/editor/done';
import { router } from 'umi';

export default ({ ticker, hasAdd = true }) => {
  const dispatch = useDispatch();
  const [fetchCommitWatchlist, { loading: commitLoading }] = useMutation(COMMIT_WATCH_LIST, {
    variables: {
      ticker: ticker.key,
    },
    onCompleted() {
      dispatch({ type: 'home/changeWatchStatus', ticker: ticker.key });
      dispatch({ type: 'assets/changeWatchStatus', ticker: ticker.key });
    },
  });

  const goDetail = useCallback(() => {
    router.push(`/asset/${ticker.key}/account`);
  });

  const [fetchDeleteWatchlist, { loading: deleteLoading }] = useMutation(DELETE_WATCH_LIST, {
    variables: {
      ticker: ticker.key,
    },
    onCompleted() {
      dispatch({ type: 'home/changeWatchStatus', ticker: ticker.key });
      dispatch({ type: 'assets/changeWatchStatus', ticker: ticker.key });
    },
  });

  return <div className={styles.stock}>
    <div className="row_box">
      <Icon className={styles.icon}
            type="organization" />
      <div
        className={styles.stock_name}>
        <a onClick={goDetail}>
          {ticker.name}
        </a>
      </div>
      {hasAdd ? !ticker.is_watch ?
        <Button onClick={fetchCommitWatchlist}
                css={{ height: '30px' }}
                isLoading={commitLoading}
                appearance="primary"
                iconBefore={<Icon type="add-white" />}
                shape="round">
          <FormattedMessage id={'home.stock.WatchList'} />
        </Button> :
        <Button onClick={fetchDeleteWatchlist}
                css={{ height: '30px' }}
                isLoading={deleteLoading}
                appearance="default"
                shape="round">
           <div className={'flex-center'}>
             <div style={{ marginLeft: '-5px', marginRight: '3px' }}
                  className={'flex-center'}>
             <EditorDoneIcon />
             </div>
             <FormattedMessage id={'home.stock.CancelWatch'} />
           </div>
      </Button> : null}
    </div>
    <Hr color="transparent" />
    <div className={styles.stock_line}
         onClick={goDetail}>
      <span className={styles.code}><FormattedMessage id="home.stock.Code" />:&nbsp;</span>
      <StockAreaCode countryCode={ticker.country_code}/>&nbsp;
      <span className={styles.code}> {ticker.symbol}</span>
    </div>
  </div>;
}
