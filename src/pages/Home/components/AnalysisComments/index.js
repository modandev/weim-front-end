import { Icon } from '@/components';
import React, { useState, useCallback, useRef, useEffect } from 'react';
import styles from './index.less';
import { formatMessage } from 'umi-plugin-react/locale';
import Button from '@/components/Buttons';
import { ANALYSIS_COMMENT, ANALYSIS_COMMENTS } from '@/shared/graphql/gql/depth';
import { useQuery } from '@apollo/react-hooks';
import Comment from '@/pages/Home/components/Comment';
import CommentList from '../CommentList';
import Spinner from '@atlaskit/spinner';

export default ({ post_id, account, addCommit, goDetail }) => {
  const { data: { analysisComments: commentsList } = {}, refetch, loading } = useQuery(ANALYSIS_COMMENTS, {
    skip: !post_id,
    variables: {
      analysisId: post_id,
    },
  });

  return (
    <section className={styles.comments}>
      <Comment tweetId={post_id}
               addCommit={addCommit}
               refresh={refetch}
               type="ANALYSIS"
               comment={ANALYSIS_COMMENT}
               account={account || {}} />
      <div className={styles.comment_list}>
        {loading ?
          <div className={styles.spinner}>
            <Spinner />
          </div> :
          <CommentList
            tweetId={post_id}
            account={account}
            type="ANALYSIS"
            pageType={'stream'}
            refetch={refetch}
            goDetail={goDetail}
            commentsList={commentsList} />
        }
        {commentsList && commentsList.length > 3 ? <div className={styles.show_more}>
          <Button appearance="subtle"
                  onClick={goDetail}
                  css={{ height: '24px' }}
                  iconAfter={<Icon type="right-black" />}>
            {formatMessage({ id: 'home.newsletter.comment.showMore' })}
          </Button>
        </div> : null}
      </div>
    </section>
  );
};


