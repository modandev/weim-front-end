import React, { useRef, useState, useCallback } from 'react';
import styles from './styles.less';
import Button from '@/components/Buttons';
import { Icon } from '@/components';
import { FormattedMessage, formatMessage } from 'umi-plugin-react/locale';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

dayjs.extend(relativeTime);


const getTime = (time) => {
  const inDay = (dayjs() - dayjs(time)) / 86400000;
  if (inDay / 365 > 1) {
    return [parseInt(inDay / 365), 'year'];
  } else if (inDay / 30 > 1) {
    return [parseInt(inDay / 30), 'month'];
  } else if (inDay / 7 > 1) {
    return [parseInt(inDay / 7), 'week'];
  } else if (inDay > 1) {
    return [parseInt(inDay), 'day'];
  } else {
    const inMin = (dayjs() - dayjs(time)) / 60000;
    if (inMin / 60 > 1) {
      return [parseInt(inMin / 60), 'hour'];
    } else if (inMin > 1) {
      return [parseInt(inMin), 'min'];
    }
    return [parseInt((dayjs() - dayjs(time)) / 1000), 'second'];
  }
};

const NewsInBrief = ({ created_at, tilte }) => {
  const [dayNo, dayUnit] = getTime(created_at);

  return (
    <div className={styles.message_title_box}>
      {tilte ? <div className={styles.message_title}>{tilte}</div> : null}
      <time className={styles.time}>
      {formatMessage({
          id: 'home.time.' + dayUnit,
        },
        {
          count: dayNo,
          s: dayNo > 1 ? 's' : '',
        })}
    </time>
    </div>
  );

};


export default NewsInBrief;
