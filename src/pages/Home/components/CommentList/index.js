import { Link } from 'umi';
import { AccountCard, FansCard, Icon, InlineDialog } from '@/components';
import React, { useState, useCallback, useRef, useEffect } from 'react';
import styles from './index.less';
import { formatMessage } from 'umi-plugin-react/locale';
import Button from '@/components/Buttons';
import { ButtonGroup } from '@atlaskit/button';
import { COMMENT, LIKE_COMMENT } from '@/shared/graphql/gql/tweet';
import { useMutation } from '@apollo/react-hooks';
import dayjs from 'dayjs';

export default ({ tweetId, account, pageType, commentsList, refetch, type = 'TWEET', goDetail }) => {
  const [commentBackId, setCommentBackId] = useState('');
  const [comments, setComments] = useState([]);

  useEffect(() => {
    if (commentsList) {
      setComments(commentsList);
    }
  }, [commentsList]);

  useEffect(() => {
  });

  const setLike = useCallback((id) => {
    const newComment = comments.map((item) => {
      if (id === item.comment_id) {
        return {
          ...item, is_like: !item.is_like, likes_no: item.likes_no + (item.is_like ? -1 : 1),
        };
      }
      return item;
    });
    setComments(newComment);
  });

  const setComment = useCallback((id, content) => {
    const newComment = comments.map((item) => {
      if (id === item.comment_id) {
        return {
          ...item,
          comments_no: item.comments_no + 1,
          comments: [...item.comments, {
            ...content,
            created_at: dayjs().unix(),
          }],
        };
      }
      return item;
    });
    setComments(newComment);
  });

  const goDashBord = useCallback((user_id, account_id) => {
    window.open(`/dashboard/${user_id}/accounts/${''}`);
  });

  return (
    <section className={styles.comment_list}>
      {comments.map((item, index) => {
        if (pageType == 'stream' && index > 2) {
          return null;
        }
        return <div key={index}
                    className={styles.comment_item}>
          <InlineDialog
            isOpen={false}
            placement="auto-start"
            content={<FansCard
              style={{ width: '300px' }}
              user={item.user}
              accounts={item.accounts}
              followingNo={item.userFollowing.following_no}
              userId={item.user_id}
              key={item.user_id} />}
            delay={500}>
              <div className={styles.avatar}>
              {item.user.avatar ?
                <img src={item.user.avatar}
                     onClick={() => {
                       goDashBord(item.user_id, item.account_id);
                     }}
                     className={styles.avatar} /> :
                <div className={styles.default_avatar}>
                  <span>
                    {(item.user.nickname).slice(0, 1)}
                  </span>
                </div>
              }
              </div>
          </InlineDialog>

        <div className={styles.content}>

        <div className={styles.text}>
           <InlineDialog
             isOpen={false}
             placement="auto-start"
             content={<FansCard
               style={{ width: '300px' }}
               user={item.user}
               accounts={item.accounts}
               followingNo={item.userFollowing.following_no}
               userId={item.user_id}
               key={item.user_id} />}
             delay={500}>
                <a onClick={() => {
                  goDashBord(item.user_id, item.account_id);
                }}>{item.user.nickname}:</a>
           </InlineDialog>
          <div className={styles.comment_text}>
          <span>{item.content}</span>
          </div>
        </div>
        <Like tweetId={tweetId}
              setLike={setLike}
              refresh={refetch}
              pageType={pageType}
              goDetail={goDetail}
              setCommentBackId={setCommentBackId}
              tweetData={item} />
          {commentBackId === item.comment_id ?
            <RenderWriteComment tweetId={tweetId}
                                setCommentBackId={setCommentBackId}
                                setComment={setComment}
                                refresh={refetch}
                                comment={item} /> :
            item.comments.map((comment, index) => {
              if (pageType === 'stream' && index > 0) {
                return null;
              }
              return (<RenderComment key={index}
                                     tweetId={tweetId}
                                     comment={comment} />);
            })
          }
        </div>
        </div>;
      })}
      </section>
  );
};

const Like = ({ tweetData, setCommentBackId, setLike, pageType, goDetail }) => {
  const [fetchLike] = useMutation(LIKE_COMMENT, {});

  const sendLike = useCallback(() => {
    setLike(tweetData.comment_id);
    fetchLike({
      variables: {
        commentId: tweetData.comment_id,
      },
    });
  });

  const cancelLike = useCallback(() => {
    setLike(tweetData.comment_id);
    fetchLike({
      variables: {
        commentId: tweetData.comment_id,
        type: 'CANCEL',
      },
    });
  });

  const onPress = useCallback(() => {
    if (tweetData.is_like) {
      cancelLike();
    } else {
      sendLike();
    }
  });

  return (
    <div className={styles.more}>
              <div className={styles.handle}>
                <ul className="clearfix">
                  <li>
                    <button onClick={onPress}>
                      <Icon style={{ width: '16.5px', height: '16.5px' }}
                            type={tweetData.is_like ? 'thumbs-up-colored' : 'thumbs-up'} />
                      {tweetData.likes_no || 0}
                    </button>
                  </li>
                  {setCommentBackId ? <li>
                    <button onClick={() => {setCommentBackId(tweetData.comment_id);
                    }}>
                      <Icon className={styles.buttonIcon}
                            type="comment" />
                      {tweetData.comments.length || 0}
                    </button>
                  </li> : null}
                </ul>
              </div>
              <div className={styles.time}>
                {dayjs(tweetData.created_at).format(formatMessage({ id: 'home.comment.time' }))}
              </div>
            </div>
  );
};

const RenderWriteComment = ({ comment, refresh, tweetId, setCommentBackId, setComment }) => {
  const inputView = useRef(null);
  const [fetchComment, { loading }] = useMutation(COMMENT, {
    onCompleted(data) {
      setTimeout(() => refresh && refresh(), 20);
      setComment(comment.comment_id, { content: inputView.current.value, ...data.comment });
      setCommentBackId('');
    },
  });

  const sendComment = useCallback(() => {
    const content = inputView.current.value;
    if (content && content.trim()) {
      fetchComment({
        variables: {
          content,
          id: comment.comment_id,
          type: 'COMMENT',
        },
      });
    }
  });

  return <div className={styles.comment_box}>
    <div className={styles.comment_input_box}>
      <div className={styles.user_name}>
        @{comment.user.nickname}:&nbsp;
      </div>
      <textarea
        maxlength="500"
        autoFocus="autofocus"
        className={styles.comment_input}
        ref={inputView}
        placeholder={formatMessage({ id: 'home.newsletter.comment.placeholder' })} />
    </div>
    <ButtonGroup>
      <Button appearance="subtle"
              onClick={() => {
                setCommentBackId('');
              }}
              css={{ width: '100px', height: '40px' }}
              shape="square">{formatMessage({ id: 'home.Cancel' })}</Button>
      <Button appearance="primary"
              onClick={sendComment}
              isLoading={loading}
              css={{ width: '100px', height: '40px' }}
              shape="square">{formatMessage({ id: 'home.Publish' })}</Button>
    </ButtonGroup>
  </div>;
};

const RenderComment = ({ comment, tweetId }) => {
  const [commentData, setCommentData] = useState(comment);
  useEffect(() => {
    setCommentData(comment);
  }, [comment]);

  const setLike = () => {
    setCommentData({
      ...commentData, is_like: !commentData.is_like, likes_no: commentData.likes_no + (commentData.is_like ? -1 : 1),
    });
  };

  return (
    <div className={styles.secondary_level}>
        <div className={styles.text}>
          <InlineDialog
            isOpen={false}
            placement="auto-start"
            content={<FansCard
              style={{ width: '300px' }}
              user={comment.user}
              accounts={comment.accounts}
              followingNo={comment.userFollowing.following_no}
              userId={comment.user_id}
              key={comment.user_id} />}
            delay={500}>
          <Link to="/">{commentData.user.nickname}:</Link>
          </InlineDialog>
          <div className={styles.comment_text}>
            <span>{commentData.content}</span>
          </div>
        </div>
        <div className={styles.time}>
          <Like tweetId={tweetId}
                setLike={setLike}
                tweetData={commentData} />
        </div>
      </div>);
};
