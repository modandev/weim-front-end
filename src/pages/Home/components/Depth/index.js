import React, { useState, useEffect, useCallback, useContext, Fragment, PureComponent, memo } from 'react';
import { Hr } from '@/components';
import TweetUser from '../TweetUser';
import StockInfo from '../StockInfo';
import styles from './styles.less';
import NewsInBrief from '../NewsInBrief';
import { InteractForDepth } from '../Interact';
import Chart from '@/components/DepthChart';
import AnalysisComments from '../AnalysisComments';
import MessageTitle from '@/pages/Home/components/MessageTitle';
import { stringify } from 'querystring';

export default memo(({ data }) => {
  const [tweet, setTweet] = useState(data);
  const [comments_no, setComments_no] = useState(data.comments_no);
  useEffect(() => {
    setTweet(data);
  }, [data]);
  useEffect(() => {
  }, [isShowComment]);

  const [isShowComment, setIsShowComment] = useState(false);

  const addCommit = useCallback(
    (no) => {
      setComments_no(comments_no + 1);
    },
  );

  const goDetail = useCallback(() => window.open(`/analyses/${tweet.post_id}?${stringify({
    userId: tweet.user_id,
    accountId: tweet.account_id,
    symbol: tweet.ticker.key,
  })}`));

  return (
    <div className={styles.content}>
      <TweetUser
        user_id={tweet.user_id}
        accountId={tweet.account_id}
        account={{
          ...(tweet.account || {}), ...tweet.accountTrading,
        }} />
      <Hr />
      <StockInfo
        ticker={tweet.ticker}
      />
      <Chart analysis={tweet}
             pagetype={'stream'}
             ticker={tweet.ticker.key} />

      <div style={{width: '100%', height: '16px', background: '#fff'}}> </div>

      <MessageTitle created_at={tweet.created_at}
                    tilte={tweet.title} />

      <NewsInBrief tradeId={data.trade_id}
                   onShowMore={goDetail}
                   created_at={data.created_at || '2020-08-11T14:21:41.917747Z'}
                   page={data.summary} />
      <Hr />

      <InteractForDepth data={tweet}
                        comments_no={comments_no}
                        onCommentPress={goDetail}
                        tweetId={tweet.post_id} />
      {isShowComment ?
        <AnalysisComments
          goDetail={goDetail}
          addCommit={addCommit}
          type='stream'
          post_id={tweet.post_id}
          user_id={tweet.user_id}
          account={tweet.account} /> : null
      }
    </div>);
});

