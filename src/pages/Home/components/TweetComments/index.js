import { Icon } from '@/components';
import React, { useState, useCallback, useRef, useEffect } from 'react';
import styles from './index.less';
import { formatMessage } from 'umi-plugin-react/locale';
import Button from '@/components/Buttons';
import { COMMENTS } from '@/shared/graphql/gql/tweet';
import { useQuery } from '@apollo/react-hooks';
import Comment from '@/pages/Home/components/Comment';
import CommentList from '../CommentList';
import Spinner from '@atlaskit/spinner';
import { COMMENT } from '@/shared/graphql/gql/tweet';

export default ({ tweetId, account, addCommit, goDetail }) => {
  const { data: { comments: commentsList } = {}, refetch, loading } = useQuery(COMMENTS, {
    skip: !tweetId,
    variables: {
      tweetId,
    },
  });

  return (
    <section className={styles.comments}>
      <Comment tweetId={tweetId}
               addCommit={addCommit}
               comment={COMMENT}
               refresh={refetch}
               account={account || {}} />
      <div className={styles.comment_list}>
        {loading ?
          <div className={styles.spinner}>
            <Spinner />
          </div> :
          <CommentList
            tweetId={tweetId}
            account={account}
            pageType={'stream'}
            refetch={refetch}
            goDetail={goDetail}
            commentsList={commentsList} />
        }
        {commentsList && commentsList.length > 3 ? <div className={styles.show_more}>
          <Button appearance="subtle"
                  onClick={goDetail}
                  css={{ height: '24px' }}
                  iconAfter={<Icon type="right-black" />}>
            {formatMessage({ id: 'home.newsletter.comment.showMore' })}
          </Button>
        </div> : null}
      </div>
    </section>
  );
};


