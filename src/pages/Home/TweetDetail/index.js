import React, { useState, useCallback, useEffect } from 'react';
import router from 'umi/router';
import { formatMessage } from 'umi-plugin-react/locale';
import { connect } from 'dva';
import { stringify, parse } from 'querystring';
import TweetUser from '@/pages/Home/components/TweetUser';
import { Hr, Icon } from '@/components';
import StockInfo from '@/pages/Home/components/StockInfo';
import NewsInBrief from '@/pages/Home/components/NewsInBrief';
import Interact from '@/pages/Home/components/Interact';
import { DefaultPage } from '@/components/DefaultPage';
import styles from './index.less';
import Chart from '../components/PieChart';
import TweetDetailComments from '../components/TweetDetailComments';
import MessageTitle from '@/pages/Home/components/MessageTitle';

export default connect(({ home }) => ({
  tweetStore: home.tweet,
}))(({ match, history, dispatch, tweetStore }) => {
  const tweetId = match.params.id;
  const isShare = parse(window.location.search)['?share'];
  const [tweet, setTweet] = useState(null);
  const [comments_no, setComment_no] = useState(0);
  useEffect(() => {
    dispatch({ type: 'home/getTweet', variables: { tweetId } });
  }, [dispatch, tweetId]);

  useEffect(() => {
    tweetStore.post_id === tweetId && setTweet(tweetStore);
    tweetStore.comments_no > comments_no && setComment_no(tweetStore.comments_no);
  }, [comments_no, tweetId, tweetStore]);

  const addCommit = useCallback(
    (no) => {
      setComment_no(comments_no + 1);
      // setTweet({ ...tweet, comments_no: tweet.comments_no + 1 });
    });

  if (!tweet) return <DefaultPage />;
  return (
    <section className={styles.newsletter_detail}>
      <section className={styles.article}>
        <div className={styles.back}>
          <a className={styles.backButton}
             onClick={() => isShare ? router.replace('/home') : router.goBack()}>
            <Icon type="back" />
            <span>{formatMessage({ id: 'common.back' })}</span>
          </a>
        </div>
        <TweetUser
          accountId={tweet.account_id}
          user_id={tweet.user_id}
          account={{
            ...(tweet.account || {}), ...tweet.accountTrading,
          }}
        />
        <Hr />
        <StockInfo
          ticker={tweet.ticker || {}}
        />
        <Chart order={tweet.order}
               height={260}
               padding = {[70, 60, 50, 70]} />
        <MessageTitle
          created_at={tweet.created_at}
          tilte={tweet.title} />
        <p className={`${styles.message}`}>
            <span>
              {tweet.content}
            </span>
        </p>
        <Interact
          data={tweet}
          comments_no={comments_no}
          tweetId={tweetId}
        />
      </section>

      <TweetDetailComments
        tweetId={tweetId}
        account={tweet.account}
        addCommit={addCommit} />
    </section>
  );
});
