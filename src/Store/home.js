import React, { createContext, useCallback } from 'react';

export const initialState = {
  streamList: [],
  watchList: [],
};

export const HomeContext = createContext({
  state: initialState,
  dispatch: () => {
  },
});

const watchListChangeStore = (state, ticker, isWatch) => {
  const { streamList } = state;
  const newStreamList = streamList.map(item => {
    if (ticker.key === item.ticker.key) {
      return {
        ...item,
        ticker: {
          ...item.ticker,
          is_watch: isWatch,
        },
      };
    }
    return item;
  });
  return newStreamList;
};

const followChangeStore = (state, account, isFollow) => {
  const { streamList } = state;
  const newStreamList = streamList.map(item => {
    if (account.account_id === item.account.account_id) {
      return {
        ...item,
        account: {
          ...item.account,
          no_of_followers: parseInt(item.account.no_of_followers || 0) + (isFollow ? 1 : -1),
          is_followed: isFollow,
        },
      };
    }
    return item;
  });
  return newStreamList;
};

const likeChangeStore = (state, tweetId) => {
  const { streamList } = state;
  const newStreamList = streamList.map(item => {
    if (tweetId === item.post_id) {
      return {
        ...item,
        likes_no: item.likes_no + (item.is_like ? -1 : 1),
        is_like: !item.is_like,
      };
    }
    return item;
  });
  return newStreamList;
};

const disLikeChangeStore = (state, tweetId) => {
  const { streamList } = state;
  const newStreamList = streamList.map(item => {
    if (tweetId === item.post_id) {
      return {
        ...item,
        dislikes_no: (parseInt(item.dislikes_no) || 0) + (item.is_dislike ? -1 : 1),
        is_dislike: !item.is_dislike,
      };
    }
    return item;
  });
  return newStreamList;
};


export function reducer(state, action) {
  switch (action.type) {
    case 'addStreamList':
      return { ...state, streamList: [action.tweet, ...state.streamList] };
    case 'setStreamList':
      return { ...state, streamList: action.streamList };
    case 'setFollow':
      return { ...state, streamList: followChangeStore(state, action.account, action.isFollow) };
    case 'setWatch':
      return { ...state, streamList: watchListChangeStore(state, action.ticker, action.isWatch) };
    case 'setLike':
      return { ...state, streamList: likeChangeStore(state, action.id) };
    case 'setDisLike':
      return { ...state, streamList: disLikeChangeStore(state, action.id) };
    default:
      throw new Error();
  }
}
