module.exports = {
  // main-color
  "primary-color": "#2F7354",
  "safe-color": "#26B633",
  // "danger-color": "#FF2C2C",
  "danger-color": "#DF2A54",
  "link-color": "#0081FF",
  "border-color": "#EEEDED",

  // hover-color
  "primary-hover-color": "#438768",
  "danger-hover-color": "#FF5454",
  "dropdown-item-hover-background-color": "rgba(0, 0, 0, .1)",

  // background-color
  "notice-background-color": '#222',
};
