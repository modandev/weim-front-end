import request from 'shared/graphql';

export async function search(params) {
  return request('search', {
    params,
  });
}

export async function searchHistory() {
  return request('searchHistory');
}

export async function deleteSearchHistory() {
  return request('deleteSearchHistory', {
    method: 'mutation'
  });
}

export async function searchHot() {
  return request('searchHot');
}

export async function commitSearchWord(params) {
  return request('commitSearchWord', {
    method: 'mutation',
    params
  });
}

export async function createFile(params) {
  return request('createFile', {
    method: 'mutation',
    params,
  });
}

export function watchlist(params) {
  return request('watchlist');
}
