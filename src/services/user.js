import request from 'shared/graphql';

export async function oauth(params) {
  return request('oauth', {
    method: 'mutation',
    params,
  });
}

export async function logIn(params) {
  return request('logIn', {
    method: 'mutation',
    params,
  });
}

export async function resetPassword(params) {
  return request('resetPassword', {
    method: 'mutation',
    params,
  });
}

export async function changePassword(params) {
  return request('changePassword', {
    method: 'mutation',
    params,
  });
}

export async function viewer() {
  return request('viewer');
}

export async function user (params) {
  return request('user', { params });
}

export async function getUserByUid ({ uid }) {
  return request('users', {
    params: {
      where: {
        uid
      },
    }
  });
}

export async function accounts(params) {
  return request('accounts', { params });
}

export async function account() {
  return request('account');
}

export async function createAccount(params) {
  return request('createAccount', {
    method: 'mutation',
    params
  });
}

export async function updateAccount(params) {
  return request('updateAccount', {
    method: 'mutation',
    params
  });
}

export async function updateUser(params) {
  return request('updateUser', {
    method: 'mutation',
    params
  });
}

export async function nav(params) {
  return request('nav', { params });
}

export async function position(params) {
  return request('position', { params });
}

export async function summary(params) {
  return request('summary', { params });
}

export async function trade(params) {
  return request('trade', { params });
}

export async function orders(params) {
  return request('orders', { params });
}

export async function tickerMarketData(params) {
  return request('tickerMarketData', { params });
}

export async function watchlist() {
  return request('watchlist');
}

export async function ticker(params) {
  return request('ticker', { params });
}

export async function tickers(params) {
  return request('tickers', { params });
}

export async function statsOfTicker(params) {
  return request('statsOfTicker', { params });
}

export async function analysis(params) {
  return request('analysis', { params });
}

export async function analysisDetail(params) {
  return request('analysisDetail', { params });
}

export async function publishDraft(params) {
  return request('publishDraft', {
    method: 'mutation',
    params
  });
}

export async function newDraft(params) {
  return request('newDraft', {
    method: 'mutation',
    params
  });
}

export async function saveDraft(params) {
  return request('saveDraft', {
    method: 'mutation',
    params
  });
}

export async function deleteDraft(params) {
  return request('deleteDraft', {
    method: 'mutation',
    params
  });
}

export async function accountFollowers(params) {
  return request('accountFollowers', { params });
}

export async function stream(params) {
  return request('stream', { params });
}

export async function logOut(params) {
  return request('logOut', {
    method: 'mutation',
    params
  });
}
