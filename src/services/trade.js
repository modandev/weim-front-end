import request from '@/shared/graphql';

export function watchlist(params) {
  return request('watchlist',);
}

export function trades(params) {
  return request('trades', params)
}
